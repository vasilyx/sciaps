#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

cd /var/www/symfony

php bin/console doctrine:migrations:migrate --no-interaction

exec "$@"