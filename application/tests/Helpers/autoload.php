<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 08/10/2018
 * Time: 16:21
 */

// tests/Helpers/autoload.php
if (isset($_ENV['BOOTSTRAP_BEFORE_TESTS'])) {
    // executes the "php bin/console cache:clear" command
    passthru(sprintf(
        'php "%s/../bin/console" doctrine:database:drop --force -e test',
        __DIR__,
        $_ENV['BOOTSTRAP_CLEAR_CACHE_ENV']
    ));
}

require __DIR__ . '/../vendor/autoload.php';
