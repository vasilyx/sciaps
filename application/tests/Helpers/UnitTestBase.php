<?php

namespace Tests\Helpers;

use PHPUnit\Framework\TestCase;

/**
 * Class UnitTestBase The unit test base should be used for tests where the scope is for a particular class only. The
 * unit tests should not have depenencies and if so, should be mocked.
 *
 * @package Tests\Helpers
 */
class UnitTestBase extends TestCase
{
    public function set($entity, $value, $propertyName = 'id')
    {
        $class = new \ReflectionClass($entity);
        $property = $class->getProperty($propertyName);
        $property->setAccessible(true);

        $property->setValue($entity, $value);
    }

    public function getFilePath(string $filename): string
    {
        return realpath('') . '/tests/Helpers/Files/' . $filename;
    }
}
