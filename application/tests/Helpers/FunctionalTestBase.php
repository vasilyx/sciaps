<?php

namespace Tests\Helpers;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Router;

/**
 * Class FunctionalTestBase The functional test base should be used where the scope of test is to ensure connected classes
 * process correctly. This will ensure e.g. database entities are created.
 *
 * @package Tests\Helpers
 */
class FunctionalTestBase extends WebTestCase
{
    /** @var  Application $application */
    protected static $application;

    /** @var  Client $client */
    protected static $client;

    /** @var  ContainerInterface $container */
    protected static $container;

    /** @var  EntityManager $entityManager */
    protected static $entityManager;

    /** @var  Registry $registry */
    protected static $registry;

    public static function setUpBeforeClass()
    {
        if (!self::$client) {
            self::runCommand('doctrine:database:drop --force -e test');
            self::runCommand('doctrine:database:create -e test');
            self::runCommand('doctrine:migrations:migrate -e test');
            self::runCommand('doctrine:fixtures:load --append --no-interaction -e test');
            self::runCommand('hautelook:fixtures:load --append -e test');
            self::runCommand('fos:elastica:populate -e test');
        }

        self::$client = static::createClient();
        self::$container = self::$client->getContainer();
        self::$entityManager = self::$container->get('doctrine.orm.entity_manager');
        self::$registry = self::$container->get('doctrine');
    }

    protected static function runCommand($command)
    {
        $command = sprintf('%s --quiet', $command);

        return self::getApplication()->run(new StringInput($command));
    }

    protected static function getApplication()
    {
        if (null === self::$application) {
            $client = static::createClient();

            self::$application = new Application($client->getKernel());
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }

    public function getFilePath(string $filename): string
    {
        return realpath('') . '/tests/Helpers/Files/'.$filename;
    }
}
