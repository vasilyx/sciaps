<?php

namespace CustomerBundle\Controller;

use CustomerBundle\Entity\User;
use CustomerBundle\FunctionalTest;
use Symfony\Component\HttpFoundation\Response;
use CustomerBundle\Services\FacebookService;

class AuthControllerTest extends FunctionalTest
{
    public function testLogin()
    {
        $requestData = [
            'email' => 'admin@test.com',
            'password' => 'qwa12'
        ];

        $this->sendRequest('POST', self::$router->generate('customer_v1_auth_login'), $requestData);
        $this->assertEquals(Response::HTTP_NOT_FOUND, self::$client->getResponse()->getStatusCode());


        $requestData = [
            'email' => 'vasilyx20@gmail.com',
            'password' => 'qwaszx10'
        ];

        $this->sendRequest('POST', self::$router->generate('customer_v1_auth_login'), $requestData);
        $this->assertEquals(Response::HTTP_OK, self::$client->getResponse()->getStatusCode());

        $arr = json_decode(self::$client->getResponse()->getContent());
        $this->assertNotEmpty($arr->token);
        $this->assertNotEmpty($arr->user);

        self::$token = $arr->token;
    }

    public function testMe()
    {
        $this->sendRequest('GET', self::$router->generate('customer_v1_auth_me'));

        $this->assertEquals(Response::HTTP_OK, self::$client->getResponse()->getStatusCode());
    }

    public function testResetPassword()
    {
        $requestData = [
            'email' => 'vasilyx20@gmail.com',
        ];

        $this->sendRequest('POST', self::$router->generate('customer_v1_auth_resetpassword'), $requestData);
        $this->assertEquals(Response::HTTP_OK, self::$client->getResponse()->getStatusCode());
    }

    public function testCheckTokenFromEmail()
    {
        $user = self::$entityManager->getRepository(User::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            'uid' => $user[0]->getId(),
            'token' => $user[0]->getConfirmEmailToken()
        ];

        $url = self::$router->generate('customer_v1_auth_checktokenfromemail', $requestData);

        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testResetPasswordConfirm()
    {
        $user = self::$entityManager->getRepository(User::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "uid" => $user[0]->getId(),
            "token" => $user[0]->getConfirmEmailToken(),
            "new_password1" => "qwaszx10",
            "new_password2" => "qwaszx10"
        ];

        $url = self::$router->generate('customer_v1_auth_resetpasswordconfirm');
        $this->sendRequest('PUT', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testGetClientId()
    {
        $user = self::$entityManager->getRepository(User::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "email" => $user[0]->getEmail(),
            "password" => $user[0]->getPassword()
        ];

        $url = self::$router->generate('customer_v1_auth_getclientid', $requestData);
        $this->sendRequest('POST', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $requestData = [
            "email" => 'test-email@test.com',
            "password" => 'asdasdqwdwdqwdq'
        ];

        $url = self::$router->generate('customer_v1_auth_getclientid', $requestData);
        $this->sendRequest('POST', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }
}
