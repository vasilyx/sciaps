<?php

namespace Tests\AppBundle\Functional\Services\ProductImport;

use AppBundle\Entity\Company;
use AppBundle\Entity\IngestionReport;
use AppBundle\Entity\ProductProvider;
use AppBundle\Entity\RegExp;
use AppBundle\Managers\IngredientLegendManager;
use AppBundle\Managers\IngredientManager;
use AppBundle\Managers\IngredientUnfilteredManager;
use AppBundle\Managers\ProductProviderManager;
use AppBundle\Services\ListenerService;
use AppBundle\Services\ProductImport\CsvDataReader;
use AppBundle\Services\ProductImport\IngredientParser;
use AppBundle\Managers\BarcodeManager;
use AppBundle\Managers\BrandManager;
use AppBundle\Services\ProductImport\ProductImporter;
use Tests\Helpers\FunctionalTestBase;

class ProductImporterTest extends FunctionalTestBase
{
    public function testImport()
    {
        $ingredientParser = new IngredientParser();
        $ingredientManager = new IngredientManager(self::$entityManager);
        $ingredientUnfilteredManager = new IngredientUnfilteredManager(self::$entityManager, $ingredientManager);
        $ingredientLegendManager = new IngredientLegendManager(self::$entityManager);
        $productProviderManager = new ProductProviderManager(self::$entityManager);
        $barcodeManager = new BarcodeManager(self::$entityManager);
        $brandManager = new BrandManager(self::$entityManager);
        $listenerService = new ListenerService(self::$entityManager);

        $reader = new CsvDataReader(new \SplFileObject($this->getFilePath('product_sample_5_products.csv')));

        $productImporter = new ProductImporter(
            self::$entityManager,
            $reader,
            $ingredientParser,
            $ingredientUnfilteredManager,
            $ingredientLegendManager,
            $productProviderManager,
            $barcodeManager,
            $brandManager,
            $listenerService
        );

        $mappingData = [
            'name' => 'Product Title',
            'manufacturer' => '',
            'brand' => 'Brand',
            'barcode' => 'EAN/UPC barcode',
            'remote_id' => 'Product Code',
            'description' => 'Description (web)',
            'ingredients' => 'Ingredients'
        ];

        $productImporter
            ->setReader($reader)
            ->setDataMapping($mappingData);

        $ingestionReport = new IngestionReport();
        /** @var Company $company */
        $company = self::$entityManager->getRepository(Company::class)->findOneBy(['id' => 1001]);
        /** @var ProductProvider $productProvider */
        $productProvider = $company->getProductProvider();

        /** @var RegExp $defaultLegendRegExp */
        $defaultLegendRegExp = self::$entityManager->getRepository(RegExp::class)->getDefaultByType(RegExp::TYPE_LEGEND_PART);
        /** @var RegExp $defaultIngredientRegExp */
        $defaultIngredientRegExp = self::$entityManager->getRepository(RegExp::class)->getDefaultByType(RegExp::TYPE_INGREDIENT_PART);

        $ingestionReport->addRegexp($defaultLegendRegExp);
        $ingestionReport->addRegexp($defaultIngredientRegExp);

        // This actually imports the data and then returns a result for number of products added or updated.
        $report = $productImporter->importToDb($productProvider, $ingestionReport, true);

        $this->assertEquals(5, $report['products']['inserted']);
        $this->assertEquals(0, $report['products']['updated']);
    }

    public function testImportChemD()
    {
        $this->markTestSkipped('ChemD only');
        $ingredientParser = new IngredientParser();
        $ingredientManager = new IngredientManager(self::$entityManager);
        $ingredientUnfilteredManager = new IngredientUnfilteredManager(self::$entityManager, $ingredientManager);
        $ingredientLegendManager = new IngredientLegendManager(self::$entityManager);
        $productProviderManager = new ProductProviderManager(self::$entityManager);
        $barcodeManager = new BarcodeManager(self::$entityManager);
        $brandManager = new BrandManager(self::$entityManager);
        $listenerService = new ListenerService(self::$entityManager);

        $reader = new CsvDataReader(new \SplFileObject($this->getFilePath('ChemD-1.csv')));

        $productImporter = new ProductImporter(
            self::$entityManager,
            $reader,
            $ingredientParser,
            $ingredientUnfilteredManager,
            $ingredientLegendManager,
            $productProviderManager,
            $barcodeManager,
            $brandManager,
            $listenerService
        );

        $mappingData = [
            'name' => 'Product Title',
            'brand' => 'Brand Name',
            'barcode' => 'Barcode',
            'remote_id' => 'Product Code',
            'description' => 'Description',
            'ingredients' => 'Ingredients'
        ];

        $productImporter
            ->setReader($reader)
            ->setDataMapping($mappingData);

        $ingestionReport = new IngestionReport();
        /** @var Company $company */
        $company = self::$entityManager->getRepository(Company::class)->findOneBy(['id' => 1001]);
        /** @var ProductProvider $productProvider */
        $productProvider = $company->getProductProvider();

        /** @var RegExp $defaultLegendRegExp */
        $defaultLegendRegExp = self::$entityManager->getRepository(RegExp::class)->getDefaultByType(RegExp::TYPE_LEGEND_PART);
        /** @var RegExp $defaultIngredientRegExp */
        $defaultIngredientRegExp = self::$entityManager->getRepository(RegExp::class)->getDefaultByType(RegExp::TYPE_INGREDIENT_PART);

        $ingestionReport->addRegexp($defaultLegendRegExp);
        $ingestionReport->addRegexp($defaultIngredientRegExp);;

        // This actually imports the data and then returns a result for number of products added or updated.
        $report = $productImporter->importToDb($productProvider, $ingestionReport, true);

        $this->assertEquals(5843, $report['products']['inserted']);
        $this->assertEquals(0, $report['products']['updated']);
    }
}
