<?php

namespace Tests\AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;


class SynonymImportCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:synonym:import');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command'  => $command->getName(),
            'f' => realpath('') . '/tests/Helpers/Files/sample_synonyms.csv'
        ));

        $output = $commandTester->getDisplay();
        $this->assertContains('Inserted: 140', $output);
    }
}
