<?php

namespace Tests\AppBundle\Unit\Utils;

use AppBundle\Utils\CategoryUtilities;
use PHPUnit\Framework\TestCase;

class CategoryUtilitiesTest extends TestCase
{
    public static $categories = [
        ['id' => 1, 'name' => 'Gift set'],
        ['id' => 2, 'name' => 'Cleanser'],
        ['id' => 3, 'name' => 'Exfoliator'],
        ['id' => 4, 'name' => 'Spot treatment'],
        ['id' => 5, 'name' => 'Sun Care'],
        ['id' => 6, 'name' => 'Tools'],
        ['id' => 7, 'name' => 'Nails'],
        ['id' => 8, 'name' => 'Shampoo'],
        ['id' => 9, 'name' => 'Conditioner'],
        ['id' => 10, 'name' => 'Hair colour'],
        ['id' => 11, 'name' => 'Styling Hair'],
        ['id' => 12, 'name' => 'Shower'],
        ['id' => 13, 'name' => 'Medicine'],
        ['id' => 14, 'name' => 'Oral care'],
        ['id' => 15, 'name' => 'Parfum'],
        ['id' => 16, 'name' => 'Hair Removal'],
        ['id' => 17, 'name' => 'Grooming'],
        ['id' => 18, 'name' => 'Aromatherapy'],
        ['id' => 19, 'name' => 'Feminine Care'],
        ['id' => 20, 'name' => 'Deodorant'],
        ['id' => 21, 'name' => 'Sexual Health'],
        ['id' => 22, 'name' => 'Tanning'],
        ['id' => 23, 'name' => 'Eye Makeup'],
        ['id' => 24, 'name' => 'Lip Makeup'],
        ['id' => 25, 'name' => 'Face makeup'],
        ['id' => 26, 'name' => 'Talcum powder'],
        ['id' => 27, 'name' => 'Anti-aging'],
        ['id' => 28, 'name' => 'First Aid'],
        ['id' => 29, 'name' => 'Repellent'],
        ['id' => 30, 'name' => 'moisturiser']
    ];

    public function testMapCategories()
    {

        $result = CategoryUtilities::mapCategories(self::$categories);

        $this->assertArrayHasKey('Gift set', $result[0]);
        $this->assertArrayHasKey('Cleanser', $result[0]);
        $this->assertEquals(2, $result[0]['Cleanser']);

        $this->assertArrayHasKey('gift sets?', $result[1]);
        $this->assertArrayHasKey('travel sets?', $result[1]);
        $this->assertEquals(["Gift set", "\bgift sets?\b"], $result[1]['gift sets?']);
        $this->assertEquals(["Gift set", "\bgift sets?\b"], $result[1]['gift sets?']);

        $this->assertArrayHasKey('suncare', $result[1]);
        $this->assertEquals(["Sun Care", "\bsuncare\b"], $result[1]['suncare']);
        $this->assertArrayHasKey('facial suncare', $result[1]);
        $this->assertEquals(["Sun Care", "\bfacial suncare\b"], $result[1]['facial suncare']);
        $this->assertArrayHasKey('exfoliators?', $result[1]);
        $this->assertEquals(["Exfoliator", "\bexfoliators?\b"], $result[1]['exfoliators?']);
    }

    public function testCategoriseProduct()
    {
        $products = [
            [ 'name' => 'Hyaluronic Plumping Mist', 'description' => 'This is a bronzing mist'],
            [ 'name' => 'Aloe Vera Hands and Body Cream', 'description' => 'This shouldnt be seen'],
            [ 'name' => 'Ambre Solaire Protect Sun Cream Spray SPF 20', 'description' => 'This shouldnt be seen']
        ];

        $this->assertEquals(
            22,
            CategoryUtilities::categoriseProduct($products[0], self::$categories)
        );

        $this->assertEquals(
            30,
            CategoryUtilities::categoriseProduct($products[1], self::$categories)
        );

        $this->assertEquals(
            5,
            CategoryUtilities::categoriseProduct($products[2], self::$categories)
        );
    }

    public function testGetProductCategory()
    {
        $this->assertEquals(
            false,
            CategoryUtilities::getProductCategory('Hyaluronic Plumping Mist', self::$categories)
        );

        $this->assertEquals(
            30,
            CategoryUtilities::getProductCategory('Aloe Vera Hands and Body Cream', self::$categories)
        );

        $this->assertEquals(
            5,
            CategoryUtilities::getProductCategory('Ambre Solaire Protect Sun Cream Spray SPF 20', self::$categories)
        );

        $this->assertEquals(
            12,
            CategoryUtilities::getProductCategory('Nutritive Shower Cream', self::$categories)
        );

        $this->assertEquals(
            25,
            CategoryUtilities::getProductCategory('HD Photogenic Concealer Wand - Light', self::$categories)
        );

        $this->assertEquals(
            3,
            CategoryUtilities::getProductCategory('Aloe Vera Body Scrub Jam', self::$categories)
        );
    }
}
