<?php

namespace Tests\AppBundle\Unit\Utils;

use AppBundle\Utils\ParsingUtilities;
use PHPUnit\Framework\TestCase;

class ParsingUtilitiesTest extends TestCase
{
    public function testBuildProductName()
    {
        $utils = new ParsingUtilities();

        $product = [
            'name' => 'Moisturiser for men',
            'brand' => 'Nivea',
            'manufacturer' => 'NiveaManu',
            'volume' => '200',
            'volume_units' => 'ml'
        ];
        $this->assertEquals('Nivea Moisturiser for men 200ml', $utils->buildProductName($product));

        $product = [
            'name' => 'Nivea Moisturiser for men 200ml',
            'brand' => 'Nivea',
            'manufacturer' => 'NiveaManu',
            'volume' => '',
            'volume_units' => ''
        ];
        $this->assertEquals('Nivea Moisturiser for men 200ml', $utils->buildProductName($product));
    }

    public function testSimilarName()
    {
        $utils = new ParsingUtilities();

        $products = [
            5 => 'Niva moisturiser',
            'vea moisturiser',
            'Nivea moisturiser',
            'Nivea moistser',
            'Nivea oisturiser'
        ];

        $name = 'Nivea moisturiser';

        $result = $utils::similarName($name, $products);

        $this->assertEquals(7, $result['best']);
    }
}
