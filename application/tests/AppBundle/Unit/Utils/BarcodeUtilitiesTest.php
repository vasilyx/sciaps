<?php

namespace Tests\AppBundle\Unit\Utils;

use AppBundle\Entity\Barcode;
use AppBundle\Utils\BarcodeUtilities;
use PHPUnit\Framework\TestCase;

class BarcodeUtilitiesTest extends TestCase
{
    public function testPadBarcode()
    {
        $this->assertEquals('123456', BarcodeUtilities::padBarcode('123456'));
        $this->assertEquals('12345678', BarcodeUtilities::padBarcode('12345678'));
        $this->assertEquals('123456789', BarcodeUtilities::padBarcode('123456789'));
        $this->assertEquals('00123456789012', BarcodeUtilities::padBarcode('123456789012'));
        $this->assertEquals('01234567890123', BarcodeUtilities::padBarcode('1234567890123'));
        $this->assertEquals('12345678901234', BarcodeUtilities::padBarcode('12345678901234'));
    }

    public function testIsFormatValid()
    {
        // Barcode construct correctly
        $this->assertFalse(BarcodeUtilities::isFormatValid(false));
        $this->assertTrue(BarcodeUtilities::isFormatValid(629104150021));
        $this->assertFalse(BarcodeUtilities::isFormatValid('62910-4150021'));
        $this->assertTrue(BarcodeUtilities::isFormatValid('629104150021'));
        $this->assertFalse(BarcodeUtilities::isFormatValid('6291041 50021'));
        $this->assertFalse(BarcodeUtilities::isFormatValid('6291041h0021'));
        $this->assertFalse(BarcodeUtilities::isFormatValid('6291041;0021'));
        $this->assertFalse(BarcodeUtilities::isFormatValid('LSBEM25B'));
    }

    public function testCalculateCheckDigit()
    {
        // EAN-8 or UPCE (7 digits, exlcuding checksum) https://www.gs1.org/services/check-digit-calculator
        $this->assertEquals(7, BarcodeUtilities::calculateCheckDigit('5512345'));

        // EAN-13 (12 digits, exlcuding checksum)
        $this->assertEquals(3, BarcodeUtilities::calculateCheckDigit('629104150021'));

        // UPCA (11 digits, exlcuding checksum)
        $this->assertEquals(7, BarcodeUtilities::calculateCheckDigit('03600024145'));
        // UPCA (6 digits, exlcuding checksum)
        $this->assertEquals(3, BarcodeUtilities::calculateCheckDigit('234567'));
        $this->assertEquals(3, BarcodeUtilities::calculateCheckDigit('02345600007'));

        // UPCA (11 digits, exlcuding checksum) https://en.wikipedia.org/wiki/Check_digit#UPC
        $this->assertEquals(5, BarcodeUtilities::calculateCheckDigit('01010101010'));

        // EAN-13 (12 digits, exlcuding checksum) https://bizfluent.com/how-5332722-calculate-ean-barcodes.html
        $this->assertEquals(4, BarcodeUtilities::calculateCheckDigit('973594056482'));

        // EAN-13 (12 digits, exlcuding checksum) http://www.keepautomation.com/ean_13/index.html#Checksum
        // This is 3 however is 1 on https://www.gs1.org/services/check-digit-calculator
        $this->assertEquals(1, BarcodeUtilities::calculateCheckDigit('690875397621'));
    }

    public function testExtractUPCEValue()
    {
        $this->assertEquals(234567, BarcodeUtilities::extractUPCEValue('234567'));
        $this->assertEquals(123456, BarcodeUtilities::extractUPCEValue('1234567'));
        $this->assertEquals(234567, BarcodeUtilities::extractUPCEValue('12345678'));
        $this->assertFalse(BarcodeUtilities::extractUPCEValue('12345'));
        $this->assertFalse(BarcodeUtilities::extractUPCEValue('123456789'));

        $this->assertEquals(
            [
                'upce' => 234567,
                'checkDigit' => '',
                'numberSystemDigit' => ''
            ], BarcodeUtilities::extractUPCEValue('234567', true));
        $this->assertEquals(
            [
                'upce' => 123456,
                'checkDigit' => 7,
                'numberSystemDigit' => ''
            ], BarcodeUtilities::extractUPCEValue('1234567', true));
        $this->assertEquals(
            [
                'upce' => 234567,
                'checkDigit' => 8,
                'numberSystemDigit' => 1
            ], BarcodeUtilities::extractUPCEValue('12345678', true));
    }

    public function testConvertUPCEtoUPCA()
    {
        // http://www.keepautomation.com/upce/upce-to-upca-conversion.html
        $this->assertEquals('023000004564', BarcodeUtilities::convertUPCEtoUPCA('2345604'));
        $this->assertEquals(false, BarcodeUtilities::convertUPCEtoUPCA('23456078'));
        $this->assertEquals('023100004563', BarcodeUtilities::convertUPCEtoUPCA('2345613'));
        $this->assertEquals('023200004562', BarcodeUtilities::convertUPCEtoUPCA('2345622'));
        $this->assertEquals('023400000562', BarcodeUtilities::convertUPCEtoUPCA('2345632'));
        $this->assertEquals('023450000062', BarcodeUtilities::convertUPCEtoUPCA('2345642'));
        $this->assertEquals('023456000059', BarcodeUtilities::convertUPCEtoUPCA('2345659'));
        $this->assertEquals('023456000066', BarcodeUtilities::convertUPCEtoUPCA('2345666'));
        $this->assertEquals('023456000073', BarcodeUtilities::convertUPCEtoUPCA('2345673'));
        $this->assertEquals('023456000080', BarcodeUtilities::convertUPCEtoUPCA('2345680'));
        $this->assertEquals('023456000097', BarcodeUtilities::convertUPCEtoUPCA('2345697'));

        $this->assertEquals('012200000018', BarcodeUtilities::convertUPCEtoUPCA('1200128'));
        $this->assertEquals('012400000016', BarcodeUtilities::convertUPCEtoUPCA('1240136'));
        $this->assertEquals('012450000011', BarcodeUtilities::convertUPCEtoUPCA('1245141'));
        $this->assertEquals('012457000052', BarcodeUtilities::convertUPCEtoUPCA('1245752'));

        $this->assertEquals(
            [
                'manufacturerNumber' => 12457,
                'itemNumber' => 00005
            ], BarcodeUtilities::convertUPCEtoUPCA('1245752', true));
    }

    public function testIsValidGTIN()
    {
        $this->assertTrue(BarcodeUtilities::isValidGTIN('023456000073'));
        $this->assertTrue(BarcodeUtilities::isValidGTIN('12345670'));
        $this->assertTrue(BarcodeUtilities::isValidGTIN('123456701229'));
        $this->assertTrue(BarcodeUtilities::isValidGTIN('1234567034553'));
        $this->assertTrue(BarcodeUtilities::isValidGTIN('999996578139'));
        $this->assertTrue(BarcodeUtilities::isValidGTIN('12345670234320'));
        $this->assertFalse(BarcodeUtilities::isValidGTIN('928347982734234'));
        $this->assertFalse(BarcodeUtilities::isValidGTIN('09123'));
        $this->assertFalse(BarcodeUtilities::isValidGTIN('123456701227'));
        $this->assertFalse(BarcodeUtilities::isValidGTIN('1234567034554'));
        $this->assertFalse(BarcodeUtilities::isValidGTIN('12345670234328'));
        $this->assertTrue(BarcodeUtilities::isValidGTIN('4001638088282'));
    }

    public function testCheckBarcodeType()
    {
        $this->assertEquals(Barcode::TYPE_EAN8, BarcodeUtilities::getBarcodeType('96385074'));
        $this->assertEquals(Barcode::TYPE_EAN8, BarcodeUtilities::getBarcodeType('12345670'));
        $this->assertNull(BarcodeUtilities::getBarcodeType('96385075'));

        $this->assertEquals(Barcode::TYPE_EAN8, BarcodeUtilities::getBarcodeType('01236446'));
        $this->assertEquals(Barcode::TYPE_UPCE, BarcodeUtilities::getBarcodeType('01236440'));
        $this->assertNull(BarcodeUtilities::getBarcodeType('9999997'));
        $this->assertEquals(Barcode::TYPE_EAN8, BarcodeUtilities::getBarcodeType('99999971'));
        $this->assertEquals(Barcode::TYPE_UPCA, BarcodeUtilities::getBarcodeType('999996578139'));
        $this->assertEquals(Barcode::TYPE_EAN13, BarcodeUtilities::getBarcodeType('9999966973125'));
        $this->assertEquals(Barcode::TYPE_GTIN14, BarcodeUtilities::getBarcodeType('99999669731247'));

        $this->assertNull(BarcodeUtilities::getBarcodeType('5901234123456'));
        $this->assertEquals(Barcode::TYPE_EAN13, BarcodeUtilities::getBarcodeType('5901234123457'));
        $this->assertEquals(Barcode::TYPE_EAN13, BarcodeUtilities::getBarcodeType('05901234123457'));
        $this->assertEquals(Barcode::TYPE_UPCA, BarcodeUtilities::getBarcodeType('00075678164125'));
        $this->assertNull(BarcodeUtilities::getBarcodeType('012345678'));
        $this->assertNull(BarcodeUtilities::getBarcodeType('04252613'));
        $this->assertEquals(Barcode::TYPE_UPCE, BarcodeUtilities::getBarcodeType('04252614'));
    }
}
