<?php

namespace Tests\AppBundle\Unit\Services\Email\Types;

use AppBundle\Entity\User;
use AppBundle\Services\Email\Types\WelcomeEmail;
use Tests\Helpers\UnitTestBase;

class WelcomeEmailTest extends UnitTestBase
{
    public function testSetSubject()
    {
        $user = new User();
        $user->setEmail('john@doe.com');
        $email = new WelcomeEmail($user);

        $subject = 'Test subject';
        $email->setSubject($subject);
        $this->assertEquals($subject, $email->getSubject());
    }

    public function testSetHeaders()
    {
        $user = new User();
        $user->setEmail('john@doe.com');
        $email = new WelcomeEmail($user);

        $headers = ['testKey' => 'testValue'];
        $email->setHeaders($headers);
        $this->assertEquals($headers, $email->getHeaders());
    }

    public function testSetBcc()
    {
        $user = new User();
        $user->setEmail('john@doe.com');
        $email = new WelcomeEmail($user);

        $bcc = ['welcome@skinninja.com' => 'SkinNinja'];
        $email->setBcc($bcc);
        $this->assertEquals($bcc, $email->getBcc());
    }

    public function testAddAttachment()
    {
        $user = new User();
        $user->setEmail('john@doe.com');
        $email = new WelcomeEmail($user);

        $file = ['file' => 'thisisafile.csv', 'mime' => 'txt', 'filename' => 'this is a file name'];
        $email->addAttachment($file);
        $this->assertTrue($email->getAttachments()[0]['file'] == 'thisisafile.csv');
    }

    public function testSetCc()
    {
        $user = new User();
        $user->setEmail('john@doe.com');
        $email = new WelcomeEmail($user);

        $cc = ['welcome@skinninja.com' => 'SkinNinja'];
        $email->setCc($cc);
        $this->assertEquals($cc, $email->getCc());
    }

    public function testSetBody()
    {
        $user = new User();
        $user->setEmail('john@doe.com');
        $email = new WelcomeEmail($user);

        $body = 'Test body';
        $email->setBody($body);
        $this->assertEquals($body, $email->getBody());
    }

    public function testSetFrom()
    {
        $user = new User();
        $user->setEmail('john@doe.com');
        $email = new WelcomeEmail($user);

        $from = ['SkinNinja' => 'skinninja@skinninja.com'];
        $email->setFrom($from);
        $this->assertEquals($from, $email->getFrom());
    }

    public function testSetTo()
    {
        $user = new User();
        $user->setEmail('john@doe.com');
        $email = new WelcomeEmail($user);

        $to = ['SkinNinja' => 'skinninja@skinninja.com'];
        $email->setTo($to);
        $this->assertEquals($to, $email->getTo());
    }

    public function testSetContentType()
    {
        $user = new User();
        $user->setEmail('john@doe.com');
        $email = new WelcomeEmail($user);

        $contentType = 'text/plain';
        $email->setContentType($contentType);
        $this->assertEquals($contentType, $email->getContentType());
    }

    public function test__construct()
    {
        $this->expectException(\InvalidArgumentException::class);

        $user = new User();
        $email = new WelcomeEmail($user);
    }
}
