<?php

namespace Tests\AppBundle\Unit\Services\Email;

use AppBundle\Entity\User;
use AppBundle\Services\Email\SendgridEmailer;
use AppBundle\Services\Email\Types\ActivationEmail;
use AppBundle\Services\Email\Types\ForgottenPasswordEmail;
use AppBundle\Services\Email\Types\ResetPasswordEmail;
use AppBundle\Services\Email\Types\WelcomeEmail;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Tests\Helpers\UnitTestBase;

class SendgridEmailerTest extends UnitTestBase
{
    /* @var Container|\PHPUnit_Framework_MockObject_MockObject $container */
    private $container;

    /** @var string $to */
    private $to;
    /** @var string $key */
    private $key;

    public function setUp()
    {
        $this->container = $this->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')->disableOriginalConstructor()->getMock();
        $this->to = 'jfndskafndskafndsaf@mailinator.com';
        $this->key = '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';
    }

    public function testGetContainer()
    {
        $emailer = new SendgridEmailer($this->container);

        $this->assertInstanceOf('Symfony\Component\DependencyInjection\ContainerInterface', $emailer->getContainer());
    }

    public function testSendWelcome()
    {
        $email = new WelcomeEmail($this->to);

        $this->setupEmailer();

        $emailer = new SendgridEmailer($this->container);

        $response = $emailer->send($email);

        $this->assertTrue($response->statusCode() == 202);
    }

    public function testSendActivation()
    {
        $email = new ActivationEmail($this->to, ['confirmEmailToken' => 'dsfasdf']);

        $this->setupEmailer();

        $emailer = new SendgridEmailer($this->container);

        $response = $emailer->send($email);

        $this->assertTrue($response->statusCode() == 202);
    }

    public function testSendForgotten()
    {
        $email = new ForgottenPasswordEmail($this->to, ['forgottenPasswordToken' => 'fdsafdasf']);

        $this->setupEmailer();

        $emailer = new SendgridEmailer($this->container);

        $response = $emailer->send($email);

        $this->assertTrue($response->statusCode() == 202);
    }

    public function testSendResetPassword()
    {
        $email = new ResetPasswordEmail($this->to);

        $this->setupEmailer();

        $emailer = new SendgridEmailer($this->container);

        $response = $emailer->send($email);

        $this->assertTrue($response->statusCode() == 202);
    }

    protected function setupEmailer(): void
    {
        $map = [
            ['mail_send_from', 'support@evrelab.com'],
            ['sendgrid_api_key', $this->key]
        ];

        $this->container->expects($this->any())
            ->method('getParameter')
            ->will($this->returnValueMap($map));
    }
}
