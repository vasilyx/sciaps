<?php

namespace Tests\AppBundle\Unit\Services\ProductImport;

use AppBundle\Services\ProductImport\IngredientParser;
use Tests\Helpers\UnitTestBase;

class IngredientParserTest extends UnitTestBase
{
    public $config = [
        'standardPhrases' => [
            'INCI\:',
            '\(INCI\)\:',
            'Ingredients \(INCI\)\:',
            'Active Ingredients:',
            'Inactive',
            'For allergens\, See ingredients in bold',
            'Does not contain nuts',
            'All blended in a unique process',
            'Source:',
            'No parabens\.*',
            "Organic Certification: DE-ÖKO-007"
        ],
        'legendPartMatch' => "[^,][\s]{0,3}[\(]{0,1}([\*|\^|†]{1,4}(?!\,|\(|\))[a-z|A-Z|0-9|\s|\,|\%|\-|\)|\:]{3,}?[\.|$])|(\(\d\)[^,|^\.|^\)][a-z|A-Z|0-9|\s|\,|\%|\-|\)|\:]{3,})",
        'itemMatch' => "([a-z|A-Z|0-9|\s|,|\(|\)|\%|\.|\*|\/|\'|\[|\]|†|\^|\+|\-|\º|\&|\é|\á|\™]{3,}?)(?:(?:,(?![\d]|[^\(]*\))|$)|(?:\.(?!\d)))"
    ];

    public $ingredientTextSample = "INCI: Aqueous extract of birch leaves*(94%), Lemon Juice**(6%), *From certified, controlled wild collection. **Biodynamically grown. Organic Certification: DE-ÖKO-007";
    public $ingredientTextSampleIngredients = "INCI: Aqueous extract of birch leaves*(94%), Lemon Juice**(6%), Organic Certification: DE-ÖKO-007";
    public $ingredientTextSampleIngredientsList = [
        "Aqueous extract of birch leaves*(94%)",
        "Lemon Juice**(6%)"
    ];
    public $ingredientTextSampleLegendList = [
        "*From certified, controlled wild collection",
        "**Biodynamically grown"
    ];

    public function testRemovePhrases()
    {
        $string = "INCI: Water (Aqua), Calcium Carbonate, Active Ingredients:   Inactive";

        $utils = new IngredientParser();
        $utils->setRegex($this->config);

        $this->assertEquals(" Water (Aqua), Calcium Carbonate,    ", $utils->removePhrases($string));
        $this->assertEquals(" Aqueous extract of birch leaves*(94%), Lemon Juice**(6%), *From certified, controlled wild collection. **Biodynamically grown. ", $utils->removePhrases($this->ingredientTextSample));
    }

    public function testRemoveNewLines()
    {
        $string = "INCI: Water (Aqua), \nCalcium Carbonate, Active Ingredients:   Inactive";

        $utils = new IngredientParser();
        $utils->setRegex($this->config);

        $this->assertEquals("INCI: Water (Aqua), ,Calcium Carbonate, Active Ingredients:   Inactive", $utils->removeNewLines($string));
        $this->assertEquals("INCI: Water (Aqua), Calcium Carbonate, Active Ingredients:   Inactive", $utils->removeNewLines($string, ''));
    }

    public function testCleanString()
    {
        $string = "     INCI: Water (Aqua), \nCalcium Carbonate, Active Ingredients:   Inactive \r\r\r\n";

        $utils = new IngredientParser();
        $utils->setRegex($this->config);

        $this->assertEquals("Water (Aqua), ,Calcium Carbonate,     ,,,", $utils->cleanString($string));
    }

    public function testGetLegendFromIngredient()
    {
        $utils = new IngredientParser();
        $utils->setRegex($this->config);

        $ingredient = "Flavor (aroma)*";
        $this->assertEquals('*', $utils->getLegendFromIngredient($ingredient));

        $ingredient = "Flavor (aroma)";
        $this->assertNull($utils->getLegendFromIngredient($ingredient));

        $ingredient = "Flavor** (aroma)";
        $this->assertEquals("**", $utils->getLegendFromIngredient($ingredient));
    }

    public function testSplitIngredientsAndLegend()
    {
        $utils = new IngredientParser();
        $utils->setRegex($this->config);

        $this->assertEquals(
            [
                $this->ingredientTextSampleIngredients,
                $this->ingredientTextSampleLegendList
            ], $utils->splitIngredientsAndLegend($this->ingredientTextSample));
    }

    public function testExplodeIngredients()
    {
        $utils = new IngredientParser();
        $utils->setRegex($this->config);

        $list = $this->ingredientTextSampleIngredientsList;
        // This is only a partial test. In the full parser this would be stripped.
        $list[] = "KO-007";

        $this->assertEquals($list, $utils->explodeIngredients($this->ingredientTextSampleIngredients));
    }

    public function testGetDelimiter()
    {
        $utils = new IngredientParser();
        $utils->setRegex($this->config);

        $string = "this. is. an. ingredients, list";
        $this->assertEquals(".", $utils->getDelimiter($string));

        $string = "this, is, an, ingredients. list";
        $this->assertEquals(",", $utils->getDelimiter($string));

        $string = "this; is; an; ingredients; list";
        $this->assertEquals(",", $utils->getDelimiter($string));

        $string = "this is an ingredients list";
        $this->assertEquals(",", $utils->getDelimiter($string));
    }

    public function testParseIngredientList()
    {
        $utils = new IngredientParser();
        $utils->setRegex($this->config);

        $parsedIngredients = $utils->parseIngredientList($this->ingredientTextSampleIngredientsList, $this->ingredientTextSampleLegendList);

        $this->assertEquals([
            [
                'name' => 'aqueous extract of birch leaves(94%)',
                'rawName' => 'aqueous extract of birch leaves*(94%)',
                'legend' => '*From certified, controlled wild collection'
            ],
            [
                'name' => 'lemon juice(6%)',
                'rawName' => 'lemon juice**(6%)',
                'legend' => '**Biodynamically grown'
            ]
        ], $parsedIngredients);
    }

    public function testGetParsedIngredientsList()
    {
        $utils = new IngredientParser();
        $utils->setRegex($this->config);

        $parsedIngredientsList = $utils->getParsedIngredientsList($this->ingredientTextSample);

        $this->assertEquals([
            [
                'name' => 'aqueous extract of birch leaves(94%)',
                'rawName' => 'aqueous extract of birch leaves*(94%)',
                'legend' => '*From certified, controlled wild collection'
            ],
            [
                'name' => 'lemon juice(6%)',
                'rawName' => 'lemon juice**(6%)',
                'legend' => '**Biodynamically grown'
            ]
        ], $parsedIngredientsList);
    }
}
