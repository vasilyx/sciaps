<?php

namespace Tests\AppBundle\Unit\Services\ProductImport;

use AppBundle\Entity\Barcode;
use AppBundle\Entity\IngestionReport;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductProvider;
use AppBundle\Managers\IngestionReportManager;
use AppBundle\Managers\IngredientLegendManager;
use AppBundle\Managers\IngredientUnfilteredManager;
use AppBundle\Managers\ProductProviderManager;
use AppBundle\Repository\ProductRepository;
use AppBundle\Services\ListenerService;
use AppBundle\Services\ProductImport\CsvDataReader;
use AppBundle\Services\ProductImport\IngredientParser;
use Doctrine\ORM\EntityManager;
use AppBundle\Managers\BarcodeManager;
use AppBundle\Managers\BrandManager;
use AppBundle\Services\ProductImport\ProductImporter;
use Tests\Helpers\UnitTestBase;

class ProductImporterTest extends UnitTestBase
{
    /* @var EntityManager|\PHPUnit_Framework_MockObject_MockObject $entityManager */
    private $entityManager;
    /* @var IngredientUnfilteredManager|\PHPUnit_Framework_MockObject_MockObject $ingredientManager */
    private $ingredientManager;
    /* @var IngredientLegendManager|\PHPUnit_Framework_MockObject_MockObject $ingredientLegendManager */
    private $ingredientLegendManager;
    /* @var ProductProviderManager|\PHPUnit_Framework_MockObject_MockObject $productProviderManager */
    private $productProviderManager;
    /* @var BarcodeManager|\PHPUnit_Framework_MockObject_MockObject $barcodeManager */
    private $barcodeManager;
    /* @var BrandManager|\PHPUnit_Framework_MockObject_MockObject $brandManager */
    private $brandManager;
    /* @var IngredientParser|\PHPUnit_Framework_MockObject_MockObject $ingredientParser */
    private $ingredientParser;
    /* @var IngestionReportManager|\PHPUnit_Framework_MockObject_MockObject $ingestionReportManager */
    private $ingestionReportManager;
    /* @var CsvDataReader|\PHPUnit_Framework_MockObject_MockObject $csvReader */
    private $csvReader;
    /* @var ListenerService|\PHPUnit_Framework_MockObject_MockObject $listenerService */
    private $listenerService;

    public function setUp()
    {
        $this->entityManager = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();

        $configuration = $this->getMockBuilder('\Doctrine\DBAL\Configuration')->disableOriginalConstructor()->getMock();
        $configuration
            ->expects($this->any())
            ->method('setSQLLogger')
            ->willReturn($this->returnValue(null));

        $connection = $this->getMockBuilder('\Doctrine\DBAL\Connection')->disableOriginalConstructor()->getMock();
        $connection
            ->expects($this->any())
            ->method('getConfiguration')
            ->willReturn($configuration);

        $this->entityManager->expects($this->any())->method('getConnection')->willReturn($connection);

        $this->ingredientManager = $this->getMockBuilder('\AppBundle\Managers\IngredientUnfilteredManager')->disableOriginalConstructor()->getMock();
        $this->ingredientLegendManager = $this->getMockBuilder('\AppBundle\Managers\IngredientLegendManager')->disableOriginalConstructor()->getMock();
        $this->productProviderManager = $this->getMockBuilder('\AppBundle\Managers\ProductProviderManager')->disableOriginalConstructor()->getMock();
        $this->barcodeManager = $this->getMockBuilder('\AppBundle\Managers\BarcodeManager')->disableOriginalConstructor()->getMock();
        $this->brandManager = $this->getMockBuilder('\AppBundle\Managers\BrandManager')->disableOriginalConstructor()->getMock();
        $this->ingredientParser = $this->getMockBuilder('\AppBundle\Services\ProductImport\IngredientParser')->disableOriginalConstructor()->getMock();
        $this->ingestionReportManager = $this->getMockBuilder('\AppBundle\Managers\IngestionReportManager')->disableOriginalConstructor()->getMock();
        $this->csvReader = $this->getMockBuilder('\AppBundle\Services\ProductImport\CsvDataReader')->disableOriginalConstructor()->getMock();

        $this->listenerService = $this->getMockBuilder('\AppBundle\Services\ListenerService')->disableOriginalConstructor()->getMock();
    }

    public function testValidateMappingData()
    {
        $reader = new CsvDataReader(new \SplFileObject($this->getFilePath('product_sample_5_products.csv')));
        $productImporter = new ProductImporter(
            $this->entityManager,
            $reader,
            $this->ingredientParser,
            $this->ingredientManager,
            $this->ingredientLegendManager,
            $this->productProviderManager,
            $this->barcodeManager,
            $this->brandManager,
            $this->listenerService);

        // No data in the first row should fail
        $firstRow = [];
        $response = $productImporter->validateMappingData($firstRow);
        $this->assertTrue($response['errors'][0] == 'CSV column headings must be provided');

        // Associative array
        $firstRow = ['key' => 'value'];
        $response = $productImporter->validateMappingData($firstRow);
        $this->assertTrue($response['errors'][0] == 'CSV column headings should not be an associative array');

        // No data mapping set should fail
        $firstRow = ['ProdName', 'ProdDesc'];
        $response = $productImporter->validateMappingData($firstRow);
        $this->assertTrue($response['errors'][0] == 'The data map cannot be empty');

        $dataMap = ['name' => 'name', 'description2' => 'description'];
        $productImporter->setDataMapping($dataMap);
        $response = $productImporter->validateMappingData($firstRow);
        // Validate required fields
        $this->assertTrue($response['errors'][0] == 'The "barcode" field is required.');
        // Validate CSV column headings
        $this->assertTrue($response['errors'][1] == 'The "name" field does not exist in the provided CSV file');
        // Validate DB fields
        $this->assertTrue($response['errors'][2] == 'The "description2" field is not an allowed database field.');
        $this->assertTrue($response['errors'][3] == 'The "description" field does not exist in the provided CSV file');

        $dataMap = ['barcode' => 'barcode', 'name' => '', 'description' => 'description'];
        $productImporter->setDataMapping($dataMap);
        $firstRow = ['barcode', 'ProdName'];
        // Data mapping != csv column headings
        $response = $productImporter->validateMappingData($firstRow);
        // Validate required fields
        $this->assertTrue($response['errors'][0] == 'The "description" field does not exist in the provided CSV file');
        $this->assertCount(1, $response['errors']);

        $firstRow = ['barcode', 'name', 'description', 'description_2'];
        $response = $productImporter->validateMappingData($firstRow);
        // Validate required fields
        $this->assertArrayNotHasKey('errors', $response);

        // The only allowable configuration is a datamap that matches exactly the available column headings.
        $dataMap = [
            'name' => 'name',
            'description' => 'description2',
            'barcode' => 'b-dizzle'
        ];
        $productImporter->setDataMapping($dataMap);
        $firstRow = ['name', 'description2', 'b-dizzle'];
        $response = $productImporter->validateMappingData($firstRow);
        $this->assertArrayNotHasKey('errors', $response);
    }

    public function testImportInsert()
    {
        $this->setupFile('product_sample_5_products.csv', 'insert');

        $this->listenerService->expects($this->once())->method('disableListener')->willReturn($this->returnValue(true));
        $this->listenerService->expects($this->once())->method('enableListeners')->willReturn($this->returnValue(true));

        $productImporter = new ProductImporter(
            $this->entityManager,
            $this->csvReader,
            $this->ingredientParser,
            $this->ingredientManager,
            $this->ingredientLegendManager,
            $this->productProviderManager,
            $this->barcodeManager,
            $this->brandManager,
            $this->listenerService);

        $mappingData = [
            'name' => 'Product Title',
            'manufacturer' => '',
            'brand' => 'Brand',
            'barcode' => 'EAN/UPC barcode',
            'remote_id' => 'Product Code',
            'description' => 'Description (web)',
            'ingredients' => 'Ingredients'
        ];
        $productImporter->setDataMapping($mappingData);

        $productProvider = new ProductProvider();
        $ingestionReport = new IngestionReport();

        // Products don't exist - inserts 5
        $report = $productImporter->importToDb($productProvider, $ingestionReport);
        $this->assertEquals(5, $report['products']['inserted']);
        $this->assertEquals(0, $report['products']['updated']);
    }

    public function testImportIgnore()
    {
        $this->setupFile('product_sample_5_products.csv', 'ignore');

        $this->listenerService->expects($this->once())->method('disableListener')->willReturn($this->returnValue(true));
        $this->listenerService->expects($this->once())->method('enableListeners')->willReturn($this->returnValue(true));

        $productImporter = new ProductImporter(
            $this->entityManager,
            $this->csvReader,
            $this->ingredientParser,
            $this->ingredientManager,
            $this->ingredientLegendManager,
            $this->productProviderManager,
            $this->barcodeManager,
            $this->brandManager,
            $this->listenerService);

        $mappingData = [
            'name' => 'Product Title',
            'manufacturer' => '',
            'brand' => 'Brand',
            'barcode' => 'EAN/UPC barcode',
            'remote_id' => 'Product Code',
            'description' => 'Description (web)',
            'ingredients' => 'Ingredients'
        ];
        $productImporter->setDataMapping($mappingData);

        $productProvider = new ProductProvider();
        $ingestionReport = new IngestionReport();

        // Products already exist and not overwriting - inserts 0, updates 0
        $report = $productImporter->importToDb($productProvider, $ingestionReport);
        $this->assertEquals(0, $report['products']['inserted']);
        $this->assertEquals(0, $report['products']['updated']);
    }

    public function testImportUpdate()
    {
        $this->setupFile('product_sample_5_products.csv', 'update');

        $this->listenerService->expects($this->once())->method('disableListener')->willReturn($this->returnValue(true));
        $this->listenerService->expects($this->once())->method('enableListeners')->willReturn($this->returnValue(true));

        $productImporter = new ProductImporter(
            $this->entityManager,
            $this->csvReader,
            $this->ingredientParser,
            $this->ingredientManager,
            $this->ingredientLegendManager,
            $this->productProviderManager,
            $this->barcodeManager,
            $this->brandManager,
            $this->listenerService);

        $mappingData = [
            'name' => 'Product Title',
            'manufacturer' => '',
            'brand' => 'Brand',
            'barcode' => 'EAN/UPC barcode',
            'remote_id' => 'Product Code',
            'description' => 'Description (web)',
            'ingredients' => 'Ingredients'
        ];
        $productImporter->setDataMapping($mappingData);

        $productProvider = new ProductProvider();
        $ingestionReport = new IngestionReport();

        // Products already exist and not overwriting - inserts 0, updates 5
        $report = $productImporter->importToDb($productProvider, $ingestionReport, true);
        $this->assertEquals(0, $report['products']['inserted']);
        $this->assertEquals(5, $report['products']['updated']);
    }

    protected function setupFile(string $fileName = 'product_sample_5_products.csv', string $status = 'insert'): void
    {
        $fileArray = file($this->getFilePath($fileName));
        $fileMap = [];

        foreach ($fileArray as $k => $row) {
            $array = str_getcsv($row);
            $fileMap[] = array_map('trim', $array);
        }

        $this->csvReader->method('getColumnHeaders')->willReturn($fileMap[0]);

        array_shift($fileMap);

        $this->csvReader->expects($this->atLeastOnce())
            ->method('getNextRow')
            ->will(
                $this->onConsecutiveCalls(
                    $fileMap[0],
                    $fileMap[1],
                    $fileMap[2],
                    $fileMap[3],
                    $fileMap[4],
                    $fileMap[5],
                    $fileMap[6],
                    null
                ));

        /* @var ProductRepository|\PHPUnit_Framework_MockObject_MockObject $parser */
        $productRepository = $this->getMockBuilder('\AppBundle\Repository\ProductRepository')->disableOriginalConstructor()->getMock();
        $product = new Product();
        $this->set($product, 1, 'id');

        if ($status == 'insert') {
            $this->entityManager->expects($this->any())->method('getRepository')->willReturn($productRepository);
        } elseif ($status == 'ignore') {
            $productRepository->expects($this->any())->method('findOneBy')->willReturn($product);
            $this->entityManager->expects($this->any())->method('getRepository')->willReturn($productRepository);
        } elseif ($status == 'update') {
            $productRepository->expects($this->any())->method('findOneBy')->willReturn($product);
            $this->entityManager->expects($this->any())->method('getRepository')->willReturn($productRepository);
        }
    }
}
