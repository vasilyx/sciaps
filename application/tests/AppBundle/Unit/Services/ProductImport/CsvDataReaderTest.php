<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 07/10/2018
 * Time: 12:06
 */

namespace Tests\AppBundle\Unit\Services\ProductImport;

use AppBundle\Services\ProductImport\CsvDataReader;
use PHPUnit\Framework\TestCase;
use Tests\Helpers\UnitTestBase;

class CsvDataReaderTest extends UnitTestBase
{

    public function testNext()
    {
        $splFileObject = new \SplFileObject($this->getFilePath('product_sample_5_products.csv'));
        $reader = new CsvDataReader($splFileObject);

        $file = $this->setupFile();

        $row = $file[2];
        $reader->seek(2);

        $this->assertEquals($row, $reader->current());

        $reader->next();
        $this->assertEquals($file[3], $reader->current());
    }

    public function testGetNextRow()
    {
        $splFileObject = new \SplFileObject($this->getFilePath('product_sample_5_products.csv'));
        $reader = new CsvDataReader($splFileObject);

        $file = $this->setupFile();

        $firstRow = $file[1];

        $this->assertEquals($firstRow, $reader->getNextRow());
    }

    public function testSetSource()
    {
        $splFileObject = new \SplFileObject($this->getFilePath('product_sample_5_products.csv'));
        $reader = new CsvDataReader($splFileObject);

        // set source is called in the constructor
        $this->assertSame($splFileObject, $reader->getSource());
    }

    public function testSetColumnHeaders()
    {
        $reader = new CsvDataReader();

        $file = $this->setupFile('product_sample_5_products.csv', true);

        $firstRow = $file[0];

        $reader->setColumnHeaders($firstRow);

        $this->assertEquals($firstRow, $reader->getColumnHeaders());
    }

    public function testGetColumnHeaders()
    {
        $reader = new CsvDataReader();

        $this->assertNull($reader->getColumnHeaders());

        $file = $this->setupFile('product_sample_5_products.csv', true);

        $firstRow = $file[0];
        $reader->setColumnHeaders($firstRow);
        $this->assertEquals($firstRow, $reader->getColumnHeaders());
    }

    public function testSeek()
    {
        $splFileObject = new \SplFileObject($this->getFilePath('product_sample_5_products.csv'));
        $reader = new CsvDataReader($splFileObject);

        $file = $this->setupFile();

        $row = $file[2];
        $reader->seek(2);

        $this->assertEquals($row, $reader->current());
    }

    public function testGetHeaderRow()
    {
        $splFileObject = new \SplFileObject($this->getFilePath('product_sample_5_products.csv'));
        $reader = new CsvDataReader($splFileObject);

        $file = $this->setupFile();

        $firstRow = $file[0];

        $this->assertEquals($firstRow, $reader->getHeaderRow());
    }

    public function testValid()
    {
        $splFileObject = new \SplFileObject($this->getFilePath('product_sample_5_products.csv'));
        $reader = new CsvDataReader($splFileObject);

        $file = $this->setupFile();

        $firstRow = $file[0];

        $reader->getNextRow();

        $this->assertTrue($reader->valid());
    }

    public function testRewind()
    {
        $splFileObject = new \SplFileObject($this->getFilePath('product_sample_5_products.csv'));
        $reader = new CsvDataReader($splFileObject);

        $file = $this->setupFile();

        $row = $file[4];

        $reader->seek(4);

        $this->assertEquals($row, $reader->current());

        $reader->rewind();

        $this->assertEquals($file[0], $reader->current());
    }

    public function testCurrent()
    {
        $splFileObject = new \SplFileObject($this->getFilePath('product_sample_5_products.csv'));
        $reader = new CsvDataReader($splFileObject);

        $file = $this->setupFile();

        $row = $file[4];

        $reader->seek(4);

        $this->assertEquals($row, $reader->current());
    }

    public function testRowCount()
    {
        $splFileObject = new \SplFileObject($this->getFilePath('product_sample_5_products.csv'));
        $reader = new CsvDataReader($splFileObject);

        $file = $this->setupFile();

        $this->assertEquals(8, $reader->rowCount());
    }

    public function testKey()
    {
        $splFileObject = new \SplFileObject($this->getFilePath('product_sample_5_products.csv'));
        $reader = new CsvDataReader($splFileObject);

        $reader->seek(5);

        $this->assertEquals(5, $reader->key());
    }

    /**
     * @param string $fileName
     * @param bool $trim
     * @return array
     */
    protected function setupFile(string $fileName = 'product_sample_5_products.csv', bool $trim = false): array
    {
        $fileArray = file($this->getFilePath($fileName));
        $fileMap = [];

        foreach ($fileArray as $k => $row) {
            $array = str_getcsv($row);
            if ($trim) {
                $fileMap[] = array_map('trim', $array);
            } else {
                $fileMap[] = $array;
            }
        }

        return $fileMap;
    }
}
