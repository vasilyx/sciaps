<?php

namespace Tests\AppBundle\Controller;

use Tests\Helpers\IntegrationTestBase;

class ProductControllerChemDTest extends IntegrationTestBase
{
    // Step 1
    public function testUpload()
    {
        $this->markTestSkipped('Only for ChemD');

        $csv = file_get_contents($this->getFilePath('ChemD-1.csv'));
        $url = self::$router->generate('app_product_uploadproduct', ['fileData' => base64_encode($csv)]);
        $this->sendRequest('POST', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    // Step 2
    public function testUploadSampleData()
    {
        $this->markTestSkipped('Only for ChemD');
        // For testUpload5Products
        $data = [
            'mappingData' => [
                'name' => 'Product Title',
                'brand' => 'Brand Name',
                'barcode' => 'Barcode',
                'remote_id' => 'Product Code',
                'description' => 'Description',
                'ingredients' => 'Ingredients'
            ]
        ];
        $url = self::$router->generate('app_product_uploadproductsampledata', $data);

        $this->sendRequest('POST', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertArrayNotHasKey('errors', $this->getContentAsArray());
    }

    // Step 3
    public function testUploadProductConfirmMappingData()
    {
        $this->markTestSkipped('Only for ChemD');
        $data = [
            'mappingData' => [
                'name' => 'Product Title',
                'brand' => 'Brand Name',
                'barcode' => 'Barcode',
                'remote_id' => 'Product Code',
                'description' => 'Description',
                'ingredients' => 'Ingredients'
            ]
        ];

        $url = self::$router->generate('app_product_uploadproductconfirmmappingdata', $data);
        $this->sendRequest('POST', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    // Step 4
    public function testUploadProductConfirmDataOverwrite()
    {
        $this->markTestSkipped('Only for ChemD');
        $url = self::$router->generate('app_product_uploadconfirm',
            ['overwriteProducts' => true]
        );

        $this->sendRequest('POST', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }
}
