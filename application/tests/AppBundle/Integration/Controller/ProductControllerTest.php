<?php

namespace Tests\AppBundle\Integration\Controller;

use AppBundle\Entity\Barcode;
use AppBundle\Entity\Brand;
use AppBundle\Entity\Manufacturer;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductProvider;
use AppBundle\Entity\Retailer;
use Symfony\Component\HttpFoundation\Response;
use Tests\Helpers\IntegrationTestBase;

class ProductControllerTest extends IntegrationTestBase
{
    public function testAddProduct()
    {
        $brand = self::$entityManager->getRepository(Brand::class)->findAll();
        $barcodes = self::$entityManager->getRepository(Barcode::class)->findAll();
        $manufacturer = self::$entityManager->getRepository(Manufacturer::class)->findAll();

        $requestData = [
            "name" => "Test Product Name",
            "description" => "Test information about product",
            "volume" => "12112",
            "volumeUnits" => "12112",
            "brand" => $brand[0]->getId(),
            "barcode" => $barcodes[0]->getId(),
            "retailer" => ['name' => 'asdasd'],
            "manufacturer" => $manufacturer[0]->getId(),
        ];

        $url = self::$router->generate('app_v1_product_addproduct');
        $this->sendRequest('POST', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertArrayHasKey('id', $this->getContentAsArray()); // created ID
    }

    public function testUpdateProduct()
    {
        $arr = self::$entityManager->getRepository(Product::class)->findBy([], ['addedAt' => 'DESC']);
        $brand = self::$entityManager->getRepository(Brand::class)->findAll();
        $barcodes = self::$entityManager->getRepository(Barcode::class)->findAll();
        $manufacturer = self::$entityManager->getRepository(Manufacturer::class)->findAll();

        $requestData = [
            "name" => "Test Product Name" . rand(),
            "description" => "Test information about product",
            "volume" => "12112",
            "volumeUnits" => "12112",
            "brand" => $brand[0]->getId(),
            "barcode" => $barcodes[0]->getId(),
            "retailer" => ['name' => 'ffff'],
            "manufacturer" => $manufacturer[0]->getId(),
        ];

        $url = self::$router->generate('app_v1_product_updateproduct', ['id' => $arr[0]->getId()]);
        $this->sendRequest('PUT', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();
        $this->assertEquals($body['name'], $requestData['name']); // created ID
    }

    public function testGetProduct()
    {
        $arr = self::$entityManager->getRepository(Product::class)->findAll();

        $url = self::$router->generate('app_v1_product_getproduct', ['id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $url = self::$router->generate('app_v1_product_getproduct', ['id' => 99999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testGetProducts()
    {
        $url = self::$router->generate('app_v1_product_getproducts');
        $this->sendRequest('GET', $url);

   //     var_dump($this->getContentAsArray());

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));
    }

    public function testGetProductIngredient()
    {
        $arr = self::$entityManager->getRepository(Product::class)->findAll();

        $url = self::$router->generate('app_v1_product_getproductingredients', ['id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testDeleteProduct()
    {
        $arr = self::$entityManager->getRepository(Product::class)->findAll();

        $url = self::$router->generate('app_v1_product_deleteproduct', ['id' => $arr[0]->getId()]);
        $this->sendRequest('DELETE', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $url = self::$router->generate('app_v1_product_deleteproduct', ['id' => 99999]);
        $this->sendRequest('DELETE', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testGetProductsByParams()
    {
        $url = self::$router->generate('app_v1_product_getproducts', ['_limit' => 2, 'id_ne' => 2]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertEquals(2, count($this->getContentAsArray()));

        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));

        $url = self::$router->generate('app_v1_product_getproducts', ['_fields' => 'id, name']);
        $this->sendRequest('GET', $url);

        $this->assertEquals(2, count($this->getContentAsArray()[0])); // only two fields in row

        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_product_getproducts', ['q' => $this->getContentAsArray()[0]['name']]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(1, count($this->getContentAsArray()));

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    // Validate base 64 encoding of file
    public function testUploadNonBase64()
    {
        $csv = file_get_contents($this->getFilePath('sample_data_v1.csv'));
        $url = self::$router->generate('app_v1_product_uploadproduct', ['fileData' => $csv]);
        $this->sendRequest('POST', $url);

        $this->assertArrayHasKey('errors', $this->getContentAsArray());
        $this->assertTrue($this->getContentAsArray()['errors'][0] == 'Please send data using base64 encoding.');
        $this->assertEquals(Response::HTTP_UNSUPPORTED_MEDIA_TYPE, self::$client->getResponse()->getStatusCode());
    }

    // Validate utf-8 encoding of file
    public function testUploadNonUTF8()
    {
        $csv = file_get_contents($this->getFilePath('non-utf-8.txt'));
        $url = self::$router->generate('app_v1_product_uploadproduct', ['fileData' => base64_encode($csv)]);
        $this->sendRequest('POST', $url);

        $this->assertArrayHasKey('errors', $this->getContentAsArray());
        $this->assertTrue($this->getContentAsArray()['errors'][0] == 'Data should be UTF-8 encoded.');
        $this->assertEquals(Response::HTTP_UNSUPPORTED_MEDIA_TYPE, self::$client->getResponse()->getStatusCode());
    }

/*    // Only CSV files are accepted
    public function testUploadNonCSV()
    {
        $this->markTestSkipped('Cannot check CSV file mime type on server ');

        $text = file_get_contents(self::$kernel->getRootDir() . '/Resources/Tests/utf-8.txt');
        $url = self::$router->generate('app_product_uploadproduct', ['fileData' => base64_encode($text)]);
        $this->sendRequest('POST', $url);

        $this->assertArrayHasKey('errors', $this->getContentAsArray());
        $this->assertTrue($this->getContentAsArray()['errors'][0] == 'Please upload data in CSV format');
        $this->assertEquals(Response::HTTP_UNSUPPORTED_MEDIA_TYPE, self::$client->getResponse()->getStatusCode());
    }*/


    // step 1
    public function testUpload()
    {
        $url = self::$router->generate('app_v1_product_uploadproduct', ['fileData' => '']);
        $this->sendRequest('POST', $url);
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, self::$client->getResponse()->getStatusCode());
        $url = self::$router->generate('app_v1_product_uploadproduct', []);
        $this->sendRequest('POST', $url);
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, self::$client->getResponse()->getStatusCode());

        $csv = file_get_contents($this->getFilePath('sample_data_v1.csv'));
        $url = self::$router->generate('app_v1_product_uploadproduct', ['fileData' => base64_encode($csv)]);
        $this->sendRequest('POST', $url);

        $this->assertArrayHasKey('fields', $this->getContentAsArray());
        $this->assertArrayHasKey('columnTitles', $this->getContentAsArray());
        $this->assertArrayHasKey('mappingData', $this->getContentAsArray());
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    // step 1
    public function testUpload5Products()
    {
        $url = self::$router->generate('app_v1_product_uploadproduct', ['fileData' => '']);
        $this->sendRequest('POST', $url);
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, self::$client->getResponse()->getStatusCode());
        $url = self::$router->generate('app_v1_product_uploadproduct', []);
        $this->sendRequest('POST', $url);
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, self::$client->getResponse()->getStatusCode());

        $csv = file_get_contents($this->getFilePath('product_sample_5_products.csv'));
        $url = self::$router->generate('app_v1_product_uploadproduct', ['fileData' => base64_encode($csv)]);
        $this->sendRequest('POST', $url);

        $this->assertArrayHasKey('fields', $this->getContentAsArray());
        $this->assertArrayHasKey('columnTitles', $this->getContentAsArray());
        $this->assertArrayHasKey('mappingData', $this->getContentAsArray());
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    // step 2
    public function testUploadSampleData()
    {
//         For testUpload
//        $data = [
//            'mappingData' => [
//                'name' => 'name',
//                'manufacturer' => '',
//                'brand' => 'brand',
//                'barcode' => 'barcode',
//                'remote_id' => 'id',
//                'description' => 'description'
//            ]
//        ];
        // For testUpload5Products
        $data = [
            'mappingData' => [
                'name' => 'Product Title',
                'manufacturer' => '',
                'brand' => 'Brand',
                'barcode' => 'EAN/UPC barcode',
                'remote_id' => 'Product Code',
                'description' => 'Description (web)',
                'ingredients' => 'Ingredients'
            ]
        ];
        $url = self::$router->generate('app_v1_product_uploadproductsampledata', $data);

        $this->sendRequest('POST', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertArrayNotHasKey('errors', $this->getContentAsArray());
//        $this->assertEquals('Holland &amp; Barrett Soya Isoflavones 120 Capsules 750mg', $this->getContentAsArray()[0]['name']);
//        $this->assertEquals('Good n Natural Uva Ursi 100 Capsules 500mg', $this->getContentAsArray()[1]['name']);
//        $this->assertEquals('Holland &amp; Barrett Soya Isoflavones 120 Capsules 75mg', $this->getContentAsArray()[2]['name']);
        $this->assertEquals('Calendula Toothpaste', $this->getContentAsArray()[0]['Product Title']);
        $this->assertEquals('Ratanhia Toothpaste', $this->getContentAsArray()[1]['Product Title']);
        $this->assertEquals('Plant Gel Toothpaste', $this->getContentAsArray()[2]['Product Title']);
    }

    // Need mapping data!
    public function testUploadSampleDataEmpty()
    {
        $data = [];
        $url = self::$router->generate('app_v1_product_uploadproductsampledata', $data);
        $this->sendRequest('POST', $url);
        $this->assertArrayHasKey('errors', $this->getContentAsArray());
        $this->assertTrue($this->getContentAsArray()['errors'][0] == 'Please upload mapping data');
        $this->assertEquals(self::$badFormSubmissionErrorCode, self::$client->getResponse()->getStatusCode());

        $data = ['mappingData' => []];
        $url = self::$router->generate('app_v1_product_uploadproductsampledata', $data);
        $this->sendRequest('POST', $url);
        $this->assertArrayHasKey('errors', $this->getContentAsArray());
        $this->assertTrue($this->getContentAsArray()['errors'][0] == 'Please upload mapping data');
        $this->assertEquals(self::$badFormSubmissionErrorCode, self::$client->getResponse()->getStatusCode());
    }

    // Need mapping data!
    public function testUploadSampleDataInvalidDBFieldName()
    {
        $data = [
            'mappingData' => [
                'name3' => 'Product Title',
                'manufacturerd' => '',
                'brand' => 'Brand',
                'barco-{} de' => 'EAN/UPC barcode',
                'remote_id' => 'Product Code',
                'description' => 'Description (web)'
            ]
        ];

        $url = self::$router->generate('app_v1_product_uploadproductsampledata', $data);
        $this->sendRequest('POST', $url);

        $this->assertArrayHasKey('errors', $this->getContentAsArray());
        $this->assertTrue($this->getContentAsArray()['errors'][0] == 'The "barcode" field is required.');
        $this->assertEquals(self::$badFormSubmissionErrorCode, self::$client->getResponse()->getStatusCode());
    }

    // step 3
    public function testUploadProductConfirmMappingData()
    {
//        For testUpload
//        $data = [
//            'mappingData' => [
//                'name' => 'name',
//                'manufacturer' => '',
//                'brand' => 'brand',
//                'barcode' => 'barcode',
//                'remote_id' => 'id',
//                'description' => 'description',
//                'ingredients' => 'ingredients',
//            ]
//        ];

//        For testUpload5Products
        $data = [
            'mappingData' => [
                'name' => 'Product Title',
                'manufacturer' => '',
                'brand' => 'Brand',
                'barcode' => 'EAN/UPC barcode',
                'remote_id' => 'Product Code',
                'description' => 'Description (web)',
                'ingredients' => 'Ingredients'
            ]
        ];

        $url = self::$router->generate('app_v1_product_uploadproductconfirmmappingdata', $data);
        $this->sendRequest('POST', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $this->assertArrayHasKey('productsFound', $this->getContentAsArray());
        $this->assertArrayHasKey('newProducts', $this->getContentAsArray());
        $this->assertArrayHasKey('productsToBeUpdated', $this->getContentAsArray());
    }

    // Empty mapping data. This is wrong because we have to test the same thing TWICE. Something needs to be refactored.
    public function testUploadProductConfirmMappingDataEmpty()
    {
        $data = [];
        $url = self::$router->generate('app_v1_product_uploadproductconfirmmappingdata', $data);
        $this->sendRequest('POST', $url);
        $this->assertArrayHasKey('errors', $this->getContentAsArray());
        $this->assertTrue($this->getContentAsArray()['errors'][0] == 'Please upload mapping data');
        $this->assertEquals(self::$badFormSubmissionErrorCode, self::$client->getResponse()->getStatusCode());

        $data = ['mappingData' => []];
        $url = self::$router->generate('app_v1_product_uploadproductconfirmmappingdata', $data);
        $this->sendRequest('POST', $url);
        $this->assertArrayHasKey('errors', $this->getContentAsArray());
        $this->assertTrue($this->getContentAsArray()['errors'][0] == 'Please upload mapping data');
        $this->assertEquals(self::$badFormSubmissionErrorCode, self::$client->getResponse()->getStatusCode());
    }

    // step 4
    public function testUploadProductConfirmDataOverwrite()
    {
        $url = self::$router->generate('app_v1_product_uploadconfirm',
            ['overwriteProducts' => true]
        );

        $this->sendRequest('POST', $url);

//        var_dump(self::$client->getResponse());
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $this->assertArrayHasKey('productsAdded', $this->getContentAsArray());
        $this->assertArrayHasKey('productsUpdated', $this->getContentAsArray());
        $this->assertTrue($this->getContentAsArray()['productsAdded'] == 5);
        $this->assertTrue($this->getContentAsArray()['productsUpdated'] == 0);
    }

    // step 4
    public function testUploadProductConfirmDataNotOverwrite()
    {
        $url = self::$router->generate('app_v1_product_uploadconfirm',
            ['overwriteProducts' => false]
        );

        $this->sendRequest('POST', $url);
//        var_dump(self::$client->getResponse());
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $this->assertArrayHasKey('productsAdded', $this->getContentAsArray());
        $this->assertArrayHasKey('productsUpdated', $this->getContentAsArray());
        $this->assertTrue($this->getContentAsArray()['productsAdded'] == 0);
        $this->assertTrue($this->getContentAsArray()['productsUpdated'] == 0);
    }
}
