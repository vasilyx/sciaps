<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Retailer;
use Tests\Helpers\IntegrationTestBase;

class RetailerControllerTest extends IntegrationTestBase
{
    public function testGetRetailer()
    {
        $arr = self::$entityManager->getRepository(Retailer::class)->findAll();

        $url = self::$router->generate('app_v1_retailer_getretailer', ['id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $url = self::$router->generate('app_v1_retailer_getretailer', ['id' => 99999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testGetRetailers()
    {
        $url = self::$router->generate('app_v1_retailer_getretailers');
        $this->sendRequest('GET', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));
    }
}
