<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Entity\Source;
use Tests\Helpers\IntegrationTestBase;

class SourceControllerTest extends IntegrationTestBase
{
    public function testAddSource()
    {
        $arr = self::$entityManager->getRepository(Company::class)->findAll();

        $requestData = [
            "title" => "Test Title" . rand(),
            "nameAuthor" => "Test name Author" . rand(),
            "articleType" =>  1,
            "company" => $arr[0]->getId(),
            "articleURL" => "https://testurl.com/" . rand()
        ];
        $url = self::$router->generate('app_v1_source_addsource');
        $this->sendRequest('POST', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertArrayHasKey('id', $this->getContentAsArray()); // created ID
    }

    public function testUpdateSource()
    {
        $arr_company = self::$entityManager->getRepository(Company::class)->findAll();
        $arr = self::$entityManager->getRepository(Source::class)->findAll();

        $requestData = [
            "title" => "Test Title 000",
            "nameAuthor" => "Test name Author 000",
            "articleType" =>  1,
            "company" => $arr_company[0]->getId(),
            "articleURL" => "https://testurl.com/" . rand()
        ];

        $url = self::$router->generate('app_v1_source_updatesource', ['id' => $arr[0]->getId()]);
        $this->sendRequest('PUT', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();

        $this->assertEquals($body['title'], $requestData['title']);
    }

    public function testGetSource()
    {
        $arr = self::$entityManager->getRepository(Source::class)->findAll();

        $url = self::$router->generate('app_v1_source_getsource', ['id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $url = self::$router->generate('app_v1_source_getsource', ['id' => 99999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testGetSources()
    {
        $url = self::$router->generate('app_v1_source_getsources');
        $this->sendRequest('GET', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));
    }
/*
    public function testDeleteSource()
    {
        $arr = self::$entityManager->getRepository(Source::class)->findAll();

        $url = self::$router->generate('app_v1_source_deletesource', ['id' => $arr[0]->getId()]);
        $this->sendRequest('DELETE', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }
*/
}
