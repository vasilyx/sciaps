<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Company;
use Tests\Helpers\IntegrationTestBase;

class CompanyControllerTest extends IntegrationTestBase
{
    public $created;

    public function testAddCompany()
    {
        $requestData = [
            "companyName" => "Test Company Name" . rand(),
            "displayName" => "Test Contact Name" . rand(),
            "websiteURL" => "http://sad.com" . rand(),
            "emailDomain" => "ddtest@com.com" . rand(),
            "services" => [100, 200, 300]
        ];

        $url = self::$router->generate('app_v1_company_addcompany');
        $this->sendRequest('POST', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $this->assertArrayHasKey('id', $this->getContentAsArray()); // created ID
    }

    public function testUpdateCompany()
    {
        $requestData = [
            "companyName" => "Update Test Company Name" . rand(),
            "displayName" => "ddd Test Contact Name" . rand(),
            "websiteURL" => "http://sad.com" . rand(),
            "emailDomain" => "update_ddtest@com.com" . rand()
        ];

        $arr = self::$entityManager->getRepository(Company::class)->findAll();

        $url = self::$router->generate('app_v1_company_updatecompany', ['id' => $arr[0]->getId()]);
        $this->sendRequest('PUT', $url, $requestData);


        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();

        $this->assertEquals($body['companyName'], $requestData['companyName']); // created ID
    }

    public function testGetCompanies()
    {
        $url = self::$router->generate('app_v1_company_getcompanies');
        $this->sendRequest('GET', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));
    }

    public function testGetCompany()
    {
        $arr = self::$entityManager->getRepository(Company::class)->findAll();

        $url = self::$router->generate('app_v1_company_getcompany', ['id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_company_getcompany', ['id' => 99999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testAddNewServiceProduct()
    {
        $arr = self::$entityManager->getRepository(Company::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "productProviderType" => 1,
            "connectionType" => 1,
            "status" => 1,
            "ingestionReports" => 1,
            "ingestionAnalytics" => 1
        ];

        $url = self::$router->generate('app_v1_company_addnewserviceproduct', ['id' => $arr[0]->getId()]);
        $this->sendRequest('POST', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertArrayHasKey('id', $this->getContentAsArray()); // created ID
    }

    public function testAddNewServiceSource()
    {
        $arr = self::$entityManager->getRepository(Company::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_company_addnewservicesource', ['id' => $arr[0]->getId()]);
        $this->sendRequest('POST', $url, []);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertArrayHasKey('id', $this->getContentAsArray()); // created ID
    }

    public function testAddNewServiceDataEntry()
    {
        $arr = self::$entityManager->getRepository(Company::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_company_addnewservicedataentry', ['id' => $arr[0]->getId()]);
        $this->sendRequest('POST', $url, []);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testUpdateServiceProduct()
    {
        $requestData = [
            "productProviderType" => 0,
            "connectionType" => 1,
            "status" => 0,
            "ingestionReports" => 1,
            "ingestionAnalytics" => 0
        ];

        $arr = self::$entityManager->getRepository(Company::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_company_updateserviceproduct', ['id' => $arr[0]->getId()]);
        $this->sendRequest('PUT', $url, $requestData);


        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();

        $this->assertEquals($body['productProviderType'], $requestData['productProviderType']); // created ID
    }

    public function testGetService()
    {
        $arr = self::$entityManager->getRepository(Company::class)->findAll();

        $url = self::$router->generate('app_v1_company_getservice', [
            'id' => $arr[0]->getId(),
            'service_code' => 100
        ]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_company_getservice', [
            'id' => $arr[0]->getId(),
            'service_code' => 200
        ]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_company_getservice', [
            'id' => $arr[0]->getId(),
            'service_code' => 300
        ]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }



/*
    public function testDeleteCompany()
    {
        $arr = self::$entityManager->getRepository(Company::class)->findAll();

        $url = self::$router->generate('app_v1_company_deletecompany', ['id' => $arr[0]->getId()]);

        $this->sendRequest('DELETE', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }
*/
}
