<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use AppBundle\Entity\User;
use Tests\Helpers\IntegrationTestBase;

class UserControllerTest extends IntegrationTestBase
{
    public function testAddUser()
    {
        $company = self::$entityManager->getRepository(Company::class)->findAll();

        $requestData = [
            'email' => 'test211@gmail.com' . rand(),
            'firstName' => 'asdas',
            'surname' => 'sdfd',
            "company" => $company[0]->getId()
        ];

        $url = self::$router->generate('app_v1_user_adduser');
        $this->sendRequest('POST', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertArrayHasKey('id', $this->getContentAsArray()); // created ID
    }

    public function testGetUser()
    {
        $userList = self::$entityManager->getRepository(User::class)->findAll();

        $url = self::$router->generate('app_v1_user_getuser', ['id' => $userList[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_user_getuser', ['id' => 999999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testUpdateUser()
    {
        $arr = self::$entityManager->getRepository(User::class)->findAll();
        $company = self::$entityManager->getRepository(Company::class)->findAll();

        $requestData = [
            'email' => 'test211@gmail.com' . rand(),
            'firstName' => 'asdas',
            'surname' => 'sdfd',
            'status' => 1,
            "company" => $company[0]->getId()
        ];

        $url = self::$router->generate('app_v1_user_updateuser', ['id' => $arr[0]->getId()]);
        $this->sendRequest('PUT', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $body = $this->getContentAsArray();
        $this->assertEquals($body['email'], $requestData['email']); // created ID
    }

    public function testGetProducts()
    {
        $url = self::$router->generate('app_v1_user_getusers');
        $this->sendRequest('GET', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));

        $url = self::$router->generate('app_v1_user_getusers', ['_sort' => 'company12.companyName']);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_user_getusers', ['_sort' => 'company.companyName']);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }
}
