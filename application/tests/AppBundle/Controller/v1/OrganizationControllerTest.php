<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Country;
use AppBundle\Entity\Organization;
use Tests\Helpers\IntegrationTestBase;

class OrganizationControllerTest extends IntegrationTestBase
{
    public function testAddOrganization()
    {
        $arr = self::$entityManager->getRepository(Country::class)->findAll();

        $requestData = [
            "companyName" => "Nike sport" . rand(),
            "websiteURL" => "http://nike.cmm" . rand(),
            "barcode" => true,
            "countries" => [$arr[0]->getId()],
            "productsProvidedVia" => "RSS Feed, CSV",
            "productsSoldThrough" => "Superdrug, Mankind, eChemist, Chemist Direct",
            "numberProducts" => 132,
            "personName" => "Jordan" . rand(),
            "personEmail" => "jordan@gmail.com" . rand(),
            "personComment" => "Some Comment lines" . rand()
        ];
        $url = self::$router->generate('app_v1_organization_addorganization');
        $this->sendRequest('POST', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertArrayHasKey('id', $this->getContentAsArray()); // created ID
        unset($requestData['countries']);
        $this->checkResponseEqual($requestData);
    }

    public function testGetOrganization()
    {
        $arr = self::$entityManager->getRepository(Organization::class)->findAll();

        $url = self::$router->generate('app_v1_organization_deleteorganization', ['id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $url = self::$router->generate('app_v1_organization_getorganization', ['id' => 99999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testGetOrganizations()
    {
        $url = self::$router->generate('app_v1_organization_getorganizations');
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testDeleteOrganization()
    {
        $arr = self::$entityManager->getRepository(Organization::class)->findAll();

        $url = self::$router->generate('app_v1_organization_deleteorganization', ['id' => $arr[0]->getId()]);
        $this->sendRequest('DELETE', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }
}
