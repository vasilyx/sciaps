<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Attribute;
use Tests\Helpers\IntegrationTestBase;

class AttributeControllerTest extends IntegrationTestBase
{
    public function testAddAttribute()
    {
        $arr = self::$entityManager->getRepository(Attribute::class)->findAll();

        $symbol = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyzQWERTYUIOPLKJHGFDSAZXCVBNM", 4)), 0, 4);

        $requestData = [
            "parent" => $arr[0]->getParent(),
            "defaultName" => "Test Default Name",
            "defaultDescription" => "Test Default Description",
            "symbol" => $symbol
        ];
        $url = self::$router->generate('app_v1_attribute_addattribute');
        $this->sendRequest('POST', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertArrayHasKey('id', $this->getContentAsArray()); // created ID
    }

    public function testUpdateAttribute()
    {
        $arr = self::$entityManager->getRepository(Attribute::class)->findAll();

        $symbol = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyzQWERTYUIOPLKJHGFDSAZXCVBNM", 4)), 0, 4);

        $requestData = [
            "parent" => $arr[0]->getParent(),
            "defaultName" => "Test Default Name",
            "defaultDescription" => "Test Default Description",
            "symbol" => $symbol
        ];

        $url = self::$router->generate('app_v1_attribute_updateattribute', ['id' => $arr[0]->getId()]);

        $this->sendRequest('PUT', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $body = $this->getContentAsArray();

        $this->assertEquals($body['defaultName'], $requestData['defaultName']); // created ID
    }

    public function testGetAttribute()
    {
        $arr = self::$entityManager->getRepository(Attribute::class)->findAll();

        $url = self::$router->generate('app_v1_attribute_getattribute', ['id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $url = self::$router->generate('app_v1_attribute_getattribute', ['id' => 999999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testGetAttributes()
    {
        $url = self::$router->generate('app_v1_attribute_getattributes');
        $this->sendRequest('GET', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));
    }

    public function testDeleteAttribute()
    {
        $arr = self::$entityManager->getRepository(Attribute::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_attribute_deleteattribute', ['id' => $arr[0]->getId()]);
        $this->sendRequest('DELETE', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }
}
