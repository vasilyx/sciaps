<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Manufacturer;
use Tests\Helpers\IntegrationTestBase;

class ManufacturerControllerTest extends IntegrationTestBase
{
    public function testGetManufacturer()
    {
        $arr = self::$entityManager->getRepository(Manufacturer::class)->findAll();

        $url = self::$router->generate('app_v1_manufacturer_getmanufacturer', ['id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $url = self::$router->generate('app_v1_manufacturer_getmanufacturer', ['id' => 99999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testGetManufacturers()
    {
        $url = self::$router->generate('app_v1_manufacturer_getmanufacturers');
        $this->sendRequest('GET', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));
    }
}
