<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Barcode;
use Tests\Helpers\IntegrationTestBase;

class BarcodeControllerTest extends IntegrationTestBase
{
    public function testGetBarcode()
    {
        $arr = self::$entityManager->getRepository(Barcode::class)->findAll();

        $url = self::$router->generate('app_v1_barcode_getbarcode', ['id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $url = self::$router->generate('app_v1_barcode_getbarcode', ['id' => 99999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testGetBarcodes()
    {
        $url = self::$router->generate('app_v1_barcode_getbarcodes');
        $this->sendRequest('GET', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));
    }

    public function testGetBarcodeValidate()
    {
        $url = self::$router->generate('app_v1_barcode_getbarcodevalidate', ['id' => 3516423343695]);
        $this->sendRequest('GET', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();

        $this->assertEquals($body['type'], 4);

        $url = self::$router->generate('app_v1_barcode_getbarcodevalidate', ['id' => 'AAA']);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();

        $this->assertEquals(isset($body['type']), false);
    }
}
