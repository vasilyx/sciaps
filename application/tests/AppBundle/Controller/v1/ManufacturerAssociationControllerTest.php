<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Attribute;
use AppBundle\Entity\Evidence;
use AppBundle\Entity\Manufacturer;
use AppBundle\Entity\ManufacturerAttributes;
use AppBundle\Entity\ManufacturerEvidences;
use AppBundle\Entity\Source;
use Tests\Helpers\IntegrationTestBase;

class ManufacturerAssociationControllerTest extends IntegrationTestBase
{
    public function testAddManufacturerEvidence()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);
        $evidence = self::$entityManager->getRepository(Evidence::class)->findBy([], ['addedAt' => 'DESC']);
        $manufacturers = self::$entityManager->getRepository(Manufacturer::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Evidence::TYPE_ORANGE,
            "description" => "Test description for code" . rand(),
            "manufacturers" => [$manufacturers[0]->getId()]
        ];

        $url = self::$router->generate('app_v1_manufacturerassociation_addmanufacturerevidence', [
            'source_id' => $source[0]->getId(),
            'evidence_id' => $evidence[0]->getId()
        ]);

        $this->sendRequest('POST', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testUpdateManufacturerEvidence()
    {
        $manufacturerEvidence = self::$entityManager->getRepository(ManufacturerEvidences::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Evidence::TYPE_ORANGE,
            "description" => "Test update description for code" . rand(),
        ];

        $url = self::$router->generate('app_v1_manufacturerassociation_updatemanufacturerevidence', [
            'source_id' => $manufacturerEvidence[0]->getSource()->getId(),
            'evidence_id' => $manufacturerEvidence[0]->getEvidence()->getId(),
            'manufacturer_id' => $manufacturerEvidence[0]->getManufacturer()->getId()
        ]);
        $this->sendRequest('PUT', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();

        $this->assertEquals($body['statusCode'], $requestData['statusCode']);
    }

    public function testGetEvidences()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_manufacturerassociation_getevidences', ['source_id' => $source[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testGetSourceEvidence()
    {
        $manufacturerEvidence = self::$entityManager->getRepository(ManufacturerEvidences::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_manufacturerassociation_getsourceevidence', [
            'source_id' => $manufacturerEvidence[0]->getSource()->getId(),
            'evidence_id' => $manufacturerEvidence[0]->getEvidence()->getId(),
            'manufacturer_id' => $manufacturerEvidence[0]->getManufacturer()->getId()
        ]);

        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_manufacturerassociation_getsourceevidence', [
            'source_id' => 9999999,
            'evidence_id' => 9999999,
            'manufacturer_id' => 9999999
        ]);

        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testDeleteManufacturerEvidence()
    {
        $manufacturerEvidence = self::$entityManager->getRepository(ManufacturerEvidences::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_manufacturerassociation_deletemanufacturerevidence', [
            'source_id' => $manufacturerEvidence[0]->getSource()->getId(),
            'evidence_id' => $manufacturerEvidence[0]->getEvidence()->getId(),
            'manufacturer_id' => $manufacturerEvidence[0]->getManufacturer()->getId()
        ]);

        $this->sendRequest('DELETE', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testAddManufacturerAttribute()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);
        $attribute = self::$entityManager->getRepository(Attribute::class)->findBy([], ['addedAt' => 'DESC']);
        $manufacturers = self::$entityManager->getRepository(Manufacturer::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Attribute::TYPE_YES,
            "description" => "Test description for code" . rand(),
            "manufacturers" => [$manufacturers[0]->getId()]
        ];

        $url = self::$router->generate('app_v1_manufacturerassociation_addmanufacturerattribute', [
            'source_id' => $source[0]->getId(),
            'attribute_id' => $attribute[0]->getId()
        ]);

        $this->sendRequest('POST', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testUpdateManufacturerAttribute()
    {
        $manufacturerAttribute = self::$entityManager->getRepository(ManufacturerAttributes::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Evidence::TYPE_ORANGE,
            "description" => "Test update description for code" . rand(),
        ];

        $url = self::$router->generate('app_v1_manufacturerassociation_updatemanufacturerattribute', [
            'source_id' => $manufacturerAttribute[0]->getSource()->getId(),
            'attribute_id' => $manufacturerAttribute[0]->getAttribute()->getId(),
            'manufacturer_id' => $manufacturerAttribute[0]->getManufacturer()->getId()
        ]);
        $this->sendRequest('PUT', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();

        $this->assertEquals($body['statusCode'], $requestData['statusCode']);
    }

    public function testGetAttribute()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_manufacturerassociation_getattribute', ['source_id' => $source[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testGetSourceAttribute()
    {
        $manufacturerAttribute = self::$entityManager->getRepository(ManufacturerAttributes::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_manufacturerassociation_getsourceattribute', [
            'source_id' => $manufacturerAttribute[0]->getSource()->getId(),
            'attribute_id' => $manufacturerAttribute[0]->getAttribute()->getId(),
            'manufacturer_id' => $manufacturerAttribute[0]->getManufacturer()->getId()
        ]);

        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_manufacturerassociation_getsourceattribute', [
            'source_id' => 9999999,
            'attribute_id' => 9999999,
            'manufacturer_id' => 9999999
        ]);

        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testDeleteManufacturerAttribute()
    {
        $manufacturerAttribute = self::$entityManager->getRepository(ManufacturerAttributes::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_manufacturerassociation_deletemanufacturerattribute', [
            'source_id' => $manufacturerAttribute[0]->getSource()->getId(),
            'attribute_id' => $manufacturerAttribute[0]->getAttribute()->getId(),
            'manufacturer_id' => $manufacturerAttribute[0]->getManufacturer()->getId()
        ]);

        $this->sendRequest('DELETE', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }
}
