<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\CrowdsourcePending;
use AppBundle\Entity\LogEvent;
use AppBundle\Entity\Product;
use Tests\Helpers\IntegrationTestBase;

class CrowdsourceControllerTest extends IntegrationTestBase
{
    public function testUpdateCrowdsource()
    {
        $arr = self::$entityManager->getRepository(CrowdsourcePending::class)->findAll();

        $requestData = [
            "name" => "Test Product Name" . rand(),
            "description" => "Test information about product",
            "volume" => "12112",
            "brand" => $arr[0]->getBrand()->getId(),
            "barcode" => $arr[0]->getBarcode()->getId(),
            "manufacturer" => $arr[0]->getManufacturer()->getId(),
            "status" => CrowdsourcePending::STATUS_APPROVED
        ];

        $url = self::$router->generate('app_v1_crowdsource_updatecrowdsource', ['id' => $arr[0]->getId()]);
        $this->sendRequest('PUT', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();
        $this->assertEquals($body['name'], $requestData['name']); // created ID
    }

    public function testUpdateNewProductCrowdsource()
    {
        $arr = self::$entityManager->getRepository(CrowdsourcePending::class)->findAll();
        $previous_products = self::$entityManager->getRepository(Product::class)->findAll();

        $requestData = [
            "name" => "Test Product Name" . rand(),
            "description" => "Test information about product",
            "volume" => "12112",
            "brand" => $arr[0]->getBrand()->getId(),
            "barcode" => $arr[0]->getBarcode()->getId(),
            "manufacturer" => $arr[0]->getManufacturer()->getId(),
            "status" => CrowdsourcePending::STATUS_HEAD_APPROVED
        ];

        $url = self::$router->generate('app_v1_crowdsource_updatecrowdsource', ['id' => $arr[0]->getId()]);
        $this->sendRequest('PUT', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();
        $this->assertEquals($body['name'], $requestData['name']); // created ID

        $products = self::$entityManager->getRepository(Product::class)->findAll();

        // check that product was created after status changed
        $this->assertGreaterThan(count($previous_products), count($products));

        // check LogEntity created after status changed
        $entityLogs = self::$entityManager->getRepository(LogEvent::class)->findByEntityId(LogEvent::ENTITY_TYPE_CROWDSOURCE);

        $this->assertGreaterThan(0, count($entityLogs));
    }

    public function testGetCrowdsource()
    {
        $arr = self::$entityManager->getRepository(CrowdsourcePending::class)->findAll();

        $url = self::$router->generate('app_v1_crowdsource_getcrowdsource', ['id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $url = self::$router->generate('app_v1_crowdsource_getcrowdsource', ['id' => 99999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testGetCrowdsources()
    {
        $url = self::$router->generate('app_v1_crowdsource_getcrowdsources');
        $this->sendRequest('GET', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));
    }



    public function testDeleteCrowdsource()
    {
        $arr = self::$entityManager->getRepository(CrowdsourcePending::class)->findAll();

        $url = self::$router->generate('app_v1_crowdsource_deletecrowdsource', ['id' => $arr[0]->getId()]);
        $this->sendRequest('DELETE', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testGetCrowdsourcesArchived()
    {
        $url = self::$router->generate('app_v1_crowdsource_getcrowdsourcesarchived');
        $this->sendRequest('GET', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));
    }

    public function testGetCrowdsourcesAccepted()
    {
        $url = self::$router->generate('app_v1_crowdsource_getcrowdsourcesaccepted');
        $this->sendRequest('GET', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));
    }

    public function testGetCrowdsourcesIngredients()
    {
        $arr = self::$entityManager->getRepository(CrowdsourcePending::class)->findAll();

        $url = self::$router->generate('app_v1_crowdsource_getcrowdsourceingredients', ['id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testGetCrowdsourcesTracking()
    {
        $url = self::$router->generate('app_v1_crowdsource_getcrowdsourcestracking');
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());


/*        $this->assertArrayHasKey('productsApproved', $this->getContentAsArray());
        $this->assertArrayHasKey('productsApprovedManager', $this->getContentAsArray());
        $this->assertArrayHasKey('productsArchived', $this->getContentAsArray());
        $this->assertArrayHasKey('productsArchivedManager', $this->getContentAsArray());
        $this->assertArrayHasKey('contractorId', $this->getContentAsArray());
        $this->assertArrayHasKey('contractorName', $this->getContentAsArray());
        $this->assertArrayHasKey('accuracy', $this->getContentAsArray());
*/
    }
}
