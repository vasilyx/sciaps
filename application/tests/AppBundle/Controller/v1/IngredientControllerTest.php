<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Ingredient;
use Tests\Helpers\IntegrationTestBase;

class IngredientControllerTest extends IntegrationTestBase
{
    public function testGetIngredients()
    {
        $url = self::$router->generate('app_v1_ingredient_getingredients');
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testUpdateIngredient()
    {
        $arr = self::$entityManager->getRepository(Ingredient::class)->findAll();

        $requestData = [
            "commonName" => "Test commonName",
            "inciName" => "Test inciName",
            "innName" => "Test innName",
            "phEurName" => "Test phEurName",
            "chemicalIupacName" => "Test chemicalIupacName",
            "description" => "Test description",
            "casNumber" => "Test casNumber",
            "ecNumber" => "Test ecNumber",
            "cosingNumber" => "Test cosingNumber",
            "pubChemId" => rand(1,100)
        ];

        $url = self::$router->generate('app_v1_ingredient_updateingredient', ['id' => $arr[0]->getId()]);

        $this->sendRequest('PUT', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testGetIngredient()
    {
        $arr = self::$entityManager->getRepository(Ingredient::class)->findAll();

        $url = self::$router->generate('app_v1_ingredient_getingredient', ['id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_ingredient_getingredient', ['id' => 999999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testIngredientsEvidences()
    {
        $arr = self::$entityManager->getRepository(Ingredient::class)->findAll();

        $url = self::$router->generate('app_v1_ingredient_getingredientsevidences', ['ingredient_id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_ingredient_getingredientsevidences', ['ingredient_id' => 999999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode());
    }

    public function testIngredientsAttributes()
    {
        $arr = self::$entityManager->getRepository(Ingredient::class)->findAll();

        $url = self::$router->generate('app_v1_ingredient_getingredientsattributes', ['ingredient_id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_ingredient_getingredientsattributes', ['ingredient_id' => 999999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode());
    }

    public function testLinkedIngredients()
    {
        $arr = self::$entityManager->getRepository(Ingredient::class)->findAll();

        $url = self::$router->generate('app_v1_ingredient_getlinkedingredients', ['ingredient_id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_ingredient_getlinkedingredients', ['ingredient_id' => 999999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode());
    }

    public function testProductIngredients()
    {
        $arr = self::$entityManager->getRepository(Ingredient::class)->findAll();

        $url = self::$router->generate('app_v1_ingredient_getproductingredients', ['ingredient_id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_ingredient_getproductingredients', ['ingredient_id' => 999999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode());
    }

    public function testGetIngredientsFromBulkAction()
    {
        $url = self::$router->generate('app_v1_ingredient_getingredientsfrombulk', ['ingredientsText' => '']);
        $this->sendRequest('POST', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertArrayHasKey('ingredients', $this->getContentAsArray());
        $this->assertArrayHasKey('legend', $this->getContentAsArray());

        $sting = "INCI: Sodium Bicarbonate, Water (Aqua), Glycerin, Silica, Mentha Piperita (Peppermint) Oil, Prunus Spinosa Fruit Juice, Commiphora Myrrha Resin Extract, Krameria Triandra Root Extract, Aesculus Hippocastanum (Horse Chestnut) Bark Extract, Arum Maculatum Root Extract**, Sodium Chloride, Cyamopsis Tetragonoloba (Guar) Gum, Simmondsia Chinensis (Jojoba) Seed Oil, Alcohol, Esculin, Lactose, Flavour (Aroma)*, Limonene*, Linalool*. *from natural essential oils. **in highly diluted form.";
        $url = self::$router->generate('app_v1_ingredient_getingredientsfrombulk', ['ingredientsText' => $sting]);
        $this->sendRequest('POST', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertArrayHasKey('ingredients', $this->getContentAsArray());
        $this->assertArrayHasKey('legend', $this->getContentAsArray());
        $this->assertGreaterThan(0, count($this->getContentAsArray()['ingredients']));
        $this->assertGreaterThan(0, count($this->getContentAsArray()['legend']));
    }


}
