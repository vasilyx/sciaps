<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\Helpers\IntegrationTestBase;


class AuthControllerTest extends IntegrationTestBase
{
    public function testLogin()
    {
        $this->sendRequest('POST', self::$router->generate('app_v1_auth_login'),
            ['email' => 'admin@test.com', 'password' => 'qwa12']
        );


        $this->assertEquals(Response::HTTP_NOT_FOUND, self::$client->getResponse()->getStatusCode());

        $requestData = [
            'email' => 'vasilyx20@gmail.com',
            'password' => 'qwaszx10'
        ];

        $this->sendRequest('POST', self::$router->generate('app_v1_auth_login'), $requestData);


        $this->assertEquals(Response::HTTP_OK, self::$client->getResponse()->getStatusCode());
        $arr = json_decode(self::$client->getResponse()->getContent());
        $this->assertNotEmpty($arr->token);
        $this->assertNotEmpty($arr->user);

        self::$token = $arr->token;
    }

    public function testCheck()
    {
        $this->sendRequest('GET', self::$router->generate('app_v1_auth_check'));

        $this->assertEquals(Response::HTTP_OK, self::$client->getResponse()->getStatusCode());
    }

    public function testMe()
    {
        $this->sendRequest('GET', self::$router->generate('app_v1_auth_me'));

        $this->assertEquals(Response::HTTP_OK, self::$client->getResponse()->getStatusCode());
    }

    public function testForgotten()
    {
        $requestData = [
            'email' => 'vasilyx20@gmail.com',
        ];

        $this->sendRequest('POST', self::$router->generate('app_v1_auth_forgotten'), ['email' => 'notfoundasa@gmail.com']);
        $this->assertEquals(Response::HTTP_NOT_FOUND, self::$client->getResponse()->getStatusCode());

        $this->sendRequest('POST', self::$router->generate('app_v1_auth_forgotten'), $requestData);
        $this->assertEquals(Response::HTTP_OK, self::$client->getResponse()->getStatusCode());

        /**
         * @var User $user
         */
        $user = self::$entityManager->getRepository(User::class)->findOneByEmail($requestData['email']);

        $this->assertNotEmpty($user->getForgottenPasswordToken());
        $this->assertNotEmpty($user->getDatetimeForgottenPassword());
    }

    public function testLogout()
    {
        $this->sendRequest('POST', self::$router->generate('app_v1_auth_logout'));

        self::$token = '';

        $this->sendRequest('GET', self::$router->generate('app_v1_auth_check'));

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, self::$client->getResponse()->getStatusCode());

        $this->sendRequest('GET', self::$router->generate('app_v1_auth_me'));

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, self::$client->getResponse()->getStatusCode());

        $requestData = [
            'email' => 'vasilyx20@gmail.com',
            'password' => 'qwaszx10'
        ];

        $this->sendRequest('POST', self::$router->generate('app_v1_auth_login'), $requestData);
    }
}
