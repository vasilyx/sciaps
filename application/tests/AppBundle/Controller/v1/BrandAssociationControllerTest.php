<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Attribute;
use AppBundle\Entity\Brand;
use AppBundle\Entity\Evidence;
use AppBundle\Entity\BrandAttributes;
use AppBundle\Entity\BrandEvidences;
use AppBundle\Entity\Source;
use Tests\Helpers\IntegrationTestBase;

class BrandAssociationControllerTest extends IntegrationTestBase
{
    public function testAddBrandEvidence()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);
        $evidence = self::$entityManager->getRepository(Evidence::class)->findBy([], ['addedAt' => 'DESC']);
        $brands = self::$entityManager->getRepository(Brand::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Evidence::TYPE_ORANGE,
            "description" => "Test description for code" . rand(),
            "brands" => [$brands[0]->getId()]
        ];

        $url = self::$router->generate('app_v1_brandassociation_addbrandevidence', [
            'source_id' => $source[0]->getId(),
            'evidence_id' => $evidence[0]->getId()
        ]);

        $this->sendRequest('POST', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testUpdateBrandEvidence()
    {
        $brandEvidence = self::$entityManager->getRepository(BrandEvidences::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Evidence::TYPE_ORANGE,
            "description" => "Test update description for code" . rand(),
        ];

        $url = self::$router->generate('app_v1_brandassociation_updatebrandevidence', [
            'source_id' => $brandEvidence[0]->getSource()->getId(),
            'evidence_id' => $brandEvidence[0]->getEvidence()->getId(),
            'brand_id' => $brandEvidence[0]->getBrand()->getId()
        ]);
        $this->sendRequest('PUT', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();

        $this->assertEquals($body['statusCode'], $requestData['statusCode']);
    }

    public function testGetEvidences()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_brandassociation_getevidences', ['source_id' => $source[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testGetSourceEvidence()
    {
        $brandEvidence = self::$entityManager->getRepository(BrandEvidences::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_brandassociation_getsourceevidence', [
            'source_id' => $brandEvidence[0]->getSource()->getId(),
            'evidence_id' => $brandEvidence[0]->getEvidence()->getId(),
            'brand_id' => $brandEvidence[0]->getBrand()->getId()
        ]);

        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_brandassociation_getsourceevidence', [
            'source_id' => 9999999,
            'evidence_id' => 9999999,
            'brand_id' => 9999999
        ]);

        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testDeleteBrandEvidence()
    {
        $brandEvidence = self::$entityManager->getRepository(BrandEvidences::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_brandassociation_deletebrandevidence', [
            'source_id' => $brandEvidence[0]->getSource()->getId(),
            'evidence_id' => $brandEvidence[0]->getEvidence()->getId(),
            'brand_id' => $brandEvidence[0]->getBrand()->getId()
        ]);

        $this->sendRequest('DELETE', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testAddBrandAttribute()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);
        $attribute = self::$entityManager->getRepository(Attribute::class)->findBy([], ['addedAt' => 'DESC']);
        $brands = self::$entityManager->getRepository(Brand::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Attribute::TYPE_YES,
            "description" => "Test description for code" . rand(),
            "brands" => [$brands[0]->getId()]
        ];

        $url = self::$router->generate('app_v1_brandassociation_addbrandattribute', [
            'source_id' => $source[0]->getId(),
            'attribute_id' => $attribute[0]->getId()
        ]);

        $this->sendRequest('POST', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testUpdateBrandAttribute()
    {
        $brandAttribute = self::$entityManager->getRepository(BrandAttributes::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Evidence::TYPE_ORANGE,
            "description" => "Test update description for code" . rand(),
        ];

        $url = self::$router->generate('app_v1_brandassociation_updatebrandattribute', [
            'source_id' => $brandAttribute[0]->getSource()->getId(),
            'attribute_id' => $brandAttribute[0]->getAttribute()->getId(),
            'brand_id' => $brandAttribute[0]->getBrand()->getId()
        ]);
        $this->sendRequest('PUT', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();

        $this->assertEquals($body['statusCode'], $requestData['statusCode']);
    }

    public function testGetAttribute()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_brandassociation_getattribute', ['source_id' => $source[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testGetSourceAttribute()
    {
        $brandAttribute = self::$entityManager->getRepository(BrandAttributes::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_brandassociation_getsourceattribute', [
            'source_id' => $brandAttribute[0]->getSource()->getId(),
            'attribute_id' => $brandAttribute[0]->getAttribute()->getId(),
            'brand_id' => $brandAttribute[0]->getBrand()->getId()
        ]);

        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_brandassociation_getsourceattribute', [
            'source_id' => 9999999,
            'attribute_id' => 9999999,
            'brand_id' => 9999999
        ]);

        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testDeleteBrandAttribute()
    {
        $brandAttribute = self::$entityManager->getRepository(BrandAttributes::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_brandassociation_deletebrandattribute', [
            'source_id' => $brandAttribute[0]->getSource()->getId(),
            'attribute_id' => $brandAttribute[0]->getAttribute()->getId(),
            'brand_id' => $brandAttribute[0]->getBrand()->getId()
        ]);

        $this->sendRequest('DELETE', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }
}
