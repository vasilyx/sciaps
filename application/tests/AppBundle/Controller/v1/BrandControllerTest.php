<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Brand;
use Tests\Helpers\IntegrationTestBase;

class BrandControllerTest extends IntegrationTestBase
{
    public function testGetBrand()
    {
        $arr = self::$entityManager->getRepository(Brand::class)->findAll();

        $url = self::$router->generate('app_v1_brand_getbrand', ['id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $url = self::$router->generate('app_v1_brand_getbrand', ['id' => 99999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testGetBrands()
    {
        $url = self::$router->generate('app_v1_brand_getbrands');
        $this->sendRequest('GET', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));
    }
}
