<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Attribute;
use AppBundle\Entity\Evidence;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductEvidences;
use AppBundle\Entity\ProductAttributes;
use AppBundle\Entity\Source;
use Tests\Helpers\IntegrationTestBase;

class ProductAssociationControllerTest extends IntegrationTestBase
{
    public function testAddProductEvidence()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);
        $evidence = self::$entityManager->getRepository(Evidence::class)->findBy([], ['addedAt' => 'DESC']);
        $products = self::$entityManager->getRepository(Product::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Evidence::TYPE_ORANGE,
            "description" => "Test description for code" . rand(),
            "products" => [$products[0]->getId()]
        ];

        $url = self::$router->generate('app_v1_productassociation_addproductevidence', [
            'source_id' => $source[0]->getId(),
            'evidence_id' => $evidence[0]->getId()
        ]);

        $this->sendRequest('POST', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testUpdateProductEvidence()
    {
        $productEvidence = self::$entityManager->getRepository(ProductEvidences::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Evidence::TYPE_ORANGE,
            "description" => "Test update description for code" . rand(),
        ];

        $url = self::$router->generate('app_v1_productassociation_updateproductevidence', [
            'source_id' => $productEvidence[0]->getSource()->getId(),
            'evidence_id' => $productEvidence[0]->getEvidence()->getId(),
            'product_id' => $productEvidence[0]->getProduct()->getId()
        ]);
        $this->sendRequest('PUT', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();

        $this->assertEquals($body['statusCode'], $requestData['statusCode']);
    }

    public function testGetEvidences()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_productassociation_getevidences', ['source_id' => $source[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testGetSourceEvidence()
    {
        $productEvidence = self::$entityManager->getRepository(ProductEvidences::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_productassociation_getsourceevidence', [
            'source_id' => $productEvidence[0]->getSource()->getId(),
            'evidence_id' => $productEvidence[0]->getEvidence()->getId(),
            'product_id' => $productEvidence[0]->getProduct()->getId()
        ]);

        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_productassociation_getsourceevidence', [
            'source_id' => 9999999,
            'evidence_id' => 9999999,
            'product_id' => 9999999
        ]);

        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testDeleteProductEvidence()
    {
        $productEvidence = self::$entityManager->getRepository(ProductEvidences::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_productassociation_deleteproductevidence', [
            'source_id' => $productEvidence[0]->getSource()->getId(),
            'evidence_id' => $productEvidence[0]->getEvidence()->getId(),
            'product_id' => $productEvidence[0]->getProduct()->getId()
        ]);

        $this->sendRequest('DELETE', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testAddProductAttribute()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);
        $attribute = self::$entityManager->getRepository(Attribute::class)->findBy([], ['addedAt' => 'DESC']);
        $products = self::$entityManager->getRepository(Product::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Attribute::TYPE_YES,
            "description" => "Test description for code" . rand(),
            "products" => [$products[0]->getId()]
        ];

        $url = self::$router->generate('app_v1_productassociation_addproductattribute', [
            'source_id' => $source[0]->getId(),
            'attribute_id' => $attribute[0]->getId()
        ]);

        $this->sendRequest('POST', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testUpdateProductAttribute()
    {
        $productAttribute = self::$entityManager->getRepository(ProductAttributes::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Evidence::TYPE_ORANGE,
            "description" => "Test update description for code" . rand(),
        ];

        $url = self::$router->generate('app_v1_productassociation_updateproductattribute', [
            'source_id' => $productAttribute[0]->getSource()->getId(),
            'attribute_id' => $productAttribute[0]->getAttribute()->getId(),
            'product_id' => $productAttribute[0]->getProduct()->getId()
        ]);
        $this->sendRequest('PUT', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();

        $this->assertEquals($body['statusCode'], $requestData['statusCode']);
    }

    public function testGetAttribute()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_productassociation_getattribute', ['source_id' => $source[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testGetSourceAttribute()
    {
        $productAttribute = self::$entityManager->getRepository(ProductAttributes::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_productassociation_getsourceattribute', [
            'source_id' => $productAttribute[0]->getSource()->getId(),
            'attribute_id' => $productAttribute[0]->getAttribute()->getId(),
            'product_id' => $productAttribute[0]->getProduct()->getId()
        ]);

        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_productassociation_getsourceattribute', [
            'source_id' => 9999999,
            'attribute_id' => 9999999,
            'product_id' => 9999999
        ]);

        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testDeleteProductAttribute()
    {
        $productAttribute = self::$entityManager->getRepository(ProductAttributes::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_productassociation_deleteproductattribute', [
            'source_id' => $productAttribute[0]->getSource()->getId(),
            'attribute_id' => $productAttribute[0]->getAttribute()->getId(),
            'product_id' => $productAttribute[0]->getProduct()->getId()
        ]);

        $this->sendRequest('DELETE', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }
}
