<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Attribute;
use AppBundle\Entity\Evidence;
use AppBundle\Entity\Ingredient;
use AppBundle\Entity\IngredientsAttributes;
use AppBundle\Entity\IngredientsEvidences;
use AppBundle\Entity\Source;
use Tests\Helpers\IntegrationTestBase;

class IngredientAssociationControllerTest extends IntegrationTestBase
{
    public function testAddIngredientEvidence()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);
        $evidence = self::$entityManager->getRepository(Evidence::class)->findBy([], ['addedAt' => 'DESC']);
        $ingredients = self::$entityManager->getRepository(Ingredient::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Evidence::TYPE_ORANGE,
            "description" => "Test description for code" . rand(),
            "ingredients" => [$ingredients[0]->getId()]
        ];

        $url = self::$router->generate('app_v1_ingredientassociation_addingredientevidence', [
            'source_id' => $source[0]->getId(),
            'evidence_id' => $evidence[0]->getId()
        ]);

        $this->sendRequest('POST', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testUpdateIngredientEvidence()
    {
        $ingredientEvidence = self::$entityManager->getRepository(IngredientsEvidences::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Evidence::TYPE_ORANGE,
            "description" => "Test update description for code" . rand(),
        ];

        $url = self::$router->generate('app_v1_ingredientassociation_updateingredientevidence', [
            'source_id' => $ingredientEvidence[0]->getSource()->getId(),
            'evidence_id' => $ingredientEvidence[0]->getEvidence()->getId(),
            'ingredient_id' => $ingredientEvidence[0]->getIngredient()->getId()
        ]);
        $this->sendRequest('PUT', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();

        $this->assertEquals($body['statusCode'], $requestData['statusCode']);
    }

    public function testGetEvidences()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_ingredientassociation_getevidences', ['source_id' => $source[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testGetSourceEvidence()
    {
        $ingredientEvidence = self::$entityManager->getRepository(IngredientsEvidences::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_ingredientassociation_getsourceevidence', [
            'source_id' => $ingredientEvidence[0]->getSource()->getId(),
            'evidence_id' => $ingredientEvidence[0]->getEvidence()->getId(),
            'ingredient_id' => $ingredientEvidence[0]->getIngredient()->getId()
        ]);

        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_ingredientassociation_getsourceevidence', [
            'source_id' => 9999999,
            'evidence_id' => 9999999,
            'ingredient_id' => 9999999
        ]);

        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testDeleteIngredientEvidence()
    {
        $ingredientEvidence = self::$entityManager->getRepository(IngredientsEvidences::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_ingredientassociation_deleteingredientevidence', [
            'source_id' => $ingredientEvidence[0]->getSource()->getId(),
            'evidence_id' => $ingredientEvidence[0]->getEvidence()->getId(),
            'ingredient_id' => $ingredientEvidence[0]->getIngredient()->getId()
        ]);

        $this->sendRequest('DELETE', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testAddIngredientAttribute()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);
        $attribute = self::$entityManager->getRepository(Attribute::class)->findBy([], ['addedAt' => 'DESC']);
        $ingredients = self::$entityManager->getRepository(Ingredient::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Attribute::TYPE_YES,
            "description" => "Test description for code" . rand(),
            "ingredients" => [$ingredients[0]->getId()]
        ];

        $url = self::$router->generate('app_v1_ingredientassociation_addingredientattribute', [
            'source_id' => $source[0]->getId(),
            'attribute_id' => $attribute[0]->getId()
        ]);

        $this->sendRequest('POST', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testUpdateIngredientAttribute()
    {
        $ingredientAttribute = self::$entityManager->getRepository(IngredientsAttributes::class)->findBy([], ['addedAt' => 'DESC']);

        $requestData = [
            "statusCode" => Evidence::TYPE_ORANGE,
            "description" => "Test update description for code" . rand(),
        ];

        $url = self::$router->generate('app_v1_ingredientassociation_updateingredientattribute', [
            'source_id' => $ingredientAttribute[0]->getSource()->getId(),
            'attribute_id' => $ingredientAttribute[0]->getAttribute()->getId(),
            'ingredient_id' => $ingredientAttribute[0]->getIngredient()->getId()
        ]);
        $this->sendRequest('PUT', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $body = $this->getContentAsArray();

        $this->assertEquals($body['statusCode'], $requestData['statusCode']);
    }

    public function testGetAttribute()
    {
        $source = self::$entityManager->getRepository(Source::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_ingredientassociation_getattribute', ['source_id' => $source[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }

    public function testGetSourceAttribute()
    {
        $ingredientAttribute = self::$entityManager->getRepository(IngredientsAttributes::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_ingredientassociation_getsourceattribute', [
            'source_id' => $ingredientAttribute[0]->getSource()->getId(),
            'attribute_id' => $ingredientAttribute[0]->getAttribute()->getId(),
            'ingredient_id' => $ingredientAttribute[0]->getIngredient()->getId()
        ]);

        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $url = self::$router->generate('app_v1_ingredientassociation_getsourceattribute', [
            'source_id' => 9999999,
            'attribute_id' => 9999999,
            'ingredient_id' => 9999999
        ]);

        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testDeleteIngredientAttribute()
    {
        $ingredientAttribute = self::$entityManager->getRepository(IngredientsAttributes::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_ingredientassociation_deleteingredientattribute', [
            'source_id' => $ingredientAttribute[0]->getSource()->getId(),
            'attribute_id' => $ingredientAttribute[0]->getAttribute()->getId(),
            'ingredient_id' => $ingredientAttribute[0]->getIngredient()->getId()
        ]);

        $this->sendRequest('DELETE', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }
}
