<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Evidence;
use Tests\Helpers\IntegrationTestBase;

class EvidenceControllerTest extends IntegrationTestBase
{
    public function testAddEvidence()
    {
        $arr = self::$entityManager->getRepository(Evidence::class)->findAll();

        if (!empty($arr)) {
            $parent = $arr[0]->getParent();
        } else {
            $parent = null;
        }

        $symbol = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyzQWERTYUIOPLKJHGFDSAZXCVBNM", 4)), 0, 4);

        $requestData = [
            "parent" => $parent,
            "defaultName" => "Test Default Name",
            "defaultDescription" => "Test Default Description",
            "symbol" => $symbol
        ];

        $url = self::$router->generate('app_v1_evidence_addevidence');
        $this->sendRequest('POST', $url, $requestData);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertArrayHasKey('id', $this->getContentAsArray()); // created ID
    }

    public function testUpdateEvidence()
    {
        $arr = self::$entityManager->getRepository(Evidence::class)->findAll();

        $symbol = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyzQWERTYUIOPLKJHGFDSAZXCVBNM", 4)), 0, 4);

        $requestData = [
            "parent" => $arr[0]->getParent(),
            "defaultName" => "Test Default Name",
            "defaultDescription" => "Test Default Description",
            "symbol" => $symbol
        ];

        $url = self::$router->generate('app_v1_evidence_updateevidence', ['id' => $arr[0]->getId()]);

        $this->sendRequest('PUT', $url, $requestData);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());

        $body = $this->getContentAsArray();

        $this->assertEquals($body['defaultName'], $requestData['defaultName']); // created ID
    }

    public function testGetEvidence()
    {
        $arr = self::$entityManager->getRepository(Evidence::class)->findAll();

        $url = self::$router->generate('app_v1_evidence_getevidence', ['id' => $arr[0]->getId()]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $url = self::$router->generate('app_v1_evidence_getevidence', ['id' => 999999]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(404, self::$client->getResponse()->getStatusCode()); // created ID
    }

    public function testGetEvidences()
    {
        $url = self::$router->generate('app_v1_evidence_getevidences');
        $this->sendRequest('GET', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));
    }

    public function testDeleteEvidence()
    {
        $arr = self::$entityManager->getRepository(Evidence::class)->findBy([], ['addedAt' => 'DESC']);

        $url = self::$router->generate('app_v1_evidence_deleteevidence', ['id' => $arr[0]->getId()]);
        $this->sendRequest('DELETE', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }
}
