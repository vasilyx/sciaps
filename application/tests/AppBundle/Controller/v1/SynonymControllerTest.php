<?php

namespace Tests\AppBundle\Controller\v1;

use Tests\Helpers\IntegrationTestBase;

class SynonymControllerTest extends IntegrationTestBase
{
    public function testGetSynonyms()
    {
        $url = self::$router->generate('app_v1_synonym_getsynonyms');
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }
}
