<?php

namespace Tests\AppBundle\Controller;

use Tests\Helpers\IntegrationTestBase;

class DashboardControllerTestControllerTest extends IntegrationTestBase
{
     public function testGetProduct()
    {
        $url = self::$router->generate('app_v1_dashboard_getdashboard');
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }
}
