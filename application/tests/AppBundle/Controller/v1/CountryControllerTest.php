<?php

namespace Tests\AppBundle\Controller;

use Tests\Helpers\IntegrationTestBase;

class CountryControllerTest extends IntegrationTestBase
{
    public $created;

    public function testGetCompanies()
    {
        $url = self::$router->generate('app_v1_country_getcountries');
        $this->sendRequest('GET', $url);

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));
    }

    public function testGetCompaniesByParams()
    {
        $url = self::$router->generate('app_v1_country_getcountries', ['_limit' => 2, 'id_ne' => 2]);
        $this->sendRequest('GET', $url);
        $this->assertTrue(self::$client->getResponse()->isSuccessful());
        $this->assertEquals(2, count($this->getContentAsArray()));

        $this->assertTrue(self::$client->getResponse()->headers->has('X-Total-Count'));

        $url = self::$router->generate('app_v1_country_getcountries', ['_fields' => 'id, name']);
        $this->sendRequest('GET', $url);

        $this->assertEquals(2, count($this->getContentAsArray()[0])); // only two fields in row

        $this->assertTrue(self::$client->getResponse()->isSuccessful());



        $url = self::$router->generate('app_v1_country_getcountries', ['q' => $this->getContentAsArray()[0]['name']]);
        $this->sendRequest('GET', $url);
        $this->assertEquals(1, count($this->getContentAsArray()));

        $this->assertTrue(self::$client->getResponse()->isSuccessful());
    }
}
