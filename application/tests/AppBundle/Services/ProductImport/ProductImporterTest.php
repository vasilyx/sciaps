<?php

namespace Tests\AppBundle\Services\ProductImport;

use AppBundle\Entity\User;
use AppBundle\Managers\BarcodeManager;
use AppBundle\Managers\BrandManager;
use AppBundle\Managers\IngestionReportManager;
use AppBundle\Managers\IngredientLegendManager;
use AppBundle\Managers\IngredientManager;
use AppBundle\Managers\IngredientUnfilteredManager;
use AppBundle\Managers\ProductProviderManager;
use AppBundle\Services\ProductImport\CsvDataReader;
use AppBundle\Services\ProductImport\IngredientParser;
use AppBundle\Services\ProductImport\ProductImporter;
use Tests\Helpers\FunctionalTestBase;

class ProductImporterTest extends FunctionalTestBase
{
    /*
     * This MUST COME FIRST! Something wrong with the entity manager
     */
    public function testImportChemD()
    {
        $this->markTestSkipped('Not included - causes memory error');

        $mappingData = [
            'name' => 'Product Title',
            'brand' => 'Brand Name',
            'barcode' => 'Barcode',
            'remote_id' => 'Product Code',
            'description' => 'Description'
        ];

        /* @var IngredientParser|\PHPUnit_Framework_MockObject_MockObject $parser */
        $parser = $this->getMockBuilder('\AppBundle\Services\ProductImport\IngredientParser')->disableOriginalConstructor()->getMock();

        $ingredientManager = new IngredientUnfilteredManager(self::$entityManager, new IngredientManager(self::$entityManager));
        $ingestionReportManager = new IngestionReportManager(self::$entityManager);
        $ingredientLegendManager = new IngredientLegendManager(self::$entityManager);
        $productProvider = new ProductProviderManager(self::$entityManager);

        $reader = new CsvDataReader(new \SplFileObject($this->getFilePath('ChemD-1.csv')));
        $productImporter = new ProductImporter(self::$entityManager, $reader, $parser, $ingredientManager, $ingredientLegendManager, $productProvider, new BarcodeManager(self::$entityManager), new BrandManager(self::$entityManager));
        $user = self::$entityManager->getRepository(User::class)->findAll();
        $productImporter
            ->setDataMapping($mappingData);

        $ingestionReport = $ingestionReportManager->createOnDataImport($user[0], $user[0]->getCompany()->getProductProvider());

        // Products don't exist - inserts 5
        $report = $productImporter->importToDb($user[0]->getCompany()->getProductProvider(), $ingestionReport, true, true);
        $this->assertGreaterThan(0, $report['products']['inserted']);
    }
}
