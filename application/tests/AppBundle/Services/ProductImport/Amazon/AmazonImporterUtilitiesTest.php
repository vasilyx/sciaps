<?php

namespace AppBundle\Tests\Services\ProductImport\Amazon;

use AppBundle\Services\ProductImport\Amazon\AmazonImporterUtilities;
use PHPUnit\Framework\TestCase;

/**
 * Class AmazonImporterTest
 * Amazon API is rate throttled to 1 / second so force unit tests to wait
 *
 * @package AppBundle\Tests\Services
 */
class AmazonImporterUtilitiesTest extends TestCase
{

    public $config = [
        'countryCode' => 'co.uk',
        'secretAssocAccessKey' => 'AKIAIWCSECMBHMU7YMQA',
        'secretAssocSecretKey' => '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',
        'secretAssocTag' => 'skin08c-21'
    ];

    public function testSearchByProductName()
    {
        $this->markTestSkipped('Amazon API test currently disabled');
        sleep(1);
        $importer = new AmazonImporterUtilities($this->config);

        $product = [
            'name' => 'Moisturiser for men',
            'brand' => 'Nivea',
            'manufacturer' => 'NiveaManu',
            'volume' => '200',
            'volume_units' => 'ml'
        ];

        $result = $importer->searchByProductName($product, 1);

        $this->assertArrayHasKey('Item', $result);
        $this->assertCount(1, $result['Item']);
        $this->assertArrayHasKey('TotalResults', $result);
        $this->assertArrayHasKey('TotalPages', $result);
    }

    public function testSearchByBarcode()
    {
        $this->markTestSkipped('Amazon API test currently disabled');
        sleep(1);
        $importer = new AmazonImporterUtilities($this->config);

        $barcode = '721865636660';

        $result = $importer->searchByBarcode($barcode, 'UPC', 1);

        $this->assertArrayHasKey('Item', $result);
        $this->assertCount(1, $result['Item']);
    }
}