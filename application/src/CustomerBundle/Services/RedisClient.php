<?php

namespace CustomerBundle\Services;

use Doctrine\Common\Cache\PredisCache;
use Predis\Client as PredisClient;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class RedisClient
{
    /**
     * Predis Client intitialised
     *
     * @var PredisClient
     */
    private $client;


    public function __construct(Container $container)
    {
        $parameters = [
            'scheme' => $container->getParameter('redis_scheme'),
            'host' =>  $container->getParameter('redis_host'),
            'port' => $container->getParameter('redis_port'),
        ];

        $dsn = "$parameters[scheme]://$parameters[host]:$parameters[port]";

        $prefix = $container->getParameter('redis_prefix');
        $password = $container->getParameter('redis_password');

        if (!empty($password)) $dsn .= "/?password=$password";

        $args = [];

        if (!empty($prefix)) $args['prefix'] = $prefix;

        $this->client = new PredisClient($parameters, $args);
    }

    public function getRedisClient()
    {
        return $this->client;
    }

    public function set($key, $value)
    {
        return $this->client->set($key, $value);
    }

    public function get($key)
    {
        if ($this->client->exists($key)) {
            return $this->client->get($key);
        }

        return false;
    }

    public function unset($key)
    {
        if ($this->client->del($key)) return true;

        return false;
    }
}
