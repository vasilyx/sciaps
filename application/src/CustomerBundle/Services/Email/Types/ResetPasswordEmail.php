<?php

namespace CustomerBundle\Services\Email\Types;


class ResetPasswordEmail extends \AppBundle\Services\Email\Types\Email
{
    public function __construct(string $email = null, $params = [])
    {
        parent::__construct($email);

        $body = "Hi, ";
        $body .= "we recently received a request to reset your password. ";
        $body .= "please click the link below to do so, ";
        $body .= "if this wasn’t you, please let us know immediately.";

        $this->setDynamicContent([
            'subject' => 'Reset password',
            'header' => 'Password Reset',
            'text' => $body,
            'c2a_link' => '/auth/password/reset-confirm?uid='. $params['uid'] . "&token=" . $params['confirmEmailToken'] . "&auth=reset-confirm",
            'c2a_button' => 'Reset Password'
        ]);

    }
}
