<?php
namespace CustomerBundle\Services\Email\Types;


class WelcomeEmail extends \AppBundle\Services\Email\Types\Email
{
    public function __construct(string $email = null, $params = [])
    {
        parent::__construct($email);

        $this->setDynamicContent([
            'subject' => 'Welcome to SkinNinja',
            'header' => 'Activate Account',
            'text' => 'It looks like you requested an account. Please click the link below to do so.'
        ]);
    }

}
