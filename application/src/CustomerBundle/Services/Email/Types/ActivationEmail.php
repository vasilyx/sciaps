<?php

namespace CustomerBundle\Services\Email\Types;

class ActivationEmail extends \AppBundle\Services\Email\Types\Email
{
    public function __construct(string $email = null, $params = [])
    {
        parent::__construct($email);

        $this->setDynamicContent([
            'subject' => 'Active Your Account',
            'header' => 'Account Activation',
            'text' => 'It looks like you requested an account. Please click the link below to do so.',
            'c2a_link' => '/auth/activation/' . $params['uid'] . "/" . $params['confirmEmailToken'],
            'c2a_button' => 'Activate My Account'
        ]);
    }
}
