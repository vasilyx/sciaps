<?php

namespace CustomerBundle\Services\Email\Types   ;


class ResetPassConfirmEmail extends \AppBundle\Services\Email\Types\Email
{
    public function __construct(string $email = null, $params = [])
    {
        parent::__construct($email);

        $body = "Hi,<br>";
        $body .= "<p>Your password has been successfully changed!</p>";
        $body .= "<p>";
        $body .= "If this wasn’t you, please let us know immediately.<br>";
        $body .= "You can email us at <a href=\"mailto:support@skinninja.com\">support@skinninja.com</a>";
        $body .= "</p><br>";
        $body .= "<p>";
        $body .= "skinninja.com";
        $body .= "</p>";

        $this->setDynamicContent([
            'subject' => 'Password successfully changed',
            'header' => 'Password successfully changed',
            'text' => $body,
            'c2a_link' => '/auth/reset/confirm/'. $params['uid'] . "/" . $params['confirmEmailToken'],
            'c2a_button' => 'Successful'
        ]);

    }
}
