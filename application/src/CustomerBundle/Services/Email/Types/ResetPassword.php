<?php

namespace CustomerBundle\Services\Email\Types;

/**
 * Class ResetPassword
 * @package CustomerBundle\Entity\Email
 */
class ResetPassword
{
    private $uid;

    private $token;

    private $new_password1;

    private $new_password2;

    public function __construct()
    {
    }

    /**
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param integer $uid
     */
    public function setUid($uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token): void
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getNewPassword1()
    {
        return $this->new_password1;
    }

    /**
     * @param string $new_password1
     */
    public function setNewPassword1($new_password1): void
    {
        $this->new_password1 = $new_password1;
    }

    /**
     * @return string
     */
    public function getNewPassword2()
    {
        return $this->new_password2;
    }

    /**
     * @param string $new_password2
     */
    public function setNewPassword2($new_password2): void
    {
        $this->new_password2 = $new_password2;
    }


}
