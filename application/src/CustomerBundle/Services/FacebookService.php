<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 26/09/2018
 * Time: 15:56
 */

namespace CustomerBundle\Services;

use Facebook\Authentication\AccessToken;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Facebook\GraphNodes\GraphUser;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class FacebookService
{
    private $fb;

    private $helper;

    /**
     * @return \Facebook\Helpers\FacebookRedirectLoginHelper
     */
    public function getHelper(): \Facebook\Helpers\FacebookRedirectLoginHelper
    {
        return $this->helper;
    }

    private $permissions;
    /**
     * @var Container
     */
    private $container;

    private $appId;

    private $accessToken;

    /**
     * FacebookService constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;

        $params = [
            'app_id' => $this->container->getParameter('fb_app_id'),
            'app_secret' => $this->container->getParameter('fb_app_secret')
        ];

        $this->appId = $this->container->getParameter('fb_app_id');

        try {
            $fb = new Facebook($params);
        } catch (FacebookSDKException $exception) {
            exit;
        }

        $this->fb = $fb;
        $this->helper = $fb->getRedirectLoginHelper();
        $this->permissions = ['email', 'public_profile'];


    }

    public function getLoginUrl()
    {
        $url = $this->helper->getLoginUrl(
            $this->container->getParameter('domain_url') . 'customer/auth/login/fb',
            $this->permissions);

        return $url;
    }

    public function getToken(Request $request)
    {
        $errors = [];

        try {
            $accessToken = $this->helper->getAccessToken();
        } catch (FacebookResponseException $e) {
            // When Graph returns an error
            $errors = ['Graph' => $e->getMessage()];
        } catch (FacebookSDKException $e) {
            // When validation fails or other local issues
            $errors = ['SDK' => $e->getMessage()];
        }

        if (!isset($accessToken)) {
            if ($this->helper->getError()) {
                $errors = ['Auth' => [
                    "Error" => $this->helper->getError(),
                    "Code" => $this->helper->getErrorCode(),
                    "Reason" => $this->helper->getErrorReason(),
                    "Description" => $this->helper->getErrorDescription()
                ]];
            } else {
                $errors = ['FBCallback' => [
                    "Description" => 'Invalid callback parameters'
                ]];
            }
        }

        if (!is_null($errors)) return ['errors' => $errors];

        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $this->fb->getOAuth2Client();

        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);

        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId($this->appId); // Replace {app-id} with your app id
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();

        if (!$accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
                exit;
            }
        }

        return $accessToken->getValue();
    }

    public function query(string $query = '/me?fields=id,name,email'): GraphUser
    {
        try {
            $response = $this->fb->get($query, $this->accessToken);
        } catch (FacebookResponseException $e) {
            throw new AuthenticationException('Graph returned an error: ' . $e->getMessage());
        } catch (FacebookSDKException $e) {
            throw new AuthenticationException('Facebook SDK returned an error: ' . $e->getMessage());
        }
        return $response->getGraphUser();
    }

}
