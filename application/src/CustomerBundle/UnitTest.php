<?php
namespace CustomerBundle;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Router;

class UnitTest extends WebTestCase
{
    /** @var  Client $client */
    protected static $client;

    /** @var  ContainerInterface $container */
    protected static $container;

    /** @var  Router $router */
    protected static $router;

    protected static $badFormSubmissionErrorCode;

    public static function setUpBeforeClass()
    {
        self::$client = static::createClient();
        self::$container = self::$client->getContainer();
        self::$router =  self::$container->get('router');
        self::$badFormSubmissionErrorCode = 422;
    }
}
