<?php

namespace CustomerBundle\Controller\v1;

use AppBundle\Controller\AbstractApiController;
use CustomerBundle\Entity\User;
use CustomerBundle\Form\UserCreationType;
use AppBundle\Managers\DataTableManager;
use AppBundle\Response\BaseResponse;
use CustomerBundle\Manager\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;

use AppBundle\Traits\ContainerHelpers;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;

use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/customer/users")
 */
class UserController extends AbstractApiController
{
    use ContainerHelpers;

    private $userManager;
    private $dataTableManager;
    private $em;

    public function __construct(UserManager $userManager, EntityManagerInterface $em, DataTableManager $dataTableManager)
    {
        $this->userManager = $userManager;
        $this->dataTableManager = $dataTableManager;
        $this->em = $em;
    }

    /**
     * @SWG\Post(
     *      summary="Register a new user",
     *      tags={"Users"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="Registers a new user in the system from a traditional signup",
     *          in="body",
     *          type="json",
     *          @Model(type=\CustomerBundle\Form\UserCreationType::class)
     *      ),
     * @SWG\Response(response=200, description="A user was created and the user object is returned.")
     * )
     * @Route("", methods={"POST"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function addUserAction(Request $request)
    {
        $form = $this->createForm(UserCreationType::class, new User());

        // Data from existing app manually configured
        $data = [
            'email' => $request->request->get('email'),
            'password' => $request->request->get('password')
        ];

        $form->submit($data);

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            $user = $this->userManager->createUser($form->getData());
            /* @todo Configure notifications for opt in / opt out
             * $request->request->get('notify_by_email_no_trans')
             * @todo configure install config with code
             * $request->request->get('code')*/

            $response = [
                'code' => 0,
                'detail' => 'Email sent',
                'client_id' => $user->getId(),
                'user' => $user
            ];
            return $response;
        }
    }

    /**
     * Get User
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="Users")
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getUserAction(User $user)
    {
        return $user;
    }
}
