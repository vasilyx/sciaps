<?php

namespace CustomerBundle\Controller\v1;

use AppBundle\Controller\AbstractApiController;
use CustomerBundle\Entity\Email\ResetPassword;
use CustomerBundle\Entity\User;
use CustomerBundle\Form\ResetPasswordConfirmType;
use CustomerBundle\Manager\UserManager;
use CustomerBundle\Security\ApiKeyUserProvider;
use CustomerBundle\Services\FacebookService;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RequestContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Route("/api/v1/customer/auth")
 */
class AuthController extends AbstractApiController
{
    private $apiKeyUserProvider;
    private $userManager;
    /**
     * @var FacebookService
     */
    private $facebookService;

    public function __construct(ApiKeyUserProvider $apiKeyUserProvider,
                                UserManager $userManager, FacebookService $facebookService)
    {
        $this->apiKeyUserProvider = $apiKeyUserProvider;
        $this->userManager = $userManager;
        $this->facebookService = $facebookService;
    }

    /**
     * @SWG\Post(
     *      summary="Login. Returns Auth token on success in element 'token' of the array ",
     *      tags={"auth"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="client_id", type="integer", example="36"),
     *              @SWG\Property(property="grant_type", type="string", example="password"),
     *              @SWG\Property(property="username", type="string", example="kop_and@tut.by"),
     *              @SWG\Property(property="password", type="string", example="zxc123"),
     *              @SWG\Property(property="refresh_token", type="string", example="asdzxc123dsa4dSA")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Wrong password"),
     *      @SWG\Response(response=404, description="Wrong username"),
     * )
     * @Route("/token", methods={"POST"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function loginAction(Request $request, UserManager $userManager)
    {
        $access_token = "";

        if ($request->get('username') && $request->get('password') && $request->get('grant_type') == 'password') {

            $access_token = $userManager->login(
                $request->request->get('client_id'),
                $request->request->get('username'),
                $request->request->get('password'),
                $request->request->get('grant_type')
            );

        } elseif ($request->get('refresh_token') && $request->get('grant_type') == 'refresh_token') {

            $access_token = $userManager->loginRefresh(
                $request->request->get('client_id'),
                $request->request->get('refresh_token'),
                $request->request->get('grant_type')
            );

            if (!$access_token) {
                return ['message' => 'Can not update access_token!'];
            }
        }

        $user = $this->apiKeyUserProvider->loadUserByClientId($request->get('client_id'), 'user_id');

        $refresh_token = $this->apiKeyUserProvider->getRefreshTokenByClientId($request->get('client_id'), 'refresh_token');

        $request->getSession()->set('currentUser', $user->getId());

        return [
            'expires_in' => $userManager::REDIS_EXPIRE_RESOLUTION,
            'scope' => $userManager::SCOPE,
            'token_type' => $userManager::TOKEN_TYPE,
            'access_token' => $access_token,
            'refresh_token' => $refresh_token
        ];
    }

    /**
     * @SWG\Post(
     *      summary="Allows validating access or refresh token.",
     *      tags={"auth"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="client_id", type="integer", example="36"),
     *              @SWG\Property(property="token", type="string", example="asdzxc123dsa4dSA")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     * )
     * @Route("/validate_token", methods={"POST"})
     */
    public function validateTokenAction(Request $request)
    {
        if ($this->userManager->isValidateToken($request)) {
            return ['valid' => 'true'];
        } else {
            return ['valid' => 'false'];
        }
    }

    /**
     * @SWG\Post(
     *      summary="Allows revoking access or refresh token.",
     *      tags={"auth"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="client_id", type="integer", example="36"),
     *              @SWG\Property(property="token", type="string", example="asdzxc123dsa4dSA")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     * )
     * @Route("/revoke_token", methods={"POST"})
     */
    public function revokeTokenAction(Request $request)
    {
        if ($this->userManager->revokeToken($request)) {
            return new Response(Response::HTTP_OK);
        } else {
            return new Response(Response::HTTP_FORBIDDEN);
        }
    }

    /**
     *  Return logged User
     * @SWG\Response(response=200, description="Success"),
     * @SWG\Response(response=401, description="Returned when the user is not authorized"),
     * @SWG\Tag(name="auth"),
     *
     * @Rest\View(serializerGroups={"current"})
     * @Route("/me", methods={"GET"})
     */
    public function meAction(Request $request)
    {
        return $this->userManager->loadByToken($request->headers->get('token'));
    }

    /**
     * @SWG\Post(
     *      summary="Reset password. Enter your e-mail and check then email.",
     *      tags={"auth"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="email", type="string", example="kop_and@mail.ru")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     * )
     * @Route("/password/reset", methods={"POST"})
     */
    public function resetPasswordAction(Request $request)
    {
        $user = $this->userManager->getUserByEmail(
            $request->request->get('email')
        );

        if ($user) {
            $this->userManager->resetPasswordByEmail($user);
        }
        return ['detail' => 'Reset password url sent.'];
    }

    /**
     *  Return 200 response if user(uid) exist in system with token(token)

     * @SWG\Response(response=200, description="Success"),
     * @SWG\Response(response=404, description="Not Found"),
     * @SWG\Tag(name="auth"),
     * @Route("/password/reset-confirm", methods={"GET"})
     * @return Response
     */
    public function checkTokenFromEmailAction(Request $request)
    {
        if ($this->userManager->checkTokenFromEmail($request) || !$request->get('uid')) {
            $response = new Response(Response::HTTP_OK);
        } else {
            $response = new Response(Response::HTTP_NOT_FOUND);
        }

        return $response;
    }

    /**
     * @SWG\PUT(
     *      summary="Reset password. Enter your e-mail and check then email.",
     *      tags={"auth"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\CustomerBundle\Form\ResetPasswordConfirmType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found"),
     * )
     * @Route("/password/reset/confirm", methods={"PUT"})
     */
    public function resetPasswordConfirmAction(Request $request)
    {
        $form = $this->createForm(ResetPasswordConfirmType::class, new ResetPassword());
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            if ($this->userManager->changePassword($form->getData())) {
                return ['detail' => 'Password changed.'];
            }
        }
    }

    /**
     * @SWG\Post(
     *      summary="Allows retrieving client ID from the e-mail and password pair.",
     *      tags={"auth"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="email", type="string", example="kop_and@mail.ru"),
     *              @SWG\Property(property="password", type="string", example="zxc123")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/credentials", methods={"POST"})
     */
    public function getClientIdAction(Request $request)
    {
        $user = $this->userManager->getUserByEmail(
            $request->request->get('email')
        );

        $client_id = $this->userManager->getClientId($request);

        if ($user && $client_id) {
            return ['client_id' => $client_id];
        }
        return ['message' => 'User with given credentials does not exist'];
    }

    /**
     * Facebook login controller.
     *
     * @SWG\Response(response=200, description="Success"),
     * @SWG\Tag(name="Auth"),
     * @Route("/login/fb", methods={"GET"})
     */
    public function facebookLoginAction(Request $request)
    {
        $response = $this->facebookService->getToken($request);
        if (!is_null($response['errors'])) return $response['errors'];
        // We've got a token.. so they are logged into facebook.
        // Are they currently a user?
        $fbUser = $this->facebookService->query();
        if (!$fbUser) return false;
        $user = $this->userManager->getUserByFacebookId($fbUser->getId());
        if (!$user) {
            $user = $this->userManager->createUserFromFacebook($fbUser);
        }
        // User exists, log them in
        $request->getSession()->set('currentUser', $user->getId());
        $request->getSession()->set('fb_access_token', $response);
        return ['fb_access_token' => $response, 'user' => $user, 'client_id' => $user->getId()];
    }

    /**
     * @SWG\Post(
     *      summary="Allows changing current user's password.",
     *      tags={"auth"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="old_password", type="string", example="zxc123"),
     *              @SWG\Property(property="new_password1", type="string", example="zxc"),
     *              @SWG\Property(property="new_password2", type="string", example="zxc"),
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/password/change", methods={"POST"})
     */
    public function changeUserPasswordAction(Request $request)
    {
        $user_id = $request->getSession()->get('currentUser');

        if ($user_id) {
            if ($this->userManager->changeUserPassword($request)) {
                return ['detail' => 'Password has been changed'];
            } else {
                return ['detail' => 'Password not changed'];
            }
        } else {
            return new Response(Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * @SWG\Post(
     *      summary="First step of registration. Creates inactive account which requires activation from the URL in the e-mail..",
     *      tags={"auth"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="email", type="string", example="kop_and@tut.by"),
     *              @SWG\Property(property="password1", type="string", example="zxc123"),
     *              @SWG\Property(property="notify_by_email_no_trans", type="boolean", example=false),
     *              @SWG\Property(property="code", type="string", example="0"),
     *              @SWG\Property(property="password2", type="string", example="zxc123"),
     *              @SWG\Property(property="first_name", type="string", example="Alex"),
     *              @SWG\Property(property="last_name", type="string", example="Lion"),
     *              @SWG\Property(property="country", type="string", example="Germany"),
     *              @SWG\Property(property="gender", type="string", example="male")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/register", methods={"POST"})
     */
    public function registerAction(Request $request)
    {
        if ($this->userManager->getUserByEmail($request->get('email'))) {
            return ['error' => 'User with this email exist in system'];
        }

        if ($request->get('password1') != $request->get('password2')) {
            return ['error' => 'Password1 must be match with password2'];
        }
        /**
         * @var $user User
         */
        $user = $this->userManager->registrationUser($request);
        if ($user) {
            return [
                'client_id' => $user->getId(),
                'detail' => 'E-mail with activation link has been sent.',
                'code' => $user->getVerificationCode()
            ];
        }
    }

    /**
     *  Account activation - registration last step. Used to activate user's account

     * @SWG\Response(response=200, description="Success"),
     * @SWG\Response(response=404, description="Not Found"),
     * @SWG\Tag(name="auth"),
     * @Route("/activate/", methods={"GET"})
     * @return Response
     */
    public function activateByEmailAction(Request $request)
    {
        if ($this->userManager->activateByEmail($request) || !$request->get('uid')) {
            $response = new Response(Response::HTTP_OK);
        } else {
            $response = new Response(Response::HTTP_NOT_FOUND);
        }

        return $response;
    }

    /**
     * @SWG\Post(
     *      summary="Allows resend activation email",
     *      tags={"auth"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="email", type="string", example="kop_and@tut.by")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/auth/resend-activation-email", methods={"POST"})
     */
    public function resendActivationAction(Request $request)
    {
        if ($request->get('email')) {
            $email = $this->userManager->resendActivationEmail($request->get('email'));
            if ($email) {
                return ['detail' => 'E-mail with activation link has been resent.'];
            }
        }
    }
}
