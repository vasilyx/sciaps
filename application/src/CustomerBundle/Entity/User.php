<?php

namespace CustomerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

use AppBundle\Traits\TimestampableEntity;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 */
class User
{
    use TimestampableEntity;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_SUSPENDED = 2;
    const STATUS_WRONG_PASSWORD = 3;
    const STATUS_LONG_TIME_INACTIVE = 4;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $email;

    /**
     * @var string|null
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var \DateTime|null
     */
    private $lastLogin;

    /**
     * @var \DateTime|null
     */
    private $currentLogin;

    /**
     * @var string|null
     */
    private $verificationCode;

    /**
     * @var string|null
     */
    private $firstName;

    /**
     * @var string|null
     */
    private $surname;

    /**
     * @var string|null
     */
    private $facebookId;

    /**
     * @var string|null
     */
    private $forgottenPasswordToken;

    /**
     * @var \DateTime|null
     */
    private $datetimeForgottenPassword;

    /**
     * @var string|null
     */
    private $confirmEmailToken;

    /**
     * @var int|null
     */
    private $status = '0';

    public function __construct()
    {
        $this->addedAt = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return User
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set lastLogin.
     *
     * @param \DateTime|null $lastLogin
     *
     * @return User
     */
    public function setLastLogin($lastLogin = null)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Get lastLogin.
     *
     * @return \DateTime|null
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set currentLogin.
     *
     * @param \DateTime|null $currentLogin
     *
     * @return User
     */
    public function setCurrentLogin($currentLogin = null)
    {
        $this->currentLogin = $currentLogin;

        return $this;
    }

    /**
     * Get currentLogin.
     *
     * @return \DateTime|null
     */
    public function getCurrentLogin()
    {
        return $this->currentLogin;
    }

    /**
     * Set verificationCode.
     *
     * @param string|null $verificationCode
     *
     * @return User
     */
    public function setVerificationCode($verificationCode = null)
    {
        $this->verificationCode = $verificationCode;

        return $this;
    }

    /**
     * Get verificationCode.
     *
     * @return string|null
     */
    public function getVerificationCode()
    {
        return $this->verificationCode;
    }

    /**
     * @return $this
     */
    public function generateConfirmationToken()
    {
        $this->setConfirmEmailToken(
            base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
        );
        return $this;
    }

    /**
     * @return $this
     */
    public function generateForgotPasswordToken()
    {
        $this->setForgottenPasswordToken(
            base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
        );
        return $this;
    }

    /**
     * Set forgottenPasswordToken.
     *
     * @param string|null $forgottenPasswordToken
     *
     * @return User
     */
    public function setForgottenPasswordToken($forgottenPasswordToken = null)
    {
        $this->forgottenPasswordToken = $forgottenPasswordToken;

        return $this;
    }

    /**
     * Get forgottenPasswordToken.
     *
     * @return string|null
     */
    public function getForgottenPasswordToken()
    {
        return $this->forgottenPasswordToken;
    }

    /**
     * Set confirmEmailToken.
     *
     * @param string|null $confirmEmailToken
     *
     * @return User
     */
    public function setConfirmEmailToken($confirmEmailToken = null)
    {
        $this->confirmEmailToken = $confirmEmailToken;

        return $this;
    }

    /**
     * Get confirmEmailToken.
     *
     * @return string|null
     */
    public function getConfirmEmailToken()
    {
        return $this->confirmEmailToken;
    }

    /**
     * Set status.
     *
     * @param int|null $status
     *
     * @return User
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set username.
     *
     * @param string|null $username
     *
     * @return User
     */
    public function setUsername($username = null)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Needed for standard encoders
     * @return null|string
     */
    public function getUsername()
    {
        return $this->username;
    }


    /**
     * Set datetimeForgottenPassword.
     *
     * @param \DateTime|null $datetimeForgottenPassword
     *
     * @return User
     */
    public function setDatetimeForgottenPassword($datetimeForgottenPassword = null)
    {
        $this->datetimeForgottenPassword = $datetimeForgottenPassword;

        return $this;
    }

    /**
     * Get datetimeForgottenPassword.
     *
     * @return \DateTime|null
     */
    public function getDatetimeForgottenPassword()
    {
        return $this->datetimeForgottenPassword;
    }

    /**
     * Set firstName.
     *
     * @param string|null $firstName
     *
     * @return User
     */
    public function setFirstName($firstName = null)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set surname.
     *
     * @param string|null $surname
     *
     * @return User
     */
    public function setSurname($surname = null)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname.
     *
     * @return string|null
     */
    public function getSurname()
    {
        return $this->surname;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new UniqueEntity(array(
            'fields'  => 'email',
        )));

        $metadata->addPropertyConstraint('email', new Assert\Email());
    }

    public function getSalt()
    {
        return null;
    }

    public function __toString()
    {
        return (string)$this->getEmail();
    }

    /**
     * @return null|string
     */
    public function getFacebookId(): ?string
    {
        return $this->facebookId;
    }

    /**
     * @param null|string $facebookId
     */
    public function setFacebookId(?string $facebookId): void
    {
        $this->facebookId = $facebookId;
    }
    /**
     * @var int
     */
    private $notifyByEmail;

    /**
     * Set notifyByEmail.
     *
     * @param int $notifyByEmail
     *
     * @return User
     */
    public function setNotifyByEmail($notifyByEmail)
    {
        $this->notifyByEmail = $notifyByEmail;

        return $this;
    }

    /**
     * Get notifyByEmail.
     *
     * @return int
     */
    public function getNotifyByEmail()
    {
        return $this->notifyByEmail;
    }
}
