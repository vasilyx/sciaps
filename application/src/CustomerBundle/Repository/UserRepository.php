<?php

namespace CustomerBundle\Repository;

use CustomerBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UserRepository extends ServiceEntityRepository implements ListPagingInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function loadByEmail($username)
    {
        return $this->findOneByEmail($username);
    }

    public function isUniqueEmail(User $user)
    {
        $res = $this->findByEmail($user->getEmail());
        return count($res) > 0 ? false : true;
    }

    public function getQueryBuilderForList($search = ''): QueryBuilder
    {
        $qb = $this->createQueryBuilder('dp');

        if ($search) {
            $qb->where('dp.email like :search')
                ->orWhere('dp.firstName like :search')
                ->orWhere('dp.surname like :search')
                ->setParameter('search', '%' . $search . '%')
            ;
        }

        return $qb;
    }

    /**
     * @return array
     */
    public function getOrderMap(): array
    {
        return [
            'id' => 'dp.id',
            'email' => 'dp.email',
            'firstName' => 'dp.firstName',
            'surname' => 'dp.surname'
        ];
    }
}
