<?php

namespace CustomerBundle\Repository;


use Doctrine\ORM\QueryBuilder;

interface ListPagingInterface
{
    public function getQueryBuilderForList($search = ''):QueryBuilder;
}
