<?php

namespace CustomerBundle\Manager;

use AppBundle\Services\Email\SendgridEmailer;
use CustomerBundle\Services\Email\Types\ActivationEmail;
use CustomerBundle\Services\Email\Types\ForgottenPasswordEmail;
use CustomerBundle\Services\Email\Types\ResetPassConfirmEmail;
use CustomerBundle\Services\Email\Types\ResetPassword;
use CustomerBundle\Services\Email\Types\ResetPasswordEmail;
use CustomerBundle\Services\Email\Types\WelcomeEmail;
use CustomerBundle\Entity\User;
use CustomerBundle\Repository\UserRepository;
use AppBundle\Services\RedisClient;
use AppBundle\Services\RedisSession;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Facebook\GraphNodes\GraphUser;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\Request;


class UserManager
{
    private $em;

    public const REDIS_SESSION_PREFIX = 'customer_session_';
    public const REDIS_EXPIRE_RESOLUTION = 36000;
    public const SCOPE = "read groups write";
    public const TOKEN_TYPE = "Bearer";

    /**
     * @var UserRepository $repository
     */
    private $repository;
    private $authorizationChecker;
    private $encoderFactory;
    private $redisSession;
    private $mailer;

    public function __construct(EntityManagerInterface $em,
                                AuthorizationCheckerInterface $authorizationChecker,
                                EncoderFactoryInterface $encoderFactory,
                                RedisSession $redisSession,
                                SendgridEmailer $mailer)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(User::class);
        $this->authorizationChecker = $authorizationChecker;
        $this->encoderFactory = $encoderFactory;
        $this->redisSession = $redisSession;
        $this->mailer = $mailer;
    }

    public function saveUser(User $userEntity, $andFlush = false)
    {
        $this->em->persist($userEntity);

        if (true === $andFlush) {
            $this->em->flush();
        }

        return $userEntity;
    }

    public function updatePassword(User $userEntity, $password, $andFlush = true)
    {
        $encoder = $this->getEncoder($userEntity);

        $userEntity->setForgottenPasswordToken(null)->setPassword($encoder->encodePassword($password, $userEntity->getSalt()));
        $userEntity->setDatetimeForgottenPassword(null);

        $user = $this->saveUser($userEntity, $andFlush);

        $params = ['uid' => $user->getId(), 'confirmEmailToken' => $user->getConfirmEmailToken()];
        $this->mailer->send(new ResetPasswordEmail($user->getEmail(), $params));

        return $user;
    }

    public function activateAccount(User $userEntity, $password, $andFlush = true)
    {
        $encoder = $this->getEncoder($userEntity);

        $userEntity->setConfirmEmailToken(null)
            ->setPassword($encoder->encodePassword($password, $userEntity->getSalt()));
        $userEntity->setStatus(User::STATUS_ACTIVE);


        $user =  $this->saveUser($userEntity, $andFlush);

        $this->mailer->send(new WelcomeEmail($user->getEmail()));

        return $user;
    }


    /**
     * @return User[]|array
     */
    public function getAllUser()
    {
        return $this->repository->findAll();
    }


    /**
     * @param $email
     * @param bool $active
     * @return mixed
     */
    public function getUserByEmail($email, $active = true): ?User
    {
        return $this->repository->findOneByEmail($email);
    }

    /**
     * @param $id
     * @param bool $active
     * @return mixed
     */
    public function getUserById($id, $active = true): ?User
    {
        return $this->repository->findOneById($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUserByFacebookId($id): ?User
    {
        return $this->repository->findOneByFacebookId($id);
    }


    /**
     * @param $token
     * @return User|void
     */
    public function getUserByForgottenPasswordToken($token): ?User
    {
        /**
         * @var User $user
         */
        if ($user = $this->repository->findOneByForgottenPasswordToken($token)) {

            if ($user->getDatetimeForgottenPassword() > new \DateTime('-24 hours')) {
                return $user;
            }
        }

        return null;
    }

    /**
     * @param $token
     * @return User|void
     */
    public function getUserByConfirmEmailToken($token): ?User
    {
        /**
         * @var User $user
         */
        if ($user = $this->repository->findOneByConfirmEmailToken($token)) {
                return $user;
        }

        return null;
    }


    /**
     * @param User $userEntity
     * @param bool $andFlush
     * @return User
     */
    public function updateForgotPasswordToken(User $userEntity, $andFlush = true): User
    {
        $userEntity->generateForgotPasswordToken();
        $userEntity->setDatetimeForgottenPassword(new \DateTime());

        $user = $this->saveUser($userEntity, $andFlush);

        $params = ['forgottenPasswordToken' => $user->getForgottenPasswordToken()];
        $this->mailer->send(new ForgottenPasswordEmail($user->getEmail(), $params));

        return $user;
    }


    /**
     * @param $client_id
     * @param $username
     * @param $password
     * @return string
     * @throws EntityNotFoundException
     */
    public function login($client_id, $username, $password, $grant_type): string
    {
        /**
         * @var User $user
         */
        $user = $this->repository->findOneByEmail($username);

        if (!$user) {
            throw new EntityNotFoundException('Provided email address could not be found');
        }

        if ($client_id != $user->getId()) {
            throw new EntityNotFoundException('Not found client_id');
        }

        if (!$this->encoderFactory->getEncoder($user)->isPasswordValid($user->getPassword(), $password, $user->getSalt())) {
            throw new BadRequestHttpException('Invalid password');
        }

        $this->saveUser($user, true);

        $access_token = $this->generateToken();
        $refresh_token = $this->generateToken();

        $session_data = [$user->getId(), $refresh_token, $access_token];

        $session_data = implode(',', $session_data);

        //$this->redisSession->write(self::REDIS_SESSION_PREFIX . $access_token, $session_data,self::REDIS_EXPIRE_RESOLUTION);
        //TODO what name the session_id?
        $this->redisSession->write(self::REDIS_SESSION_PREFIX . $client_id, $session_data,self::REDIS_EXPIRE_RESOLUTION);

        return $access_token;
    }

    public function loginRefresh($client_id, $refresh_token, $grant_type): string
    {
        /**
         * @var User $user
         */
        $user = $this->repository->find($client_id);

        if (!$user) {
            throw new EntityNotFoundException('Provided client_id could not be found');
        }

        $key = $this->redisSession->read(self::REDIS_SESSION_PREFIX . $client_id);

        if (!$key) return null;

        $session_data = explode(',',$key);

        $refresh_token_redis = $session_data[1];

        if ($refresh_token != $refresh_token_redis) {
            throw new BadRequestHttpException('Refresh_token is not exist!');
        }

        $access_token = $this->generateToken();
        $refresh_token = $this->generateToken();

        $session_data = [$user->getId(), $refresh_token];

        $session_data = implode(',', $session_data);

        //TODO what name the session_id?
        $this->redisSession->write(self::REDIS_SESSION_PREFIX . $client_id, $session_data,self::REDIS_EXPIRE_RESOLUTION);

        return $access_token;
    }

    /**
     * @param $token
     */
    public function destroySession($token)
    {
        $this->redisSession->destroy(self::REDIS_SESSION_PREFIX . $token);
    }


    /**
     * @param User $user
     * @return \Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface
     */
    protected function getEncoder(User $user)
    {
        return $this->encoderFactory->getEncoder($user);
    }

    /**
     * @return string
     */
    private function generateToken(): string
    {
        return base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
    }

    /**
     * @param $client_id
     * @param null $param
     * @return User|null|object
     */
    public function loadByClientId($client_id, $param = null)
    {
        $key = $this->redisSession->read(self::REDIS_SESSION_PREFIX . $client_id);

        if (!$key) return null;

        $session_data = explode(',',$key);

        if ($param == 'user_id') {
            $user_id = $session_data[0];

            return $this->repository->find($user_id);
        } elseif ($param == 'refresh_token') {
            $refresh_token = $session_data[1];

            return $refresh_token;
        } else {
            return $this->repository->find($client_id);
        }

    }

    /**
     * @param User $user
     * @return User
     */
    public function createUser(User $user): User
    {
        $encoder = $this->encoderFactory->getEncoder($user);
        //$password = $this->generatePassword();

        //$user->setPassword($password);
        $user->setPassword($encoder->encodePassword($user->getPassword(), $user->getSalt()));
        $user->setStatus(User::STATUS_INACTIVE);
        $user->generateConfirmationToken();

        $this->em->persist($user);
        $this->em->flush();

        $params = ['uid' => $user->getId(), 'token' => $user->getConfirmEmailToken()];
        $this->mailer->send(new ActivationEmail($user->getEmail(), $params));

        return $user;
    }

    /**
     * @param User $user
     * @return User
     */
    public function createUserFromFacebook(GraphUser $fbUser): User
    {
        $user = new User();
        $user->setEmail($fbUser->getEmail());
        $user->setStatus(User::STATUS_ACTIVE);
        $user->setFacebookId($fbUser->getId());

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * @param User $user
     * @return User
     */
    public function updateUser(User $user): User
    {
        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * @param int $length
     * @return string
     */
    public function generatePassword($length = 8): string
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }

    /**
     * @param User $user
     * @return User
     */
    protected function addPermissionsToUser(User $user)
    {
        // hack, adding all permissions
        // TODO add logic - priviledges depends on services

        $permissions = $this->em->getRepository(Permission::class)->findAll();

        foreach ($permissions as $permission) {
            $user->addPermission($permission);
        }

        return $user;
    }

    /**
     * @param User $user
     * @return User
     * @throws \Exception
     */
    public function resetPasswordByEmail(User $user)
    {
        $params = ['uid' => $user->getId(), 'confirmEmailToken' => $user->getConfirmEmailToken()];
        $this->mailer->send(new ResetPasswordEmail($user->getEmail(), $params));

        return $user;
    }

    /**
     * @param null $uid
     * @param null $token
     * @return bool
     */
    public function checkTokenFromEmail(Request $request)
    {
        $uid = $request->get('uid');
        $token = $request->get('token');

        $user = $this->repository->find($uid);

        if ($user) {
            if ($token == $user->getConfirmEmailToken()) {
                return true;
            }
        }
        else {
            return false;
        }
    }

    /**
     * @param ResetPassword $resetPassword
     * @return bool
     * @throws \Exception
     */
    public function changePassword(ResetPassword $resetPassword)
    {
        $user = $this->repository->find($resetPassword->getUid());

        if ($user
            && $user->getConfirmEmailToken() == $resetPassword->getToken()
            && $resetPassword->getNewPassword1() == $resetPassword->getNewPassword2()) {

            $encoder = $this->encoderFactory->getEncoder($user);

            $user->setPassword($encoder->encodePassword($resetPassword->getNewPassword1(), $user->getSalt()));
            $user->generateConfirmationToken();

            $this->em->persist($user);
            $this->em->flush();

            $params = ['uid' => $user->getId(), 'confirmEmailToken' => $user->getConfirmEmailToken()];
            $this->mailer->send(new ResetPasswordEmail($user->getEmail(), $params));
        }

        return false;
    }

    /**
     * @param Request $request
     * @return bool|int
     */
    public function getClientId(Request $request)
    {
        $user = $this->getUserByEmail($request->request->get('email'));

        if ($user && !is_null($user->getPassword()) && $this->encoderFactory->getEncoder($user)->isPasswordValid($user->getPassword(), $request->request->get('password'), $user->getSalt())) {
            return $user->getId();
        }

        return false;
    }

    public function isValidateToken(Request $request)
    {
        $client_id = $request->request->get('client_id');
        $token = $request->request->get('token');

        $key = $this->redisSession->read(self::REDIS_SESSION_PREFIX . $client_id);

        if (!$key) return null;

        $session_data = explode(',',$key);

        if ($session_data[1] == $token || $session_data[2] == $token) {
            return true;
        }

        return false;
    }

    public function revokeToken(Request $request)
    {
        if ($this->isValidateToken($request)) {
            $this->redisSession->destroy(self::REDIS_SESSION_PREFIX . $request->request->get('client_id'));
            $request->getSession()->set('currentUser', 0);

            return true;
        }
        return false;
    }

    public function changeUserPassword(Request $request)
    {
        $user_id = $request->getSession()->get('currentUser');

        $user = $this->repository->find($user_id);
        $encoder = $this->encoderFactory->getEncoder($user);

        if ($user
            && $encoder->encodePassword($request->get('old_password'), $user->getSalt()) == $user->getPassword()
            && $request->get('new_password1') == $request->get('new_password2') ) {

            $user->setPassword($encoder->encodePassword($request->get('new_password1'), $user->getSalt()));
            $user->generateConfirmationToken();

            $this->em->persist($user);
            $this->em->flush();

            $params = ['uid' => $user->getId(), 'confirmEmailToken' => $user->getConfirmEmailToken()];
            $this->mailer->send(new ResetPassConfirmEmail($user->getEmail(), $params));

            return true;
        }

        return false;
    }

    /**
     * @param Request $request
     * @return User
     * @throws \Exception
     */
    public function registrationUser(Request $request)
    {
        $user = new User();

        $encoder = $this->encoderFactory->getEncoder($user);

        $user->setEmail($request->get('email'));
        $user->setPassword($encoder->encodePassword($request->get('password1'), $user->getSalt()));
        $user->setStatus(User::STATUS_INACTIVE);
        $user->setNotifyByEmail(0);
        $user->setVerificationCode($request->get('code'));
        $user->generateConfirmationToken();

        $this->em->persist($user);
        $this->em->flush();

        $params = ['uid' => $user->getId(), 'confirmEmailToken' => $user->getConfirmEmailToken()];
        $this->mailer->send(new ActivationEmail($user->getEmail(), $params));

        return $user;
    }

    public function activateByEmail(Request $request)
    {
        $uid = $request->get('uid');
        $token = $request->get('token');

        $user = $this->repository->find($uid);

        if ($user) {
            if ($token == $user->getConfirmEmailToken()) {
                $user->setStatus(USER::STATUS_ACTIVE);

                return true;
            }
        }
        else {
            return false;
        }
    }

    public function resendActivationEmail(string $email = null)
    {
        $user = $this->getUserByEmail($email);

        if (!$user) {
            throw new EntityNotFoundException('Provided email address could not be found or is already active');
        }

        if ($user->getStatus() != 0) {
            throw new EntityNotFoundException('User is already active');
        }

        $user->generateConfirmationToken();

        $this->em->persist($user);
        $this->em->flush();

        $params = ['uid' => $user->getId(), 'confirmEmailToken' => $user->getConfirmEmailToken()];
        $this->mailer->send(new ActivationEmail($user->getEmail(), $params));

        return $user;
    }
}
