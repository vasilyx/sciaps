<?php

namespace CustomerBundle\Manager;

use CustomerBundle\Entity\Email\Email;
use SendGrid;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;


class EmailManager
{
    private $mailer;
    private $defaultRenderParams = [];

    /**
     * EmailManager constructor.
     *
     * @param \Swift_Mailer $mailer
     * @param \Twig_Environment $twig
     * @param Container $container
     */
    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig, Container $container)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->container = $container;

        $this->setDefaultParams();
    }

    public function send(Email $email, string $type = 'API')
    {
        if ($type == 'API') {
            return $this->sendApiMail($email);
        } elseif ($type == 'SWIFT') {
            return $this->sendSwiftMail($email);
        }

        return false;
    }


    protected function setDefaultParams()
    {
        $this->defaultRenderParams = [
            'baseUrl' => $this->container->get('router')->getContext()->getScheme() . "://" .
                $this->container->get('router')->getContext()->getHost(),
            'frontEndUrl' => $this->container->get('router')->getContext()->getScheme() . "://" .
                $this->container->get('router')->getContext()->getHost() . "/frontend/index.html#/"
        ];
    }

    /**
     * @param \Swift_Message $message
     * @param Email $email
     * @return \Swift_Message
     */
    protected function setHtmlMessageBody(\Swift_Message $message, Email $email): \Swift_Message
    {
        $render_params = $email->getRenderParams();

        foreach ($render_params as $key => $param) {
            if ($key === 'link') {
                $render_params[$key] = $this->container->getParameter('domain_url') . $param;
            }
        }

        $email->setRenderParams($render_params);

        $message->setBody($this->twig->render(
            $email->getTemplate(),
            array_merge($email->getRenderParams(), $this->defaultRenderParams)
        ));

        return $message;
    }

    /**
     * @param Email $email
     * @return null|SendGrid\Response
     * @throws SendGrid\Mail\TypeException
     */
    function sendApiMail(Email $email)
    {
        $message = new SendGrid\Mail\Mail();
        $message->setFrom($this->container->getParameter('mail_send_from'), "SkinNinja Support");
        $message->setSubject($email->getSubject());
        $message->setGlobalSubject($email->getSubject());
        foreach ($email->getTo() as $address) {
            $message->addTo($address);
        }
        foreach ($email->getCc() as $address) {
            $message->addCc($address);
        }
        foreach ($email->getBcc() as $address) {
            $message->addBcc($address);
        }

        if ($email->getSendGridTemplateId()) {
            $message->setTemplateId($email->getSendGridTemplateId());

            foreach ($email->getDynamicContent() as $k => $v) {
                $data[$k] = strpos($k, 'link') === false ? $v : $this->container->getParameter('domain_url').$v;
            }

            $message->addDynamicTemplateDatas($data);
        } else {
            $message->addContent("text/html", $email->getBody());
        }

        foreach ($email->getAttachments() as $attachment) {
            $file_encoded = base64_encode(file_get_contents($attachment['file']));
            $message->addAttachment(
                $file_encoded,
                $attachment['mime'],
                $attachment['filename'],
                "attachment"
            );
        }

        $sendGrid = new SendGrid($this->container->getParameter('sendgrid_api_key'));
        try {
            $response = $sendGrid->send($message);
            if ($response->statusCode() != "202") {
                file_put_contents('php://stderr', $response->body());
            }
        } catch (\Exception $e) {
            $response = null;
            file_put_contents('php://stderr', $e->getMessage());
        }

        return $response;
    }

    /**
     * @param Email $email
     * @return int|null
     */
    public function sendSwiftMail(Email $email)
    {
        $message = new \Swift_Message();
        $message->setCC($email->getCc());
        $message->setBcc($email->getBcc());
        $message->setFrom([$this->container->getParameter('mail_send_from')]);
        $message->setTo($email->getTo());
        $message->setSubject($email->getSubject());
        $message->setContentType($email->getContentType());


        if ($email->getTemplate()) {
            $message = $this->setHtmlMessageBody($message, $email);
        } else {
            $message->setBody($email->getBody());
        }

        foreach ($email->getAttachments() as $attachment) {
            $attach = \Swift_Attachment::fromPath($attachment['file'], $attachment['mime'])->setFilename($attachment['filename']);
            $message->attach($attach);
        }

        try {
            $result = $this->mailer->send($message);
        } catch (\Exception $e) {
            $result = null;
            file_put_contents('php://stderr', $e->getMessage());
        }

        return $result;
    }
}