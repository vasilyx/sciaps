<?php

namespace CustomerBundle\Form;

use CustomerBundle\Entity\Email\ResetPassword;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ResetPasswordConfirmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('uid', IntegerType::class, [
                'required' => false
            ])
            ->add('token', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max' => 255,
                        'maxMessage' => 'Your email cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('new_password1', PasswordType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max' => 24,
                        'maxMessage' => 'Your email cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('new_password2', PasswordType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max' => 24,
                        'maxMessage' => 'Your password cannot be more than {{ limit }} characters',
                    ])
                ]
            ]);
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ResetPassword::class
        ));
    }

    public function getName()
    {
        return 'resetPassword';
    }
}
