<?php
namespace CustomerBundle;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\HttpFoundation\Response;

//use PHPUnit\Framework\TestCase;

class FunctionalTest extends WebTestCase
{
    /** @var  Application $application */
    protected static $application;

    /** @var  Client $client */
    protected static $client;


    protected static $server_parameters;

    /** @var  ContainerInterface $container */
    protected static $container;

    /** @var  EntityManager $entityManager */
    protected static $entityManager;

    /** @var  Registry $registry */
    protected static $registry;

    /** @var  Router $router */
    protected static $router;

    static $token;

    protected static $badFormSubmissionErrorCode;

    public static function setUpBeforeClass()
    {/*
        if (!self::$client) {
            self::runCommand('doctrine:database:drop --force -e test');
            self::runCommand('doctrine:database:create -e test');
            self::runCommand('doctrine:migrations:migrate -e test');
            self::runCommand('doctrine:fixtures:load --append --no-interaction -e test');
            self::runCommand('hautelook:fixtures:load --append -e test');
        }*/

        self::$client = static::createClient();
        self::$container = self::$client->getContainer();
        self::$entityManager = self::$container->get('doctrine.orm.entity_manager');
        self::$registry = self::$container->get('doctrine');
        self::$router =  self::$container->get('router');
        self::$badFormSubmissionErrorCode = 422;

        self::$server_parameters = [];
        self::$server_parameters['CONTENT_TYPE'] = 'application/json';
        self::$server_parameters['ACCEPT'] = 'application/json';
    }

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $requestData = [
            'email' => 'vasilyx20@gmail.com',
            'password' => 'qwaszx10'
        ];

        $this->sendRequest('POST', self::$router->generate('customer_auth_login'), $requestData);

        $arr = json_decode(self::$client->getResponse()->getContent());

        self::$token = $arr->token;

        parent::setUp();
    }

    protected static function runCommand($command)
    {
        $command = sprintf('%s --quiet', $command);

        return self::getApplication()->run(new StringInput($command));
    }

    protected static function getApplication()
    {
        if (null === self::$application) {
            $client = static::createClient();

            self::$application = new Application($client->getKernel());
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }


    /**
     * @param string $method
     * @param $url
     * @param array $content
     * @param array $parameters
     * @param array $files
     */
    public function sendRequest($method = 'GET', $url, $content = [], $parameters = [], $files = [])
    {
        self::$server_parameters['HTTP_Token'] =  self::$token;  // HTTP prefix in needed
        self::$client->request($method, $url, $parameters, $files, self::$server_parameters, json_encode($content));
    }

    /**
     * Returns response content as object
     *
     * @return object
     */
    protected function getContentAsArray()
    {
        return json_decode(self::$client->getResponse()->getContent(), true);
    }

    /**
     * {@inheritDoc}
     */
    /*
    protected function tearDown()
    {
        self::$entityManager->close();
        self::$entityManager = null; // avoid memory leaks
        parent::tearDown();
    }
    */

    public static function tearDownAfterClass()
    {
/*            self::$entityManager->close();
            self::$entityManager = null; // avoid memory leaks
            self::runCommand('doctrine:database:drop --force -e test');
            parent::tearDown();*/
    }

    /**
     * @param $expectedObject
     */
    public function checkResponseEqual($expectedObject)
    {
        $body = json_decode(self::$client->getResponse()->getContent(), true);

        foreach ($expectedObject as $key => $value) {

            $this->assertArrayHasKey($key, $body);

            if ($body[$key]) {
                $this->assertEquals($body[$key], $expectedObject[$key]);
            }
        }
    }
}
