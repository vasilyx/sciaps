<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * LogEvent
 */
class LogEvent
{
    CONST ACTION_CHANGED_STATUS = 1;

    CONST ENTITY_TYPE_CROWDSOURCE = 1;
    CONST ENTITY_TYPE_COMPANY = 2;

    /**
     * @var int
     */
    private $id;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $addedAt;

    /**
     * @var int
     */
    private $actionId;

    /**
     * @var int
     */
    private $entityId;

    /**
     * @var int
     */
    private $entityTypeId;

    /**
     * @var string|null
     */
    private $field;

    /**
     * @var string|null
     */
    private $value;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get addedAt.
     */
    public function getAddedAt()
    {
        return $this->addedAt;
    }

    /**
     * Set actionId.
     *
     * @param int $actionId
     *
     * @return LogEvent
     */
    public function setActionId($actionId)
    {
        $this->actionId = $actionId;

        return $this;
    }

    /**
     * Get actionId.
     *
     * @return int
     */
    public function getActionId()
    {
        return $this->actionId;
    }



    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return LogEvent
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Set entityId.
     *
     * @param int $entityId
     *
     * @return LogEvent
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * Get entityId.
     *
     * @return int
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * Set entityTypeId.
     *
     * @param int $entityTypeId
     *
     * @return LogEvent
     */
    public function setEntityTypeId($entityTypeId)
    {
        $this->entityTypeId = $entityTypeId;

        return $this;
    }

    /**
     * Get entityTypeId.
     *
     * @return int
     */
    public function getEntityTypeId()
    {
        return $this->entityTypeId;
    }

    /**
     * Set field.
     *
     * @param string|null $field
     *
     * @return LogEvent
     */
    public function setField($field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field.
     *
     * @return string|null
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set value.
     *
     * @param string|null $value
     *
     * @return LogEvent
     */
    public function setValue($value = null)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string|null
     */
    public function getValue()
    {
        return $this->value;
    }
}
