<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * SourceDataProvider
 */
class SourceDataProvider
{
    use TimestampableEntity;

    const TYPE_INDEPENDENT = 1;
    const TYPE_RESEARCH_ORG = 2;
    const TYPE_GOVERNMENT = 3;
    const TYPE_NGO = 4;

    private $id;

    private $company;

    /**
     * SourceDataProvider constructor.
     */
    public function __construct()
    {
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set company.
     *
     * @param \AppBundle\Entity\Company|null $company
     *
     * @return SourceDataProvider
     */
    public function setCompany(\AppBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \AppBundle\Entity\Company|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new UniqueEntity(array(
            'fields'  => 'company',
            'message' => 'This value is already used'
        )));
    }
}
