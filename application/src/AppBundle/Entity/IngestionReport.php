<?php

namespace AppBundle\Entity;

/**
 * IngestionReport
 */
class IngestionReport
{
    CONST STATUS_INITIAL = 1;
    CONST STATUS_SUCCESS = 5;
    CONST STATUS_ERROR = 9;

    /**
     * @var float
     */
    private $complexity;

    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime|null
     */
    private $runDate;

    /**
     * @var int|null
     */
    private $status;

    /**
     * @var string|null
     */
    private $responseJson;


    /**
     * @var \AppBundle\Entity\User
     */
    private $user;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set runDate.
     *
     * @param \DateTime|null $runDate
     *
     * @return IngestionReport
     */
    public function setRunDate($runDate = null)
    {
        $this->runDate = $runDate;

        return $this;
    }

    /**
     * Get runDate.
     *
     * @return \DateTime|null
     */
    public function getRunDate()
    {
        return $this->runDate;
    }

    /**
     * Set status.
     *
     * @param int|null $status
     *
     * @return IngestionReport
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set responseJson.
     *
     * @param string|null $responseJson
     *
     * @return IngestionReport
     */
    public function setResponseJson($responseJson = null)
    {
        $this->responseJson = $responseJson;

        return $this;
    }

    /**
     * Get responseJson.
     *
     * @return string|null
     */
    public function getResponseJson()
    {
        return $this->responseJson;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User|null $user
     *
     * @return IngestionReport
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var \AppBundle\Entity\ProductProvider
     */
    private $productProvider;


    /**
     * Set productProvider.
     *
     * @param \AppBundle\Entity\ProductProvider|null $productProvider
     *
     * @return IngestionReport
     */
    public function setProductProvider(\AppBundle\Entity\ProductProvider $productProvider = null)
    {
        $this->productProvider = $productProvider;

        return $this;
    }

    /**
     * Get productProvider.
     *
     * @return \AppBundle\Entity\ProductProvider|null
     */
    public function getProductProvider()
    {
        return $this->productProvider;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $regexp;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->status = self::STATUS_INITIAL;
        $this->regexp = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add regexp.
     *
     * @param \AppBundle\Entity\RegExp $regexp
     *
     * @return IngestionReport
     */
    public function addRegexp(\AppBundle\Entity\RegExp $regexp)
    {
        $this->regexp[] = $regexp;

        return $this;
    }

    /**
     * Remove regexp.
     *
     * @param \AppBundle\Entity\RegExp $regexp
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRegexp(\AppBundle\Entity\RegExp $regexp)
    {
        return $this->regexp->removeElement($regexp);
    }

    /**
     * Get regexp.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegexp()
    {
        return $this->regexp;
    }

    /**
     * Set complexity.
     *
     * @param int $complexity
     *
     * @return IngestionReport
     */
    public function setComplexity($complexity)
    {
        $this->complexity = $complexity;

        return $this;
    }

    /**
     * Get complexity.
     *
     * @return int
     */
    public function getComplexity()
    {
        return $this->complexity;
    }
}
