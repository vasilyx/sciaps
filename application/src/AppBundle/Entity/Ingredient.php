<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;
/**
 * Ingredient
 */
class Ingredient
{
    use TimestampableEntity;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $commonName;

    /**
     * @var string|null
     */
    private $inciName;

    /**
     * @var string|null
     */
    private $innName;

    /**
     * @var string|null
     */
    private $phEurName;

    /**
     * @var string|null
     */
    private $chemicalIupacName;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var string|null
     */
    private $casNumber;

    /**
     * @var string|null
     */
    private $ecNumber;

    /**
     * @var string|null
     */
    private $cosingNumber;

    /**
     * @var string|null
     */
    private $cosingHash;

    /**
     * @var int|null
     */
    private $pubChemId;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Ingredient
     */
    public function setName($name = null)
    {
        $this->name = strtolower($name);

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set commonName.
     *
     * @param string|null $commonName
     *
     * @return Ingredient
     */
    public function setCommonName($commonName = null)
    {
        $this->commonName = strtolower($commonName);

        return $this;
    }

    /**
     * Get commonName.
     *
     * @return string|null
     */
    public function getCommonName()
    {
        return $this->commonName;
    }

    /**
     * Set inciName.
     *
     * @param string|null $inciName
     *
     * @return Ingredient
     */
    public function setInciName($inciName = null)
    {
        $this->inciName = strtolower($inciName);

        return $this;
    }

    /**
     * Get inciName.
     *
     * @return string|null
     */
    public function getInciName()
    {
        return $this->inciName;
    }

    /**
     * Set innName.
     *
     * @param string|null $innName
     *
     * @return Ingredient
     */
    public function setInnName($innName = null)
    {
        $this->innName = strtolower($innName);

        return $this;
    }

    /**
     * Get innName.
     *
     * @return string|null
     */
    public function getInnName()
    {
        return $this->innName;
    }

    /**
     * Set phEurName.
     *
     * @param string|null $phEurName
     *
     * @return Ingredient
     */
    public function setPhEurName($phEurName = null)
    {
        $this->phEurName = strtolower($phEurName);

        return $this;
    }

    /**
     * Get phEurName.
     *
     * @return string|null
     */
    public function getPhEurName()
    {
        return $this->phEurName;
    }

    /**
     * Set chemicalIupacName.
     *
     * @param string|null $chemicalIupacName
     *
     * @return Ingredient
     */
    public function setChemicalIupacName($chemicalIupacName = null)
    {
        $this->chemicalIupacName = strtolower($chemicalIupacName);

        return $this;
    }

    /**
     * Get chemicalIupacName.
     *
     * @return string|null
     */
    public function getChemicalIupacName()
    {
        return $this->chemicalIupacName;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Ingredient
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set casNumber.
     *
     * @param string|null $casNumber
     *
     * @return Ingredient
     */
    public function setCasNumber($casNumber = null)
    {
        $this->casNumber = $casNumber;

        return $this;
    }

    /**
     * Get casNumber.
     *
     * @return string|null
     */
    public function getCasNumber()
    {
        return $this->casNumber;
    }

    /**
     * Set ecNumber.
     *
     * @param string|null $ecNumber
     *
     * @return Ingredient
     */
    public function setEcNumber($ecNumber = null)
    {
        $this->ecNumber = $ecNumber;

        return $this;
    }

    /**
     * Get ecNumber.
     *
     * @return string|null
     */
    public function getEcNumber()
    {
        return $this->ecNumber;
    }

    /**
     * Set cosingNumber.
     *
     * @param string|null $cosingNumber
     *
     * @return Ingredient
     */
    public function setCosingNumber($cosingNumber = null)
    {
        $this->cosingNumber = $cosingNumber;

        return $this;
    }

    /**
     * Get cosingNumber.
     *
     * @return string|null
     */
    public function getCosingNumber()
    {
        return $this->cosingNumber;
    }

    /**
     * Set cosingHash.
     *
     * @param string|null $cosingHash
     *
     * @return Ingredient
     */
    public function setCosingHash($cosingHash = null)
    {
        $this->cosingHash = $cosingHash;

        return $this;
    }

    /**
     * Get cosingHash.
     *
     * @return string|null
     */
    public function getCosingHash()
    {
        return $this->cosingHash;
    }

    /**
     * Set pubChemId.
     *
     * @param int|null $pubChemId
     *
     * @return Ingredient
     */
    public function setPubChemId($pubChemId = null)
    {
        $this->pubChemId = $pubChemId;

        return $this;
    }

    /**
     * Get pubChemId.
     *
     * @return int|null
     */
    public function getPubChemId()
    {
        return $this->pubChemId;
    }

    public function __sleep()
    {
        return ['inciName', 'innName', 'phEurName', 'description', 'casNumber', 'ecNumber'];
    }

    /**
     * @return string
     */
    public function getIngredientName()
    {
        if (!empty($this->getCommonName())) {
            return $this->getCommonName();
        } else {
            if (!empty($this->getInciName())) {
                return $this->getInciName();
            } else {
                return $this->getInnName();
            }
        }
    }

}
