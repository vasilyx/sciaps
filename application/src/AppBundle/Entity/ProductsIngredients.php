<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;
/**
 * ProductsIngredients
 */
class ProductsIngredients
{
    use TimestampableEntity;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $ingredientOrder;


    /**
     * @var \AppBundle\Entity\Product
     */
    private $product;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ingredientOrder.
     *
     * @param int|null $ingredientOrder
     *
     * @return ProductsIngredients
     */
    public function setIngredientOrder($ingredientOrder = null)
    {
        $this->ingredientOrder = $ingredientOrder;

        return $this;
    }

    /**
     * Get ingredientOrder.
     *
     * @return int|null
     */
    public function getIngredientOrder()
    {
        return $this->ingredientOrder;
    }



    /**
     * Set product.
     *
     * @param \AppBundle\Entity\Product|null $product
     *
     * @return ProductsIngredients
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return \AppBundle\Entity\Product|null
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @var \AppBundle\Entity\IngredientUnfiltered
     */
    private $ingredientUnfiltered;

    /**
     * Set ingredientUnfiltered.
     *
     * @param \AppBundle\Entity\IngredientUnfiltered|null $ingredientUnfiltered
     *
     * @return ProductsIngredients
     */
    public function setIngredientUnfiltered(\AppBundle\Entity\IngredientUnfiltered $ingredientUnfiltered = null)
    {
        $this->ingredientUnfiltered = $ingredientUnfiltered;

        return $this;
    }

    /**
     * Get ingredientUnfiltered.
     *
     * @return \AppBundle\Entity\IngredientUnfiltered|null
     */
    public function getIngredientUnfiltered()
    {
        return $this->ingredientUnfiltered;
    }


    /**
     * @var \AppBundle\Entity\IngredientLegend
     */
    private $ingredientLegend;


    /**
     * Set ingredientLegend.
     *
     * @param \AppBundle\Entity\IngredientLegend|null $ingredientLegend
     *
     * @return ProductsIngredients
     */
    public function setIngredientLegend(\AppBundle\Entity\IngredientLegend $ingredientLegend = null)
    {
        $this->ingredientLegend = $ingredientLegend;

        return $this;
    }

    /**
     * Get ingredientLegend.
     *
     * @return \AppBundle\Entity\IngredientLegend|null
     */
    public function getIngredientLegend()
    {
        return $this->ingredientLegend;
    }
}
