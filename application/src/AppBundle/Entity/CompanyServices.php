<?php

namespace AppBundle\Entity;


class CompanyServices
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Company
     */
    private $company;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company.
     *
     * @param \AppBundle\Entity\Company|null $company
     *
     * @return CompanyServices
     */
    public function setCompany(\AppBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \AppBundle\Entity\Company|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @var int
     */
    private $serviceType;


    /**
     * Set serviceType.
     *
     * @param int $serviceType
     *
     * @return CompanyServices
     */
    public function setServiceType($serviceType)
    {
        $this->serviceType = $serviceType;

        return $this;
    }

    /**
     * Get serviceType.
     *
     * @return int
     */
    public function getServiceType()
    {
        return $this->serviceType;
    }
}
