<?php

namespace AppBundle\Entity;
use AppBundle\Traits\TimestampableEntity;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

class Evidence
{
    use TimestampableEntity;

    const TYPE_RED = 100;
    const TYPE_ORANGE = 200;
    const TYPE_GREEN = 300;

    private $id;

    private $defaultName;

    private $defaultDescription;

    private $symbol;

    private $parent;


    /**
     * Attribute constructor.
     */
    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDefaultName(): ?string
    {
        return $this->defaultName;
    }

    public function setDefaultName(?string $defaultName): self
    {
        $this->defaultName = $defaultName;

        return $this;
    }

    public function getDefaultDescription(): ?string
    {
        return $this->defaultDescription;
    }

    public function setDefaultDescription(?string $defaultDescription): self
    {
        $this->defaultDescription = $defaultDescription;

        return $this;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setSymbol(?string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }
    /**
     * @var \AppBundle\Entity\Company
     */
    private $company;


    /**
     * Set company.
     *
     * @param \AppBundle\Entity\Company|null $company
     *
     * @return Evidence
     */
    public function setCompany(\AppBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \AppBundle\Entity\Company|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new UniqueEntity(array(
            'fields'  => 'symbol',
        )));
    }
}
