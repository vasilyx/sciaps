<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;
/**
 * IngestionAnalytics
 */
class IngestionAnalytics
{
    use TimestampableEntity;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $attempts;

    /**
     * @var int|null
     */
    private $attemptsFailed;

    /**
     * @var int|null
     */
    private $attemptsSuccessful;

    /**
     * @var \DateTime|null
     */
    private $lastAttempt;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set attempts.
     *
     * @param int|null $attempts
     *
     * @return IngestionAnalytics
     */
    public function setAttempts($attempts = null)
    {
        $this->attempts = $attempts;

        return $this;
    }

    /**
     * Get attempts.
     *
     * @return int|null
     */
    public function getAttempts()
    {
        return $this->attempts;
    }

    /**
     * Set attemptsFailed.
     *
     * @param int|null $attemptsFailed
     *
     * @return IngestionAnalytics
     */
    public function setAttemptsFailed($attemptsFailed = null)
    {
        $this->attemptsFailed = $attemptsFailed;

        return $this;
    }

    /**
     * Get attemptsFailed.
     *
     * @return int|null
     */
    public function getAttemptsFailed()
    {
        return $this->attemptsFailed;
    }

    /**
     * Set attemptsSuccessful.
     *
     * @param int|null $attemptsSuccessful
     *
     * @return IngestionAnalytics
     */
    public function setAttemptsSuccessful($attemptsSuccessful = null)
    {
        $this->attemptsSuccessful = $attemptsSuccessful;

        return $this;
    }

    /**
     * Get attemptsSuccessful.
     *
     * @return int|null
     */
    public function getAttemptsSuccessful()
    {
        return $this->attemptsSuccessful;
    }

    /**
     * Set lastAttempt.
     *
     * @param \DateTime|null $lastAttempt
     *
     * @return IngestionAnalytics
     */
    public function setLastAttempt($lastAttempt = null)
    {
        $this->lastAttempt = $lastAttempt;

        return $this;
    }

    /**
     * Get lastAttempt.
     *
     * @return \DateTime|null
     */
    public function getLastAttempt()
    {
        return $this->lastAttempt;
    }

    /**
     * @var \AppBundle\Entity\ProductProvider
     */
    private $productProvider;


    /**
     * Set productProvider.
     *
     * @param \AppBundle\Entity\ProductProvider|null $productProvider
     *
     * @return IngestionAnalytics
     */
    public function setProductProvider(\AppBundle\Entity\ProductProvider $productProvider = null)
    {
        $this->productProvider = $productProvider;

        return $this;
    }

    /**
     * Get productProvider.
     *
     * @return \AppBundle\Entity\ProductProvider|null
     */
    public function getProductProvider()
    {
        return $this->productProvider;
    }
}
