<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;

class Organization
{
    use TimestampableEntity;

    const TYPE_OTHER = 1;
    const TYPE_SUPERDRUG = 2;
    const TYPE_MANKIND = 3;
    const TYPE_ECHEMIST = 4;
    const TYPE_CHEMIST_DIRECT = 5;
    const TYPE_HOUSE_OF_FRASER = 6;
    const TYPE_JOHN_LEWIS = 7;
    const TYPE_LOOK_FANTASTIC = 8;
    const TYPE_HOLLAND_BARRETT = 9;
    const TYPE_GREEN_PEOPLE = 10;
    const TYPE_PLANET_ORGANIC = 11;
    const TYPE_BIG_GREEN_SMILE = 12;
    const TYPE_OWN_WEBSITE = 13;
    const TYPE_NOT_ONLINE = 14;

    const CONNECTION_OTHER = 1;
    const CONNECTION_API = 2;
    const CONNECTION_XML = 3;
    const CONNECTION_RSS_FEED = 4;
    const CONNECTION_CSV= 5;
    const CONNECTION_WEBSITE_ONLY = 6;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $companyName;

    /**
     * @var string|null
     */
    private $websiteURL;

    /**
     * @var bool|null
     */
    private $barcode;

    /**
     * @var string|null
     */
    private $personName;

    /**
     * @var int|null
     */
    private $numberProducts;

    /**
     * @var string|null
     */
    private $productsProvidedVia;

    /**
     * @var string|null
     */
    private $productsSoldThrough;

    /**
     * @var string|null
     */
    private $personEmail;

    /**
     * @var string|null
     */
    private $personComment;


    public function __construct()
    {
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyName.
     *
     * @param string|null $companyName
     *
     * @return Organization
     */
    public function setCompanyName($companyName = null)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName.
     *
     * @return string|null
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set websiteURL.
     *
     * @param string|null $websiteURL
     *
     * @return Organization
     */
    public function setWebsiteURL($websiteURL = null)
    {
        $this->websiteURL = $websiteURL;

        return $this;
    }

    /**
     * Get websiteURL.
     *
     * @return string|null
     */
    public function getWebsiteURL()
    {
        return $this->websiteURL;
    }

    /**
     * Set barcode.
     *
     * @param bool|null $barcode
     *
     * @return Organization
     */
    public function setBarcode($barcode = null)
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * Get barcode.
     *
     * @return bool|null
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * Set numberProducts.
     *
     * @param int|null $numberProducts
     *
     * @return Organization
     */
    public function setNumberProducts($numberProducts = null)
    {
        $this->numberProducts = $numberProducts;

        return $this;
    }

    /**
     * Get numberProducts.
     *
     * @return int|null
     */
    public function getNumberProducts()
    {
        return $this->numberProducts;
    }

    /**
     * Set personName.
     *
     * @param string|null $personName
     *
     * @return Organization
     */
    public function setPersonName($personName = null)
    {
        $this->personName = $personName;

        return $this;
    }

    /**
     * Get personName.
     *
     * @return string|null
     */
    public function getPersonName()
    {
        return $this->personName;
    }

    /**
     * Set personEmail.
     *
     * @param string|null $personEmail
     *
     * @return Organization
     */
    public function setPersonEmail($personEmail = null)
    {
        $this->personEmail = $personEmail;

        return $this;
    }

    /**
     * Get personEmail.
     *
     * @return string|null
     */
    public function getPersonEmail()
    {
        return $this->personEmail;
    }

    /**
     * Set personComment.
     *
     * @param string|null $personComment
     *
     * @return Organization
     */
    public function setPersonComment($personComment = null)
    {
        $this->personComment = $personComment;

        return $this;
    }

    /**
     * Get personComment.
     *
     * @return string|null
     */
    public function getPersonComment()
    {
        return $this->personComment;
    }

    /**
     * Set productsProvidedVia.
     *
     * @param string|null $productsProvidedVia
     *
     * @return Organization
     */
    public function setProductsProvidedVia($productsProvidedVia = null)
    {
        $this->productsProvidedVia = $productsProvidedVia;

        return $this;
    }

    /**
     * Get productsProvidedVia.
     *
     * @return string|null
     */
    public function getProductsProvidedVia()
    {
        return $this->productsProvidedVia;
    }

    /**
     * Set productsSoldThrough.
     *
     * @param string|null $productsSoldThrough
     *
     * @return Organization
     */
    public function setProductsSoldThrough($productsSoldThrough = null)
    {
        $this->productsSoldThrough = $productsSoldThrough;

        return $this;
    }

    /**
     * Get productsSoldThrough.
     *
     * @return string|null
     */
    public function getProductsSoldThrough()
    {
        return $this->productsSoldThrough;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $countries;


    /**
     * Add country.
     *
     * @param \AppBundle\Entity\Country $country
     *
     * @return Organization
     */
    public function addCountry(\AppBundle\Entity\Country $country)
    {
        $this->countries[] = $country;

        return $this;
    }

    /**
     * Remove country.
     *
     * @param \AppBundle\Entity\Country $country
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCountry(\AppBundle\Entity\Country $country)
    {
        return $this->countries->removeElement($country);
    }

    /**
     * Get countries.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @return string
     */
    public function getCountriesList()
    {
        $return = [];
        foreach ($this->getCountries() as $country) {
            array_push($return, $country->getName());
        }

        return implode(', ', $return);
    }
}
