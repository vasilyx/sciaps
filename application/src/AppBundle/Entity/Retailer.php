<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;
/**
 * Retailer
 */
class Retailer
{
    use TimestampableEntity;

    /**
     * @var int
     */
    private $id;


    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $logoFilename;

    /**
     * @var \AppBundle\Entity\Country
     */
    private $country;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Retailer
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set logoFilename.
     *
     * @param string|null $logoFilename
     *
     * @return Retailer
     */
    public function setLogoFilename($logoFilename = null)
    {
        $this->logoFilename = $logoFilename;

        return $this;
    }

    /**
     * Get logoFilename.
     *
     * @return string|null
     */
    public function getLogoFilename()
    {
        return $this->logoFilename;
    }

    /**
     * Set country.
     *
     * @param \AppBundle\Entity\Country|null $country
     *
     * @return Retailer
     */
    public function setCountry(\AppBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \AppBundle\Entity\Country|null
     */
    public function getCountry()
    {
        return $this->country;
    }
    /**
     * @var int|null
     */
    private $productProviderId;

    /**
     * @var int|null
     */
    private $productProviderMerchantId;


    /**
     * Set productProviderId.
     *
     * @param int|null $productProviderId
     *
     * @return Retailer
     */
    public function setProductProviderId($productProviderId = null)
    {
        $this->productProviderId = $productProviderId;

        return $this;
    }

    /**
     * Get productProviderId.
     *
     * @return int|null
     */
    public function getProductProviderId()
    {
        return $this->productProviderId;
    }

    /**
     * Set productProviderMerchantId.
     *
     * @param int|null $productProviderMerchantId
     *
     * @return Retailer
     */
    public function setProductProviderMerchantId($productProviderMerchantId = null)
    {
        $this->productProviderMerchantId = $productProviderMerchantId;

        return $this;
    }

    /**
     * Get productProviderMerchantId.
     *
     * @return int|null
     */
    public function getProductProviderMerchantId()
    {
        return $this->productProviderMerchantId;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $products;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Add product.
     *
     * @param \AppBundle\Entity\Product $product
     *
     * @return Retailer
     */
    public function addProduct(\AppBundle\Entity\Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product.
     *
     * @param \AppBundle\Entity\Product $product
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeProduct(\AppBundle\Entity\Product $product)
    {
        return $this->products->removeElement($product);
    }

    /**
     * Get products.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }
}
