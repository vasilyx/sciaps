<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * DataEntryProvider
 */
class DataEntryProvider
{
    use TimestampableEntity;

    /**
     * @var int
     */
    private $id;


    /**
     * @var \AppBundle\Entity\Company
     */
    private $company;

    public function __construct()
    {
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company.
     *
     * @param \AppBundle\Entity\Company|null $company
     *
     * @return DataEntryProvider
     */
    public function setCompany(\AppBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \AppBundle\Entity\Company|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new UniqueEntity(array(
            'fields'  => 'company',
            'message' => 'This value is already used'
        )));
    }
}
