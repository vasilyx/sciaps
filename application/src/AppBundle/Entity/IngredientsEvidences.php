<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;

/**
 * IngredientsEvidences
 */
class IngredientsEvidences
{
    use TimestampableEntity;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $statusCode;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var \AppBundle\Entity\Source
     */
    private $source;

    /**
     * @var \AppBundle\Entity\Evidence
     */
    private $evidence;

    /**
     * @var \AppBundle\Entity\Ingredient
     */
    private $ingredient;

    /**
     * @var array
     */
    private $ingredients;

    public function __construct()
    {
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get $ingredients.
     *
     * @return array
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * @return null|array
     */
    public function setIngredients($ingredients = null)
    {
        $this->ingredients = $ingredients;

        return $this;
    }

    /**
     * Set statusCode.
     *
     * @param int|null $statusCode
     *
     * @return IngredientsEvidences
     */
    public function setStatusCode($statusCode = null)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Get statusCode.
     *
     * @return int|null
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return IngredientsEvidences
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set source.
     *
     * @param \AppBundle\Entity\Source|null $source
     *
     * @return IngredientsEvidences
     */
    public function setSource(\AppBundle\Entity\Source $source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return \AppBundle\Entity\Source|null
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set evidence.
     *
     * @param \AppBundle\Entity\Evidence|null $evidence
     *
     * @return IngredientsEvidences
     */
    public function setEvidence(\AppBundle\Entity\Evidence $evidence = null)
    {
        $this->evidence = $evidence;

        return $this;
    }

    /**
     * Get evidence.
     *
     * @return \AppBundle\Entity\Evidence|null
     */
    public function getEvidence()
    {
        return $this->evidence;
    }

    /**
     * Set ingredient.
     *
     * @param \AppBundle\Entity\Ingredient|null $ingredient
     *
     * @return IngredientsEvidences
     */
    public function setIngredient(\AppBundle\Entity\Ingredient $ingredient = null)
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    /**
     * Get ingredient.
     *
     * @return \AppBundle\Entity\Ingredient|null
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }
}
