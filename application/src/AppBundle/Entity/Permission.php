<?php

namespace AppBundle\Entity;

class Permission
{
    private $id;

    private $description;

    private $user;

    CONST PERMISSION_PRODUCT_PROVIDER_ADMIN = 1;
    CONST PERMISSION_SOURCE_PROVIDER_ADMIN = 2;
    CONST PERMISSION_CONTRACTOR_ADMIN = 3;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }


    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $group;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->group = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add group.
     *
     * @param \AppBundle\Entity\PermissionGroup $group
     *
     * @return Permission
     */
    public function addGroup(\AppBundle\Entity\PermissionGroup $group)
    {
        $this->group[] = $group;

        return $this;
    }

    /**
     * Remove group.
     *
     * @param \AppBundle\Entity\PermissionGroup $group
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeGroup(\AppBundle\Entity\PermissionGroup $group)
    {
        return $this->group->removeElement($group);
    }

    /**
     * Get group.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Permission
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Add user.
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Permission
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user.
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        return $this->user->removeElement($user);
    }

    /**
     * Get user.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }
}
