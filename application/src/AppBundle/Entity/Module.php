<?php

namespace AppBundle\Entity;

/**
 * Module
 */
class Module
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $moduleCode;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $description;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set moduleCode.
     *
     * @param string|null $moduleCode
     *
     * @return Module
     */
    public function setModuleCode($moduleCode = null)
    {
        $this->moduleCode = $moduleCode;

        return $this;
    }

    /**
     * Get moduleCode.
     *
     * @return string|null
     */
    public function getModuleCode()
    {
        return $this->moduleCode;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Module
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Module
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }
}
