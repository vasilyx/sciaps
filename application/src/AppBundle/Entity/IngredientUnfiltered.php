<?php

namespace AppBundle\Entity;

/**
 * IngredientUnfiltered
 */
class IngredientUnfiltered
{
    CONST MATCH_TYPE_COMMON = 1; // common_name
    CONST MATCH_TYPE_INCI = 2; // inci_name
    CONST MATCH_TYPE_INN = 3; // inn_name
    CONST MATCH_TYPE_CONFLICT = 99; // Conflicted result

    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $addedAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var int|null
     */
    private $matchType;

    /**
     * @var int|null
     */
    private $ingredientId;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return IngredientUnfiltered
     */
    public function setName($name = null)
    {
        $this->name = strtolower($name);

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set matchType.
     *
     * @param int|null $matchType
     *
     * @return IngredientUnfiltered
     */
    public function setMatchType($matchType = null)
    {
        $this->matchType = $matchType;

        return $this;
    }

    /**
     * Get matchType.
     *
     * @return int|null
     */
    public function getMatchType()
    {
        return $this->matchType;
    }

    /**
     * Set ingredientId.
     *
     * @param int|null $ingredientId
     *
     * @return IngredientUnfiltered
     */
    public function setIngredientId($ingredientId = null)
    {
        $this->ingredientId = $ingredientId;

        return $this;
    }

    /**
     * Get ingredientId.
     *
     * @return int|null
     */
    public function getIngredientId()
    {
        return $this->ingredientId;
    }

    /**
     * Set addedAt.
     *
     * @param \DateTime $addedAt
     *
     * @return IngredientUnfiltered
     */
    public function setAddedAt($addedAt)
    {
        $this->addedAt = $addedAt;

        return $this;
    }

    /**
     * Get addedAt.
     *
     * @return \DateTime
     */
    public function getAddedAt()
    {
        return $this->addedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return IngredientUnfiltered
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
