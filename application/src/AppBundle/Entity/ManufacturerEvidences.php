<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;

class ManufacturerEvidences
{
    use TimestampableEntity;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $statusCode;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var \AppBundle\Entity\Source
     */
    private $source;

    /**
     * @var \AppBundle\Entity\Evidence
     */
    private $evidence;

    /**
     * @var \AppBundle\Entity\Manufacturer
     */
    private $manufacturer;

    private $manufacturers;

    public function __construct()
    {
    }

    /**
     * Get $manufacturers.
     *
     * @return array
     */
    public function getManufacturers()
    {
        return $this->manufacturers;
    }

    /**
     * @return null|array
     */
    public function setManufacturers($manufacturers = null)
    {
        $this->manufacturers = $manufacturers;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statusCode.
     *
     * @param int|null $statusCode
     *
     * @return ManufacturerEvidences
     */
    public function setStatusCode($statusCode = null)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Get statusCode.
     *
     * @return int|null
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return ManufacturerEvidences
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set source.
     *
     * @param \AppBundle\Entity\Source|null $source
     *
     * @return ManufacturerEvidences
     */
    public function setSource(\AppBundle\Entity\Source $source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return \AppBundle\Entity\Source|null
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set evidence.
     *
     * @param \AppBundle\Entity\Evidence|null $evidence
     *
     * @return ManufacturerEvidences
     */
    public function setEvidence(\AppBundle\Entity\Evidence $evidence = null)
    {
        $this->evidence = $evidence;

        return $this;
    }

    /**
     * Get evidence.
     *
     * @return \AppBundle\Entity\Evidence|null
     */
    public function getEvidence()
    {
        return $this->evidence;
    }

    /**
     * Set manufacturer.
     *
     * @param \AppBundle\Entity\Manufacturer|null $manufacturer
     *
     * @return ManufacturerEvidences
     */
    public function setManufacturer(\AppBundle\Entity\Manufacturer $manufacturer = null)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer.
     *
     * @return \AppBundle\Entity\Manufacturer|null
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }
}
