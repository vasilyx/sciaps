<?php

namespace AppBundle\Entity;

class CrowdsourcePending
{
    CONST STATUS_ARCHIVED = 1;
    CONST STATUS_APPROVED = 2;
    CONST STATUS_HEAD_ARCHIVED = 3; // archived by head manager
    CONST STATUS_HEAD_APPROVED = 4; // approved by head manager

    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $customerUserId;

    /**
     * @var int|null
     */
    private $status;

    /**
     * @var bool|null
     */
    private $isWiyc;

    /**
     * @var \AppBundle\Entity\ProductImage
     */
    private $backProductImage;

    /**
     * @var \AppBundle\Entity\Barcode
     */
    private $barcode;

    /**
     * @var \AppBundle\Entity\ProductImage
     */
    private $frontProductImage;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerUserId.
     *
     * @param int|null $customerUserId
     *
     * @return CrowdsourcePending
     */
    public function setCustomerUserId($customerUserId = null)
    {
        $this->customerUserId = $customerUserId;

        return $this;
    }

    /**
     * Get customerUserId.
     *
     * @return int|null
     */
    public function getCustomerUserId()
    {
        return $this->customerUserId;
    }

    /**
     * Set status.
     *
     * @param int|null $status
     *
     * @return CrowdsourcePending
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set isWiyc.
     *
     * @param bool|null $isWiyc
     *
     * @return CrowdsourcePending
     */
    public function setIsWiyc($isWiyc = null)
    {
        $this->isWiyc = $isWiyc;

        return $this;
    }

    /**
     * Get isWiyc.
     *
     * @return bool|null
     */
    public function getIsWiyc()
    {
        return $this->isWiyc;
    }

    /**
     * Set backProductImage.
     *
     * @param \AppBundle\Entity\ProductImage|null $backProductImage
     *
     * @return CrowdsourcePending
     */
    public function setBackProductImage(\AppBundle\Entity\ProductImage $backProductImage = null)
    {
        $this->backProductImage = $backProductImage;

        return $this;
    }

    /**
     * Get backProductImage.
     *
     * @return \AppBundle\Entity\ProductImage|null
     */
    public function getBackProductImage()
    {
        return $this->backProductImage;
    }

    /**
     * Set barcode.
     *
     * @param \AppBundle\Entity\Barcode|null $barcode
     *
     * @return CrowdsourcePending
     */
    public function setBarcode(\AppBundle\Entity\Barcode $barcode = null)
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * Get barcode.
     *
     * @return \AppBundle\Entity\Barcode|null
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * Set frontProductImage.
     *
     * @param \AppBundle\Entity\ProductImage|null $frontProductImage
     *
     * @return CrowdsourcePending
     */
    public function setFrontProductImage(\AppBundle\Entity\ProductImage $frontProductImage = null)
    {
        $this->frontProductImage = $frontProductImage;

        return $this;
    }

    /**
     * Get frontProductImage.
     *
     * @return \AppBundle\Entity\ProductImage|null
     */
    public function getFrontProductImage()
    {
        return $this->frontProductImage;
    }
    /**
     * @var \DateTime
     */
    private $addedAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var string|null
     */
    private $volume;

    /**
     * @var int|null
     */
    private $volumeUnits;

    /**
     * @var \AppBundle\Entity\Brand
     */
    private $brand;

    /**
     * @var \AppBundle\Entity\Manufacturer
     */
    private $manufacturer;


    /**
     * Set addedAt.
     *
     * @param \DateTime $addedAt
     *
     * @return CrowdsourcePending
     */
    public function setAddedAt($addedAt)
    {
        $this->addedAt = $addedAt;

        return $this;
    }

    /**
     * Get addedAt.
     *
     * @return \DateTime
     */
    public function getAddedAt()
    {
        return $this->addedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return CrowdsourcePending
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return CrowdsourcePending
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return CrowdsourcePending
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set volume.
     *
     * @param string|null $volume
     *
     * @return CrowdsourcePending
     */
    public function setVolume($volume = null)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume.
     *
     * @return string|null
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set volumeUnits.
     *
     * @param int|null $volumeUnits
     *
     * @return CrowdsourcePending
     */
    public function setVolumeUnits($volumeUnits = null)
    {
        $this->volumeUnits = $volumeUnits;

        return $this;
    }

    /**
     * Get volumeUnits.
     *
     * @return int|null
     */
    public function getVolumeUnits()
    {
        return $this->volumeUnits;
    }

    /**
     * Set brand.
     *
     * @param \AppBundle\Entity\Brand|null $brand
     *
     * @return CrowdsourcePending
     */
    public function setBrand(\AppBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand.
     *
     * @return \AppBundle\Entity\Brand|null
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set manufacturer.
     *
     * @param \AppBundle\Entity\Manufacturer|null $manufacturer
     *
     * @return CrowdsourcePending
     */
    public function setManufacturer(\AppBundle\Entity\Manufacturer $manufacturer = null)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer.
     *
     * @return \AppBundle\Entity\Manufacturer|null
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ingredients;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ingredients = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ingredient.
     *
     * @param \AppBundle\Entity\CrowdsourcePendingIngredients $ingredient
     *
     * @return CrowdsourcePending
     */
    public function addIngredient(\AppBundle\Entity\CrowdsourcePendingIngredients $ingredient)
    {
        $this->ingredients[] = $ingredient;

        return $this;
    }

    /**
     * Remove ingredient.
     *
     * @param \AppBundle\Entity\CrowdsourcePendingIngredients $ingredient
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeIngredient(\AppBundle\Entity\CrowdsourcePendingIngredients $ingredient)
    {
        return $this->ingredients->removeElement($ingredient);
    }

    /**
     * Get ingredients.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }
}
