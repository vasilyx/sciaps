<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;
/**
 * ProductCategory
 */
class ProductCategory
{
    use TimestampableEntity;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var int|null
     */
    private $parentId;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return ProductCategory
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parentId.
     *
     * @param int|null $parentId
     *
     * @return ProductCategory
     */
    public function setParentId($parentId = null)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId.
     *
     * @return int|null
     */
    public function getParentId()
    {
        return $this->parentId;
    }


    /**
     * @var \AppBundle\Entity\productProvider
     */
    private $productProvider;


    /**
     * Set productProvider.
     *
     * @param \AppBundle\Entity\productProvider|null $productProvider
     *
     * @return ProductCategory
     */
    public function setProductProvider(\AppBundle\Entity\productProvider $productProvider = null)
    {
        $this->productProvider = $productProvider;

        return $this;
    }

    /**
     * Get productProvider.
     *
     * @return \AppBundle\Entity\productProvider|null
     */
    public function getProductProvider()
    {
        return $this->productProvider;
    }
}
