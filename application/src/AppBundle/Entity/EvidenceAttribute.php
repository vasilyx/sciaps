<?php

namespace AppBundle\Entity;

/**
 * EvidenceAttribute
 */
class EvidenceAttribute
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $statusCode;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var \AppBundle\Entity\Attribute
     */
    private $attribute;

    /**
     * @var \AppBundle\Entity\EvidenceData
     */
    private $evidenceData;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statusCode.
     *
     * @param int|null $statusCode
     *
     * @return EvidenceAttribute
     */
    public function setStatusCode($statusCode = null)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Get statusCode.
     *
     * @return int|null
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return EvidenceAttribute
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return EvidenceAttribute
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set attribute.
     *
     * @param \AppBundle\Entity\Attribute|null $attribute
     *
     * @return EvidenceAttribute
     */
    public function setAttribute(\AppBundle\Entity\Attribute $attribute = null)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get attribute.
     *
     * @return \AppBundle\Entity\Attribute|null
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set evidenceData.
     *
     * @param \AppBundle\Entity\EvidenceData|null $evidenceData
     *
     * @return EvidenceAttribute
     */
    public function setEvidenceData(\AppBundle\Entity\EvidenceData $evidenceData = null)
    {
        $this->evidenceData = $evidenceData;

        return $this;
    }

    /**
     * Get evidenceData.
     *
     * @return \AppBundle\Entity\EvidenceData|null
     */
    public function getEvidenceData()
    {
        return $this->evidenceData;
    }
}
