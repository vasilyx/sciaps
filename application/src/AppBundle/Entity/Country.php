<?php

namespace AppBundle\Entity;

/**
 * Country
 */
class Country
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $shortcode;

    private $retailers;

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortcode.
     *
     * @param string $shortcode
     *
     * @return Country
     */
    public function setShortcode($shortcode)
    {
        $this->shortcode = $shortcode;

        return $this;
    }

    /**
     * Get shortcode.
     *
     * @return string
     */
    public function getShortcode()
    {
        return $this->shortcode;
    }

    /**
     * Add retailer.
     *
     * @param \AppBundle\Entity\Retailer $retailer
     *
     * @return Country
     */
    public function addRetailer(\AppBundle\Entity\Retailer $retailer)
    {
        $this->retailers[] = $retailer;

        return $this;
    }

    /**
     * Remove retailer.
     *
     * @param \AppBundle\Entity\Retailer $retailer
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRetailer(\AppBundle\Entity\Retailer $retailer)
    {
        return $this->retailers->removeElement($retailer);
    }

    /**
     * Get retailers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRetailers()
    {
        return $this->retailers;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $organizations;

    /**
     * Add organization.
     *
     * @param \AppBundle\Entity\Organization $organization
     *
     * @return Country
     */
    public function addOrganization(\AppBundle\Entity\Organization $organization)
    {
        $this->organizations[] = $organization;

        return $this;
    }

    /**
     * Remove organization.
     *
     * @param \AppBundle\Entity\Organization $organization
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeOrganization(\AppBundle\Entity\Organization $organization)
    {
        return $this->organizations->removeElement($organization);
    }

    /**
     * Get organizations.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrganizations()
    {
        return $this->organizations;
    }
}
