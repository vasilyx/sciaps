<?php

namespace AppBundle\Entity;

/**
 * BarcodeType
 */
class BarcodeType
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $type;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param string|null $type
     *
     * @return BarcodeType
     */
    public function setType($type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
}
