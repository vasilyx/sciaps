<?php

namespace AppBundle\Entity;

/**
 * IngredientSynonym
 */
class IngredientSynonym
{
    /**
     * @var int
     */
    private $cid;

    /**
     * @var \AppBundle\Entity\Ingredient
     */
    private $ingredient;


    /**
     * Set cid.
     *
     * @param int $cid
     *
     * @return IngredientSynonym
     */
    public function setCid($cid)
    {
        $this->cid = $cid;

        return $this;
    }

    /**
     * Get cid.
     *
     * @return int
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set ingredient.
     *
     * @param \AppBundle\Entity\Ingredient|null $ingredient
     *
     * @return IngredientSynonym
     */
    public function setIngredient(\AppBundle\Entity\Ingredient $ingredient = null)
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    /**
     * Get ingredient.
     *
     * @return \AppBundle\Entity\Ingredient|null
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }
    /**
     * @var int
     */
    private $id;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}