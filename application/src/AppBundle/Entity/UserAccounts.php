<?php

namespace AppBundle\Entity;

/**
 * UserAccounts
 */
class UserAccounts
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Module
     */
    private $module;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set module.
     *
     * @param \AppBundle\Entity\Module|null $module
     *
     * @return UserAccounts
     */
    public function setModule(\AppBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module.
     *
     * @return \AppBundle\Entity\Module|null
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User|null $user
     *
     * @return UserAccounts
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
