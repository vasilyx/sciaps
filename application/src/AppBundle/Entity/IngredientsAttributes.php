<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;

/**
 * IngredientsAttributes
 */
class IngredientsAttributes
{
    use TimestampableEntity;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $statusCode;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var \AppBundle\Entity\Source
     */
    private $source;

    /**
     * @var \AppBundle\Entity\Attribute
     */
    private $attribute;

    /**
     * @var \AppBundle\Entity\Ingredient
     */
    private $ingredient;

    /**
     * @var array
     */
    private $ingredients;

    public function __construct()
    {
    }

    /**
     * Get $ingredients.
     *
     * @return array
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * @return null|array
     */
    public function setIngredients($ingredients = null)
    {
        $this->ingredients = $ingredients;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statusCode.
     *
     * @param int|null $statusCode
     *
     * @return IngredientsAttributes
     */
    public function setStatusCode($statusCode = null)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Get statusCode.
     *
     * @return int|null
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return IngredientsAttributes
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set source.
     *
     * @param \AppBundle\Entity\Source|null $source
     *
     * @return IngredientsAttributes
     */
    public function setSource(\AppBundle\Entity\Source $source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return \AppBundle\Entity\Source|null
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set attribute.
     *
     * @param \AppBundle\Entity\Attribute|null $attribute
     *
     * @return IngredientsAttributes
     */
    public function setAttribute(\AppBundle\Entity\Attribute $attribute = null)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get attribute.
     *
     * @return \AppBundle\Entity\Attribute|null
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set ingredient.
     *
     * @param \AppBundle\Entity\Ingredient|null $ingredient
     *
     * @return IngredientsAttributes
     */
    public function setIngredient(\AppBundle\Entity\Ingredient $ingredient = null)
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    /**
     * Get ingredient.
     *
     * @return \AppBundle\Entity\Ingredient|null
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }
}
