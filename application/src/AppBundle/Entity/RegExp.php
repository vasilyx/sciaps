<?php

namespace AppBundle\Entity;

/**
 * RegExp
 */
class RegExp
{
    CONST TYPE_LEGEND_PART = 1;
    CONST TYPE_INGREDIENT_PART = 2;


    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $addedAt;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var string
     */
    private $regex;

    /**
     * @var int
     */
    private $regexType;

    /**
     * @var int
     */
    private $version = 0;

    /**
     * @var int
     */
    private $default = 0;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set addedAt.
     *
     * @param \DateTime $addedAt
     *
     * @return RegExp
     */
    public function setAddedAt($addedAt)
    {
        $this->addedAt = $addedAt;

        return $this;
    }

    /**
     * Get addedAt.
     *
     * @return \DateTime
     */
    public function getAddedAt()
    {
        return $this->addedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return RegExp
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set regex.
     *
     * @param string $regex
     *
     * @return RegExp
     */
    public function setRegex($regex)
    {
        $this->regex = $regex;

        return $this;
    }

    /**
     * Get regex.
     *
     * @return string
     */
    public function getRegex(): string
    {
        return $this->regex;
    }

    /**
     * Set regexType.
     *
     * @param int $regexType
     *
     * @return RegExp
     */
    public function setRegexType($regexType)
    {
        $this->regexType = $regexType;

        return $this;
    }

    /**
     * Get regexType.
     *
     * @return int
     */
    public function getRegexType()
    {
        return $this->regexType;
    }

    /**
     * Set version.
     *
     * @param int $version
     *
     * @return RegExp
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version.
     *
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ingestionReport;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ingestionReport = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add ingestionReport.
     *
     * @param \AppBundle\Entity\IngestionReport $ingestionReport
     *
     * @return RegExp
     */
    public function addIngestionReport(\AppBundle\Entity\IngestionReport $ingestionReport)
    {
        $this->ingestionReport[] = $ingestionReport;

        return $this;
    }

    /**
     * Remove ingestionReport.
     *
     * @param \AppBundle\Entity\IngestionReport $ingestionReport
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeIngestionReport(\AppBundle\Entity\IngestionReport $ingestionReport)
    {
        return $this->ingestionReport->removeElement($ingestionReport);
    }

    /**
     * Get ingestionReport.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIngestionReport()
    {
        return $this->ingestionReport;
    }

    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Get version.
     *
     * @return int
     */
    public function getDefault()
    {
        return $this->default;
    }
}
