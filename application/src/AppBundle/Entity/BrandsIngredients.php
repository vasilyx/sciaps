<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;

/**
 * BrandsIngredients
 */
class BrandsIngredients
{
    use TimestampableEntity;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $tag;

    /**
     * @var \AppBundle\Entity\Brand
     */
    private $brand;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tag.
     *
     * @param string|null $tag
     *
     * @return BrandsIngredients
     */
    public function setTag($tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag.
     *
     * @return string|null
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set brand.
     *
     * @param \AppBundle\Entity\Brand|null $brand
     *
     * @return BrandsIngredients
     */
    public function setBrand(\AppBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand.
     *
     * @return \AppBundle\Entity\Brand|null
     */
    public function getBrand()
    {
        return $this->brand;
    }
    /**
     * @var \AppBundle\Entity\Ingredient
     */
    private $ingredient;

    /**
     * Set ingredient.
     *
     * @param \AppBundle\Entity\Ingredient|null $ingredient
     *
     * @return BrandsIngredients
     */
    public function setIngredient(\AppBundle\Entity\Ingredient $ingredient = null)
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    /**
     * Get ingredient.
     *
     * @return \AppBundle\Entity\Ingredient|null
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }
}
