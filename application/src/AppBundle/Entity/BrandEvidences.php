<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;

class BrandEvidences
{
    use TimestampableEntity;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $statusCode;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var \AppBundle\Entity\Source
     */
    private $source;

    /**
     * @var \AppBundle\Entity\Evidence
     */
    private $evidence;

    /**
     * @var \AppBundle\Entity\Brand
     */
    private $brand;

    private $brands;

    public function __construct()
    {
    }

    /**
     * Get $brands.
     *
     * @return array
     */
    public function getBrands()
    {
        return $this->brands;
    }

    /**
     * @return null|array
     */
    public function setBrands($brands = null)
    {
        $this->brands = $brands;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statusCode.
     *
     * @param int|null $statusCode
     *
     * @return BrandEvidences
     */
    public function setStatusCode($statusCode = null)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Get statusCode.
     *
     * @return int|null
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return BrandEvidences
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set source.
     *
     * @param \AppBundle\Entity\Source|null $source
     *
     * @return BrandEvidences
     */
    public function setSource(\AppBundle\Entity\Source $source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return \AppBundle\Entity\Source|null
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set evidence.
     *
     * @param \AppBundle\Entity\Evidence|null $evidence
     *
     * @return BrandEvidences
     */
    public function setEvidence(\AppBundle\Entity\Evidence $evidence = null)
    {
        $this->evidence = $evidence;

        return $this;
    }

    /**
     * Get evidence.
     *
     * @return \AppBundle\Entity\Evidence|null
     */
    public function getEvidence()
    {
        return $this->evidence;
    }

    /**
     * Set brand.
     *
     * @param \AppBundle\Entity\Brand|null $brand
     *
     * @return BrandEvidences
     */
    public function setBrand(\AppBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand.
     *
     * @return \AppBundle\Entity\Brand|null
     */
    public function getBrand()
    {
        return $this->brand;
    }
}
