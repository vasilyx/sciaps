<?php

namespace AppBundle\Entity;
use AppBundle\Traits\TimestampableEntity;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Attribute
 */
class Attribute
{
    use TimestampableEntity;

    const TYPE_YES = 100;
    const TYPE_NO = 200;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $defaultName;

    /**
     * @var string|null
     */
    private $defaultDescription;

    /**
     * @var string|null
     */
    private $symbol;

    /**
     * @var \AppBundle\Entity\Attribute
     */
    private $parent;

    /**
     * Attribute constructor.
     */
    public function __construct()
    {
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set defaultName.
     *
     * @param string|null $defaultName
     *
     * @return Attribute
     */
    public function setDefaultName($defaultName = null)
    {
        $this->defaultName = $defaultName;

        return $this;
    }

    /**
     * Get defaultName.
     *
     * @return string|null
     */
    public function getDefaultName()
    {
        return $this->defaultName;
    }

    /**
     * Set defaultDescription.
     *
     * @param string|null $defaultDescription
     *
     * @return Attribute
     */
    public function setDefaultDescription($defaultDescription = null)
    {
        $this->defaultDescription = $defaultDescription;

        return $this;
    }

    /**
     * Get defaultDescription.
     *
     * @return string|null
     */
    public function getDefaultDescription()
    {
        return $this->defaultDescription;
    }

    /**
     * Set symbol.
     *
     * @param string|null $symbol
     *
     * @return Attribute
     */
    public function setSymbol($symbol = null)
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * Get symbol.
     *
     * @return string|null
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * Set parent.
     *
     * @param \AppBundle\Entity\Attribute|null $parent
     *
     * @return Attribute
     */
    public function setParent(Attribute $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return \AppBundle\Entity\Attribute|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new UniqueEntity(array(
            'fields'  => 'symbol',
        )));
    }
}
