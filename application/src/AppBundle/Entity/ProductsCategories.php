<?php

namespace AppBundle\Entity;
use AppBundle\Traits\TimestampableEntity;

/**
 * ProductsCategories
 */
class ProductsCategories
{
    use TimestampableEntity;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $categoryOrder;

    /**
     * @var \AppBundle\Entity\ProductCategory
     */
    private $productCategory;

    /**
     * @var \AppBundle\Entity\Product
     */
    private $product;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryOrder.
     *
     * @param string|null $categoryOrder
     *
     * @return ProductsCategories
     */
    public function setCategoryOrder($categoryOrder = null)
    {
        $this->categoryOrder = $categoryOrder;

        return $this;
    }

    /**
     * Get categoryOrder.
     *
     * @return string|null
     */
    public function getCategoryOrder()
    {
        return $this->categoryOrder;
    }

    /**
     * Set productCategory.
     *
     * @param \AppBundle\Entity\ProductCategory|null $productCategory
     *
     * @return ProductsCategories
     */
    public function setProductCategory(\AppBundle\Entity\ProductCategory $productCategory = null)
    {
        $this->productCategory = $productCategory;

        return $this;
    }

    /**
     * Get productCategory.
     *
     * @return \AppBundle\Entity\ProductCategory|null
     */
    public function getProductCategory()
    {
        return $this->productCategory;
    }

    /**
     * Set product.
     *
     * @param \AppBundle\Entity\Product|null $product
     *
     * @return ProductsCategories
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return \AppBundle\Entity\Product|null
     */
    public function getProduct()
    {
        return $this->product;
    }
}
