<?php

namespace AppBundle\Entity;
use AppBundle\Traits\TimestampableEntity;
use AppBundle\Utils\BarcodeUtilities as Utils;

/**
 * Barcode
 */
class Barcode
{
    use TimestampableEntity;

    const TYPE_UPCA = 1;
    const TYPE_UPCE = 2;
    const TYPE_EAN8 = 3;
    const TYPE_EAN13 = 4;
    const TYPE_GTIN14 = 5;
    const TYPE_UNKNOWN = 99;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $barcode;

    /**
     * @var string|null
     */
    private $barcodeOriginal;

    /**
     * @var int
     */
    private $barcodeOriginalType;

    /**
     * @var bool $valid Declaration of whether the barcode is a valid barcode or not
     */
    private $valid;

    /**
     * Barcode constructor.
     *
     * @param string $barcode
     */
    public function __construct(string $barcode = '')
    {
        $this->setBarcode($barcode);
        $this->setBarcodeOriginal($barcode);
        $this->setBarcodeOriginalType(Utils::getBarcodeType($this->barcode));

        //if (!$this->isValid()) throw new \InvalidArgumentException();
    }

    public static function create($barcode) {
        try {
            return new Barcode($barcode);
        } catch (\InvalidArgumentException $e) {
            return false;
        }
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set barcode.
     *
     * @param string|null $barcode
     *
     * @return Barcode
     */
    public function setBarcode($barcode = null)
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * Get barcode.
     *
     * @return string|null
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * Set barcodeOriginal.
     *
     * @param string|null $barcodeOriginal
     *
     * @return Barcode
     */
    public function setBarcodeOriginal($barcodeOriginal = null)
    {
        $this->barcodeOriginal = $barcodeOriginal;

        return $this;
    }

    /**
     * Get barcodeOriginal.
     *
     * @return string|null
     */
    public function getBarcodeOriginal()
    {
        return $this->barcodeOriginal;
    }

    /**
     * Set barcodeType.
     *
     * @param int|null $barcodeOriginalType
     *
     * @return Barcode
     */
    public function setBarcodeOriginalType(int $barcodeOriginalType = null)
    {
        $this->barcodeOriginalType = $barcodeOriginalType;

        return $this;
    }

    /**
     * Get barcodeType.
     *
     * @return int|null
     */
    public function getBarcodeOriginalType()
    {
        return $this->barcodeOriginalType;
    }

    public function setIsValid(bool $valid)
    {
        $this->valid = $valid;
    }

    public function isValid()
    {
        if ($this->valid) {
            $valid = $this->valid;
        } else {
            $valid = true;

            if (!Utils::isFormatValid($this->barcode)) {
                // This isn't even a valid format - nevermind a valid barcode
                $valid = false;
            }

            if (!Utils::isValidGTIN($this->barcode)) {
                // This isn't a valid GTIN barcode
                $valid = false;
            }

            if (!Utils::getBarcodeType($this->barcode)) {
                // Cannot determine the type of this barcode
                $valid = false;
            }
            $this->setIsValid($valid);
        }

        return $valid;
    }
}
