<?php

namespace AppBundle\Entity;

/**
 * MappingData
 */
class MappingData
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @var string
     */
    private $data;

    /**
     * @var \AppBundle\Entity\ProductProvider
     */
    private $productProvider;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime|null $updatedAt
     *
     * @return MappingData
     */
    public function setUpdatedAt($updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set data.
     *
     * @param string $data
     *
     * @return MappingData
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data.
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set productProvider.
     *
     * @param \AppBundle\Entity\ProductProvider|null $productProvider
     *
     * @return MappingData
     */
    public function setProductProvider(\AppBundle\Entity\ProductProvider $productProvider = null)
    {
        $this->productProvider = $productProvider;

        return $this;
    }

    /**
     * Get productProvider.
     *
     * @return \AppBundle\Entity\ProductProvider|null
     */
    public function getProductProvider()
    {
        return $this->productProvider;
    }
}
