<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
/**
 * Product
 */
class Product
{
    use TimestampableEntity;

    CONST STATUS_ACTIVE = 1;
    CONST STATUS_NOT_ACTIVE = 0;

    CONST VOLUME_UNITS_MG = 1;
    CONST VOLUME_UNITS_G = 2;
    CONST VOLUME_UNITS_KG = 3;
    CONST VOLUME_UNITS_LB = 4;
    CONST VOLUME_UNITS_PERCENT = 5;
    CONST VOLUME_UNITS_ML = 6;
    CONST VOLUME_UNITS_CL = 7;
    CONST VOLUME_UNITS_L = 8;

    CONST FIELD_INGREDIENT = 'ingredients';

    CONST CSV_DEFAULT_MAPPING = [
            'name' => 'Product Title',
            'manufacturer' => '',
            'brand' => 'Brand',
            'barcode' => 'EAN/UPC barcode',
            'remote_id' => 'Product Code',
            'description' => 'Description (web)',
            self::FIELD_INGREDIENT => 'Ingredients'
    ];

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var int|null
     */
    private $version;

    /**
     * @var string|null
     */
    private $remoteId;

    /**
     * @var string|null
     */
    private $holdingCompany;

    /**
     * @var string|null
     */
    private $ingredients;

    /**
     * @var string|null
     */
    private $instructions;

    /**
     * @var string|null
     */
    private $advisoryInformation;

    /**
     * @var string|null
     */
    private $healthBenefits;

    /**
     * @var string|null
     */
    private $volume;

    /**
     * @var int|null
     */
    private $volumeUnits;

    /**
     * @var string|null
     */
    private $uuid;

    /**
     * @var \AppBundle\Entity\Brand
     */
    private $brand;

    /**
     * @var \AppBundle\Entity\Manufacturer
     */
    private $manufacturer;

    /**
     * @var \AppBundle\Entity\Product
     */
    private $parent;

    private $safetyWarnings;

    private $storageAdvisory;

    private $productClaims;

    private $freeFromAdvisory;

    private $countryOfOrigin;

    private $healthConditionsAdvisory;

    private $skinTypesAdvisory;

    private $hairTypesAdvisory;

    private $applicationAreaAdvisory;

    private $packageType;

    private $recyclingInformation;

    private $barcode;

    private $productProvider;

    private $images;

    /**
     * @var int
     */
    private $status = '0';


    public function __construct()
    {
        $this->images = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Product
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Product
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set version.
     *
     * @param int|null $version
     *
     * @return Product
     */
    public function setVersion($version = null)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version.
     *
     * @return int|null
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set remoteId.
     *
     * @param string|null $remoteId
     *
     * @return Product
     */
    public function setRemoteId($remoteId = null)
    {
        $this->remoteId = $remoteId;

        return $this;
    }

    /**
     * Get remoteId.
     *
     * @return string|null
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * Set holdingCompany.
     *
     * @param string|null $holdingCompany
     *
     * @return Product
     */
    public function setHoldingCompany($holdingCompany = null)
    {
        $this->holdingCompany = $holdingCompany;

        return $this;
    }

    /**
     * Get holdingCompany.
     *
     * @return string|null
     */
    public function getHoldingCompany()
    {
        return $this->holdingCompany;
    }

    /**
     * Set instructions.
     *
     * @param string|null $instructions
     *
     * @return Product
     */
    public function setInstructions($instructions = null)
    {
        $this->instructions = $instructions;

        return $this;
    }

    /**
     * Get instructions.
     *
     * @return string|null
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * Set advisoryInformation.
     *
     * @param string|null $advisoryInformation
     *
     * @return Product
     */
    public function setAdvisoryInformation($advisoryInformation = null)
    {
        $this->advisoryInformation = $advisoryInformation;

        return $this;
    }

    /**
     * Get advisoryInformation.
     *
     * @return string|null
     */
    public function getAdvisoryInformation()
    {
        return $this->advisoryInformation;
    }

    /**
     * Set healthBenefits.
     *
     * @param string|null $healthBenefits
     *
     * @return Product
     */
    public function setHealthBenefits($healthBenefits = null)
    {
        $this->healthBenefits = $healthBenefits;

        return $this;
    }

    /**
     * Get healthBenefits.
     *
     * @return string|null
     */
    public function getHealthBenefits()
    {
        return $this->healthBenefits;
    }

    /**
     * Set volume.
     *
     * @param string|null $volume
     *
     * @return Product
     */
    public function setVolume($volume = null)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume.
     *
     * @return string|null
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set volumeUnits.
     *
     * @param int|null $volumeUnits
     *
     * @return Product
     */
    public function setVolumeUnits($volumeUnits = null)
    {
        $this->volumeUnits = $volumeUnits;

        return $this;
    }

    /**
     * Get volumeUnits.
     *
     * @return int|null
     */
    public function getVolumeUnits()
    {
        return $this->volumeUnits;
    }

    /**
     * Set uuid.
     *
     * @param string|null $uuid
     *
     * @return Product
     */
    public function setUuid($uuid = null)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid.
     *
     * @return string|null
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set brand.
     *
     * @param \AppBundle\Entity\Brand|null $brand
     *
     * @return Product
     */
    public function setBrand(\AppBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand.
     *
     * @return \AppBundle\Entity\Brand|null
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set manufacturer.
     *
     * @param \AppBundle\Entity\Manufacturer|null $manufacturer
     *
     * @return Product
     */
    public function setManufacturer(\AppBundle\Entity\Manufacturer $manufacturer = null)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer.
     *
     * @return \AppBundle\Entity\Manufacturer|null
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set parent.
     *
     * @param \AppBundle\Entity\Product|null $parent
     *
     * @return Product
     */
    public function setParent(\AppBundle\Entity\Product $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return \AppBundle\Entity\Product|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function getSafetyWarnings(): ?string
    {
        return $this->safetyWarnings;
    }

    public function setSafetyWarnings(?string $safetyWarnings): self
    {
        $this->safetyWarnings = $safetyWarnings;

        return $this;
    }

    public function getStorageAdvisory(): ?string
    {
        return $this->storageAdvisory;
    }

    public function setStorageAdvisory(?string $storageAdvisory): self
    {
        $this->storageAdvisory = $storageAdvisory;

        return $this;
    }

    public function getProductClaims(): ?string
    {
        return $this->productClaims;
    }

    public function setProductClaims(?string $productClaims): self
    {
        $this->productClaims = $productClaims;

        return $this;
    }

    public function getFreeFromAdvisory(): ?string
    {
        return $this->freeFromAdvisory;
    }

    public function setFreeFromAdvisory(?string $freeFromAdvisory): self
    {
        $this->freeFromAdvisory = $freeFromAdvisory;

        return $this;
    }

    public function getCountryOfOrigin(): ?string
    {
        return $this->countryOfOrigin;
    }

    public function setCountryOfOrigin(?string $countryOfOrigin): self
    {
        $this->countryOfOrigin = $countryOfOrigin;

        return $this;
    }

    public function getHealthConditionsAdvisory(): ?string
    {
        return $this->healthConditionsAdvisory;
    }

    public function setHealthConditionsAdvisory(?string $healthConditionsAdvisory): self
    {
        $this->healthConditionsAdvisory = $healthConditionsAdvisory;

        return $this;
    }

    public function getSkinTypesAdvisory(): ?string
    {
        return $this->skinTypesAdvisory;
    }

    public function setSkinTypesAdvisory(?string $skinTypesAdvisory): self
    {
        $this->skinTypesAdvisory = $skinTypesAdvisory;

        return $this;
    }

    public function getHairTypesAdvisory(): ?string
    {
        return $this->hairTypesAdvisory;
    }

    public function setHairTypesAdvisory(?string $hairTypesAdvisory): self
    {
        $this->hairTypesAdvisory = $hairTypesAdvisory;

        return $this;
    }

    public function getApplicationAreaAdvisory(): ?string
    {
        return $this->applicationAreaAdvisory;
    }

    public function setApplicationAreaAdvisory(?string $applicationAreaAdvisory): self
    {
        $this->applicationAreaAdvisory = $applicationAreaAdvisory;

        return $this;
    }

    public function getPackageType(): ?int
    {
        return $this->packageType;
    }

    public function setPackageType(?int $packageType): self
    {
        $this->packageType = $packageType;

        return $this;
    }

    public function getRecyclingInformation(): ?string
    {
        return $this->recyclingInformation;
    }

    public function setRecyclingInformation(?string $recyclingInformation): self
    {
        $this->recyclingInformation = $recyclingInformation;

        return $this;
    }

    public function getBarcode(): ?Barcode
    {
        return $this->barcode;
    }

    public function setBarcode(?Barcode $barcode): self
    {
        $this->barcode = $barcode;

        return $this;
    }

    public function getProductProvider(): ?ProductProvider
    {
        return $this->productProvider;
    }

    public function setProductProvider(?ProductProvider $productProvider): self
    {
        $this->productProvider = $productProvider;

        return $this;
    }

    /**
     * @return Collection|ProductImage[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(ProductImage $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setProduct($this);
        }

        return $this;
    }

    public function removeImage(ProductImage $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getProduct() === $this) {
                $image->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getBarcodeList()
    {
        return $this->getBarcode()->getBarcode();
    }

    /**
     * @return string
     */
    public function getBrandList()
    {
        return $this->getBrand() ? $this->getBrand()->getName() : "";
    }

    //TODO rebuild status
    /**
     * @return int
     */
    public function getStatusList()
    {
        return 0;
    }

    /**
     * @return string
     */
    public function getProductProviderList()
    {
        return $this->getProductProvider() ? $this->getProductProvider()->getCompany()->getCompanyName() : "";
    }

    /**
     * Set status.
     *
     * @param int $status
     *
     * @return Product
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $productsIngredients;


    /**
     * Add productsIngredient.
     *
     * @param \AppBundle\Entity\ProductsIngredients $productsIngredient
     *
     * @return Product
     */
    public function addProductsIngredient(\AppBundle\Entity\ProductsIngredients $productsIngredient)
    {
        $this->productsIngredients[] = $productsIngredient;

        return $this;
    }

    /**
     * Remove productsIngredient.
     *
     * @param ProductsIngredients $productsIngredient
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeProductsIngredient(ProductsIngredients $productsIngredient)
    {
        return $this->productsIngredients->removeElement($productsIngredient);
    }

    /**
     * Get productsIngredients.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductsIngredients()
    {
        return $this->productsIngredients;
    }

    /**
     * @param null|string $ingredients
     * @return Product
     */
    public function setIngredients(?string $ingredients): Product
    {
        $this->ingredients = $ingredients;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getIngredients(): ?string
    {
        return $this->ingredients;
    }

    /**
     * @var \AppBundle\Entity\Retailer
     */
    private $retailer;


    /**
     * Set retailer.
     *
     * @param \AppBundle\Entity\Retailer|null $retailer
     *
     * @return Product
     */
    public function setRetailer(\AppBundle\Entity\Retailer $retailer = null)
    {
        $this->retailer = $retailer;

        return $this;
    }

    /**
     * Get retailer.
     *
     * @return \AppBundle\Entity\Retailer|null
     */
    public function getRetailer()
    {
        return $this->retailer;
    }
}
