<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;

class Source
{
    use TimestampableEntity;

    const TYPE_ONLINE_ARTICLE = 100;
    const TYPE_PDF = 200;
    const TYPE_OFFLINE_ARTICLE = 300;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $title;

    /**
     * @var string|null
     */
    private $nameAuthor;

    /**
     * @var string|null
     */
    private $articleURL;

    /**
     * @var int|null
     */
    private $articleType;

    /**
     * @var \DateTime|null
     */
    private $importDataDate;

    /**
     * @var \DateTime|null
     */
    private $evidenceUploadDate;

    /**
     * @var \DateTime|null
     */
    private $lastCheckedDate;

    /**
     * @var \AppBundle\Entity\Company
     */
    private $company;

    /**
     * Source constructor.
     */
    public function __construct()
    {
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return Source
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set nameAuthor.
     *
     * @param string|null $nameAuthor
     *
     * @return Source
     */
    public function setNameAuthor($nameAuthor = null)
    {
        $this->nameAuthor = $nameAuthor;

        return $this;
    }

    /**
     * Get nameAuthor.
     *
     * @return string|null
     */
    public function getNameAuthor()
    {
        return $this->nameAuthor;
    }

    /**
     * Set articleURL.
     *
     * @param string|null $articleURL
     *
     * @return Source
     */
    public function setArticleURL($articleURL = null)
    {
        $this->articleURL = $articleURL;

        return $this;
    }

    /**
     * Get articleURL.
     *
     * @return string|null
     */
    public function getArticleURL()
    {
        return $this->articleURL;
    }

    /**
     * Set importDataDate.
     *
     * @param \DateTime|null $importDataDate
     *
     * @return Source
     */
    public function setImportDataDate($importDataDate = null)
    {
        $this->importDataDate = $importDataDate;

        return $this;
    }

    /**
     * Get importDataDate.
     *
     * @return \DateTime|null
     */
    public function getImportDataDate()
    {
        return $this->importDataDate;
    }

    /**
     * Set evidenceUploadDate.
     *
     * @param \DateTime|null $evidenceUploadDate
     *
     * @return Source
     */
    public function setEvidenceUploadDate($evidenceUploadDate = null)
    {
        $this->evidenceUploadDate = $evidenceUploadDate;

        return $this;
    }

    /**
     * Get evidenceUploadDate.
     *
     * @return \DateTime|null
     */
    public function getEvidenceUploadDate()
    {
        return $this->evidenceUploadDate;
    }

    /**
     * Set lastCheckedDate.
     *
     * @param \DateTime|null $lastCheckedDate
     *
     * @return Source
     */
    public function setLastCheckedDate($lastCheckedDate = null)
    {
        $this->lastCheckedDate = $lastCheckedDate;

        return $this;
    }

    /**
     * Get lastCheckedDate.
     *
     * @return \DateTime|null
     */
    public function getLastCheckedDate()
    {
        return $this->lastCheckedDate;
    }

    /**
     * Set company.
     *
     * @param \AppBundle\Entity\Company $company
     *
     * @return Source
     */
    public function setCompany(\AppBundle\Entity\Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \AppBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set articleType.
     *
     * @param int|null $articleType
     *
     * @return Source
     */
    public function setArticleType($articleType = null)
    {
        $this->articleType = $articleType;

        return $this;
    }

    /**
     * Get articleType.
     *
     * @return int|null
     */
    public function getArticleType()
    {
        return $this->articleType;
    }
}
