<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Company
 */
class Company
{
    use TimestampableEntity;

    const PRODUCT_TYPE = 100;
    const SOURCE_TYPE = 200;
    const DATAENTRY_TYPE = 300;

    /**
     * @var int COMPANY_DEFAULT_ID
     * SkinNinja default company id
     */
    const COMPANY_DEFAULT_ID = 1000;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $companyName;

    /**
     * @var string|null
     */
    private $websiteURL;


    /**
     * @var string|null
     */

    private $phone;
    /**
     * @var string|null
     */
    private $emailDomain;

    private $users;

    /**
     * @var \AppBundle\Entity\Evidence
     */
    private $evidence;

    private $companyServices;

    /**
     * @var array
     */
    public $services;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->companyServices = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get $services.
     *
     * @return array
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @return null|array
     */
    public function setServices($services = null)
    {
        $this->services = $services;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    /**
     * @param null|string $companyName
     *
     * @return Company
     */
    public function setCompanyName(?string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getWebsiteURL(): ?string
    {
        return $this->websiteURL;
    }

    /**
     * @param null|string $websiteURL
     *
     * @return Company
     */
    public function setWebsiteURL(?string $websiteURL): self
    {
        $this->websiteURL = $websiteURL;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param null|string $phone
     *
     * @return Company
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getEmailDomain(): ?string
    {
        return $this->emailDomain;
    }

    /**
     * @param null|string $emailDomain
     *
     * @return Company
     */
    public function setEmailDomain(?string $emailDomain): self
    {
        $this->emailDomain = $emailDomain;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }

        return $this;
    }

    /**
     * Set evidence.
     *
     * @param \AppBundle\Entity\Evidence|null $evidence
     *
     * @return Company
     */
    public function setEvidence(\AppBundle\Entity\Evidence $evidence = null)
    {
        $this->evidence = $evidence;

        return $this;
    }

    /**
     * Get evidence.
     *
     * @return \AppBundle\Entity\Evidence|null
     */
    public function getEvidence()
    {
        return $this->evidence;
    }

    /**
     * Add companyServices.
     *
     * @param \AppBundle\Entity\CompanyServices $companyServices
     *
     * @return Company
     */
    public function addCompanyServices(\AppBundle\Entity\CompanyServices $companyServices)
    {
        $this->companyServices[] = $companyServices;

        return $this;
    }

    /**
     * Remove companyServices.
     *
     * @param \AppBundle\Entity\CompanyServices $companyServices
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompanyServices(\AppBundle\Entity\CompanyServices $companyServices)
    {
        return $this->companyServices->removeElement($companyServices);
    }

    /**
     * Get Collection|CompanyServices[].
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanyServices()
    {
        return $this->companyServices;
    }

    /**
     * @var \AppBundle\Entity\ProductProvider
     */
    private $productProvider;


    /**
     * Set productProvider.
     *
     * @param \AppBundle\Entity\ProductProvider|null $productProvider
     *
     * @return Company
     */
    public function setProductProvider(\AppBundle\Entity\ProductProvider $productProvider = null)
    {
        $this->productProvider = $productProvider;

        return $this;
    }

    /**
     * Get productProvider.
     *
     * @return \AppBundle\Entity\ProductProvider|null
     */
    public function getProductProvider()
    {
        return $this->productProvider;
    }

    /**
     * Add companyService.
     *
     * @param \AppBundle\Entity\CompanyServices $companyService
     *
     * @return Company
     */
    public function addCompanyService(\AppBundle\Entity\CompanyServices $companyService)
    {
        $this->companyServices[] = $companyService;

        return $this;
    }

    /**
     * Remove companyService.
     *
     * @param \AppBundle\Entity\CompanyServices $companyService
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompanyService(\AppBundle\Entity\CompanyServices $companyService)
    {
        return $this->companyServices->removeElement($companyService);
    }

    /**
     * @var \AppBundle\Entity\SourceDataProvider
     */
    private $sourceDataProvider;

    /**
     * @var \AppBundle\Entity\DataEntryProvider
     */
    private $dataEntryProvider;


    /**
     * Set sourceDataProvider.
     *
     * @param \AppBundle\Entity\SourceDataProvider|null $sourceDataProvider
     *
     * @return Company
     */
    public function setSourceDataProvider(\AppBundle\Entity\SourceDataProvider $sourceDataProvider = null)
    {
        $this->sourceDataProvider = $sourceDataProvider;

        return $this;
    }

    /**
     * Get sourceDataProvider.
     *
     * @return \AppBundle\Entity\SourceDataProvider|null
     */
    public function getSourceDataProvider()
    {
        return $this->sourceDataProvider;
    }

    /**
     * Set dataEntryProvider.
     *
     * @param \AppBundle\Entity\DataEntryProvider|null $dataEntryProvider
     *
     * @return Company
     */
    public function setDataEntryProvider(\AppBundle\Entity\DataEntryProvider $dataEntryProvider = null)
    {
        $this->dataEntryProvider = $dataEntryProvider;

        return $this;
    }

    /**
     * Get dataEntryProvider.
     *
     * @return \AppBundle\Entity\DataEntryProvider|null
     */
    public function getDataEntryProvider()
    {
        return $this->dataEntryProvider;
    }
    /**
     * @var string|null
     */
    private $displayName;


    /**
     * Set displayName.
     *
     * @param string|null $displayName
     *
     * @return Company
     */
    public function setDisplayName($displayName = null)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName.
     *
     * @return string|null
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new UniqueEntity(array(
            'fields'  => 'companyName',
            'message' => 'This value is already used'
        )));

        $metadata->addConstraint(new UniqueEntity(array(
            'fields'  => 'displayName',
            'message' => 'This value is already used'
        )));

        $metadata->addConstraint(new UniqueEntity(array(
            'fields'  => 'websiteURL',
            'message' => 'This value is already used'
        )));

        $metadata->addConstraint(new UniqueEntity(array(
            'fields'  => 'emailDomain',
            'message' => 'This value is already used'
        )));
    }
}
