<?php

namespace AppBundle\Entity;

/**
 * Synonym
 */
class Synonym
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $cid;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cid.
     *
     * @param int $cid
     *
     * @return Synonym
     */
    public function setCid($cid)
    {
        $this->cid = $cid;

        return $this;
    }

    /**
     * Get cid.
     *
     * @return int
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Synonym
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
