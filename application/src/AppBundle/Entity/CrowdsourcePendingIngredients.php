<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;

/**
 * CrowdsourcePendingIngredients
 */
class CrowdsourcePendingIngredients
{
    use TimestampableEntity;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $ingredientOrder;

    /**
     * @var \AppBundle\Entity\Ingredient
     */
    private $ingredient;

    /**
     * @var \AppBundle\Entity\CrowdsourcePending
     */
    private $crowdsource_pending;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ingredientOrder.
     *
     * @param int|null $ingredientOrder
     *
     * @return CrowdsourcePendingIngredients
     */
    public function setIngredientOrder($ingredientOrder = null)
    {
        $this->ingredientOrder = $ingredientOrder;

        return $this;
    }

    /**
     * Get ingredientOrder.
     *
     * @return int|null
     */
    public function getIngredientOrder()
    {
        return $this->ingredientOrder;
    }

    /**
     * Set ingredient.
     *
     * @param \AppBundle\Entity\Ingredient|null $ingredient
     *
     * @return CrowdsourcePendingIngredients
     */
    public function setIngredient(\AppBundle\Entity\Ingredient $ingredient = null)
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    /**
     * Get ingredient.
     *
     * @return \AppBundle\Entity\Ingredient|null
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }

    /**
     * Set crowdsourcePending.
     *
     * @param \AppBundle\Entity\CrowdsourcePending|null $crowdsourcePending
     *
     * @return CrowdsourcePendingIngredients
     */
    public function setCrowdsourcePending(\AppBundle\Entity\CrowdsourcePending $crowdsourcePending = null)
    {
        $this->crowdsource_pending = $crowdsourcePending;

        return $this;
    }

    /**
     * Get crowdsourcePending.
     *
     * @return \AppBundle\Entity\CrowdsourcePending|null
     */
    public function getCrowdsourcePending()
    {
        return $this->crowdsource_pending;
    }
}
