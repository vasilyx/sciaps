<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

use AppBundle\Traits\TimestampableEntity;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 */
class User
{
    use TimestampableEntity;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_SUSPENDED = 2;
    const STATUS_WRONG_PASSWORD = 3;
    const STATUS_LONG_TIME_INACTIVE = 4;

    /*
    const ROLE_ADMIN = 1;
    const ROLE_SKINNINJA = 2;
    const ROLE_DATA_PROVIDER = 3;
    const ROLE_EVIDENCE_PROVIDER = 4;
    const ROLE_CONTRACTOR = 5;

    const ROLE_TITLE = [
        self::ROLE_ADMIN => 'ROLE_ADMIN',
        self::ROLE_SKINNINJA => 'ROLE_SKINNINJA',
        self::ROLE_DATA_PROVIDER => 'ROLE_DATA_PROVIDER ',
        self::ROLE_EVIDENCE_PROVIDER => 'ROLE_EVIDENCE_PROVIDER',
        self::ROLE_CONTRACTOR => 'ROLE_CONTRACTOR',
    ];
    */

    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var \DateTime|null
     */
    private $lastLogin;

    /**
     * @var \DateTime|null
     */
    private $currentLogin;

    /**
     * @var string|null
     */
    private $verificationCode;

    private $permission;

    public function __construct()
    {
        $this->permission = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return User
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set lastLogin.
     *
     * @param \DateTime|null $lastLogin
     *
     * @return User
     */
    public function setLastLogin($lastLogin = null)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Get lastLogin.
     *
     * @return \DateTime|null
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set currentLogin.
     *
     * @param \DateTime|null $currentLogin
     *
     * @return User
     */
    public function setCurrentLogin($currentLogin = null)
    {
        $this->currentLogin = $currentLogin;

        return $this;
    }

    /**
     * Get currentLogin.
     *
     * @return \DateTime|null
     */
    public function getCurrentLogin()
    {
        return $this->currentLogin;
    }

    /**
     * Set verificationCode.
     *
     * @param string|null $verificationCode
     *
     * @return User
     */
    public function setVerificationCode($verificationCode = null)
    {
        $this->verificationCode = $verificationCode;

        return $this;
    }

    /**
     * Get verificationCode.
     *
     * @return string|null
     */
    public function getVerificationCode()
    {
        return $this->verificationCode;
    }

    /**
     * @return Collection|Permission[]
     */
    public function getPermission(): Collection
    {
        return $this->permission;
    }

    public function addPermission(Permission $permission): self
    {
        if (!$this->permission->contains($permission)) {
            $this->permission[] = $permission;
            $permission->setUser($this);
        }

        return $this;
    }

    public function removePermission(Permission $permission): self
    {
        if ($this->permission->contains($permission)) {
            $this->permission->removeElement($permission);
            // set the owning side to null (unless already changed)
            if ($permission->getUser() === $this) {
                $permission->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function generateConfirmationToken()
    {
        $this->setConfirmEmailToken(
            base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
        );
        return $this;
    }

    /**
     * @return $this
     */
    public function generateForgotPasswordToken()
    {
        $this->setForgottenPasswordToken(
            base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
        );
        return $this;
    }


    /**
     * @var string|null
     */
    private $forgottenPasswordToken;

    /**
     * @var string|null
     */
    private $confirmEmailToken;

    /**
     * @var int|null
     */
    private $status = '0';


    /**
     * Set forgottenPasswordToken.
     *
     * @param string|null $forgottenPasswordToken
     *
     * @return User
     */
    public function setForgottenPasswordToken($forgottenPasswordToken = null)
    {
        $this->forgottenPasswordToken = $forgottenPasswordToken;

        return $this;
    }

    /**
     * Get forgottenPasswordToken.
     *
     * @return string|null
     */
    public function getForgottenPasswordToken()
    {
        return $this->forgottenPasswordToken;
    }

    /**
     * Set confirmEmailToken.
     *
     * @param string|null $confirmEmailToken
     *
     * @return User
     */
    public function setConfirmEmailToken($confirmEmailToken = null)
    {
        $this->confirmEmailToken = $confirmEmailToken;

        return $this;
    }

    /**
     * Get confirmEmailToken.
     *
     * @return string|null
     */
    public function getConfirmEmailToken()
    {
        return $this->confirmEmailToken;
    }

    /**
     * Set status.
     *
     * @param int|null $status
     *
     * @return User
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getSalt()
    {
        return null;
    }

    public function __toString(){
        return (string)$this->getEmail();
    }

    /**
     * Needed for standard encoders
     * @return null|string
     */
    public function getUsername()
    {
        return $this->email;
    }
    /**
     * @var \DateTime|null
     */
    private $datetimeForgottenPassword;


    /**
     * Set datetimeForgottenPassword.
     *
     * @param \DateTime|null $datetimeForgottenPassword
     *
     * @return User
     */
    public function setDatetimeForgottenPassword($datetimeForgottenPassword = null)
    {
        $this->datetimeForgottenPassword = $datetimeForgottenPassword;

        return $this;
    }

    /**
     * Get datetimeForgottenPassword.
     *
     * @return \DateTime|null
     */
    public function getDatetimeForgottenPassword()
    {
        return $this->datetimeForgottenPassword;
    }

    /**
     * @var \AppBundle\Entity\Company
     */
    private $company;


    /**
     * Set company.
     *
     * @param \AppBundle\Entity\Company $company
     *
     * @return User
     */
    public function setCompany(\AppBundle\Entity\Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \AppBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @var string|null
     */
    private $surname;

    /**
     * Set surname.
     *
     * @param string|null $surname
     *
     * @return User
     */
    public function setSurname($surname = null)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname.
     *
     * @return string|null
     */
    public function getSurname()
    {
        return $this->surname;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new UniqueEntity(array(
            'fields'  => 'email',
        )));

        $metadata->addPropertyConstraint('email', new Assert\Email());
    }

    /**
     * @var string|null
     */
    private $firstName;


    /**
     * Set firstName.
     *
     * @param string|null $firstName
     *
     * @return User
     */
    public function setFirstName($firstName = null)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $logEvents;


    /**
     * Add logEvent.
     *
     * @param \AppBundle\Entity\LogEvent $logEvent
     *
     * @return User
     */
    public function addLogEvent(\AppBundle\Entity\LogEvent $logEvent)
    {
        $this->logEvents[] = $logEvent;

        return $this;
    }

    /**
     * Remove logEvent.
     *
     * @param \AppBundle\Entity\LogEvent $logEvent
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeLogEvent(\AppBundle\Entity\LogEvent $logEvent)
    {
        return $this->logEvents->removeElement($logEvent);
    }

    /**
     * Get logEvents.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLogEvents()
    {
        return $this->logEvents;
    }
}
