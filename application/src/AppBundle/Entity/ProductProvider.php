<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * ProductProvider
 */
class ProductProvider
{
    use TimestampableEntity;

    const TYPE_SKINNINJA = 1;
    const TYPE_CROWDSOURCE = 2;
    const TYPE_RETAILER = 3;
    const TYPE_BRAND = 4;
    const TYPE_MANUFACTURER = 5;
    const TYPE_HOLDING_COMPANY = 6;
    const TYPE_DIRECT_AFFILIATE = 7;
    const TYPE_AFFILIATE_NETWORK = 8;

    const CONNECTION_API = 1;
    const CONNECTION_CSV_BASIC = 2;
    const CONNECTION_XML_BASIC = 3;
    const CONNECTION_CSV_ADV = 4;
    const CONNECTION_XML_ADV = 5;
    const CONNECTION_WEB_SCRAPE= 6;

    /**
     * @var int PRODUCT_PROVIDER_DEFAULT_ID
     * SkinNinja default provider id
     */
    const PRODUCT_PROVIDER_DEFAULT_ID = 1000;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $connectionType;

    /**
     * @var bool|null
     */
    private $status;

    /**
     * @var int|null
     */
    private $productProviderType;

    private $products;


    /**
     * ProductProvider constructor.
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->productProviderType = 0;
        $this->connectionType = 0;
        $this->status = 0;
        $this->ingestionReports = 0;
        $this->ingestionAnalytics = 0;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return ProductProvider
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status.
     *
     * @param bool|null $status
     *
     * @return ProductProvider
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get ingestionAnalytics.
     *
     * @return bool|null
     */
    public function getIngestionAnalytics()
    {
        return $this->ingestionAnalytics;
    }

    /**
     * Set productProviderType.
     *
     * @param int|null $productProviderType
     *
     * @return ProductProvider
     */
    public function setProductProviderType(int $productProviderType = null)
    {
        $this->productProviderType = $productProviderType;

        return $this;
    }

    /**
     * Get productProviderType.
     *
     * @return int|null
     */
    public function getProductProviderType()
    {
        return $this->productProviderType;
    }

    /**
     * @return Collection|ProvidersProducts[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct($product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setProductProvider($this);
        }

        return $this;
    }

    public function removeProduct($product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getProductProvider() === $this) {
                $product->setProductProvider(null);
            }
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getConnectionType(): ?int
    {
        return $this->connectionType;
    }

    /**
     * @param int|null $connectionType
     */
    public function setConnectionType(?int $connectionType): void
    {
        $this->connectionType = $connectionType;
    }

    /**
     * @var \AppBundle\Entity\Company
     */
    private $company;


    /**
     * Set company.
     *
     * @param \AppBundle\Entity\Company|null $company
     *
     * @return ProductProvider
     */
    public function setCompany(\AppBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \AppBundle\Entity\Company|null
     */
    public function getCompany()
    {
        return $this->company;
    }
    /**
     * @var int|null
     */
    private $ingestionReports;

    /**
     * @var int|null
     */
    private $ingestionAnalytics;


    /**
     * Set ingestionReports.
     *
     * @param int|null $ingestionReports
     *
     * @return ProductProvider
     */
    public function setIngestionReports($ingestionReports = null)
    {
        $this->ingestionReports = $ingestionReports;

        return $this;
    }

    /**
     * Get ingestionReports.
     *
     * @return int|null
     */
    public function getIngestionReports()
    {
        return $this->ingestionReports;
    }

    /**
     * Set ingestionAnalytics.
     *
     * @param int|null $ingestionAnalytics
     *
     * @return ProductProvider
     */
    public function setIngestionAnalytics($ingestionAnalytics = null)
    {
        $this->ingestionAnalytics = $ingestionAnalytics;

        return $this;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new UniqueEntity(array(
            'fields'  => 'company',
            'message' => 'This value is already used'
        )));
    }
    /**
     * @var \AppBundle\Entity\MappingData
     */
    private $mappingData;


    /**
     * Set mappingData.
     *
     * @param \AppBundle\Entity\MappingData|null $mappingData
     *
     * @return ProductProvider
     */
    public function setMappingData(\AppBundle\Entity\MappingData $mappingData = null)
    {
        $this->mappingData = $mappingData;

        return $this;
    }

    /**
     * Get mappingData.
     *
     * @return \AppBundle\Entity\MappingData|null
     */
    public function getMappingData()
    {
        return $this->mappingData;
    }
}
