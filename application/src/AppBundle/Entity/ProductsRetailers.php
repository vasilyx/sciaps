<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;
/**
 * ProductsRetailers
 */
class ProductsRetailers
{
    use TimestampableEntity;

    /**
     * @var int
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Product
     */
    private $product;

    /**
     * @var \AppBundle\Entity\Retailer
     */
    private $retailer;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product.
     *
     * @param \AppBundle\Entity\Product|null $product
     *
     * @return ProductsRetailers
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return \AppBundle\Entity\Product|null
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set retailer.
     *
     * @param \AppBundle\Entity\Retailer|null $retailer
     *
     * @return ProductsRetailers
     */
    public function setRetailer(\AppBundle\Entity\Retailer $retailer = null)
    {
        $this->retailer = $retailer;

        return $this;
    }

    /**
     * Get retailer.
     *
     * @return \AppBundle\Entity\Retailer|null
     */
    public function getRetailer()
    {
        return $this->retailer;
    }
}
