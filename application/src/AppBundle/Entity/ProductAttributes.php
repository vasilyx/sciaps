<?php

namespace AppBundle\Entity;

use AppBundle\Traits\TimestampableEntity;

/**
 * ProductAttributes
 */
class ProductAttributes
{
    use TimestampableEntity;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int|null
     */
    private $statusCode;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var \AppBundle\Entity\Source
     */
    private $source;

    /**
     * @var \AppBundle\Entity\Attribute
     */
    private $attribute;

    /**
     * @var \AppBundle\Entity\Product
     */
    private $product;

    private $products;

    public function __construct()
    {
    }

    /**
     * Get $products.
     *
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return null|array
     */
    public function setProducts($products = null)
    {
        $this->products = $products;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statusCode.
     *
     * @param int|null $statusCode
     *
     * @return ProductAttributes
     */
    public function setStatusCode($statusCode = null)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Get statusCode.
     *
     * @return int|null
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return ProductAttributes
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set source.
     *
     * @param \AppBundle\Entity\Source|null $source
     *
     * @return ProductAttributes
     */
    public function setSource(\AppBundle\Entity\Source $source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return \AppBundle\Entity\Source|null
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set attribute.
     *
     * @param \AppBundle\Entity\Attribute|null $attribute
     *
     * @return ProductAttributes
     */
    public function setAttribute(\AppBundle\Entity\Attribute $attribute = null)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get attribute.
     *
     * @return \AppBundle\Entity\Attribute|null
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set product.
     *
     * @param \AppBundle\Entity\Product|null $product
     *
     * @return ProductAttributes
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return \AppBundle\Entity\Product|null
     */
    public function getProduct()
    {
        return $this->product;
    }
}
