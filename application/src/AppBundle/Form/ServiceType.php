<?php

namespace AppBundle\Form;

use AppBundle\Entity\Company;
use AppBundle\Entity\CompanyServices;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;


class ServiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('serviceType', TextType::class)
            ->add('serviceId', TextType::class)
            ->add('company',EntityType::class, [
                'required' => true,
                'class'   => Company::class,
                'choice_label' => 'description',
                'choice_value' => 'id',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('est');
                },
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Please enter a valid company'
                    ]),
                ]
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CompanyServices::class
        ));
    }

    public function getName()
    {
        return 'companyService';
    }

}