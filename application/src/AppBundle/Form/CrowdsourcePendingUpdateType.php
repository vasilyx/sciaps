<?php

namespace AppBundle\Form;

use AppBundle\Entity\Barcode;
use AppBundle\Entity\Brand;
use AppBundle\Entity\CrowdsourcePending;
use AppBundle\Entity\Manufacturer;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class CrowdsourcePendingUpdateType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('barcode', EntityType::class, [
                'required' => true,
                'class'   => Barcode::class,
                'choice_label' => 'description',
                'choice_value' => 'id',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('est');
                }
            ])
            ->add('name', TextType::class, [
                'required' => false,
                'constraints' => [
                    new Assert\Length([
                        'max'        => 45,
                        'maxMessage' => 'Your first name cannot be longer than {{ limit }} characters',
                    ]),
                ]
            ])
            ->add('description', TextType::class)
            ->add('brand', EntityType::class, [
                'required' => false,
                'class'   => Brand::class,
                'choice_label' => 'description',
                'choice_value' => 'id',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('est');
                }
            ])
            ->add('manufacturer', EntityType::class, [
                'required' => false,
                'class'   => Manufacturer::class,
                'choice_label' => 'description',
                'choice_value' => 'id',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('est');
                }
            ])
            ->add('volume', IntegerType::class, [
                'required' => false
            ])
            ->add('volumeUnits', IntegerType::class, [
                'required' => false
            ])
            ->add('status', IntegerType::class, [
                'required' => false
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CrowdsourcePending::class
        ));
    }

    public function getName()
    {
        return 'crowdsourcePendingUpdate';
    }
}