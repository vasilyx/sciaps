<?php

namespace AppBundle\Form;

use AppBundle\Entity\Country;
use AppBundle\Entity\Organization;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class OrganizationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyName', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 45,
                        'maxMessage' => 'Your Company name cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('websiteURL', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\Url(),
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 45,
                        'maxMessage' => 'Your Website URL cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('barcode', NumberType::class)
            ->add('countries', EntityType::class, [
                'required' => true,
                'multiple'  => true,
                'class'   => Country::class,
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('est');
                },
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Please enter a valid product provider'
                    ]),
                ]
            ])
            ->add('productsProvidedVia', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ])
                ]
            ])
            ->add('productsSoldThrough', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ])
                ]
            ])
            ->add('numberProducts', NumberType::class,[
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                ]
            ])
            ->add('personName', TextType::class,[
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 45,
                        'maxMessage' => 'Your name cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('personEmail', TextType::class,[
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 45,
                        'maxMessage' => 'Your Email cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('personComment', TextType::class)
        ;
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Organization::class
        ));
    }

    public function getName()
    {
        return 'organization';
    }
}