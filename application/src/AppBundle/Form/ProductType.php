<?php

namespace AppBundle\Form;

use AppBundle\Entity\Barcode;
use AppBundle\Entity\Brand;
use AppBundle\Entity\Manufacturer;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductProvider;
use AppBundle\Entity\Retailer;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ProductType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 45,
                        'maxMessage' => 'Your name cannot be longer than {{ limit }} characters',
                    ]),
                ]
            ])
            ->add('description', TextType::class)
            ->add('brand', EntityType::class, [
                'required' => true,
                'class'   => Brand::class,
                'choice_label' => 'description',
                'choice_value' => 'id',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('est');
                },
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Please enter a valid brand'
                    ]),
                ]
            ])
            ->add('manufacturer', EntityType::class, [
                'required' => true,
                'class'   => Manufacturer::class,
                'choice_label' => 'description',
                'choice_value' => 'id',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('est');
                },
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Please enter a valid manufacturer'
                    ]),
                ]
            ])
            ->add('barcode', EntityType::class, [
                'required' => true,
                'class'   => Barcode::class,
                'choice_label' => 'description',
                'choice_value' => 'id',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('est');
                },
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Please enter a valid barcode'
                    ]),
                ]
            ])
            ->add('retailer', EntityType::class, [
                'required' => true,
                'class'   => Retailer::class,
                'choice_label' => 'description',
                'choice_value' => 'id',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('est');
                },
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Please enter a valid retailer'
                    ]),
                ]
            ])
            ->add('volume', NumberType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                ]
            ])
            ->add('volumeUnits', NumberType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                ]
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Product::class
        ));
    }

    public function getName()
    {
        return 'product';
    }
}