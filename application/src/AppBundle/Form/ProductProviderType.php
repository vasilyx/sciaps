<?php

namespace AppBundle\Form;

use AppBundle\Entity\ProductProvider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;


class ProductProviderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('productProviderType', NumberType::class,[
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be null',
                    ])
                ]
            ])
            ->add('connectionType', NumberType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be null',
                    ])
                ]
            ])
            ->add('status', NumberType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be null',
                    ])
                ]
            ])
            ->add('ingestionReports', NumberType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be null',
                    ])
                ]
            ])
            ->add('ingestionAnalytics', NumberType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be null',
                    ])
                ]
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ProductProvider::class
        ));
    }

    public function getName()
    {
        return 'productProvider';
    }

}