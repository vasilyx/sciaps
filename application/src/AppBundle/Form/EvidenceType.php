<?php

namespace AppBundle\Form;

use AppBundle\Entity\Evidence;
use Doctrine\ORM\EntityRepository;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;


class EvidenceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('parent', EntityType::class, [
                'required' => false,
                'class'   => Evidence::class,
                'choice_label' => 'description',
                'choice_value' => 'id',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('est');
                }
            ])
            ->add('defaultName', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 45,
                        'maxMessage' => 'Your defaultName cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('defaultDescription', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 65535,
                        'maxMessage' => 'Your defaultDescription cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('symbol', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 4,
                        'maxMessage' => 'Your symbol cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Evidence::class
        ));
    }

    public function getName()
    {
        return 'evidence';
    }
}
