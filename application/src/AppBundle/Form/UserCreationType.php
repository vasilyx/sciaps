<?php

namespace AppBundle\Form;

use AppBundle\AppBundle;
use AppBundle\Entity\Company;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class UserCreationType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 255,
                        'maxMessage' => 'Your email cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('firstName', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 128,
                        'maxMessage' => 'Your first name cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('surname', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 128,
                        'maxMessage' => 'Your surname cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('company',EntityType::class, [
                'required' => true,
                'class'   => Company::class,
                'choice_label' => 'description',
                'choice_value' => 'id',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('est');
                },
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Please enter a valid company'
                    ]),
                ]
            ]);
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class
        ));
    }

    public function getName()
    {
        return 'user';
    }
}