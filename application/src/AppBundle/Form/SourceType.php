<?php

namespace AppBundle\Form;

use AppBundle\Entity\Company;
use AppBundle\Entity\Source;
use AppBundle\Entity\SourceDataProvider;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class SourceType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 45,
                        'maxMessage' => 'Your name cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('nameAuthor', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 45,
                        'maxMessage' => 'Your url cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('articleType', NumberType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                ]
            ])
            ->add('company',EntityType::class, [
                'required' => true,
                'class'   => Company::class,
                'choice_label' => 'description',
                'choice_value' => 'id',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('est');
                },
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Please enter a valid company'
                    ]),
                ]
            ])
            ->add('articleURL', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\Url(),
                    new Assert\NotBlank([
                        'message' => 'Please enter a valid URL'
                    ]),
                    new Assert\Length([
                        'max'        => 45,
                        'maxMessage' => 'Your website url cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            /* TODO check date?
            ->add('evidenceUploadDate', DateTimeType::class, [
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'YYYY-MM-dd HH:mm',
            ])
            ->add('lastCheckedDate', DateTimeType::class, [
                'format' => 'yyyy-MM-dd HH:mm',
                'input' => 'datetime',
                'format' => 'YYYY-MM-dd HH:mm',
            ])
            */
        ;
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Source::class
        ));
    }

    public function getName()
    {
        return 'source';
    }
}