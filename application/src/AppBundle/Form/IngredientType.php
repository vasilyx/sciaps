<?php

namespace AppBundle\Form;

use AppBundle\Entity\Ingredient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class IngredientType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('commonName', TextType::class, [
                'constraints' => [
                    new Assert\Length([
                        'max'        => 1500,
                        'maxMessage' => 'Your defaultName cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('inciName', TextType::class, [
                'constraints' => [
                    new Assert\Length([
                        'max'        => 1500,
                        'maxMessage' => 'Your defaultName cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('innName', TextType::class, [
                'constraints' => [
                    new Assert\Length([
                        'max'        => 1500,
                        'maxMessage' => 'Your defaultName cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('phEurName', TextType::class, [
                'constraints' => [
                    new Assert\Length([
                        'max'        => 1500,
                        'maxMessage' => 'Your defaultName cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('chemicalIupacName', TextType::class, [
                'constraints' => [
                    new Assert\Length([
                        'max'        => 1500,
                        'maxMessage' => 'Your defaultName cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('description', TextType::class, [
                'constraints' => [
                    new Assert\Length([
                        'max'        => 1500,
                        'maxMessage' => 'Your defaultName cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('casNumber', TextType::class, [
                'constraints' => [
                    new Assert\Length([
                        'max'        => 1500,
                        'maxMessage' => 'Your defaultName cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('ecNumber', TextType::class, [
                'constraints' => [
                    new Assert\Length([
                        'max'        => 1500,
                        'maxMessage' => 'Your defaultName cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('cosingNumber', TextType::class, [
                'constraints' => [
                    new Assert\Length([
                        'max'        => 1500,
                        'maxMessage' => 'Your defaultName cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('pubChemId', NumberType::class, [
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be null',
                    ])
                ]
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Ingredient::class
        ));
    }

    public function getName()
    {
        return 'ingredient';
    }
}
