<?php

namespace AppBundle\Form;

use AppBundle\Entity\Company;
use AppBundle\Entity\DataEntryProvider;
use AppBundle\Entity\ProductProvider;
use AppBundle\Entity\SourceDataProvider;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;

class CompanyUpdateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyName', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 45,
                        'maxMessage' => 'Your company name cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('displayName', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 45,
                        'maxMessage' => 'Your display name cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('websiteURL', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\Url(),
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 45,
                        'maxMessage' => 'Your website url cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
            ->add('emailDomain', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank([
                        'message' => 'Can not be empty',
                    ]),
                    new Assert\Length([
                        'max'        => 45,
                        'maxMessage' => 'Your email domain cannot be longer than {{ limit }} characters',
                    ])
                ]
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Company::class
        ));
    }

    public function getName()
    {
        return 'company';
    }
}
