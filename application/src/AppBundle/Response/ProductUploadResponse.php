<?php
namespace AppBundle\Response;


class ProductUploadResponse extends BaseResponse
{
    protected $fields;
    protected $columnTitles;
    protected $mappingData;

    /**
     * @return mixed
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param mixed $fields
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    /**
     * @return mixed
     */
    public function getColumnTitles()
    {
        return $this->columnTitles;
    }

    /**
     * @param mixed $columnTitles
     */
    public function setColumnTitles($columnTitles)
    {
        $this->columnTitles = $columnTitles;
    }

    /**
     * @return mixed
     */
    public function getMappingData()
    {
        return $this->mappingData;
    }

    /**
     * @param mixed $mappingData
     */
    public function setMappingData($mappingData)
    {
        $this->mappingData = $mappingData;
    }
}
