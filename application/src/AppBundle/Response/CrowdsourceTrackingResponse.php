<?php
namespace AppBundle\Response;


class CrowdsourceTrackingResponse
{
    protected $contractorId;
    protected $contractorName;
    protected $productsApproved;
    protected $productsApprovedManager;
    protected $productsArchived;
    protected $productsArchivedManager;
    protected $accuracy;

    /**
     * @return mixed
     */
    public function getProductsArchived()
    {
        return $this->productsArchived;
    }

    /**
     * @param mixed $productsArchived
     */
    public function setProductsArchived($productsArchived)
    {
        $this->productsArchived = $productsArchived;
    }


    /**
     * @return mixed
     */
    public function getContractorId()
    {
        return $this->contractorId;
    }

    /**
     * @param mixed $contractorId
     */
    public function setContractorId($contractorId)
    {
        $this->contractorId = $contractorId;
    }

    /**
     * @return mixed
     */
    public function getContractorName()
    {
        return $this->contractorName;
    }

    /**
     * @param mixed $contractorName
     */
    public function setContractorName($contractorName)
    {
        $this->contractorName = $contractorName;
    }

    /**
     * @return mixed
     */
    public function getProductsApproved()
    {
        return $this->productsApproved;
    }

    /**
     * @param mixed $productsApproved
     */
    public function setProductsApproved($productsApproved)
    {
        $this->productsApproved = $productsApproved;
    }

    /**
     * @return mixed
     */
    public function getProductsApprovedManager()
    {
        return $this->productsApprovedManager;
    }

    /**
     * @param mixed $productsApprovedManager
     */
    public function setProductsApprovedManager($productsApprovedManager)
    {
        $this->productsApprovedManager = $productsApprovedManager;
    }

    /**
     * @return mixed
     */
    public function getProductsArchivedManager()
    {
        return $this->productsArchivedManager;
    }

    /**
     * @param mixed $productsArchivedManager
     */
    public function setProductsArchivedManager($productsArchivedManager)
    {
        $this->productsArchivedManager = $productsArchivedManager;
    }

    /**
     * @return mixed
     */
    public function getAccuracy()
    {
        return $this->accuracy;
    }

    /**
     * @param mixed $accuracy
     */
    public function setAccuracy($accuracy)
    {
        $this->accuracy = $accuracy;
    }
}