<?php
namespace AppBundle\Response;

class ProductUploadConfirmMappingDataResponse extends BaseResponse
{
    protected $productsFound = 0;
    protected $newProducts = 0;
    protected $productsToBeUpdated = 0;

    /**
     * @return int
     */
    public function getProductsFound(): int
    {
        return $this->productsFound;
    }

    /**
     * @param int $productsFound
     */
    public function setProductsFound(int $productsFound)
    {
        $this->productsFound = $productsFound;
    }

    /**
     * @return int
     */
    public function getNewProducts(): int
    {
        return $this->newProducts;
    }

    /**
     * @param int $newProducts
     */
    public function setNewProducts(int $newProducts)
    {
        $this->newProducts = $newProducts;
    }

    /**
     * @return int
     */
    public function getProductsToBeUpdated(): int
    {
        return $this->productsToBeUpdated;
    }

    /**
     * @param int $productsToBeUpdated
     */
    public function setProductsToBeUpdated(int $productsToBeUpdated)
    {
        $this->productsToBeUpdated = $productsToBeUpdated;
    }
}
