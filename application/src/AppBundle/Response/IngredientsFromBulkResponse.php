<?php

namespace AppBundle\Response;


class IngredientsFromBulkResponse
{
    protected $ingredients = [];
    protected $legend = [];


    /**
     * @return array
     */
    public function getLegend(): array
    {
        return $this->legend;
    }

    /**
     * @param array $legend
     * @return $this
     */
    public function setLegend(array $legend)
    {
        $this->legend = $legend;
        return $this;
    }


    /**
     * @return array
     */
    public function getIngredients(): array
    {
        return $this->ingredients;
    }

    /**
     * @param array $ingredients
     * @return $this
     */
    public function setIngredients(array $ingredients)
    {
        $this->ingredients = $ingredients;
        return $this;
    }
}