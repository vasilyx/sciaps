<?php
namespace AppBundle\Response;


class ProductDashboardResponse
{
    protected $totalProducts = 0;
    protected $activeProducts = 0;
    protected $users = 0;
    protected $lastImport;

    /**
     * @return int
     */
    public function getTotalProducts(): int
    {
        return $this->totalProducts;
    }

    /**
     * @param int $totalProducts
     */
    public function setTotalProducts(int $totalProducts)
    {
        $this->totalProducts = $totalProducts;
    }

    /**
     * @return int
     */
    public function getActiveProducts(): int
    {
        return $this->activeProducts;
    }

    /**
     * @param int $activeProducts
     */
    public function setActiveProducts(int $activeProducts)
    {
        $this->activeProducts = $activeProducts;
    }

    /**
     * @return int
     */
    public function getUsers(): int
    {
        return $this->users;
    }

    /**
     * @param int $users
     */
    public function setUsers(int $users)
    {
        $this->users = $users;
    }

    /**
     * @return mixed
     */
    public function getLastImport()
    {
        return $this->lastImport;
    }

    /**
     * @param mixed $lastImport
     */
    public function setLastImport($lastImport)
    {
        $this->lastImport = $lastImport;
    }


}