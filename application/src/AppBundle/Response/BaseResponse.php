<?php
namespace AppBundle\Response;

use Symfony\Component\HttpFoundation\Response;

abstract class BaseResponse
{
    /**
     * @var boolean
     */
    protected $status = true;

    /**
     * @var string[]
     */
    protected $errors;

    /* @var int $statusCode Status code for the response */
    protected $statusCode;

    /**
     * BaseResponse constructor.
     */
    public function __construct()
    {
        $this->statusCode = Response::HTTP_OK;
    }

    /**
     * @param int $statusCode
     * @return BaseResponse
     */
    public function setStatusCode(int $statusCode): BaseResponse
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param string[] $errors
     * @return BaseResponse
     */
    public function setErrors(array $errors): BaseResponse
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getErrors(): ?array
    {
        return $this->errors;
    }


}
