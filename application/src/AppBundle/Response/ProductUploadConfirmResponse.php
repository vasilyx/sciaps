<?php
namespace AppBundle\Response;


class ProductUploadConfirmResponse extends BaseResponse
{
    protected $productsAdded = 0;
    protected $productsUpdated = 0;

    /**
     * @return int
     */
    public function getProductsAdded(): int
    {
        return $this->productsAdded;
    }

    /**
     * @param int $productsAdded
     */
    public function setProductsAdded(int $productsAdded)
    {
        $this->productsAdded = $productsAdded;
    }

    /**
     * @return int
     */
    public function getProductsUpdated(): int
    {
        return $this->productsUpdated;
    }

    /**
     * @param int $productsUpdated
     */
    public function setProductsUpdated(int $productsUpdated)
    {
        $this->productsUpdated = $productsUpdated;
    }
}
