<?php

namespace AppBundle\DataFixtures;

use Faker\Provider\Base as BaseProvider;

class AttributeProvider extends BaseProvider
{
    protected $nameArray = ['Microbeads', 'Vegan', 'Cruelty Free'];

    public function attributeName($current)
    {
        $current = $current - 1;

        if (isset($this->nameArray[$current])) {
            return $this->nameArray[$current];
        } else {
            return $this->nameArray[rand(0, count($this->nameArray) - 1)];
        }

    }
    /**
     * @return string Random symbol
     */
    public function symbol_attr()
    {
        $symbol = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyzQWERTYUIOPLKJHGFDSAZXCVBNM", 4)), 0, 4);
        return $symbol;
    }
}
