<?php

namespace AppBundle\DataFixtures;

use Faker\Provider\Base as BaseProvider;

class Company extends BaseProvider
{
    protected $companyNameArray = ['Nivea', 'L\'Oreal', 'Weleda', 'Bulldog', 'E45'];

    public function companyName($current)
    {
        $current = $current - 1;

        if (isset($this->companyNameArray[$current])) {
            return $this->companyNameArray[$current];
        } else {
            return $this->companyNameArray[rand(0, count($this->companyNameArray) - 1)];
        }

    }
}
