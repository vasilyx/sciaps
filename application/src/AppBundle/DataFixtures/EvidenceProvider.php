<?php

namespace AppBundle\DataFixtures;

use Faker\Provider\Base as BaseProvider;

class EvidenceProvider extends BaseProvider
{
    protected $nameArray = ['Carcinogen', 'Contact Allergen', 'Mutagen'];

    public function evidenceName($current)
    {
        $current = $current - 1;

        if (isset($this->nameArray[$current])) {
            return $this->nameArray[$current];
        } else {
            return $this->nameArray[rand(0, count($this->nameArray) - 1)];
        }

    }

    /**
     * @return string Random symbol
     */
    public function symbol_evid()
    {
        $symbol = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyzQWERTYUIOPLKJHGFDSAZXCVBNM", 4)), 0, 4);
        return $symbol;
    }
}
