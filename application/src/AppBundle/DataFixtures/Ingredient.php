<?php

namespace AppBundle\DataFixtures;

use Faker\Provider\Base as BaseProvider;

class Ingredient extends BaseProvider
{
    protected $nameArray = ['water (aqua)', 'calcium carbonate', 'glycerin', 'magnesium aluminium silicate', 'alcohol', 'calendula officinalis extract', 'commiphora myrrha resin extract', 'xanthan gum', 'ammonium glycyrrhizate', 'flavor (aroma)', 'limonene'];

    public function commonName_attr($current)
    {
        $current = $current - 1;

        if (isset($this->nameArray[$current])) {
            return $this->nameArray[$current];
        } else {
            return $this->nameArray[rand(0, count($this->nameArray) - 1)];
        }

    }

    public function inciName_attr($current)
    {
        return $this->commonName_attr($current);
    }

    public function innName_attr($current)
    {
        return $this->commonName_attr($current);
    }
}
