<?php

namespace AppBundle\DataFixtures;

use AppBundle\Managers\SourceDataProviderManager;
use AppBundle\Managers\SourceManager;
use AppBundle\Repository\ProductProviderRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class DataFixtures extends Fixture
{
    private $sourceManager;
    /**
     * @var SourceDataProviderManager
     */
    private $sourceDataProviderManager;
    /**
     * @var ProductProviderRepository
     */
    private $productProviderRepository;

    /**
     * DataFixtures constructor.
     *
     * @param SourceManager             $sourceManager
     * @param SourceDataProviderManager $sourceDataProviderManager
     * @param ProductProviderRepository    $productProviderRepository
     */
    public function __construct(SourceManager $sourceManager,
                                SourceDataProviderManager $sourceDataProviderManager,
                                ProductProviderRepository $productProviderRepository)
    {
        $this->sourceManager = $sourceManager;
        $this->sourceDataProviderManager = $sourceDataProviderManager;
        $this->productProviderRepository = $productProviderRepository;
    }

    public function load(ObjectManager $em)
    {

    }
}