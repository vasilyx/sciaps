<?php

namespace AppBundle\DataFixtures;

use Faker\Provider\Base as BaseProvider;

class User extends BaseProvider
{
    protected $firstnameArray = ['John', 'Sarah', 'Jane', 'Michael', 'Steven', 'Rachel'];
    protected $surnameArray = ['Smith', 'Mills', 'Baker', 'Jones', 'Macher', 'Doe'];

    public function customName()
    {
        return $this->firstnameArray[rand(0, count($this->firstnameArray) - 1)];
    }

    public function customSurname()
    {
        return $this->surnameArray[rand(0, count($this->surnameArray) - 1)];
    }
}
