<?php

namespace AppBundle\DataFixtures;

use Faker\Provider\Base as BaseProvider;

class Source extends BaseProvider
{
    protected $title = ['A whole new world of ingredients', 'Bad carcinogens!', 'Pharma company goes crazy'];
    protected $author = ['John Doe', 'Dr. Strange', 'Ironman', 'Mr and Mrs Smith'];

    public function sourceTitle()
    {
        return $this->title[rand(0, count($this->title) - 1)];
    }

    public function sourceAuthor()
    {
        return $this->author[rand(0, count($this->author) - 1)];
    }
}
