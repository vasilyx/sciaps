<?php

namespace AppBundle\DataFixtures;

use Faker\Provider\Base as BaseProvider;

class Synonym extends BaseProvider
{
    protected $nameArray = [
            'Acetyl-DL-carnitine',
            'acetylcarnitine',
            'DL-O-Acetylcarnitine',
            'Acetyl carnitine',
            'Acetyl carnitine',
            'DL-Acetylcarnitine',
            '3-(acetyloxy)-4-(trimethylazaniumyl)butanoate',
            'CHEBI:73024, (3-carboxy-2-hydroxypropyl)trimethyl-, hydroxide, inner salt, acetate, DL-',
            '870-77-9',
            'SCHEMBL69781',
            'DTXSID2048117',
            'Acetyllcarnitine'
    ];

    public function synonymName_attr($current)
    {
        $current = $current - 1;

        if (isset($this->nameArray[$current])) {
            return $this->nameArray[$current];
        } else {
            return $this->nameArray[rand(0, count($this->nameArray) - 1)];
        }

    }
}
