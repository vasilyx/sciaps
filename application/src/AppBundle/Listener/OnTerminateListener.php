<?php

namespace AppBundle\Listener;

use AppBundle\Services\OnTerminateDo;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;

class OnTerminateListener implements ContainerAwareInterface
{
    /**
     * @var Container
     */
    protected $container;

    public function setContainer(Container $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param PostResponseEvent $eventArgs
     * @param OnTerminateDo $onTerminateDo
     */
    public function onTerminate(PostResponseEvent $eventArgs, OnTerminateDo $onTerminateDo)
    {
        $onTerminateDo->doJobs();
    }
}
