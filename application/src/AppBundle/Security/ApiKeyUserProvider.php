<?php

namespace AppBundle\Security;

use AppBundle\Managers\UserManager;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use AppBundle\Repository\UserRepository;


class ApiKeyUserProvider implements UserProviderInterface
{
    private $userRepository;
    private $userManager;

    public function __construct(UserRepository $userRepository, UserManager $userManager)
    {
        $this->userRepository = $userRepository;
        $this->userManager = $userManager;
    }

    public function loadUserByUsername($username)
    {
        if ($user = $this->userRepository->loadByEmail($username)) {
            return $user;
        }

        throw new AuthenticationException('User was not found');
    }

    public function loadUserByToken($token)
    {
        if ($user = $this->userManager->loadByToken($token)) {
            return $user;
        }

        throw new AuthenticationException('User was not found');
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
      //  throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return $class === $this->userRepository->getClassName();
    }
}
