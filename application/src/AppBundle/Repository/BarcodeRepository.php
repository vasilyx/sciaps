<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Barcode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class BarcodeRepository extends ServiceEntityRepository implements InterfaceListPaging
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Barcode::class);
    }

    public function createFindAllQuery()
    {
        return $this->createQueryBuilder('b');
    }

    /**
     * @param string $search
     * @return QueryBuilder
     */
    public function getQueryBuilderForList($search = ''): QueryBuilder
    {
        $qb = $this->createQueryBuilder('b');

        if ($search) {
            $qb->where('b.barcode like :search')->setParameter('search', '%' . $search . '%');
        }

        return $qb;
    }

    public function getBarcodesAsArray()
    {
        $qb = $this->createFindAllQuery();

        $qb->select('b.id, b.barcodeOriginal')->orderBy('b.id', 'asc');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array
     */
    public function getOrderMap(): array
    {
        return [
            'id' => 'b.id',
            'barcode' => 'b.barcode'
        ];
    }
}
