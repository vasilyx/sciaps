<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Company;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductProvider;
use AppBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProductRepository extends ServiceEntityRepository implements InterfaceListPaging
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function createFindAllQuery()
    {
        return $this->createQueryBuilder('dp');
    }

    public function getQueryBuilderForList($search = ''): QueryBuilder
    {
        $qb = $this->createQueryBuilder('dp');
        $qb->where('dp. ');

        if ($search) {
            $qb->where('dp.name like :search')
                ->setParameter('search', '%' . $search . '%')
            ;
        }

        return $qb;
    }

    /**
     * @param $barcodes
     * @param User $user
     * @return mixed
     */
    public function getCountBarcodes($barcodes, User $user)
    {
        $qb = $this->createQueryBuilder('dp');
        $qb->select('count(dp.id)');
        $qb->leftJoin('dp.barcode', 'barcode');

        if ($barcodes) {
            $qb->where('dp.company = :company')
                ->andWhere('barcode.barcode IN :search')
                ->setParameter('search', '(' . implode(',', $barcodes) . ')')
                ->setParameter('company', $user->getCompany())
            ;
        }

        return $qb->getQuery()
                  ->getSingleScalarResult();
    }


    /**
     * @param ProductProvider $productProvider
     * @param bool $onlyActive
     * @return mixed
     */
    public function getCountByProductProvider(ProductProvider $productProvider, $onlyActive = false)
    {
        $qb = $this->createQueryBuilder('dp');
        $qb->select('count(dp.id)');

        $qb->where('dp.productProvider = :productProvider')
            ->setParameter('productProvider', $productProvider);

        if ($onlyActive) {
                $qb->andWhere('dp.status = :status')
                    ->setParameter('status', 1)
            ;
        }

        return $qb->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @return array
     */
    public function getOrderMap(): array
    {
        return [
            'id' => 'dp.id',
            'name' => 'dp.name',
            'displayName' => 'dp.displayName',
            'websiteURL' => 'dp.websiteURL'
        ];
    }
}
