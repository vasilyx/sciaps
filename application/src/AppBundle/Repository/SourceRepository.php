<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Source;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Source|null find($id, $lockMode = null, $lockVersion = null)
 * @method Source|null findOneBy(array $criteria, array $orderBy = null)
 * @method Source[]    findAll()
 * @method Source[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SourceRepository extends ServiceEntityRepository implements InterfaceListPaging
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Source::class);
    }

    public function createFindAllQuery()
    {
        return $this->createQueryBuilder('dp');
    }

    public function getQueryBuilderForList($search = ''): QueryBuilder
    {
        $qb = $this->createQueryBuilder('dp');


        if ($search) {
            $qb->where('dp.name like :search')
                ->setParameter('search', '%' . $search . '%')
            ;
        }

        return $qb;
    }

    /**
     * @return array
     */
    public function getOrderMap(): array
    {
        return [
            'id' => 'dp.id',
            'name' => 'dp.name'
        ];
    }
}
