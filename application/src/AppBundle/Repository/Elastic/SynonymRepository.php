<?php

namespace AppBundle\Repository\Elastic;

use Elastica\Query\BoolQuery;
use Elastica\Query\Term;

class SynonymRepository extends CommonRepository
{
    public function getByCID(string $cid)
    {
        $query = new BoolQuery();
        $query->addMust(new Term(['cid', $cid]));

        return $this->find($query);
    }
}
