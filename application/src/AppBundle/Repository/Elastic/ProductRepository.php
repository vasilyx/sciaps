<?php

namespace AppBundle\Repository\Elastic;

use FOS\ElasticaBundle\Repository;
use Elastica\Query\BoolQuery;
use Elastica\Query\Terms;
use Elastica\Query;

class ProductRepository extends CommonRepository
{
    public function getById($id)
    {
        $query = new BoolQuery();
        $query->addMust(new Terms('id', $id));

        return $this->find($query);
    }
}
