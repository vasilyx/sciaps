<?php

namespace AppBundle\Repository\Elastic;

use FOS\ElasticaBundle\Repository;
use Elastica\Query\BoolQuery;
use Elastica\Query\Term;
use Elastica\Query;

class CommonRepository extends Repository
{
    /**
     * @param string $name
     * @param bool $fuzzy
     * @return array
     */
    public function getByName(string $name, $fuzzy = false)
    {
        if ($fuzzy) {
            return $this->fuzzyExists($name, 'name');
        } else {
            return $this->exists($name, 'name');
        }
    }

    /**
     * @param string $value
     * @param string $field
     * @return array
     */
    public function exists(string $value, string $field)
    {
        $query = new Query\Match($field, $value);

        $hits = $this->find($query);

        return $hits;
    }

    /**
     * @param string $value
     * @param string $field
     * @return array
     */
    public function fuzzyExists(string $value, string $field)
    {
        $query = new Query\Fuzzy($field, $value);

        $hits = $this->find($query);

        return $hits;
    }
}
