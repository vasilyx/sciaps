<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Brand;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class BrandRepository extends ServiceEntityRepository implements InterfaceListPaging
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Brand::class);
    }

    public function createFindAllQuery()
    {
        return $this->createQueryBuilder('br');
    }

    public function getBrandsAsArray()
    {
        $qb = $this->createFindAllQuery();

        $qb->select('br.id, br.name')->orderBy('br.id', 'asc');

        return $qb->getQuery()->getResult();
    }

    public function getQueryBuilderForList($search = ''): QueryBuilder
    {
        $qb = $this->createQueryBuilder('br');


        if ($search) {
            $qb->where('br.name like :search')->setParameter('search', '%' . $search . '%');
        }

        return $qb;
    }

    /**
     * @return array
     */
    public function getOrderMap(): array
    {
        return [
            'id' => 'br.id',
            'name' => 'br.name',
        ];
    }
}
