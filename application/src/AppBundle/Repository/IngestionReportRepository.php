<?php

namespace AppBundle\Repository;

use AppBundle\Entity\ProductProvider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class IngestionReportRepository extends \Doctrine\ORM\EntityRepository
{
    public function getLastReportByProvider(ProductProvider $productProvider)
    {
        $qb = $this->createQueryBuilder('dp');
        $qb->where('dp.productProvider = :productProvider')
            ->setParameter('productProvider', $productProvider);

        $res = $qb->orderBy('dp.id', 'desc')
                ->getQuery()
                ->getResult();

        return $res[0] ?? null;
    }
}
