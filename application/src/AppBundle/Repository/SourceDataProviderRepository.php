<?php

namespace AppBundle\Repository;

use AppBundle\Entity\SourceDataProvider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\QueryBuilder;


class SourceDataProviderRepository extends ServiceEntityRepository implements InterfaceListPaging
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SourceDataProvider::class);
    }

    public function createFindAllQuery()
    {
        return $this->createQueryBuilder('dp');
    }

    public function getQueryBuilderForList($search = ''): QueryBuilder
    {
        $qb = $this->createQueryBuilder('dp');


        if ($search) {
            $qb->where('dp.companyName like :search')
                ->orWhere('dp.displayName like :search')
                ->orWhere('dp.websiteURL like :search')
                ->setParameter('search', '%' . $search . '%')
            ;
        }

        return $qb;
    }

    public function save($entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush();

        return true;
    }

    /**
     * @return array
     */
    public function getOrderMap(): array
    {
        return [
            'id' => 'dp.id',
            'companyName' => 'dp.companyName',
            'displayName' => 'dp.displayName',
            'websiteURL' => 'dp.websiteURL'
        ];
    }
}
