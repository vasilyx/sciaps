<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Ingredient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class IngredientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ingredient::class);
    }

    public function getIngredientByNameAndMatchTypes(string $name)
    {
        $qb = $this->createFindAllQuery();

        $qb->where('i.inciName = :name')
            ->orWhere('i.commonName = :name')
            ->orWhere('i.innName = :name')
            ->setParameter('name', $name);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
            return $result;
        } catch (NonUniqueResultException $e) {
            throw $e;
        }
    }

    public function createFindAllQuery()
    {
        return $this->createQueryBuilder('i');
    }

    public function getQueryBuilderForList($search = ''): QueryBuilder
    {
        $qb = $this->createFindAllQuery();

        if ($search) {
            $qb->where('i.commonName like :search')
                ->orWhere('i.inciName like :search')
                ->orWhere('i.innName like :search')
                ->orWhere('i.casNumber like :search')
                ->orWhere('i.ecNumber like :search')
                ->setParameter('search', '%' . $search . '%')
            ;
        }

        return $qb;
    }

    /**
     * @return array
     */
    public function getOrderMap(): array
    {
        return [
            'id' => 'i.id',
            'commonName' => 'i.commonName',
            'innName' => 'i.innName',
            'inciName' => 'i.inciName',
            'casNumber' => 'i.casNumber',
            'ecNumber' => 'i.ecNumber',
        ];
    }
}
