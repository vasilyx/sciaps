<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Company;
use AppBundle\Entity\LogEvent;
use AppBundle\Entity\User;

class LogEventRepository extends \Doctrine\ORM\EntityRepository
{
    public function getTrackingContractors()
    {
        $qb = $this->createQueryBuilder('dp');

        $qb->where('dp.actionId = :actionId')
            ->andWhere('dp.entityTypeId = :entityTypeId')
            ->setParameter('actionId', LogEvent::ACTION_CHANGED_STATUS)
            ->setParameter('entityTypeId', LogEvent::ENTITY_TYPE_CROWDSOURCE);

        $qb->groupBy('dp.user');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param bool $status
     * @return mixed
     */
    public function getCountTrackingByUser(User $user, $status = false)
    {
        $qb = $this->createQueryBuilder('dp');
        $qb->select('count(dp.id)');

        $qb->where('dp.user = :user')
            ->andWhere('dp.actionId = :actionId')
            ->andWhere('dp.entityTypeId = :entityTypeId')
            ->setParameter('actionId', LogEvent::ACTION_CHANGED_STATUS)
            ->setParameter('entityTypeId', LogEvent::ENTITY_TYPE_CROWDSOURCE)
            ->setParameter('user', $user);

        if ($status) {
            $qb->andWhere('dp.value = :status')
                ->setParameter('status', $status)
            ;
        }

        return $qb->getQuery()
            ->getSingleScalarResult();
    }
}
