<?php

namespace AppBundle\Repository;

use AppBundle\Entity\CrowdsourcePending;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;


class CrowdsourcePendingIngredientsRepository extends ServiceEntityRepository implements InterfaceListPaging
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CrowdsourcePending::class);
    }

    public function createFindAllQuery()
    {
        return $this->createQueryBuilder('dp');
    }

    public function getQueryBuilderForList($search = ''): QueryBuilder
    {
        $qb = $this->createQueryBuilder('dp');


        if ($search) {
            $qb->andWhere('dp.name like :search')->setParameter('search', '%' . $search . '%');
            $qb->orWhere('dp.description like :search')->setParameter('search', '%' . $search . '%');
        }

        return $qb;
    }

    /**
     * @return array
     */
    public function getOrderMap(): array
    {
        return [
            'id' => 'dp.id',
            'name' => 'dp.name'
        ];
    }
}