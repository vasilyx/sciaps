<?php
namespace AppBundle\Repository;

use Doctrine\ORM\QueryBuilder;

interface InterfaceListPaging
{
    public function getQueryBuilderForList($search = ''):QueryBuilder;
    public function getOrderMap():array;
}
