<?php

namespace AppBundle\Repository;

use AppBundle\Entity\CompanyServices;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\QueryBuilder;

class CompanyServicesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CompanyServices::class);
    }

    public function createFindAllQuery()
    {
        return $this->createQueryBuilder('c');
    }

    public function getQueryBuilderForList($search = ''): QueryBuilder
    {
        $qb = $this->createQueryBuilder('c');

        if ($search) {
            $qb->where('c.serviceType like :search')
                ->setParameter('search', '%' . $search . '%')
            ;
        }

        return $qb;
    }

    /**
     * @return array
     */
    public function getOrderMap(): array
    {
        return [
            'id' => 'c.id',
            'serviceType' => 'c.serviceType',
        ];
    }
}
