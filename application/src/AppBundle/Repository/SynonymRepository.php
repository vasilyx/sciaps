<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Synonym;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class SynonymRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Synonym::class);
    }

    public function createFindAllQuery()
    {
        return $this->createQueryBuilder('s');
    }

    public function getQueryBuilderForList($search = ''): QueryBuilder
    {
        $qb = $this->createFindAllQuery();

        if ($search) {
            $qb->where('s.name like :search')
                ->setParameter('search', '%' . $search . '%')
            ;
        }

        return $qb;
    }

    /**
     * @return array
     */
    public function getOrderMap(): array
    {
        return [
            'id' => 's.id',
            'name' => 's.name'
        ];
    }
}
