<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Organization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class OrganizationRepository extends ServiceEntityRepository implements InterfaceListPaging
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Organization::class);
    }

    public function createFindAllQuery()
    {
        return $this->createQueryBuilder('dp');
    }

    public function getQueryBuilderForList($search = ''): QueryBuilder
    {
        $qb = $this->createQueryBuilder('dp');


        if ($search) {
            $qb->where('dp.companyName like :search')
                ->orWhere('dp.websiteURL like :search')
                ->orWhere('dp.personName like :search')
                ->orWhere('dp.personEmail like :search')
                ->setParameter('search', '%' . $search . '%')
            ;
        }

        return $qb;
    }

    /**
     * @return array
     */
    public function getOrderMap(): array
    {
        return [
            'id' => 'dp.id',
            'companyName' => 'dp.companyName',
            'displayName' => 'dp.displayName',
            'personName' => 'dp.personName',
            'personEmail' => 'dp.personEmail',
        ];
    }
}