<?php
/**
 * Created by PhpStorm.
 * User: jasonlinthwaite
 * Date: 17/07/2018
 * Time: 15:35
 */

namespace AppBundle\Traits;

trait EntityHelpers
{
    public static function getConstant($constant)
    {
        $cClass = new \ReflectionClass(__CLASS__);
        $constant = $cClass->getConstant($constant);

        return $constant;
    }

    public static function hasClassConstant($constant)
    {
        return (bool)self::getConstant($constant);
    }

    public static function getClassConstants($constant = '')
    {
        $cClass = new \ReflectionClass(__CLASS__);
        $constants = $cClass->getConstants();
        if ($constant != '') {
            $tmp = [];
            foreach ($constants as $k => $v) {
                if(substr($k, 0, strlen($constant)) === $constant){
                    $tmp[$k] = $v;
                }
            }
            $constants = $tmp;
        }
        return $constants;
    }

    public static function getClassConstantsByCodes($constant = '')
    {
        return array_flip(self::getClassConstants($constant));
    }
}