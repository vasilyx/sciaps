<?php

namespace AppBundle\Traits;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Trait TimestampableEntity
 *
 * @ORM\Entity
 * @package AppBundle\Traits
 */
trait TimestampableEntity
{
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $addedAt;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;


    public function getAddedAt()
    {
        return $this->addedAt;
    }


    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
