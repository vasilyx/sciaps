<?php
namespace AppBundle\Traits;

use AppBundle\Entity\ProductProvider;
use AppBundle\Managers\SourceManager;

trait ContainerHelpers
{
    public function getProductProviderRepo()
    {
        return $this->getRepo(ProductProvider::class);
    }

    public function getSourceManager()
    {
        return $this->getContainerObj()->get(SourceManager::class);
    }

    private function getContainerObj()
    {
        if (!empty($this->container)) {
            return $this->container;
        }

        if (method_exists($this, 'getContainer')) {
            return $this->getContainer();
        }

        throw new \Exception('Container not found');
    }

    private function getDoctrineObj()
    {
        if (!$this->getContainerObj()->has('doctrine')) {
            throw new \LogicException('The DoctrineBundle is not registered in your application.');
        }

        return $this->getContainerObj()->get('doctrine');
    }

    public function getRepo($class)
    {
        return $this->getDoctrineObj()->getRepository($class);
    }
}