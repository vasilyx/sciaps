<?php
namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Context\Context;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

abstract class AbstractApiController extends FOSRestController
{
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    /**
     *
     * @param $content
     * @param string $status
     * @param $code
     * @return View
     */
    public function getResponse($content, $status = self::STATUS_SUCCESS, $code = Response::HTTP_OK)
    {
        $view = View::create(['status'  => $status, 'content' => $content], $code);
        $view->setContext(new Context());
//        $view->setSerializationContext(SerializationContext::create()->enableMaxDepthChecks());
        return $view;
    }

    /**
     * Get array of constraint violations messages. Prepares validation errors for api response.
     *
     * @param  ConstraintViolationListInterface $violations
     * @return array
     */
    protected function getConstraintViolations(ConstraintViolationListInterface $violations)
    {
        $errors = [];

        /* @var ConstraintViolation $violation */
        foreach ($violations as $violation) {
            $constraint = $violation->getConstraint();

            /*
             * We try to use payload property path before take it from getPropertyPath
             */
            $propertyPath = isset($violation->getConstraint()->payload['propertyPath']) ? $violation->getConstraint()->payload['propertyPath'] : $violation->getPropertyPath();

            $errors[$propertyPath] = [
                'code'    => isset($constraint->payload['code']) ? $constraint->payload['code'] : $violation->getMessage(),
                'message' => $violation->getMessage()
            ];
        }

        return $errors;
    }

    /**
     * Response view for request data validation violations
     *
     * @param  ConstraintViolationListInterface $violations
     * @return View
     */
    protected function getValidationErrorResponse(ConstraintViolationListInterface $violations)
    {
        return $this->getResponse($this->getConstraintViolations($violations), self::STATUS_ERROR, Response::HTTP_BAD_REQUEST);
    }


    /**
     * @param FormInterface $form
     *
     * @return array
     */
    protected function getFormErrors(FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            if ($form->isRoot()) {
                $cause = $error->getCause();
                if($cause) {
                    $propertyName = str_replace("data.", "", $cause->getPropertyPath());
                    $propertyName = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $propertyName));
                    $errors[$propertyName][] = $error->getMessage();
                } else {
                    //Error with not visible path
                }
            } else {
                $errors[] = $error->getMessage();
            }
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getFormErrors($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }


    protected function getFormErrorResponse(FormInterface $form)
    {
        return JsonResponse::create(['errors' => [
            'form' => $this->getFormErrors($form)
            ]
        ], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @return \AppBundle\Entity\User
     */
    protected function getCurrentUser()
    {
        return $this->container->get('security.token_storage')->getToken()->getUser();
    }
}