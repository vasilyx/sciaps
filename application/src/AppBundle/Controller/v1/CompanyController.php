<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\Company;
use AppBundle\Entity\DataEntryProvider;
use AppBundle\Entity\ProductProvider;
use AppBundle\Entity\SourceDataProvider;
use AppBundle\Form\CompanyType;
use AppBundle\Form\CompanyUpdateType;
use AppBundle\Form\DataEntryProviderType;
use AppBundle\Form\ProductProviderType;
use AppBundle\Form\SourceDataProviderType;
use AppBundle\Managers\CompanyManager;
use AppBundle\Managers\DataTableManager;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;
use FOS\RestBundle\View\View;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Serializer\Tests\Normalizer\ObjectSerializerNormalizer;

/**
 * @Route("/api/v1/core/companies")
 */
class CompanyController extends \AppBundle\Controller\AbstractApiController
{
    private $companyManager;
    private $dataTableManager;

    public function __construct(CompanyManager $companyManager, DataTableManager $dataTableManager)
    {
        $this->companyManager = $companyManager;
        $this->dataTableManager = $dataTableManager;
    }

    /**
     * Returns a list of companies allowing for filtering, pagination and sorting
     *
     * @QueryParam(name="_page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="_limit", requirements="\d+", default="25", description="Page count limit")
     * @QueryParam(name="_order", requirements="(asc|desc)", allowBlank=true, default="asc", description="Sort
     *     direction")
     * @QueryParam(name="_sort", requirements="\w+", allowBlank=true, description="Sorts based on a particular field")
     * @QueryParam(name="q", requirements="\w+", allowBlank=true, description="Searches for a particular text string")
     * @QueryParam(name="_fields", requirements="[\w,]+", allowBlank=true, description="Searches for a company by matching text")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="companies")
     *
     * @Route("", methods={"GET"})
     * @return Response
     */
    public function getCompaniesAction(Request $request)
    {
        $data = $this->dataTableManager->getListData(Company::class, $request, [], true);

        //$data['rows'] = $this->companyManager->convertServices($data['rows']);
        $ref = 0;
        $row =& $ref;
        $i = 0;
        foreach ($data['rows'] as $row) {
            $company = $this->companyManager->convertServices($row);
            $data['rows'][$i] = $company;
            $i++;
        }

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['details'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }

    /**
     * Get Company
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="companies")
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getCompanyAction(Company $company)
    {
        $company = $this->companyManager->convertServices($company);
        return $company;
    }

    /**
     * Delete Company
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="companies")
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @return View
     */
    public function deleteCompanyAction(Company $company)
    {
        $this->companyManager->removeCompany($company); // TODO rebuild
        $this->getResponse(null);
    }

    /**
     * @SWG\Post(
     *      summary="Add Company",
     *      tags={"companies"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\CompanyType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("", methods={"POST"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function addCompanyAction(Request $request)
    {
        $form = $this->createForm(CompanyType::class, new Company());

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->companyManager->createNewCompany($form->getData(),$request);
        }
    }

    /**
     * @SWG\Put(
     *      summary="Update Company",
     *      tags={"companies"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\CompanyUpdateType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateCompanyAction(Request $request, Company $company = null)
    {
        if (!$company) return new Response('Company with this id did not exist', Response::HTTP_NOT_FOUND);

        $form = $this->createForm(CompanyUpdateType::class, $company);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->companyManager->updateCompany($form->getData());
        }
    }

    /**
     * Get All Services for Company
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="companies")
     *
     * @Route("/{id}/services", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getServicesAction(Company $company)
    {
        return $this->companyManager->getServices($company);
    }

    /**
     * @SWG\Post(
     *      summary="Turn On Service for Company [ADMIN, SKINNINJA] ",
     *      description="",
     *      tags={"companies"},
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{id}/enableservice/{service_code}", methods={"POST"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function turnOnServiceAction(Company $company, $service_code)
    {
        return $this->companyManager->turnOnService($company, $service_code);
    }

    /**
     * GET Service for Company
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="companies")
     *
     * @Route("/{id}/service/{service_code}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getServiceAction(Company $company, $service_code)
    {
        return $this->companyManager->getService($company, $service_code);
    }

    /**
     * @SWG\Post(
     *      summary="Add Product Service (100) for Company [ADMIN, SKINNINJA] ",
     *      description="",
     *      tags={"companies"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\ProductProviderType::class)
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("/{id}/service/100", methods={"POST"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function addNewServiceProductAction(Request $request, Company $company)
    {
        $form = $this->createForm(ProductProviderType::class, new ProductProvider());
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->companyManager->createNewService($form->getData(),$company, Company::PRODUCT_TYPE);
        }
    }

    /**
     * @SWG\Post(
     *      summary="Add Source Service (200) for Company [ADMIN, SKINNINJA] ",
     *      description="",
     *      tags={"companies"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\SourceDataProviderType::class)
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("/{id}/service/200", methods={"POST"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function addNewServiceSourceAction(Request $request, Company $company)
    {
        $form = $this->createForm(SourceDataProviderType::class, new SourceDataProvider());
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->companyManager->createNewService($form->getData(),$company, Company::SOURCE_TYPE);
        }
    }

    /**
     * @SWG\Post(
     *      summary="Add DataEntry Service (300) for Company [ADMIN, SKINNINJA] ",
     *      description="",
     *      tags={"companies"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\DataEntryProviderType::class)
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("/{id}/service/300", methods={"POST"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function addNewServiceDataEntryAction(Request $request, Company $company)
    {
        $form = $this->createForm(DataEntryProviderType::class, new DataEntryProvider());
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->companyManager->createNewService($form->getData(),$company, Company::DATAENTRY_TYPE);
        }
    }

    /**
     * @SWG\Put(
     *      summary="Update Product Service (100) for Company",
     *      tags={"companies"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\ProductProviderType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{id}/service/100", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateServiceProductAction(Request $request, Company $company = null)
    {
        if (!$company) return new Response('Company with this id did not exist', Response::HTTP_NOT_FOUND);

        $productProvider = $this->companyManager->getProduct($company, Company::PRODUCT_TYPE);
        $form = $this->createForm(ProductProviderType::class, $productProvider);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->companyManager->updateService($form->getData(),Company::PRODUCT_TYPE);
        }
    }

    /**
     * @SWG\Put(
     *      summary="Update Source Service (200) for Company",
     *      tags={"companies"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\SourceDataProviderType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{id}/service/200", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateServiceSourceAction(Request $request, Company $company = null)
    {
        if (!$company) return new Response('Company with this id did not exist', Response::HTTP_NOT_FOUND);

        $sourceProvider = $this->companyManager->getProduct($company, Company::SOURCE_TYPE);
        $form = $this->createForm(SourceDataProviderType::class, $sourceProvider);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->companyManager->updateService($form->getData(),Company::SOURCE_TYPE);
        }
    }

    /**
     * @SWG\Put(
     *      summary="Update DataEntry Service (300) for Company",
     *      tags={"companies"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\SourceDataProviderType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{id}/service/300", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateServiceDataEntryAction(Request $request, Company $company = null)
    {
        if (!$company) return new Response('Company with this id did not exist', Response::HTTP_NOT_FOUND);

        $dataEntryProvider = $this->companyManager->getProduct($company, Company::DATAENTRY_TYPE);
        $form = $this->createForm(DataEntryProviderType::class, $dataEntryProvider);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->companyManager->updateService($form->getData(),Company::DATAENTRY_TYPE);
        }
    }
}
