<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\Barcode;

use AppBundle\Managers\DataTableManager;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;

use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/core/barcodes")
 */
class BarcodeController extends \AppBundle\Controller\AbstractApiController
{
    private $dataTableManager;

    public function __construct(DataTableManager $dataTableManager)
    {
        $this->dataTableManager = $dataTableManager;
    }

    /**
     * Returns a list of barcodes allowing for filtering, pagination and sorting
     *
     * @QueryParam(name="_page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="_limit", requirements="\d+",   default="25", description="Page count limit")
     * @QueryParam(name="_order", requirements="(asc|desc)", allowBlank=true, default="asc", description="Sort
     *     direction")
     * @QueryParam(name="_sort", requirements="\w+", allowBlank=true, description="Sorts based on a particular field")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="barcodes")
     *
     * @Route("", methods={"GET"})
     * @return Response
     */
    public function getBarcodesAction(Request $request)
    {
        $data =  $this->dataTableManager->getListData(Barcode::class, $request);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
             SerializationContext::create()->setGroups(['list'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }

    /**
     * Get barcode
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="barcodes")
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getBarcodeAction(Barcode $barcode)
    {
        return $barcode;
    }

    /**
     * Validate barcode
     * TYPE_UPCA = 1;
     * TYPE_UPCE = 2;
     * TYPE_EAN8 = 3;
     * TYPE_EAN13 = 4;
     * TYPE_GTIN14 = 5;
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="barcodes")
     *
     * @Route("/validate/{id}", methods={"GET"})
     */
    public function getBarcodeValidateAction($id)
    {
        $type = \AppBundle\Utils\BarcodeUtilities::getBarcodeType($id);

        if (!$type || $type == Barcode::TYPE_UNKNOWN) {
            $type = null;
        }

        return new Response($this->container->get('jms_serializer')->serialize(
            ['type' => $type],
            'json'), Response::HTTP_OK);
    }
}
