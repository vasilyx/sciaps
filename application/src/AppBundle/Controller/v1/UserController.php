<?php
namespace AppBundle\Controller\v1;

use AppBundle\Entity\User;
use AppBundle\Form\UserCreationType;
use AppBundle\Form\UserUpdateType;
use AppBundle\Managers\DataTableManager;
use AppBundle\Response\BaseResponse;
use AppBundle\Managers\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;

use AppBundle\Traits\ContainerHelpers;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;

use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/core/users")
 */
class UserController extends \AppBundle\Controller\AbstractApiController
{
    use ContainerHelpers;

    private $userManager;
    private $dataTableManager;
    private $em;

    public function __construct(UserManager $userManager, EntityManagerInterface $em, DataTableManager $dataTableManager)
    {
        $this->userManager = $userManager;
        $this->dataTableManager = $dataTableManager;
        $this->em = $em;
    }


    /**
     *
     * @SWG\Post(
     *      summary="Add user",
     *      tags={"users"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\UserCreationType::class )
     *      ),
     * @SWG\Response(response=200, description="Success")
     * )
     * @Route("", methods={"POST"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function addUserAction(Request $request)
    {
        $form = $this->createForm(UserCreationType::class, new User());

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            $user = $this->userManager->createUser($form->getData());
            return $user;
        }
    }

    /**
     * Get User
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="users")
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getUserAction(User $user)
    {
        return $user;
    }

    /**
     * Returns a list of users allowing for filtering, pagination and sorting
     *
     * @QueryParam(name="_page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="_limit", requirements="\d+",   default="25", description="Page count limit")
     * @QueryParam(name="_order", requirements="(asc|desc)", allowBlank=true, default="asc", description="Sort
     *     direction")
     * @QueryParam(name="_sort", requirements="\w+", allowBlank=true, description="Sorts based on a particular field")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="users")
     *
     * @Route("", methods={"GET"})
     * @return Response
     */
    public function getUsersAction(Request $request)
    {
        $data = $this->dataTableManager->getListData(User::class, $request);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['userList'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }


    /**
     * @SWG\Put(
     *      summary="Update user",
     *      tags={"users"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\UserUpdateType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateUserAction(Request $request, User $user)
    {
        $form = $this->createForm(UserUpdateType::class, $user);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->userManager->updateUser($form->getData(), $this->getCurrentUser());
        }
    }
}
