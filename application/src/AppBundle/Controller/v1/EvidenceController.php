<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\Evidence;
use AppBundle\Form\EvidenceType;
use AppBundle\Managers\DataTableManager;
use AppBundle\Managers\EvidenceManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/core/evidences")
 */
class EvidenceController extends \AppBundle\Controller\AbstractApiController
{
    private $evidenceManager;
    private $dataTableManager;

    public function __construct(EvidenceManager $evidenceManager, DataTableManager $dataTableManager)
    {
        $this->evidenceManager = $evidenceManager;
        $this->dataTableManager = $dataTableManager;
    }

    /**
     * Returns a list of data evidences allowing for filtering, pagination and sorting
     *
     * @QueryParam(name="_page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="_limit", requirements="\d+",   default="25", description="Page count limit")
     * @QueryParam(name="_order", requirements="(asc|desc)", allowBlank=true, default="asc", description="Sort
     *     direction")
     * @QueryParam(name="_sort", requirements="\w+", allowBlank=true, description="Sorts based on a particular field")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="evidences")
     *
     * @Route("", methods={"GET"})
     * @return Response
     */
    public function getEvidencesAction(Request $request)
    {
        $data =  $this->dataTableManager->getListData(Evidence::class, $request);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['list'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }

    /**
     * Get Evidence
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="evidences")
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @return View
     */
    public function getEvidenceAction(Evidence $evidence)
    {
        return $evidence;
    }

    /**
     * Delete Evidence
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Response(response=405, description="Method Not Allowed")
     * @SWG\Tag(name="evidences")
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @return JsonResponse|Response
     */
    public function deleteEvidenceAction(Evidence $evidence)
    {
        if ($this->evidenceManager->removeEvidence($evidence)) {
            $response = new Response("", Response::HTTP_OK);
        }
        else {
            $response = new JsonResponse(['errors' => 'This evidence has existing relationships. Please remove these relationships first and then try again!'], Response::HTTP_METHOD_NOT_ALLOWED);
        }
        return $response;
    }

    /**
     * @SWG\Post(
     *      summary="Add Evidence [ADMIN, SKINNINJA]",
     *      tags={"evidences"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\EvidenceType::class)
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("", methods={"POST"})
     */
    public function addEvidenceAction(Request $request)
    {
        $form = $this->createForm(EvidenceType::class, new Evidence());

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->evidenceManager->createNewEvidence($form->getData());
        }
    }

    /**
     * @SWG\Put(
     *      summary="Update Evidence",
     *      tags={"evidences"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\EvidenceType::class)
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateEvidenceAction(Request $request, Evidence $evidence)
    {
        $form = $this->createForm(EvidenceType::class, $evidence);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->evidenceManager->createNewEvidence($form->getData());
        }
    }
}
