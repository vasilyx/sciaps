<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\CrowdsourcePending;
use AppBundle\Entity\CrowdsourcePendingIngredients;
use AppBundle\Form\CrowdsourcePendingUpdateType;
use AppBundle\Managers\DataTableManager;
use AppBundle\Managers\CrowdsourceManager;
use AppBundle\Managers\LogEventManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/core/crowdsource")
 */
class CrowdsourceController extends \AppBundle\Controller\AbstractApiController
{
    private $crowdsourceManager;
    private $dataTableManager;
    private $logEventManager;

    public function __construct(CrowdsourceManager $crowdsourceManager,
                                DataTableManager $dataTableManager,
                                LogEventManager $logEventManager)
    {
        $this->crowdsourceManager = $crowdsourceManager;
        $this->dataTableManager = $dataTableManager;
        $this->logEventManager = $logEventManager;
    }

    /**
     * Returns a list of crowdsources allowing for filtering, pagination and sorting
     *
     * @QueryParam(name="_page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="_limit", requirements="\d+",   default="25", description="Page count limit")
     * @QueryParam(name="_order", requirements="(asc|desc)", allowBlank=true, default="asc", description="Sort
     *     direction")
     * @QueryParam(name="_sort", requirements="\w+", allowBlank=true, description="Sorts based on a particular field")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="crowdsources")
     *
     * @Route("", methods={"GET"})
     * @return Response
     */
    public function getCrowdSourcesAction(Request $request)
    {
        $data =  $this->dataTableManager->getListData(CrowdsourcePending::class, $request);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['list'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }

    /**
     * Returns a list of achived crowdsources allowing for filtering, pagination and sorting
     *
     * @QueryParam(name="_page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="_limit", requirements="\d+",   default="25", description="Page count limit")
     * @QueryParam(name="_order", requirements="(asc|desc)", allowBlank=true, default="asc", description="Sort
     *     direction")
     * @QueryParam(name="_sort", requirements="\w+", allowBlank=true, description="Sorts based on a particular field")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="crowdsources")
     *
     * @Route("/archived", methods={"GET"})
     * @return Response
     */
    public function getCrowdSourcesArchivedAction(Request $request)
    {
        $data =  $this->dataTableManager->getListData(CrowdsourcePending::class,
            $request,
            ['status' => CrowdsourcePending::STATUS_ARCHIVED]);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['list'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }

    /**
     * Returns a list of accepted crowdsources allowing for filtering, pagination and sorting
     *
     * @QueryParam(name="_page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="_limit", requirements="\d+",   default="25", description="Page count limit")
     * @QueryParam(name="_order", requirements="(asc|desc)", allowBlank=true, default="asc", description="Sort
     *     direction")
     * @QueryParam(name="_sort", requirements="\w+", allowBlank=true, description="Sorts based on a particular field")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="crowdsources")
     *
     * @Route("/accepted", methods={"GET"})
     * @return Response
     */
    public function getCrowdSourcesAcceptedAction(Request $request)
    {
        $data =  $this->dataTableManager->getListData(CrowdsourcePending::class,
            $request,
            ['status' => CrowdsourcePending::STATUS_APPROVED]);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['list'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }

    /**
     * Returns crowdsource tracking
     *
     * @QueryParam(name="date_start", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="date_end", requirements="\d+",   default="25", description="Page count limit")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="crowdsources")
     *
     * @Route("/tracking", methods={"GET"})
     * @return Response
     */
    public function getCrowdSourcesTrackingAction(Request $request)
    {
        return $this->logEventManager->getCrowdsourceTracking($this->getCurrentUser(), $request);
    }

    /**
     * Delete Crowdsource
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="crowdsources")
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @return View
     */
    public function deleteCrowdsourceAction(CrowdsourcePending $Crowdsource)
    {
        $this->crowdsourceManager->removeCrowdsource($Crowdsource);

        $this->getResponse(null);
    }

    /**
     * @SWG\Post(
     *      summary="Add crowdsources",
     *      tags={"crowdsources"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="barcode", type="string", example="123123123"),
     *              @SWG\Property(property="is_wiyc_upload", type="boolean", example="false"),
     *              @SWG\Property(property="user_id", type="string", example="12"),
     *              @SWG\Property(property="description", type="string", example="description"),
     *              @SWG\Property(property="ingredients_photo", type="file", example=""),
     *              @SWG\Property(property="product_photo", type="file", example="")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("/upload", methods={"POST"})
     */
    public function addCrowdsourceAction(Request $request)
    {
        return $this->crowdsourceManager->createNewCrowdsource($request);    // "/api/upload/" - should be

    }

    /**
     * @SWG\Put(
     *      summary="Update crowdsource",
     *      tags={"crowdsources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\CrowdsourcePendingUpdateType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateCrowdsourceAction(Request $request, CrowdsourcePending $crowdsourcePending)
    {
        $form = $this->createForm(CrowdsourcePendingUpdateType::class, $crowdsourcePending);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->crowdsourceManager->updateNewCrowdsource($form->getData());
        }
    }

    /**
     * Returns a list of stats for crowdsources ingredients
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="crowdsources")
     *
     * @Route("/{id}/ingredients", methods={"GET"})
     * @return Response
     */
    public function getCrowdsourceIngredientsAction(CrowdsourcePending $Crowdsource)
    {
        return $Crowdsource->getIngredients();
    }


    /**
     * Get Crowdsource
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="crowdsources")
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getCrowdsourceAction(CrowdsourcePending $Crowdsource)
    {
        return $Crowdsource;
    }
}
