<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\Ingredient;
use AppBundle\Entity\IngredientsAttributes;
use AppBundle\Entity\IngredientsEvidences;
use AppBundle\Entity\IngredientUnfiltered;
use AppBundle\Form\IngredientType;
use AppBundle\Managers\DataTableManager;
use AppBundle\Managers\IngredientManager;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/core/ingredients")
 */
class IngredientController extends \AppBundle\Controller\AbstractApiController
{
    private $ingredientManager;
    private $dataTableManager;

    public function __construct(IngredientManager $ingredientManager, DataTableManager $dataTableManager)
    {
        $this->ingredientManager = $ingredientManager;
        $this->dataTableManager = $dataTableManager;
    }

    /**
     * Returns a list of data Ingredients allowing search by name and limit the number
     *
     * @QueryParam(name="q", requirements="\w+", allowBlank=true, description="Search by field")
     * @QueryParam(name="_limit", requirements="\d+", default="25", description="Page count limit")
     * @QueryParam(name="q", requirements="\w+", allowBlank=true, description="Searches for a particular text string")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="ingredients")
     *
     * @Route("", methods={"GET"})
     * @return Response
     */
    public function getIngredientsAction(Request $request)
    {
        $data = $this->dataTableManager->getListData(Ingredient::class, $request, [], true);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['productList'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }

    /**
     * Get ingredient
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="ingredients")
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getIngredientAction(Ingredient $ingredient)
    {
        return $ingredient;
    }

    /**
     * @SWG\Put(
     *      summary="Update Ingredient",
     *      tags={"ingredients"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\IngredientType::class)
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateIngredientAction(Request $request, Ingredient $ingredient)
    {
        $form = $this->createForm(IngredientType::class, $ingredient);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->ingredientManager->updateIngredient($form->getData());
        }
    }

    /**
     * Returns a list of data Evidences for this Ingredient
     *
     * @QueryParam(name="q", requirements="\w+", allowBlank=true, description="Search by field")
     * @QueryParam(name="_limit", requirements="\d+", default="25", description="Page count limit")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="ingredients")
     *
     * @Route("/{ingredient_id}/evidences", methods={"GET"})
     * @return Response
     */
    public function getIngredientsEvidencesAction(Request $request, Ingredient $ingredient_id)
    {
        $data = $this->dataTableManager->getListData(IngredientsEvidences::class, $request, ['ingredient' => $ingredient_id]);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['listIngred'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }

    /**
     * Returns a list of data Attributes for this Ingredient
     *
     * @QueryParam(name="q", requirements="\w+", allowBlank=true, description="Search by field")
     * @QueryParam(name="_limit", requirements="\d+", default="25", description="Page count limit")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="ingredients")
     *
     * @Route("/{ingredient_id}/attributes", methods={"GET"})
     * @return Response
     */
    public function getIngredientsAttributesAction(Request $request, Ingredient $ingredient_id)
    {
        $data = $this->dataTableManager->getListData(IngredientsAttributes::class, $request, ['ingredient' => $ingredient_id]);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['listIngred'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }

    /**
     * Returns a list of data Linked Ingredients
     *
     * @QueryParam(name="q", requirements="\w+", allowBlank=true, description="Search by field")
     * @QueryParam(name="_limit", requirements="\d+", default="25", description="Page count limit")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="ingredients")
     *
     * @Route("/{ingredient_id}/linked-ingredients", methods={"GET"})
     * @return Response
     */
    public function getLinkedIngredientsAction(Request $request, Ingredient $ingredient_id)
    {
        $data = $this->dataTableManager->getListData(IngredientUnfiltered::class, $request, ['ingredientId' => $ingredient_id]);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['list'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }

    /**
     * Returns a list of data Product Ingredients
     *
     * @QueryParam(name="q", requirements="\w+", allowBlank=true, description="Search by field")
     * @QueryParam(name="_limit", requirements="\d+", default="25", description="Page count limit")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="ingredients")
     *
     * @Route("/{ingredient_id}/products", methods={"GET"})
     * @return Response
     */
    public function getProductsIngredientsAction(Request $request, Ingredient $ingredient_id)
    {
        $data = $this->ingredientManager->getProductsByIngredient($ingredient_id);

        $data = ['rows' => $data, 'totalCount' => count($data)];

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['productIngredient'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }


    /**
     *
     * @SWG\Post(
     *      summary="Get ingredients from Bulk",
     *      description="Response [ingredients[name, symbol], legends[symbol, legends]]",
     *      tags={"ingredients"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="ingredientsText", type="string", example="")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("/parse", methods={"POST"})
     */
    public function getIngredientsFromBulkAction(Request $request)
    {
        return $this->ingredientManager->getIngredientsFromBulk($request->get('ingredientsText'));
    }
}
