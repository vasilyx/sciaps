<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\Manufacturer;
use AppBundle\Managers\DataTableManager;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;

use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/core/manufacturers")
 */
class ManufacturerController extends \AppBundle\Controller\AbstractApiController
{
    private $dataTableManager;

    public function __construct(DataTableManager $dataTableManager)
    {
        $this->dataTableManager = $dataTableManager;
    }

    /**
     * Returns a list of manufacturers allowing for filtering, pagination and sorting
     *
     * @QueryParam(name="_page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="_limit", requirements="\d+",   default="25", description="Page count limit")
     * @QueryParam(name="_order", requirements="(asc|desc)", allowBlank=true, default="asc", description="Sort
     *     direction")
     * @QueryParam(name="_sort", requirements="\w+", allowBlank=true, description="Sorts based on a particular field")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="manufacturers")
     *
     * @Route("", methods={"GET"})
     * @return Response
     */
    public function getManufacturersAction(Request $request)
    {
        $data =  $this->dataTableManager->getListData(Manufacturer::class, $request, [], true);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
             SerializationContext::create()->setGroups(['list'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }

    /**
     * Get manufacture
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="manufacturers")
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getManufacturerAction(Manufacturer $manufacturer)
    {
        return $manufacturer;
    }
}
