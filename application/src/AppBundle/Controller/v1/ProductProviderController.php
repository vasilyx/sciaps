<?php
/*
 * 1
 */
namespace AppBundle\Controller\v1;

use AppBundle\Managers\DataTableManager;
use AppBundle\Managers\ProductProviderManager;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Traits\ContainerHelpers;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/core/product-providers")
 */
class ProductProviderController extends \AppBundle\Controller\AbstractApiController
{
    use ContainerHelpers;

    private $productProviderManager;
    private $dataTableManager;

    public function __construct(ProductProviderManager $productProviderManager, DataTableManager $dataTableManager )
    {
        $this->productProviderManager = $productProviderManager;
        $this->dataTableManager = $dataTableManager;
    }
}
