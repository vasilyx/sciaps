<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\Attribute;
use AppBundle\Entity\Evidence;
use AppBundle\Entity\Manufacturer;
use AppBundle\Entity\ManufacturerAttributes;
use AppBundle\Entity\ManufacturerEvidences;
use AppBundle\Entity\Source;
use AppBundle\Form\Associations\AttributeManufacturerCreationType;
use AppBundle\Form\Associations\AttributeManufacturerUpdateType;
use AppBundle\Form\Associations\EvidenceManufacturerCreationType;
use AppBundle\Form\Associations\EvidenceManufacturerUpdateType;
use AppBundle\Managers\DataTableManager;
use AppBundle\Managers\ManufacturerAssociationManager;
use Nelmio\ApiDocBundle\Annotation\Model;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/core/sources")
 */
class ManufacturerAssociationController extends \AppBundle\Controller\AbstractApiController
{
    private $manufacturerManager;
    private $dataTableManager;

    public function __construct(ManufacturerAssociationManager $manufacturerManager, DataTableManager $dataTableManager)
    {
        $this->manufacturerManager = $manufacturerManager;
        $this->dataTableManager = $dataTableManager;
    }

    /**
     * Get all Evidence for Source
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/evidence/manufacturer", methods={"GET"})
     *
     * @return Response
     */
    public function getEvidencesAction(Source $source_id)
    {
        $data = $this->manufacturerManager->getEvidences($source_id);
        $list = $this->container->get('jms_serializer')->serialize(
            $data,
            'json',
            SerializationContext::create()->setGroups(['details'])
        );

        return new Response($list, Response::HTTP_OK);
    }

    /**
     * @SWG\Post(
     *      summary="Add ManufacturerEvidence association [ADMIN, SKINNINJA] ",
     *      description="",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\EvidenceManufacturerCreationType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{source_id}/evidence/{evidence_id}/manufacturer", methods={"POST"})
     * @return Response
     */
    public function addManufacturerEvidenceAction(Request $request, Source $source_id, Evidence $evidence_id)
    {
        $form = $this->createForm(EvidenceManufacturerCreationType::class, new ManufacturerEvidences());
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            if ($this->manufacturerManager->createManufacturerEvidence($form->getData(), $request, $source_id, $evidence_id)) {
                return new Response(Response::HTTP_OK);
            }
        }
    }

    /**
     * @SWG\Put(
     *      summary="Update ManufacturerEvidence association [ADMIN, SKINNINJA]",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\EvidenceManufacturerUpdateType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{source_id}/evidence/{evidence_id}/manufacturer/{manufacturer_id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateManufacturerEvidenceAction(Request $request, Source $source_id, Evidence $evidence_id, Manufacturer $manufacturer_id)
    {
        $manufacturersEvidence = $this->manufacturerManager->findManufacturersEvidence($source_id, $evidence_id, $manufacturer_id);
        if (!$manufacturersEvidence) return new Response('ManufacturersEvidence with this ID\'s did not exist', Response::HTTP_NOT_FOUND);

        $form = $this->createForm(EvidenceManufacturerUpdateType::class, $manufacturersEvidence);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->manufacturerManager->updateManufacturerEvidence($form->getData());
        }
    }

    /**
     * Delete Manufacturer for Evidence
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/evidence/{evidence_id}/manufacturer/{manufacturer_id}", methods={"DELETE"})
     *
     * @return View
     */
    public function deleteManufacturerEvidenceAction(Source $source_id, Evidence $evidence_id, Manufacturer $manufacturer_id)
    {
        $this->manufacturerManager->deleteManufacturerEvidence($source_id, $evidence_id, $manufacturer_id);

        $this->getResponse(null);
    }

    /**
     * Get SourceEvidenceManufacturer
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/evidence/{evidence_id}/manufacturer/{manufacturer_id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getSourceEvidenceAction(Source $source_id, Evidence $evidence_id, Manufacturer $manufacturer_id)
    {
        return $this->manufacturerManager->getManufacturersEvidences($source_id,$evidence_id, $manufacturer_id);
    }

    /**
     * Get all Attribute for Source
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/attribute/manufacturer", methods={"GET"})
     *
     * @return Response
     */
    public function getAttributeAction(Source $source_id)
    {
        $data = $this->manufacturerManager->getAttributes($source_id);
        $list = $this->container->get('jms_serializer')->serialize(
            $data,
            'json',
            SerializationContext::create()->setGroups(['details'])
        );

        return new Response($list, Response::HTTP_OK);
    }

    /**
     * @SWG\Post(
     *      summary="Add ManufacturerAttribute association [ADMIN, SKINNINJA] ",
     *      description="",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\AttributeManufacturerCreationType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("/{source_id}/attribute/{attribute_id}/manufacturer", methods={"POST"})
     * @return Response
     */
    public function addManufacturerAttributeAction(Request $request, Source $source_id, Attribute $attribute_id)
    {
        $form = $this->createForm(AttributeManufacturerCreationType::class, new ManufacturerAttributes());
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            if ($this->manufacturerManager->createManufacturerAttribute($form->getData(), $request, $source_id, $attribute_id)) {
                return new Response(Response::HTTP_OK);
            }
        }
    }

    /**
     * @SWG\Put(
     *      summary="Update ManufacturerAttribute association [ADMIN, SKINNINJA]",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\AttributeManufacturerUpdateType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{source_id}/attribute/{attribute_id}/manufacturer/{manufacturer_id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateManufacturerAttributeAction(Request $request, Source $source_id, Attribute $attribute_id, Manufacturer $manufacturer_id)
    {
        $manufacturersAttribute = $this->manufacturerManager->findManufacturersAttribute($source_id, $attribute_id, $manufacturer_id);
        if (empty($manufacturersAttribute)) return new Response('ManufacturersAttribute with this ID\'s did not exist', Response::HTTP_NOT_FOUND);

        $form = $this->createForm(AttributeManufacturerUpdateType::class, $manufacturersAttribute);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->manufacturerManager->updateManufacturerAttribute($form->getData());
        }
    }

    /**
     * Delete Attribute for Evidence
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/attribute/{attribute_id}/manufacturer/{manufacturer_id}", methods={"DELETE"})
     *
     * @return View
     */
    public function deleteManufacturerAttributeAction(Source $source_id, Attribute $attribute_id, Manufacturer $manufacturer_id)
    {
        $this->manufacturerManager->deleteManufacturerAttribute($source_id, $attribute_id, $manufacturer_id);

        $this->getResponse(null);
    }

    /**
     * Get SourceAttributeManufacturer
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/attribute/{attribute_id}/manufacturer/{manufacturer_id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getSourceAttributeAction(Source $source_id, Attribute $attribute_id, Manufacturer $manufacturer_id)
    {
        return $this->manufacturerManager->getManufacturersAttributes($source_id,$attribute_id, $manufacturer_id);
    }
}
