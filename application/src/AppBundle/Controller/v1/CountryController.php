<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\Country;
use AppBundle\Managers\CountryManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;
use AppBundle\Managers\DataTableManager;
use JMS\Serializer\SerializationContext;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/core/countries")
 */
class CountryController extends \AppBundle\Controller\AbstractApiController
{
    private $countryManager;
    private $dataTableManager;

    public function __construct(CountryManager $countryManager, DataTableManager $dataTableManager)
    {
        $this->countryManager = $countryManager;
        $this->dataTableManager = $dataTableManager;
    }



    /**
     *  Returns a list of products allowing for filtering, pagination and sorting
     *
     * @QueryParam(name="_page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="_limit", requirements="\d+",   default="25", description="Page count limit")
     * @QueryParam(name="_order", requirements="(asc|desc)", allowBlank=true, default="asc", description="Sort
     *     direction")
     * @QueryParam(name="_sort", requirements="\w+", allowBlank=true, description="Sorts based on a particular field")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="countries")
     *
     * @Route("", methods={"GET"})
     * @return Response
     */
    public function getCountriesAction(Request $request)
    {
        $data =  $this->dataTableManager->getListData(Country::class, $request);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['list'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }
}