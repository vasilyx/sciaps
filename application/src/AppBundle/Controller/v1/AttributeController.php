<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\Attribute;
use AppBundle\Form\AttributeType;
use AppBundle\Managers\AttributeManager;
use AppBundle\Managers\DataTableManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/core/attributes")
 */
class AttributeController extends \AppBundle\Controller\AbstractApiController
{
    private $attributeManager;
    private $dataTableManager;

    public function __construct(AttributeManager $attributeManager, DataTableManager $dataTableManager)
    {
        $this->attributeManager = $attributeManager;
        $this->dataTableManager = $dataTableManager;
    }

    /**
     * Returns a list of data attributes allowing for filtering, pagination and sorting
     *
     * @QueryParam(name="_page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="_limit", requirements="\d+", default="25", description="Page count limit")
     * @QueryParam(name="_order", requirements="(asc|desc)", allowBlank=true, default="asc", description="Sort
     *     direction")
     * @QueryParam(name="_sort", requirements="\w+", allowBlank=true, description="Sorts based on a particular field")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="attributes")
     *
     * @Route("", methods={"GET"})
     * @return Response
     */
    public function getAttributesAction(Request $request)
    {
        $data =  $this->dataTableManager->getListData(Attribute::class, $request);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['list'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }

    /**
     * Get Attribute
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="attributes")
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @return View
     */
    public function getAttributeAction(Attribute $attribute)
    {
        return $attribute;
    }

    /**
     * Delete Attribute
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Response(response=405, description="Method Not Allowed")
     * @SWG\Tag(name="attributes")
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @return JsonResponse|Response
     */
    public function deleteAttributeAction(Attribute $attribute)
    {
        if ($this->attributeManager->removeAttribute($attribute)) {
            $response = new Response("", Response::HTTP_OK);
        }
        else {
            $response = new JsonResponse(['errors' => 'This attribute has existing relationships. Please remove these relationships first and then try again!'], Response::HTTP_METHOD_NOT_ALLOWED);
        }
        return $response;
    }

    /**
     * @SWG\Post(
     *      summary="Add Attribute [ADMIN, SKINNINJA]",
     *      tags={"attributes"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\AttributeType::class)
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("", methods={"POST"})
     */
    public function addAttributeAction(Request $request)
    {
        $form = $this->createForm(AttributeType::class, new Attribute());

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->attributeManager->createNewAttribute($form->getData());
        }
    }

    /**
     * @SWG\Put(
     *      summary="Update Attribute",
     *      tags={"attributes"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\AttributeType::class)
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateAttributeAction(Request $request, Attribute $attribute)
    {
        $form = $this->createForm(AttributeType::class, $attribute);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->attributeManager->createNewAttribute($form->getData());
        }
    }
}
