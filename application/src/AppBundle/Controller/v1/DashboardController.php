<?php
namespace AppBundle\Controller\v1;


use AppBundle\Managers\ProductManager;
use AppBundle\Response\BaseResponse;

use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;

use AppBundle\Traits\ContainerHelpers;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;

use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/core")
 */
class DashboardController extends \AppBundle\Controller\AbstractApiController
{
    use ContainerHelpers;

    private $productManager;

    public function __construct(ProductManager $productManager)
    {
        $this->productManager = $productManager;
    }

    /**
     * Get product dashboard
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="dashboard")
     *
     * @Route("/dashboard", methods={"GET"})
     *
     * @Rest\View()
     */
    public function getDashboardAction()
    {
        return $this->productManager->getDashboard($this->getCurrentUser());
    }

}
