<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\Attribute;
use AppBundle\Entity\Evidence;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductAttributes;
use AppBundle\Entity\ProductEvidences;
use AppBundle\Entity\Source;
use AppBundle\Form\Associations\AttributeProductCreationType;
use AppBundle\Form\Associations\AttributeProductUpdateType;
use AppBundle\Form\Associations\EvidenceProductCreationType;
use AppBundle\Form\Associations\EvidenceProductUpdateType;
use AppBundle\Managers\DataTableManager;
use AppBundle\Managers\ProductAssociationManager;
use Nelmio\ApiDocBundle\Annotation\Model;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;


/**
 * @Route("/api/v1/core/sources")
 */
class ProductAssociationController extends \AppBundle\Controller\AbstractApiController
{
    private $productManager;
    private $dataTableManager;

    public function __construct(ProductAssociationManager $productManager, DataTableManager $dataTableManager)
    {
        $this->productManager = $productManager;
        $this->dataTableManager = $dataTableManager;
    }

    /**
     * Get all Evidence for Source
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/evidence/product", methods={"GET"})
     *
     * @return Response
     */
    public function getEvidencesAction(Source $source_id)
    {
        $data = $this->productManager->getEvidences($source_id);
        $list = $this->container->get('jms_serializer')->serialize(
            $data,
            'json',
            SerializationContext::create()->setGroups(['details'])
        );

        return new Response($list, Response::HTTP_OK);
    }

    /**
     * @SWG\Post(
     *      summary="Add ProductEvidence association [ADMIN, SKINNINJA] ",
     *      description="",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\EvidenceProductCreationType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{source_id}/evidence/{evidence_id}/product", methods={"POST"})
     * @return Response
     */
    public function addProductEvidenceAction(Request $request, Source $source_id, Evidence $evidence_id)
    {
        $form = $this->createForm(EvidenceProductCreationType::class, new ProductEvidences());
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            if ($this->productManager->createProductEvidence($form->getData(), $request, $source_id, $evidence_id)) {
                return new Response(Response::HTTP_OK);
            }
        }
    }

    /**
     * @SWG\Put(
     *      summary="Update ProductEvidence association [ADMIN, SKINNINJA]",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\EvidenceProductUpdateType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{source_id}/evidence/{evidence_id}/product/{product_id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateProductEvidenceAction(Request $request, Source $source_id, Evidence $evidence_id, Product $product_id)
    {
        $productsEvidence = $this->productManager->findProductsEvidence($source_id, $evidence_id, $product_id);
        if (!$productsEvidence) return new Response('ProductsEvidence with this ID\'s did not exist', Response::HTTP_NOT_FOUND);

        $form = $this->createForm(EvidenceProductUpdateType::class, $productsEvidence);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->productManager->updateProductEvidence($form->getData());
        }
    }

    /**
     * Delete Product for Evidence
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/evidence/{evidence_id}/product/{product_id}", methods={"DELETE"})
     *
     * @return View
     */
    public function deleteProductEvidenceAction(Source $source_id, Evidence $evidence_id, Product $product_id)
    {
        $this->productManager->deleteProductEvidence($source_id, $evidence_id, $product_id);

        $this->getResponse(null);
    }

    /**
     * Get SourceEvidenceProduct
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/evidence/{evidence_id}/product/{product_id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getSourceEvidenceAction(Source $source_id, Evidence $evidence_id, Product $product_id)
    {
        return $this->productManager->getProductsEvidences($source_id,$evidence_id, $product_id);
    }

    /**
     * Get all Attribute for Source
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/attribute/product", methods={"GET"})
     *
     * @return Response
     */
    public function getAttributeAction(Source $source_id)
    {
        $data = $this->productManager->getAttributes($source_id);
        $list = $this->container->get('jms_serializer')->serialize(
            $data,
            'json',
            SerializationContext::create()->setGroups(['details'])
        );

        return new Response($list, Response::HTTP_OK);
    }

    /**
     * @SWG\Post(
     *      summary="Add ProductAttribute association [ADMIN, SKINNINJA] ",
     *      description="",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\AttributeProductCreationType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("/{source_id}/attribute/{attribute_id}/product", methods={"POST"})
     * @return Response
     */
    public function addProductAttributeAction(Request $request, Source $source_id, Attribute $attribute_id)
    {
        $form = $this->createForm(AttributeProductCreationType::class, new ProductAttributes());
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            if ($this->productManager->createProductAttribute($form->getData(), $request, $source_id, $attribute_id)) {
                return new Response(Response::HTTP_OK);
            }
        }
    }

    /**
     * @SWG\Put(
     *      summary="Update ProductAttribute association [ADMIN, SKINNINJA]",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\AttributeProductUpdateType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{source_id}/attribute/{attribute_id}/product/{product_id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateProductAttributeAction(Request $request, Source $source_id, Attribute $attribute_id, Product $product_id)
    {
        $productsAttribute = $this->productManager->findProductsAttribute($source_id, $attribute_id, $product_id);
        if (empty($productsAttribute)) return new Response('ProductsAttribute with this ID\'s did not exist', Response::HTTP_NOT_FOUND);

        $form = $this->createForm(AttributeProductUpdateType::class, $productsAttribute);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->productManager->updateProductAttribute($form->getData());
        }
    }

    /**
     * Delete Attribute for Evidence
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/attribute/{attribute_id}/product/{product_id}", methods={"DELETE"})
     *
     * @return View
     */
    public function deleteProductAttributeAction(Source $source_id, Attribute $attribute_id, Product $product_id)
    {
        $this->productManager->deleteProductAttribute($source_id, $attribute_id, $product_id);

        $this->getResponse(null);
    }

    /**
     * Get SourceAttributeProduct
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/attribute/{attribute_id}/product/{product_id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getSourceAttributeAction(Source $source_id, Attribute $attribute_id, Product $product_id)
    {
        return $this->productManager->getProductsAttributes($source_id,$attribute_id, $product_id);
    }
}
