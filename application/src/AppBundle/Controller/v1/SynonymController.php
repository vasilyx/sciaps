<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\Synonym;
use AppBundle\Managers\DataTableManager;
use AppBundle\Managers\SynonymManager;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/core/synonyms")
 */
class SynonymController extends \AppBundle\Controller\AbstractApiController
{
    private $synonymManager;
    private $dataTableManager;

    public function __construct(SynonymManager $synonymManager, DataTableManager $dataTableManager)
    {
        $this->synonymManager = $synonymManager;
        $this->dataTableManager = $dataTableManager;
    }

    /**
     * Returns a list of data Synonyms allowing search by name and limit the number
     *
     * @QueryParam(name="q", requirements="\w+", allowBlank=true, description="Search by field")
     * @QueryParam(name="_limit", requirements="\d+", default="25", description="Page count limit")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="synonyms")
     *
     * @Route("", methods={"GET"})
     * @return Response
     */
    public function getSynonymsAction(Request $request)
    {
        $data = $this->dataTableManager->getListData(Synonym::class, $request, [], true);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['list'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }
}
