<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\Attribute;
use AppBundle\Entity\Evidence;
use AppBundle\Entity\Ingredient;
use AppBundle\Entity\IngredientsAttributes;
use AppBundle\Entity\IngredientsEvidences;
use AppBundle\Entity\Source;
use AppBundle\Form\Associations\AttributeIngredientCreationType;
use AppBundle\Form\Associations\AttributeIngredientUpdateType;
use AppBundle\Form\Associations\EvidenceIngredientCreationType;
use AppBundle\Form\Associations\EvidenceIngredientUpdateType;
use AppBundle\Managers\DataTableManager;
use AppBundle\Managers\IngredientAssociationManager;
use Nelmio\ApiDocBundle\Annotation\Model;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;


/**
 * @Route("/api/v1/core/sources")
 */
class IngredientAssociationController extends \AppBundle\Controller\AbstractApiController
{
    private $ingredientManager;
    private $dataTableManager;

    public function __construct(IngredientAssociationManager $ingredientManager, DataTableManager $dataTableManager)
    {
        $this->ingredientManager = $ingredientManager;
        $this->dataTableManager = $dataTableManager;
    }

    /**
     * Get all Evidence for Source
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/evidence/ingredient", methods={"GET"})
     *
     * @return Response
     */
    public function getEvidencesAction(Source $source_id)
    {
        $data = $this->ingredientManager->getEvidences($source_id);
        $list = $this->container->get('jms_serializer')->serialize(
            $data,
            'json',
            SerializationContext::create()->setGroups(['details'])
        );

        return new Response($list, Response::HTTP_OK);
    }

    /**
     * @SWG\Post(
     *      summary="Add IngredientEvidence association [ADMIN, SKINNINJA] ",
     *      description="",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\EvidenceIngredientCreationType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{source_id}/evidence/{evidence_id}/ingredient", methods={"POST"})
     * @return Response
     */
    public function addIngredientEvidenceAction(Request $request, Source $source_id, Evidence $evidence_id)
    {
        $form = $this->createForm(EvidenceIngredientCreationType::class, new IngredientsEvidences());
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            if ($this->ingredientManager->createIngredientEvidence($form->getData(), $request, $source_id, $evidence_id)) {
                return new Response(Response::HTTP_OK);
            }
        }
    }

    /**
     * @SWG\Put(
     *      summary="Update IngredientEvidence association [ADMIN, SKINNINJA]",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\EvidenceIngredientUpdateType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{source_id}/evidence/{evidence_id}/ingredient/{ingredient_id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateIngredientEvidenceAction(Request $request, Source $source_id, Evidence $evidence_id, Ingredient $ingredient_id)
    {
        $ingredientsEvidence = $this->ingredientManager->findIngredientsEvidence($source_id, $evidence_id, $ingredient_id);
        if (!$ingredientsEvidence) return new Response('IngredientsEvidence with this ID\'s did not exist', Response::HTTP_NOT_FOUND);

        $form = $this->createForm(EvidenceIngredientUpdateType::class, $ingredientsEvidence);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->ingredientManager->updateIngredientEvidence($form->getData());
        }
    }

    /**
     * Delete Ingredient for Evidence
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/evidence/{evidence_id}/ingredient/{ingredient_id}", methods={"DELETE"})
     *
     * @return View
     */
    public function deleteIngredientEvidenceAction(Source $source_id, Evidence $evidence_id, Ingredient $ingredient_id)
    {
        $this->ingredientManager->deleteIngredientEvidence($source_id, $evidence_id, $ingredient_id);

        $this->getResponse(null);
    }

    /**
     * Get SourceEvidenceIngredient
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/evidence/{evidence_id}/ingredient/{ingredient_id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getSourceEvidenceAction(Source $source_id, Evidence $evidence_id, Ingredient $ingredient_id)
    {
        return $this->ingredientManager->getIngredientsEvidences($source_id,$evidence_id, $ingredient_id);
    }

    /**
     * Get all Attribute for Source
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/attribute/ingredient", methods={"GET"})
     *
     * @return Response
     */
    public function getAttributeAction(Source $source_id)
    {
        $data = $this->ingredientManager->getAttributes($source_id);
        $list = $this->container->get('jms_serializer')->serialize(
            $data,
            'json',
            SerializationContext::create()->setGroups(['details'])
        );

        return new Response($list, Response::HTTP_OK);
    }

    /**
     * @SWG\Post(
     *      summary="Add IngredientAttribute association [ADMIN, SKINNINJA] ",
     *      description="",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\AttributeIngredientCreationType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("/{source_id}/attribute/{attribute_id}/ingredient", methods={"POST"})
     * @return Response
     */
    public function addIngredientAttributeAction(Request $request, Source $source_id, Attribute $attribute_id)
    {
        $form = $this->createForm(AttributeIngredientCreationType::class, new IngredientsAttributes());
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            if ($this->ingredientManager->createIngredientAttribute($form->getData(), $request, $source_id, $attribute_id)) {
                return new Response(Response::HTTP_OK);
            }
        }
    }

    /**
     * @SWG\Put(
     *      summary="Update IngredientAttribute association [ADMIN, SKINNINJA]",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\AttributeIngredientUpdateType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{source_id}/attribute/{attribute_id}/ingredient/{ingredient_id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateIngredientAttributeAction(Request $request, Source $source_id, Attribute $attribute_id, Ingredient $ingredient_id)
    {
        $ingredientsAttribute = $this->ingredientManager->findIngredientsAttribute($source_id, $attribute_id, $ingredient_id);
        if (empty($ingredientsAttribute)) return new Response('IngredientsAttribute with this ID\'s did not exist', Response::HTTP_NOT_FOUND);

        $form = $this->createForm(AttributeIngredientUpdateType::class, $ingredientsAttribute);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->ingredientManager->updateIngredientAttribute($form->getData());
        }
    }

    /**
     * Delete Attribute for Evidence
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/attribute/{attribute_id}/ingredient/{ingredient_id}", methods={"DELETE"})
     *
     * @return View
     */
    public function deleteIngredientAttributeAction(Source $source_id, Attribute $attribute_id, Ingredient $ingredient_id)
    {
        $this->ingredientManager->deleteIngredientAttribute($source_id, $attribute_id, $ingredient_id);

        $this->getResponse(null);
    }

    /**
     * Get SourceAttributeIngredient
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/attribute/{attribute_id}/ingredient/{ingredient_id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getSourceAttributeAction(Source $source_id, Attribute $attribute_id, Ingredient $ingredient_id)
    {
        return $this->ingredientManager->getIngredientsAttributes($source_id,$attribute_id, $ingredient_id);
    }

}
