<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use AppBundle\Form\ProductType;
use AppBundle\Managers\ProductManager;
use AppBundle\Managers\DataTableManager;
use AppBundle\Services\ExternalCommands;
use AppBundle\Services\OnTerminateDo;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;

use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/core/products")
 */
class ProductController extends \AppBundle\Controller\AbstractApiController
{
    private $productManager;
    private $dataTableManager;

    public function __construct(ProductManager $productManager, DataTableManager $dataTableManager)
    {
        $this->productManager = $productManager;
        $this->dataTableManager = $dataTableManager;
    }

    /**
     * Returns a list of products allowing for filtering, pagination and sorting
     *
     * @QueryParam(name="_page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="_limit", requirements="\d+",   default="25", description="Page count limit")
     * @QueryParam(name="_order", requirements="(asc|desc)", allowBlank=true, default="asc", description="Sort
     *     direction")
     * @QueryParam(name="_sort", requirements="\w+", allowBlank=true, description="Sorts based on a particular field")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="products")
     *
     * @Route("", methods={"GET"})
     * @return Response
     */
    public function getProductsAction(Request $request)
    {
        $where = ['productProvider' =>$this->getCurrentUser()->getCompany()->getProductProvider()];
        $data = $this->dataTableManager->getListData(Product::class, $request, $where, true);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
             SerializationContext::create()->setGroups(['productList'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }

    /**
     * Get Product
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="products")
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getProductAction(Product $product)
    {
        return $product;
    }

    /**
     * Get Product Ingredients
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="products")
     *
     * @Route("/{id}/ingredients", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getProductIngredientsAction(Product $product)
    {
        return $product->getIngredients();
    }


    /**
     * Delete Product
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="products")
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @return View
     */
    public function deleteProductAction(Product $product)
    {
        $this->productManager->removeProduct($product);

        $this->getResponse(null);
    }

    /**
     * @SWG\Post(
     *      summary="Add Product",
     *      tags={"products"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\ProductType::class)
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("", methods={"POST"})
     */
    public function addProductAction(Request $request)
    {
        $request = $this->productManager->rebuildQueryForFormAddProduct($request);

        $form = $this->createForm(ProductType::class, new Product());
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->productManager->createNewProduct($form->getData(), $this->getCurrentUser());
        }
    }

    /**
     * @SWG\Put(
     *      summary="Update Product",
     *      tags={"products"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\ProductType::class)
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateProductAction(Request $request, Product $product)
    {
        $request = $this->productManager->rebuildQueryForFormAddProduct($request);

        $form = $this->createForm(ProductType::class, $product);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->productManager->createNewProduct($form->getData(), $this->getCurrentUser());
        }
    }

    /**
     * @SWG\POST(
     *      summary="Step1, Upload CSV file in base64",
     *      tags={"products-upload"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="fileData", type="string", example="")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("/upload", methods={"POST"})
     */
    public function uploadProductAction(Request $request)
    {
        $response = $this->productManager->uploadCSV($request->get('fileData'), $this->getCurrentUser());

        if ($response->getErrors() !== null) {
            $response = new JsonResponse(['errors' => $response->getErrors()], $response->getStatusCode());
        }

        return $response;
    }

    /**
     * @SWG\POST(
     *      summary="Step2, Get sample of data",
     *      tags={"products-upload"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="mappingData", type="string", example="")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("/upload/sample-data", methods={"POST"})
     */
    public function uploadProductSampleDataAction(Request $request)
    {
        $response = $this->productManager->getSampleData($request->get('mappingData'), $this->getCurrentUser());

        if (!is_array($response) && $response->getErrors() !== null) {
            $response = new JsonResponse(['errors' => $response->getErrors()], $response->getStatusCode());
        }

        return $response;
    }

    /**
     * @SWG\POST(
     *      summary="Step3, Confirm mapping and display summary of mapping",
     *      tags={"products-upload"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="mappingData", type="string", example="")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("/upload/confirm-mapping-data", methods={"POST"})
     */
    public function uploadProductConfirmMappingDataAction(Request $request)
    {
        $response = $this->productManager->getConfirmMappingData($request->get('mappingData'), $this->getCurrentUser());

        if (!is_array($response) && $response->getErrors() !== null) {
            $response = new JsonResponse(['errors' => $response->getErrors()], $response->getStatusCode());
        }

        return $response;
    }

    /**
     * @SWG\POST(
     *      summary="Step4, Includes summary and confirmation of upload",
     *      tags={"products-upload"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="overwriteProducts", type="string", example="")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("/upload/confirm", methods={"POST"})
     * @param Request $request
     * @param OnTerminateDo $terminateDo
     * @param ExternalCommands $externalCommands
     * @return \AppBundle\Response\ProductUploadConfirmResponse|JsonResponse
     */
    public function uploadConfirmAction(Request $request)
    {
//        $terminateDo->add($externalCommands, 'runImportProducts', []);

        // @todo Update tochange import script to be external run command instead of in browser.
        $response = $this->productManager->getUploadConfirm($this->getCurrentUser(), $request->get('overwriteProducts'));

        if (!is_array($response) && $response->getErrors() !== null) {
            $response = new JsonResponse(['errors' => $response->getErrors()], $response->getStatusCode());
        }

        return $response;
    }
}
