<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\SourceDataProvider;
use AppBundle\Form\SourceDataProviderType;
use AppBundle\Managers\DataTableManager;
use AppBundle\Managers\SourceDataProviderManager;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/core/source-providers")
 */
class SourceDataProviderController extends \AppBundle\Controller\AbstractApiController
{
    private $sourceProviderManager;
    private $dataTableManager;

    public function __construct(SourceDataProviderManager $sourceProviderManager, DataTableManager $dataTableManager)
    {
        $this->sourceProviderManager = $sourceProviderManager;
        $this->dataTableManager = $dataTableManager;
    }
}
