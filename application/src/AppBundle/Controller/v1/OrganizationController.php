<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\Organization;
use AppBundle\Form\OrganizationType;
use AppBundle\Managers\DataTableManager;
use AppBundle\Managers\OrganizationManager;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/v1/core/organizations")
 */
class OrganizationController extends \AppBundle\Controller\AbstractApiController
{
    private $organizationManager;
    private $dataTableManager;

    public function __construct(OrganizationManager $organizationManager, DataTableManager $dataTableManager)
    {
        $this->organizationManager = $organizationManager;
        $this->dataTableManager = $dataTableManager;
    }

    /**
     * Returns a list of organizations allowing for filtering, pagination and sorting
     *
     * @QueryParam(name="_page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="_limit", requirements="\d+",   default="25", description="Page count limit")
     * @QueryParam(name="_order", requirements="(asc|desc)", allowBlank=true, default="asc", description="Sort
     *     direction")
     * @QueryParam(name="_sort", requirements="\w+", allowBlank=true, description="Sorts based on a particular field")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="organizations")
     *
     * @Route("", methods={"GET"})
     * @return Response
     */
    public function getOrganizationsAction(Request $request)
    {
        $data =  $this->dataTableManager->getListData(Organization::class, $request);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['list'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }

    /**
     * Returns all organizations on CSV file
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="organizations")
     *
     * @Route("/download", methods={"GET"})
     * @return StreamedResponse
     */
    public function createCSVAction(Request $request)
    {
        return $this->organizationManager->createCSV();
    }

    /**
     * Get Organization
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="organizations")
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getOrganizationAction(Organization $organization)
    {
        return $organization;
    }

    /**
     * Delete Organization
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="organizations")
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @return View
     */
    public function deleteOrganizationAction(Organization $organization)
    {
        $this->organizationManager->removeOrganization($organization);

        $this->getResponse(null);
    }

    /**
     * @SWG\Post(
     *      summary="Add Organization",
     *      tags={"organizations"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\OrganizationType::class)
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("", methods={"POST"})
     */
    public function addOrganizationAction(Request $request)
    {
        $form = $this->createForm(OrganizationType::class, new Organization());

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->organizationManager->createNewOrganization($form->getData());
        }
    }
}
