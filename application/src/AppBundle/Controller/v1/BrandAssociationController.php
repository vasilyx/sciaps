<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\Attribute;
use AppBundle\Entity\Evidence;
use AppBundle\Entity\Brand;
use AppBundle\Entity\BrandAttributes;
use AppBundle\Entity\BrandEvidences;
use AppBundle\Entity\Source;
use AppBundle\Form\Associations\AttributeBrandCreationType;
use AppBundle\Form\Associations\AttributeBrandUpdateType;
use AppBundle\Form\Associations\EvidenceBrandCreationType;
use AppBundle\Form\Associations\EvidenceBrandUpdateType;
use AppBundle\Managers\DataTableManager;
use AppBundle\Managers\BrandAssociationManager;
use Nelmio\ApiDocBundle\Annotation\Model;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;


/**
 * @Route("/api/v1/core/sources")
 */
class BrandAssociationController extends \AppBundle\Controller\AbstractApiController
{
    private $brandManager;
    private $dataTableManager;

    public function __construct(BrandAssociationManager $brandManager, DataTableManager $dataTableManager)
    {
        $this->brandManager = $brandManager;
        $this->dataTableManager = $dataTableManager;
    }

    /**
     * Get all Evidence for Source
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/evidence/brand", methods={"GET"})
     *
     * @return Response
     */
    public function getEvidencesAction(Source $source_id)
    {
        $data = $this->brandManager->getEvidences($source_id);
        $list = $this->container->get('jms_serializer')->serialize(
            $data,
            'json',
            SerializationContext::create()->setGroups(['details'])
        );

        return new Response($list, Response::HTTP_OK);
    }

    /**
     * @SWG\Post(
     *      summary="Add BrandEvidence association [ADMIN, SKINNINJA] ",
     *      description="",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\EvidenceBrandCreationType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{source_id}/evidence/{evidence_id}/brand", methods={"POST"})
     * @return Response
     */
    public function addBrandEvidenceAction(Request $request, Source $source_id, Evidence $evidence_id)
    {
        $form = $this->createForm(EvidenceBrandCreationType::class, new BrandEvidences());
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            if ($this->brandManager->createBrandEvidence($form->getData(), $request, $source_id, $evidence_id)) {
                return new Response(Response::HTTP_OK);
            }
        }
    }

    /**
     * @SWG\Put(
     *      summary="Update BrandEvidence association [ADMIN, SKINNINJA]",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\EvidenceBrandUpdateType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{source_id}/evidence/{evidence_id}/brand/{brand_id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateBrandEvidenceAction(Request $request, Source $source_id, Evidence $evidence_id, Brand $brand_id)
    {
        $brandsEvidence = $this->brandManager->findbrandsEvidence($source_id, $evidence_id, $brand_id);
        if (!$brandsEvidence) return new Response('BrandsEvidence with this ID\'s did not exist', Response::HTTP_NOT_FOUND);

        $form = $this->createForm(EvidenceBrandUpdateType::class, $brandsEvidence);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->brandManager->updateBrandEvidence($form->getData());
        }
    }

    /**
     * Delete Brand for Evidence
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/evidence/{evidence_id}/brand/{brand_id}", methods={"DELETE"})
     *
     * @return View
     */
    public function deleteBrandEvidenceAction(Source $source_id, Evidence $evidence_id, Brand $brand_id)
    {
        $this->brandManager->deleteBrandEvidence($source_id, $evidence_id, $brand_id);

        $this->getResponse(null);
    }

    /**
     * Get SourceEvidenceBrand
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/evidence/{evidence_id}/brand/{brand_id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getSourceEvidenceAction(Source $source_id, Evidence $evidence_id, Brand $brand_id)
    {
        return $this->brandManager->getBrandsEvidences($source_id,$evidence_id, $brand_id);
    }

    /**
     * Get all Attribute for Source
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/attribute/brand", methods={"GET"})
     *
     * @return Response
     */
    public function getAttributeAction(Source $source_id)
    {
        $data = $this->brandManager->getAttributes($source_id);
        $list = $this->container->get('jms_serializer')->serialize(
            $data,
            'json',
            SerializationContext::create()->setGroups(['details'])
        );

        return new Response($list, Response::HTTP_OK);
    }

    /**
     * @SWG\Post(
     *      summary="Add BrandAttribute association [ADMIN, SKINNINJA] ",
     *      description="",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\AttributeBrandCreationType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("/{source_id}/attribute/{attribute_id}/brand", methods={"POST"})
     * @return Response
     */
    public function addBrandAttributeAction(Request $request, Source $source_id, Attribute $attribute_id)
    {
        $form = $this->createForm(AttributeBrandCreationType::class, new BrandAttributes());
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            if ($this->brandManager->createBrandAttribute($form->getData(), $request, $source_id, $attribute_id)) {
                return new Response(Response::HTTP_OK);
            }
        }
    }

    /**
     * @SWG\Put(
     *      summary="Update BrandAttribute association [ADMIN, SKINNINJA]",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\Associations\AttributeBrandUpdateType::class )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{source_id}/attribute/{attribute_id}/brand/{brand_id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function updateBrandAttributeAction(Request $request, Source $source_id, Attribute $attribute_id, Brand $brand_id)
    {
        $brandsAttribute = $this->brandManager->findBrandsAttribute($source_id, $attribute_id, $brand_id);
        if (empty($brandsAttribute)) return new Response('BrandsAttribute with this ID\'s did not exist', Response::HTTP_NOT_FOUND);

        $form = $this->createForm(AttributeBrandUpdateType::class, $brandsAttribute);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->brandManager->updateBrandAttribute($form->getData());
        }
    }

    /**
     * Delete Attribute for Evidence
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/attribute/{attribute_id}/brand/{brand_id}", methods={"DELETE"})
     *
     * @return View
     */
    public function deleteBrandAttributeAction(Source $source_id, Attribute $attribute_id, Brand $brand_id)
    {
        $this->brandManager->deleteBrandAttribute($source_id, $attribute_id, $brand_id);

        $this->getResponse(null);
    }

    /**
     * Get SourceAttributeBrand
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{source_id}/attribute/{attribute_id}/brand/{brand_id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getSourceAttributeAction(Source $source_id, Attribute $attribute_id, Brand $brand_id)
    {
        return $this->brandManager->getBrandsAttributes($source_id,$attribute_id, $brand_id);
    }
}
