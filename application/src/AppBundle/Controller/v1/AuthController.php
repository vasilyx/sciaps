<?php

namespace AppBundle\Controller\v1;

use AppBundle\Managers\UserManager;
use AppBundle\Security\ApiKeyUserProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\User;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RequestContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Route("/api/v1/core/auth")
 */
class AuthController extends \AppBundle\Controller\AbstractApiController
{
    private $apiKeyUserProvider;
    private $userManager;

    public function __construct(ApiKeyUserProvider $apiKeyUserProvider,
                                UserManager $userManager)
    {
        $this->apiKeyUserProvider = $apiKeyUserProvider;
        $this->userManager = $userManager;
    }


    /**
     * @SWG\Post(
     *      summary="Login. Returns Auth token on success in element 'token' of the array ",
     *      tags={"auth"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="email", type="string", example="vasilyx20@gmail.com"),
     *              @SWG\Property(property="password", type="string", example="qwaszx10")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Wrong password"),
     *      @SWG\Response(response=404, description="Wrong username"),
     * )
     * @Route("/login", methods={"POST"})
     * @Rest\View(serializerGroups={"details"})
     */
    public function loginAction(Request $request, UserManager $userManager)
    {
        $token = $userManager->login(
            $request->request->get('email'),
            $request->request->get('password')
        );

        $user = $this->apiKeyUserProvider->loadUserByToken($token);
        $request->getSession()->set('currentUser', $user->getId());

        return ['token' => $token, 'user' => $user];
     }


    /**
     *  Return 200 response if user logged

     * @SWG\Response(response=200, description="Success"),
     * @SWG\Response(response=401, description="Returned when the user is not authorized"),
     * @SWG\Tag(name="auth"),
     * @Route("/check", methods={"GET"})
     */
    public function checkAction()
    {
        return new Response(null);
    }


    /**
     *  Return logged User

     * @SWG\Response(response=200, description="Success"),
     * @SWG\Response(response=401, description="Returned when the user is not authorized"),
     * @SWG\Tag(name="auth"),
     *
     * @Rest\View(serializerGroups={"current"})

     * @Route("/me", methods={"GET"})
     */
    public function meAction(Request $request)
    {
        return $this->userManager->loadByToken($request->headers->get('token'));
    }

    /**
     *  Logout. Return 200 if OK. Auth token must be removed
     * @SWG\Response(response=200, description="Success"),
     * @SWG\Tag(name="auth"),
     * @Route("/logout", methods={"POST"})
     */
    public function logoutAction(Request $request)
    {
        $token = $request->headers->get('token');
        $request->getSession()->remove('currentUser');
        $this->userManager->destroySession($token);

        return new Response(null);
    }


    /**
     * @SWG\Post(
     *      summary="Forgot password. Enter your username and check email.",
     *      tags={"auth"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="email", type="string", example="vasilyx20@gmail.com")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Email not found"),
     * )
     * @Route("/forgot-password", methods={"POST"})
     */
    public function forgottenAction(Request $request)
    {
        $user = $this->userManager->getUserByEmail(
            $request->request->get('email')
        );

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Email not found');
        }

        $this->userManager->updateForgotPasswordToken($user);

        return new Response(null);
    }

    /**
     * @SWG\Post(
     *      summary="Reset password. Enter your username. Token - `t` param from email.",
     *      tags={"auth"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="password", type="string", example="qwaszx10"),
     *              @SWG\Property(property="token", type="string", example="!aA1oQw23!")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Wrong password"),
     *      @SWG\Response(response=404, description="Invitation key is invalid"),
     * )
     * @Route("/reset-password", methods={"POST"})
     */
    public function resetPasswordAction(Request $request)
    {
        $user = $this->userManager->getUserByForgottenPasswordToken(
            $request->request->get('token')
        );

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Invitation key is invalid');
        }

        $this->userManager->updatePassword(
            $user,
            $request->request->get('password')
        );

        return new Response(null);
    }


    /**
     * @SWG\Post(
     *      summary="Activate user. Token - `t` param from email.",
     *      tags={"auth"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="password", type="string", example="qwaszx10"),
     *              @SWG\Property(property="token", type="string", example="!aA1oQw23!")
     *          )
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=400, description="Wrong password"),
     *      @SWG\Response(response=404, description="Invitation key is invalid"),
     * )
     * @Route("/activation", methods={"POST"})
     */
    public function activationAction(Request $request)
    {
        $user = $this->userManager->getUserByConfirmEmailToken(
            $request->request->get('token')
        );

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Invitation key is invalid');
        }

        $this->userManager->activateAccount(
            $user,
            $request->request->get('password')
        );

        return new Response(null);
    }
}
