<?php

namespace AppBundle\Controller\v1;

use AppBundle\Entity\Source;
use AppBundle\Form\SourceType;
use AppBundle\Managers\SourceManager;
use AppBundle\Managers\DataTableManager;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\QueryParam as QueryParam;
use FOS\RestBundle\View\View;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations as Rest;


/**
 * @Route("/api/v1/core/sources")
 */
class SourceController extends \AppBundle\Controller\AbstractApiController
{
    private $sourceManager;
    private $dataTableManager;

    public function __construct(SourceManager $sourceManager, DataTableManager $dataTableManager)
    {
        $this->sourceManager = $sourceManager;
        $this->dataTableManager = $dataTableManager;
    }

    /**
     * Returns a list of sources allowing for filtering, pagination and sorting
     *
     * @QueryParam(name="_page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="_limit", requirements="\d+",   default="25", description="Page count limit")
     * @QueryParam(name="_order", requirements="(asc|desc)", allowBlank=true, default="asc", description="Sort
     *     direction")
     * @QueryParam(name="_sort", requirements="\w+", allowBlank=true, description="Sorts based on a particular field")
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Tag(name="sources")
     *
     * @Route("", methods={"GET"})
     * @return Response
     */
    public function getSourcesAction(Request $request)
    {
        $data =  $this->dataTableManager->getListData(Source::class, $request);

        $list = $this->container->get('jms_serializer')->serialize(
            $data['rows'],
            'json',
            SerializationContext::create()->setGroups(['list'])
        );

        return new Response($list, Response::HTTP_OK, ['X-Total-Count' => $data['totalCount']]);
    }

    /**
     * Get Source
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @Rest\View(serializerGroups={"details"})
     */
    public function getSourceAction(Source $source)
    {
        return $source;
    }

    /**
     * Delete Source
     *
     * @SWG\Response(response=200, description="Success")
     * @SWG\Response(response=404, description="Not Found")
     * @SWG\Tag(name="sources")
     *
     * @Route("/{id}", methods={"DELETE"})
     *
     * @return View
     */
    public function deleteSourceAction(Source $source)
    {
        $this->sourceManager->removeSource($source);

        $this->getResponse(null);
    }

    /**
     * @SWG\Post(
     *      summary="Add Source",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\SourceType::class)
     *      ),
     *      @SWG\Response(response=200, description="Success")
     * )
     * @Route("", methods={"POST"})
     */
    public function addSourceAction(Request $request)
    {
        $form = $this->createForm(SourceType::class, new Source());

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->sourceManager->createNewSource($form->getData());

        }
    }

    /**
     * @SWG\Put(
     *      summary="Update Source",
     *      tags={"sources"},
     *      @SWG\Parameter(
     *          name="body",
     *          description="",
     *          in="body",
     *          type="json",
     *          @Model(type=\AppBundle\Form\SourceType::class)
     *      ),
     *      @SWG\Response(response=200, description="Success"),
     *      @SWG\Response(response=404, description="Not Found")
     * )
     * @Route("/{id}", methods={"PUT"})
     */
    public function updateSourceAction(Request $request, Source $source)
    {
        $form = $this->createForm(SourceType::class, $source);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->getFormErrorResponse($form);
        } else {
            return $this->sourceManager->createNewSource($form->getData());
        }
    }
}
