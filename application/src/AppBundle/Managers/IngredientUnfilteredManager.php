<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Ingredient;
use AppBundle\Entity\IngredientLegend;
use AppBundle\Entity\IngredientUnfiltered;
use AppBundle\Repository\IngredientUnfilteredRepository;
use Doctrine\ORM\EntityManagerInterface;

class IngredientUnfilteredManager
{
    private $em;
    /** @var IngredientUnfilteredRepository $repository */
    private $repository;

    /**
     * @var IngredientManager
     */
    private $ingredientManager;

    public function __construct(EntityManagerInterface $em, IngredientManager $ingredientManager)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(IngredientUnfiltered::class);
        $this->ingredientManager = $ingredientManager;
    }

    /**
     * @return array|null
     */
    public function getIngredientUnfilteredHashmap(): array
    {
        $array = $this->repository->getIngredientUnfilteredAsArray();

        $tmp = [];
        foreach ($array as $k => $v) {
            $tmp[md5(strtolower($v['name']))] = $v['id'];
        }

        return $tmp;
    }

    /**
     * @param string $ingredientUnfiltered
     * @return IngredientUnfiltered
     */
    public function getIngredientUnfilteredByName($ingredientUnfiltered)
    {
        return $this->repository->findOneByName($ingredientUnfiltered);
    }

    /**
     * @param string $id
     * @return IngredientUnfiltered
     */
    public function getIngredientUnfilteredById($id)
    {
        return $this->repository->findOneById($id);
    }

    /**
     * Assign all unfiltered ingredients that are not currently assigned to ingredients
     * @return void
     */
    public function assignToIngredients()
    {
        $unfilteredIngredients = $this->repository->getUnassignedUnfilteredIngredientsAsIterable();
        $iterableResult = $unfilteredIngredients->iterate();

        $count = 1;
        foreach ($iterableResult as $row) {
            /** @var IngredientUnfiltered $unfilteredIngredient */
            $unfilteredIngredient = $row[0];

            $ingredientArray = $this->ingredientManager->getIngredientByNameWithMatchType($unfilteredIngredient->getName());

            if (isset($ingredientArray[0]) && $ingredientArray[0]->getId() !== null)
                $unfilteredIngredient->setIngredientId($ingredientArray[0]->getId());

            if (isset($ingredientArray[1]) && $ingredientArray[1] !== null)
                $unfilteredIngredient->setMatchType($ingredientArray[1]);

            if ($count % 200 === 0) {
                $this->em->flush();
                $this->em->clear(IngredientUnfiltered::class);
            }
            $count++;
        }
        // Clear the rest
        $this->em->flush();
        $this->em->clear(IngredientUnfiltered::class);
    }

}
