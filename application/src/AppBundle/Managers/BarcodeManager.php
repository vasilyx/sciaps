<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Barcode;
use AppBundle\Repository\BarcodeRepository;
use Doctrine\ORM\EntityManagerInterface;

class BarcodeManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Barcode::class);
    }

    /**
     * @return array|null
     */
    public function getBarcodesHashmap(): ?array
    {
        $array = $this->repository->getBarcodesAsArray();

        foreach ($array as $k => $v) {
            $tmp[md5($v['barcodeOriginal'])] = $v['id'];
        }

        return $tmp ?? null;
    }

    /**
     * @param string $barcode
     * @return Barcode
     */
    public function getBarcode($barcode)
    {
        return $this->repository->findOneByBarcode($barcode);
    }

    /**
     * @param string $barcode
     * @param bool $flush
     * @return Barcode
     */
    public function getBarcodeOrCreate($barcode, $flush = true)
    {
        if ($obj = $this->getBarcode($barcode)) {
            return $obj;
        } else {
            $obj = new Barcode($barcode);
            return $this->createNew($obj, $flush);
        }
    }

    /**
     * @param Barcode $barcode
     * @param bool $flush
     * @return Barcode
     */
    public function createNew(Barcode $barcode, $flush = true)
    {
        $this->em->persist($barcode);

        if ($flush) $this->em->flush();

        return $barcode;
    }
}
