<?php

namespace AppBundle\Managers;

use AppBundle\Entity\CrowdsourcePending;
use AppBundle\Entity\LogEvent;
use AppBundle\Entity\User;
use AppBundle\Response\CrowdsourceTrackingResponse;
use Doctrine\ORM\EntityManagerInterface;

class LogEventManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(LogEvent::class);
    }

    public function getCrowdsourceTracking(User $user)
    {
        $events = $this->repository->getTrackingContractors();
        $response = [];

        if ($events) {
            /**
             * @var LogEvent $event
             */
            foreach ($events as $event) {

                $track = new CrowdsourceTrackingResponse();
                $track->setContractorId($event->getUser()->getId());
                $track->setContractorName($event->getUser()->getFirstName());
                $track->setProductsApproved($this->repository->getCountTrackingByUser($user, CrowdsourcePending::STATUS_APPROVED));
                $track->setProductsApprovedManager($this->repository->getCountTrackingByUser($user, CrowdsourcePending::STATUS_HEAD_APPROVED));
                $track->setProductsArchived($this->repository->getCountTrackingByUser($user, CrowdsourcePending::STATUS_ARCHIVED));
                $track->setProductsArchivedManager($this->repository->getCountTrackingByUser($user, CrowdsourcePending::STATUS_HEAD_ARCHIVED));

                $track->setAccuracy(0); // TODO how to get

                array_push($response, $track);
            }
        }

        return $response;
    }
 }
