<?php

namespace AppBundle\Managers;

use AppBundle\Entity\IngredientLegend;
use Doctrine\ORM\EntityManagerInterface;

class IngredientLegendManager
{
    private $em;
    private $repository;


    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(IngredientLegend::class);
    }

    /**
     * @return array|null
     */
    public function getIngredientLegendHashmap(): array
    {
        $array = $this->repository->getIngredientLegendAsArray();

        $tmp = [];
        foreach ($array as $k => $v) {
            $tmp[md5(strtolower($v['name']))] = $v['id'];
        }

        return $tmp;
    }

    /**
     * @param string $ingredientLegend
     * @return IngredientLegend
     */
    public function getIngredientLegendByName($ingredientLegend)
    {
        return $this->repository->findOneByName($ingredientLegend);
    }

    /**
     * @param $name
     * @return IngredientLegend|null
     */
    public function getIngredientLegendByNameOrCreate($name): ?IngredientLegend
    {
        if (!$name) return null;

        if ($this->repository->findOneByName($name)) {
            return $this->repository->findOneByName($name);
        } else {

            $ingredientLegend = new IngredientLegend();
            $ingredientLegend->setName($name);

            return $this->createNew($ingredientLegend);
        }
    }

    /**
     * @param IngredientLegend $ingredientLegend
     * @return IngredientLegend
     */
    public function createNew(IngredientLegend $ingredientLegend)
    {
        $this->em->persist($ingredientLegend);
        $this->em->flush();

        return $ingredientLegend;
    }
}
