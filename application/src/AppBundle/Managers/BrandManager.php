<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Brand;
use AppBundle\Repository\BrandRepository;
use Doctrine\ORM\EntityManagerInterface;

class BrandManager
{
    private $em;
    /** @var BrandRepository $repository  */
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Brand::class);
    }

    /**
     * @return array|null
     */
    public function getBrandsHashmap(): ?array
    {
        $array = $this->repository->getBrandsAsArray();

        foreach ($array as $k => $v) {
            $tmp[md5(strtolower($v['name']))] = $v['id'];
        }

        return $tmp ?? null;
    }

    /**
     * @param string $name
     * @return Brand
     */
    public function getBrandByName($name)
    {
        return $this->repository->findOneByName($name);
    }

    /**
     * @param string $id
     * @return Brand
     */
    public function getBrandById($id)
    {
        return $this->repository->findOneById($id);
    }

    /**
     * @param string $Brand
     * @return Brand
     */
    public function getBrand($Brand)
    {
        return $this->repository->findOneByName($Brand);
    }

    /**
     * @param string $Brand
     * @param bool $flush
     * @return Brand
     */
    public function getBrandOrCreate($Brand, $flush = true)
    {
        if ($obj = $this->getBrand($Brand)) {
            return $obj;
        } else {
            $obj = new Brand();
            $obj->setName($Brand);
            return $this->createNew($obj, $flush);
        }
    }

    /**
     * @param Brand $Brand
     * @param bool $flush
     * @return Brand
     */
    public function createNew(Brand $Brand, $flush = true)
    {
        $this->em->persist($Brand);

        if ($flush) $this->em->flush();

        return $Brand;
    }
}
