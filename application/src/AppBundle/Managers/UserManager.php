<?php

namespace AppBundle\Managers;

use AppBundle\Services\Email\SendgridEmailer;
use AppBundle\Services\Email\Types\ActivationEmail;
use AppBundle\Services\Email\Types\ForgottenPasswordEmail;
use AppBundle\Services\Email\Types\ResetPasswordEmail;
use AppBundle\Services\Email\Types\WelcomeEmail;
use AppBundle\Entity\Permission;
use AppBundle\Entity\User;

use AppBundle\Repository\UserRepository;
use AppBundle\Services\RedisSession;
use Doctrine\ORM\EntityManagerInterface;

use Doctrine\ORM\EntityNotFoundException;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserManager
{
    private $em;

    public const REDIS_SESSION_PREFIX = 'session_';

    /**
     * @var UserRepository $repository
     */
    private $repository;
    private $authorizationChecker;
    private $encoderFactory;
    private $redisSession;
    private $mailer;

    public function __construct(EntityManagerInterface $em,
                                AuthorizationCheckerInterface $authorizationChecker,
                                EncoderFactoryInterface $encoderFactory,
                                RedisSession $redisSession,
                                SendgridEmailer $mailer)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(User::class);
        $this->authorizationChecker = $authorizationChecker;
        $this->encoderFactory = $encoderFactory;
        $this->redisSession = $redisSession;
        $this->mailer = $mailer;
    }

    public function saveUser(User $userEntity, $andFlush = false)
    {
        $this->em->persist($userEntity);

        if (true === $andFlush) {
            $this->em->flush();
        }

        return $userEntity;
    }

    public function updatePassword(User $userEntity, $password, $andFlush = true)
    {
        $encoder = $this->getEncoder($userEntity);

        $userEntity->setForgottenPasswordToken(null)->setPassword($encoder->encodePassword($password, $userEntity->getSalt()));
        $userEntity->setDatetimeForgottenPassword(null);

        $user = $this->saveUser($userEntity, $andFlush);

        $this->mailer->send(new ResetPasswordEmail($user->getEmail()));

        return $user;
    }

    public function activateAccount(User $userEntity, $password, $andFlush = true)
    {
        $encoder = $this->getEncoder($userEntity);

        $userEntity->setConfirmEmailToken(null)
            ->setPassword($encoder->encodePassword($password, $userEntity->getSalt()));
        $userEntity->setStatus(User::STATUS_ACTIVE);


        $user =  $this->saveUser($userEntity, $andFlush);

        $params = ['token' => $user->getConfirmEmailToken()];
        $this->mailer->send(new WelcomeEmail($user->getEmail(), $params));

        return $user;
    }


    /**
     * @return User[]|array
     */
    public function getAllUser()
    {
        return $this->repository->findAll();
    }


    /**
     * @param $email
     * @param bool $active
     * @return mixed
     */
    public function getUserByEmail($email, $active = true): ?User
    {
        return $this->repository->findOneByEmail($email);
    }

    /**
     * @param $id
     * @param bool $active
     * @return mixed
     */
    public function getUserById($id, $active = true): ?User
    {
        return $this->repository->findOneById($id);
    }


    /**
     * @param $token
     * @return User|void
     */
    public function getUserByForgottenPasswordToken($token): ?User
    {
        /**
         * @var User $user
         */
        if ($user = $this->repository->findOneByForgottenPasswordToken($token)) {

            if ($user->getDatetimeForgottenPassword() > new \DateTime('-24 hours')) {
                return $user;
            }
        }

        return null;
    }

    /**
     * @param $token
     * @return User|void
     */
    public function getUserByConfirmEmailToken($token): ?User
    {
        /**
         * @var User $user
         */
        if ($user = $this->repository->findOneByConfirmEmailToken($token)) {
                return $user;
        }

        return null;
    }


    /**
     * @param User $userEntity
     * @param bool $andFlush
     * @return User
     */
    public function updateForgotPasswordToken(User $userEntity, $andFlush = true): User
    {
        $userEntity->generateForgotPasswordToken();
        $userEntity->setDatetimeForgottenPassword(new \DateTime());

        $user = $this->saveUser($userEntity, $andFlush);

        $params = ['forgottenPasswordToken' => $user->getForgottenPasswordToken()];
        $this->mailer->send(new ForgottenPasswordEmail($user->getEmail(), $params));

        return $user;
    }


    /**
     * @param $username
     * @param $password
     * @return string
     * @throws EntityNotFoundException
     */
    public function login($username, $password): string
    {
        /**
         * @var User $user
         */
        $user = $this->repository->findOneByEmail($username);

        if (!$user) {
            throw new EntityNotFoundException('Provided email address could not be found');
        }


        if (!$this->encoderFactory->getEncoder($user)->isPasswordValid($user->getPassword(), $password, $user->getSalt())) {
            throw new BadRequestHttpException('Invalid password');
        }

        $this->saveUser($user, true);

        $token = $this->generateToken();

        $this->redisSession->write(self::REDIS_SESSION_PREFIX . $token, $user->getId());

        return $token;
    }

    /**
     * @param $token
     */
    public function destroySession($token)
    {
        $this->redisSession->destroy(self::REDIS_SESSION_PREFIX . $token);
    }


    /**
     * @param User $user
     * @return \Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface
     */
    protected function getEncoder(User $user)
    {
        return $this->encoderFactory->getEncoder($user);
    }

    /**
     * @return string
     */
    private function generateToken(): string
    {
        return base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
    }

    /**
     * @param $token
     * @return User|null|object
     */
    public function loadByToken($token): ?User
    {
        $key = $this->redisSession->read(self::REDIS_SESSION_PREFIX . $token);

        if (!$key) return null;

        return $this->repository->find($key);
    }

    /**
     * @param User $user
     * @return User
     */
    public function createUser(User $user): User
    {
        $encoder = $this->encoderFactory->getEncoder($user);
        $password = $this->generatePassword();

        $user->setPassword($password);
        $user->setPassword($encoder->encodePassword($user->getPassword(), $user->getSalt()));
        $user->setStatus(User::STATUS_INACTIVE);
        $user->generateConfirmationToken();
        $this->addPermissionsToUser($user);
        $this->em->persist($user);
        $this->em->flush();

        $params = ['confirmEmailToken' => $user->getConfirmEmailToken()];
        $this->mailer->send(new ActivationEmail($user->getEmail(), $params));

        return $user;
    }

    /**
     * @param User $user
     * @return User
     */
    public function updateUser(User $user): User
    {
        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * @param int $length
     * @return string
     */
    public function generatePassword($length = 8): string
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }

    /**
     * @param User $user
     * @return User
     */
    protected function addPermissionsToUser(User $user)
    {
        // hack, adding all permissions
        // TODO add logic - priviledges depends on services

        $permissions = $this->em->getRepository(Permission::class)->findAll();

        foreach ($permissions as $permission) {
            $user->addPermission($permission);
        }

        return $user;
    }
}
