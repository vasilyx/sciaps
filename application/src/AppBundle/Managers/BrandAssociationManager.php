<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Attribute;
use AppBundle\Entity\Evidence;
use AppBundle\Entity\Brand;
use AppBundle\Entity\Source;
use AppBundle\Entity\BrandAttributes;
use AppBundle\Entity\BrandEvidences;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class BrandAssociationManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(BrandEvidences::class);
    }

    /**
     * @param BrandEvidences $brandEvidences
     * @param Request|null $request
     * @param Source $source
     * @param Evidence $evidence
     * @return bool
     */
    public function createBrandEvidence(BrandEvidences $brandEvidences, Request $request = null, Source $source, Evidence $evidence)
    {
        $array_brands = $request->request->get('brands');

        foreach ($array_brands as $key => $val) {
            $brand = $this->em->getRepository(Brand::class)->find(trim($val));
            if (empty($brand)) continue;
            $brandEvidence = $this->repository->findBy(array(
                'source' => $source,
                'evidence' => $evidence,
                'brand' => $brand
            ));
            if (empty($brandEvidence)) {
                $brandEvidence = new BrandEvidences();
                $brandEvidence->setSource($source);
                $brandEvidence->setEvidence($evidence);
                $brandEvidence->setBrand($brand);
                $brandEvidence->setStatusCode($brandEvidences->getStatusCode());
                $brandEvidence->setDescription($brandEvidences->getDescription());
                $this->em->persist($brandEvidence);
            }
        }

        $this->em->flush();

        return true;
    }

    /**
     * @param Source $source
     * @param Evidence $evidence
     * @param Brand $brand
     * @return BrandEvidences
     */
    public function findBrandsEvidence(Source $source, Evidence $evidence, Brand $brand): BrandEvidences
    {
        $brandEvidence = $this->repository->findOneBy(array(
            'source' => $source,
            'evidence' => $evidence,
            'brand' => $brand
        ));

        return $brandEvidence;
    }

    /**
     * @param BrandEvidences $brandEvidences
     * @return BrandEvidences
     */
    public function updateBrandEvidence(BrandEvidences $brandEvidences): BrandEvidences
    {
        $this->em->persist($brandEvidences);
        $this->em->flush();

        return $brandEvidences;
    }

    /**
     * @param Source $source
     * @param Evidence $evidence
     * @param Brand $brand
     * @return BrandEvidences|null
     */
    public function deleteBrandEvidence(Source $source, Evidence $evidence, Brand $brand)
    {
        $brandEvidence = $this->repository->findOneBy(array(
            'source' => $source,
            'evidence' => $evidence,
            'brand' => $brand
        ));
        if (!empty($brandEvidence)) {
            $this->em->remove($brandEvidence);
            $this->em->flush();
        }

        return $brandEvidence;
    }

    /**
     * @param BrandAttributes $brandAttributes
     * @param Request|null $request
     * @param Source $source
     * @param Attribute $attribute
     * @return bool
     */
    public function createBrandAttribute(BrandAttributes $brandAttributes, Request $request = null, Source $source, Attribute $attribute)
    {
        $array_brands = $request->request->get('brands');

        foreach ($array_brands as $key => $val) {
            $brand = $this->em->getRepository(Brand::class)->find(trim($val));
            if (empty($brand)) continue;
            $brandAttribute = $this->em->getRepository(BrandAttributes::class)->findBy(array(
                'source' => $source,
                'attribute' => $attribute,
                'brand' => $brand
            ));
            if (empty($brandAttribute)) {
                $brandAttribute = new BrandAttributes();
                $brandAttribute->setSource($source);
                $brandAttribute->setAttribute($attribute);
                $brandAttribute->setBrand($brand);
                $brandAttribute->setStatusCode($brandAttributes->getStatusCode());
                $brandAttribute->setDescription($brandAttributes->getDescription());
                $this->em->persist($brandAttribute);
            }
        }

        $this->em->flush();

        return true;
    }

    /**
     * @param Source $source
     * @param Attribute $attribute
     * @param Brand $brand
     * @return BrandAttributes
     */
    public function findBrandsAttribute(Source $source, Attribute $attribute, Brand $brand): BrandAttributes
    {
        $brandAttribute = $this->em->getRepository(BrandAttributes::class)->findOneBy(array(
            'source' => $source,
            'attribute' => $attribute,
            'brand' => $brand
        ));

        return $brandAttribute;
    }

    /**
     * @param BrandAttributes $brandAttributes
     * @return BrandAttributes
     */
    public function updateBrandAttribute(BrandAttributes $brandAttributes): BrandAttributes
    {
        $this->em->persist($brandAttributes);
        $this->em->flush();

        return $brandAttributes;
    }

    /**
     * @param Source $source
     * @param Attribute $attribute
     * @param Brand $brand
     * @return BrandAttributes|null
     */
    public function deleteBrandAttribute(Source $source, Attribute $attribute, Brand $brand)
    {
        $brandAttribute = $this->em->getRepository(BrandAttributes::class)->findOneBy(array(
            'source' => $source,
            'attribute' => $attribute,
            'brand' => $brand
        ));
        if (!empty($brandAttribute)) {
            $this->em->remove($brandAttribute);
            $this->em->flush();
        }

        return $brandAttribute;
    }

    /**
     * @param Source $source
     * @return array
     */
    public function getEvidences(Source $source): array
    {
        $brandsEvidence = $this->repository->findBy(array(
            'source' => $source
        ));

        return $brandsEvidence;
    }

    /**
     * @param Source $source
     * @return array
     */
    public function getAttributes(Source $source): array
    {
        $brandsAttribute = $this->em->getRepository(BrandAttributes::class)->findBy(array(
            'source' => $source
        ));

        return $brandsAttribute;
    }

    /**
     * @param Source $source
     * @param Evidence $evidence
     * @param Brand $brand
     * @return BrandEvidences|null
     */
    public function getBrandsEvidences(Source $source, Evidence $evidence, Brand $brand)
    {
        $brandEvidence = $this->repository->findOneBy(array(
            'source' => $source,
            'evidence' => $evidence,
            'brand' => $brand
        ));
        return $brandEvidence;
    }

    /**
     * @param Source $source
     * @param Attribute $attribute
     * @param Brand $brand
     * @return BrandAttributes|null
     */
    public function getBrandsAttributes(Source $source, Attribute $attribute, Brand $brand)
    {
        $brandAttribute = $this->em->getRepository(BrandAttributes::class)->findOneBy(array(
            'source' => $source,
            'attribute' => $attribute,
            'brand' => $brand
        ));
        return $brandAttribute;
    }
}
