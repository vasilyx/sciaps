<?php

namespace AppBundle\Managers;

use AppBundle\Entity\SourceDataProvider;
use Doctrine\ORM\EntityManagerInterface;

class SourceDataProviderManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(SourceDataProvider::class);
    }

    /**
     * @param SourceDataProvider $sourceDataProvider
     * @return SourceDataProvider
     */
    public function createNewSourceDataProvider(SourceDataProvider $sourceDataProvider)
    {
        $this->em->persist($sourceDataProvider);
        $this->em->flush();

        return $sourceDataProvider;
    }

    /**
     * @param SourceDataProvider $sourceDataProvider
     */
    public function removeSourceDataProvider(SourceDataProvider $sourceDataProvider)
    {
        $sourceDataProvider->setIsDeleted(true);
        $this->em->flush();
    }
}
