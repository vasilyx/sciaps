<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Attribute;
use AppBundle\Entity\BrandAttributes;
use AppBundle\Entity\IngredientsAttributes;
use AppBundle\Entity\ManufacturerAttributes;
use AppBundle\Entity\ProductAttributes;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;


class AttributeManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Attribute::class);
    }

    /**
     * @param Attribute $attribute
     * @return Attribute
     */
    public function createNewAttribute(Attribute $attribute): Attribute
    {
        if (!is_null($attribute->getParent())) {
            $parent_attribute = $this->repository->find($attribute->getParent()->getId());
            if (empty($parent_attribute->getParent())) {
                $this->em->persist($attribute);
                $this->em->flush();
            }
        } else {
            $this->em->persist($attribute);
            $this->em->flush();
        }

        return $attribute;
    }

    /**
     * @param Attribute $attribute
     * @return bool
     */
    public function removeAttribute(Attribute $attribute)
    {
        if (!$this->checkAssociations($attribute)) {
            $this->em->remove($attribute);
            $this->em->flush();

            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @param Attribute $attribute
     * @return bool
     */
    public function checkAssociations(Attribute $attribute)
    {
        if (
            $this->em->getRepository(BrandAttributes::class)->findOneBy(['attribute' => $attribute]) ||
            $this->em->getRepository(IngredientsAttributes::class)->findOneBy(['attribute' => $attribute]) ||
            $this->em->getRepository(ProductAttributes::class)->findOneBy(['attribute' => $attribute]) ||
            $this->em->getRepository(ManufacturerAttributes::class)->findOneBy(['attribute' => $attribute])
        ) {
            return true;
        }

        return false;
    }
}
