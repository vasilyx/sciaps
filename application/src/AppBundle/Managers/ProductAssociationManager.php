<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Attribute;
use AppBundle\Entity\Evidence;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductAttributes;
use AppBundle\Entity\ProductEvidences;
use AppBundle\Entity\Source;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class ProductAssociationManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(ProductEvidences::class);
    }

    /**
     * @param ProductEvidences $productEvidences
     * @param Request|null $request
     * @param Source $source
     * @param Evidence $evidence
     * @return bool
     */
    public function createProductEvidence(ProductEvidences $productEvidences, Request $request = null, Source $source, Evidence $evidence)
    {
        $array_products = $request->request->get('products');

        foreach ($array_products as $key => $val) {
            $product = $this->em->getRepository(Product::class)->find(trim($val));
            if (empty($product)) continue;
            $productEvidence = $this->repository->findBy(array(
                'source' => $source,
                'evidence' => $evidence,
                'product' => $product
            ));
            if (empty($productEvidence)) {
                $productEvidence = new ProductEvidences();
                $productEvidence->setSource($source);
                $productEvidence->setEvidence($evidence);
                $productEvidence->setProduct($product);
                $productEvidence->setStatusCode($productEvidences->getStatusCode());
                $productEvidence->setDescription($productEvidences->getDescription());
                $this->em->persist($productEvidence);
            }
        }

        $this->em->flush();

        return true;
    }

    /**
     * @param Source $source
     * @param Evidence $evidence
     * @param Product $product
     * @return ProductEvidences
     */
    public function findProductsEvidence(Source $source, Evidence $evidence, Product $product): ProductEvidences
    {
        $productEvidence = $this->repository->findOneBy(array(
            'source' => $source,
            'evidence' => $evidence,
            'product' => $product
        ));

        return $productEvidence;
    }

    /**
     * @param ProductEvidences $productEvidences
     * @return ProductEvidences
     */
    public function updateProductEvidence(ProductEvidences $productEvidences): ProductEvidences
    {
        $this->em->persist($productEvidences);
        $this->em->flush();

        return $productEvidences;
    }

    /**
     * @param Source $source
     * @param Evidence $evidence
     * @param Product $product
     * @return ProductEvidences|null
     */
    public function deleteProductEvidence(Source $source, Evidence $evidence, Product $product)
    {
        $productEvidence = $this->repository->findOneBy(array(
            'source' => $source,
            'evidence' => $evidence,
            'product' => $product
        ));
        if (!empty($productEvidence)) {
            $this->em->remove($productEvidence);
            $this->em->flush();
        }

        return $productEvidence;
    }

    /**
     * @param ProductAttributes $productAttributes
     * @param Request|null $request
     * @param Source $source
     * @param Attribute $attribute
     * @return bool
     */
    public function createProductAttribute(ProductAttributes $productAttributes, Request $request = null, Source $source, Attribute $attribute)
    {
        $array_products = $request->request->get('products');

        foreach ($array_products as $key => $val) {
            $product = $this->em->getRepository(Product::class)->find(trim($val));
            if (empty($product)) continue;
            $productAttribute = $this->em->getRepository(ProductAttributes::class)->findBy(array(
                'source' => $source,
                'attribute' => $attribute,
                'product' => $product
            ));
            if (empty($productAttribute)) {
                $productAttribute = new ProductAttributes();
                $productAttribute->setSource($source);
                $productAttribute->setAttribute($attribute);
                $productAttribute->setProduct($product);
                $productAttribute->setStatusCode($productAttributes->getStatusCode());
                $productAttribute->setDescription($productAttributes->getDescription());
                $this->em->persist($productAttribute);
            }
        }

        $this->em->flush();

        return true;
    }

    /**
     * @param Source $source
     * @param Attribute $attribute
     * @param Product $product
     * @return ProductAttributes
     */
    public function findProductsAttribute(Source $source, Attribute $attribute, Product $product): ProductAttributes
    {
        $productAttribute = $this->em->getRepository(ProductAttributes::class)->findOneBy(array(
            'source' => $source,
            'attribute' => $attribute,
            'product' => $product
        ));

        return $productAttribute;
    }

    /**
     * @param ProductAttributes $productAttributes
     * @return ProductAttributes
     */
    public function updateProductAttribute(ProductAttributes $productAttributes): ProductAttributes
    {
        $this->em->persist($productAttributes);
        $this->em->flush();

        return $productAttributes;
    }

    /**
     * @param Source $source
     * @param Attribute $attribute
     * @param Product $product
     * @return mixed
     */
    public function deleteProductAttribute(Source $source, Attribute $attribute, Product $product)
    {
        $productAttribute = $this->em->getRepository(ProductAttributes::class)->findOneBy(array(
            'source' => $source,
            'attribute' => $attribute,
            'product' => $product
        ));
        if (!empty($productAttribute)) {
            $this->em->remove($productAttribute);
            $this->em->flush();
        }

        return $productAttribute;
    }

    /**
     * @param Source $source
     * @return array
     */
    public function getEvidences(Source $source): array
    {
        $productsEvidence = $this->repository->findBy(array(
            'source' => $source
        ));

        return $productsEvidence;
    }

    /**
     * @param Source $source
     * @return array
     */
    public function getAttributes(Source $source): array
    {
        $productsAttribute = $this->em->getRepository(ProductAttributes::class)->findBy(array(
            'source' => $source
        ));

        return $productsAttribute;
    }

    /**
     * @param Source $source
     * @param Evidence $evidence
     * @param Product $product
     * @return ProductEvidences|null
     */
    public function getProductsEvidences(Source $source, Evidence $evidence, Product $product)
    {
        $productEvidence = $this->repository->findOneBy(array(
            'source' => $source,
            'evidence' => $evidence,
            'product' => $product
        ));
        return $productEvidence;
    }

    /**
     * @param Source $source
     * @param Attribute $attribute
     * @param Product $product
     * @return ProductAttributes|null
     */
    public function getProductsAttributes(Source $source, Attribute $attribute, Product $product)
    {
        $productAttribute = $this->em->getRepository(ProductAttributes::class)->findOneBy(array(
            'source' => $source,
            'attribute' => $attribute,
            'product' => $product
        ));
        return $productAttribute;
    }
}
