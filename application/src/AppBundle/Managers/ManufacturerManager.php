<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Brand;
use AppBundle\Entity\Manufacturer;
use AppBundle\Repository\BrandRepository;
use Doctrine\ORM\EntityManagerInterface;

class ManufacturerManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Manufacturer::class);
    }

    /**
     * @param string $Manufacturer
     * @return Manufacturer
     */
    public function getManufacturer($Manufacturer)
    {
        return $this->repository->findOneByName($Manufacturer);
    }

    /**
     * @param string $Manufacturer
     * @param bool $flush
     * @return Manufacturer
     */
    public function getManufacturerOrCreate($Manufacturer, $flush = true)
    {
        if ($obj = $this->getManufacturer($Manufacturer)) {
            return $obj;
        } else {
            $obj = new Manufacturer();
            $obj->setName($Manufacturer);
            return $this->createNew($obj, $flush);
        }
    }

    /**
     * @param Manufacturer $Manufacturer
     * @param bool $flush
     * @return Manufacturer
     */
    public function createNew(Manufacturer $Manufacturer, $flush = true)
    {
        $this->em->persist($Manufacturer);

        if ($flush) $this->em->flush();

        return $Manufacturer;
    }
}
