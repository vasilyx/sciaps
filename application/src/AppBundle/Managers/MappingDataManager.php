<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Company;
use AppBundle\Entity\MappingData;
use AppBundle\Entity\Organization;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductProvider;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class MappingDataManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(MappingData::class);
    }

    /**
     * @param $mapping
     * @param Company $company
     * @return array|null
     */
    public function updateDataMappingByCompany($mapping, Company $company): ?array
    {
        /**
         * @var ProductProvider $productProvider
         */
        $productProvider = $this->em->getRepository(ProductProvider::class)->findOneByCompany($company);

        $mappingData = !$productProvider->getMappingData() ? new MappingData(): $productProvider->getMappingData();

        $encoded = json_encode($mapping);
        if ($encoded === null && json_last_error() !== JSON_ERROR_NONE) {
            return null;
        }

        $mappingData->setData($encoded);
        $mappingData->setProductProvider($productProvider);

        $this->em->persist($mappingData);
        $this->em->flush();
        $productProvider->setMappingData($mappingData);
        $this->em->flush();

        $decoded = json_decode($mappingData->getData(), true);
        if ($decoded === null && json_last_error() !== JSON_ERROR_NONE) {
            return null;
        }

        return $decoded;
    }

    /**
     * @param Company $company
     * @return array|null
     */
    public function getDataMappingByCompany(Company $company): ?array
    {
        /**
         * @var ProductProvider $productProvider
         */
        $productProvider = $this->em->getRepository(ProductProvider::class)->findOneByCompany($company);

        if ($productProvider->getMappingData()) {
            $decoded = json_decode($productProvider->getMappingData()->getData(), true);
            if ($decoded === null && json_last_error() !== JSON_ERROR_NONE) {
                return null;
            }
            return $decoded;
        } else {
            return null;
        }
    }
}
