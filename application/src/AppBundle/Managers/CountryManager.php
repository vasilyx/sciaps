<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Company;
use AppBundle\Entity\Country;
use Doctrine\ORM\EntityManagerInterface;

class CountryManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Country::class);
    }

    /**
     * @return Country []
     */
    public function getAllCountries()
    {
        $result = $this->repository->findAll();
        return $result;
    }
}
