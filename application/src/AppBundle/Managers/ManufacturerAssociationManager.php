<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Attribute;
use AppBundle\Entity\Evidence;
use AppBundle\Entity\Manufacturer;
use AppBundle\Entity\ManufacturerAttributes;
use AppBundle\Entity\ManufacturerEvidences;
use AppBundle\Entity\Source;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class ManufacturerAssociationManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(ManufacturerEvidences::class);
    }

    /**
     * @param ManufacturerEvidences $manufacturerEvidences
     * @param Request|null $request
     * @param Source $source
     * @param Evidence $evidence
     * @return bool
     */
    public function createManufacturerEvidence(ManufacturerEvidences $manufacturerEvidences, Request $request = null, Source $source, Evidence $evidence)
    {
        $array_manufacturers = $request->request->get('manufacturers');

        foreach ($array_manufacturers as $key => $val) {

            $manufacturer = $this->em->getRepository(Manufacturer::class)->find(trim($val));
            if (empty($manufacturer)) continue;

            $manufacturerEvidence = $this->repository->findBy([
                'source' => $source,
                'evidence' => $evidence,
                'manufacturer' => $manufacturer
            ]);

            if (empty($manufacturerEvidence)) {
                $manufacturerEvidence = new ManufacturerEvidences();
                $manufacturerEvidence->setSource($source);
                $manufacturerEvidence->setEvidence($evidence);
                $manufacturerEvidence->setManufacturer($manufacturer);
                $manufacturerEvidence->setStatusCode($manufacturerEvidences->getStatusCode());
                $manufacturerEvidence->setDescription($manufacturerEvidences->getDescription());
                $this->em->persist($manufacturerEvidence);
                $this->em->flush();
            }
        }

        return true;
    }

    /**
     * @param Source $source
     * @param Evidence $evidence
     * @param Manufacturer $manufacturer
     * @return ManufacturerEvidences
     */
    public function findManufacturersEvidence(Source $source, Evidence $evidence, Manufacturer $manufacturer): ManufacturerEvidences
    {
        $manufacturerEvidence = $this->repository->findOneBy([
            'source' => $source,
            'evidence' => $evidence,
            'manufacturer' => $manufacturer
        ]);

        return $manufacturerEvidence;
    }

    /**
     * @param ManufacturerEvidences $manufacturerEvidences
     * @return ManufacturerEvidences
     */
    public function updateManufacturerEvidence(ManufacturerEvidences $manufacturerEvidences): ManufacturerEvidences
    {
        $this->em->persist($manufacturerEvidences);
        $this->em->flush();

        return $manufacturerEvidences;
    }

    /**
     * @param Source $source
     * @param Evidence $evidence
     * @param Manufacturer $manufacturer
     * @return ManufacturerEvidences|null
     */
    public function deleteManufacturerEvidence(Source $source, Evidence $evidence, Manufacturer $manufacturer)
    {
        $manufacturerEvidence = $this->repository->findOneBy([
            'source' => $source,
            'evidence' => $evidence,
            'manufacturer' => $manufacturer
        ]);

        if (!empty($manufacturerEvidence)) {
            $this->em->remove($manufacturerEvidence);
            $this->em->flush();
        }

        return $manufacturerEvidence;
    }

    /**
     * @param ManufacturerAttributes $manufacturerAttributes
     * @param Request|null $request
     * @param Source $source
     * @param Attribute $attribute
     * @return null
     */
    public function createManufacturerAttribute(ManufacturerAttributes $manufacturerAttributes, Request $request = null, Source $source, Attribute $attribute)
    {
        $array_manufacturers = $request->request->get('manufacturers');

        foreach ($array_manufacturers as $key => $val) {
            $manufacturer = $this->em->getRepository(Manufacturer::class)->find(trim($val));
            if (empty($manufacturer)) continue;
            $manufacturerAttribute = $this->em->getRepository(ManufacturerAttributes::class)->findBy([
                'source' => $source,
                'attribute' => $attribute,
                'manufacturer' => $manufacturer
            ]);

            if (empty($manufacturerAttribute)) {
                $manufacturerAttribute = new ManufacturerAttributes();
                $manufacturerAttribute->setSource($source);
                $manufacturerAttribute->setAttribute($attribute);
                $manufacturerAttribute->setManufacturer($manufacturer);
                $manufacturerAttribute->setStatusCode($manufacturerAttributes->getStatusCode());
                $manufacturerAttribute->setDescription($manufacturerAttributes->getDescription());
                $this->em->persist($manufacturerAttribute);
            }
        }

        $this->em->flush();

        return true;
    }

    /**
     * @param Source $source
     * @param Attribute $attribute
     * @param Manufacturer $manufacturer
     * @return ManufacturerAttributes
     */
    public function findManufacturersAttribute(Source $source, Attribute $attribute, Manufacturer $manufacturer): ManufacturerAttributes
    {
        $manufacturerAttribute = $this->em->getRepository(ManufacturerAttributes::class)->findOneBy([
            'source' => $source,
            'attribute' => $attribute,
            'manufacturer' => $manufacturer
        ]);

        return $manufacturerAttribute;
    }

    /**
     * @param ManufacturerAttributes $manufacturerAttributes
     * @return ManufacturerAttributes
     */
    public function updateManufacturerAttribute(ManufacturerAttributes $manufacturerAttributes): ManufacturerAttributes
    {
        $this->em->persist($manufacturerAttributes);
        $this->em->flush();

        return $manufacturerAttributes;
    }

    /**
     * @param Source $source
     * @param Attribute $attribute
     * @param Manufacturer $manufacturer
     * @return ManufacturerAttributes|null
     */
    public function deleteManufacturerAttribute(Source $source, Attribute $attribute, Manufacturer $manufacturer)
    {
        $manufacturerAttribute = $this->em->getRepository(ManufacturerAttributes::class)->findOneBy([
            'source' => $source,
            'attribute' => $attribute,
            'manufacturer' => $manufacturer
        ]);

        if (!empty($manufacturerAttribute)) {
            $this->em->remove($manufacturerAttribute);
            $this->em->flush();
        }

        return $manufacturerAttribute;
    }

    /**
     * @param Source $source
     * @return array
     */
    public function getEvidences(Source $source): array
    {
        $manufacturersEvidence = $this->repository->findBy([
            'source' => $source
        ]);

        return $manufacturersEvidence;
    }

    /**
     * @param Source $source
     * @return array
     */
    public function getAttributes(Source $source): array
    {
        $manufacturersAttribute = $this->em->getRepository(ManufacturerAttributes::class)->findBy([
            'source' => $source
        ]);

        return $manufacturersAttribute;
    }

    /**
     * @param Source $source
     * @param Evidence $evidence
     * @param Manufacturer $manufacturer
     * @return ManufacturerEvidences|null
     */
    public function getManufacturersEvidences(Source $source, Evidence $evidence, Manufacturer $manufacturer)
    {
        $manufacturerEvidence = $this->repository->findOneBy([
            'source' => $source,
            'evidence' => $evidence,
            'manufacturer' => $manufacturer
        ]);

        return $manufacturerEvidence;
    }

    /**
     * @param Source $source
     * @param Attribute $attribute
     * @param Manufacturer $manufacturer
     * @return ManufacturerAttributes|null
     */
    public function getManufacturersAttributes(Source $source, Attribute $attribute, Manufacturer $manufacturer)
    {
        $manufacturerAttribute = $this->em->getRepository(ManufacturerAttributes::class)->findOneBy([
            'source' => $source,
            'attribute' => $attribute,
            'manufacturer' => $manufacturer
        ]);

        return $manufacturerAttribute;
    }
}
