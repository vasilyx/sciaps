<?php

namespace AppBundle\Managers;

use AppBundle\Entity\IngestionReport;
use AppBundle\Entity\LogEvent;
use AppBundle\Entity\Manufacturer;
use AppBundle\Entity\Product;

use AppBundle\Entity\ProductProvider;
use AppBundle\Entity\User;
use AppBundle\Repository\LogEventRepository;
use AppBundle\Response\ProductDashboardResponse;
use AppBundle\Response\ProductUploadConfirmResponse;
use AppBundle\Response\ProductUploadConfirmMappingDataResponse;
use AppBundle\Response\ProductUploadResponse;
use AppBundle\Services\FileUploads\S3FileManager;
use AppBundle\Services\ProductImport\CsvDataReader;
use AppBundle\Services\ProductImport\ProductImporter;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ProductManager
{
    private $em;
    private $repository;
    private $mappingDataManager;
    private $productImporter;
    private $ingestionReportManager;
    /**
     * @var Container
     */
    private $container;

    private $fileManager;
    private $brandManager;
    private $barcodeManager;
    private $retailerManager;
    private $manufacturerManager;

    public function __construct(EntityManagerInterface $em,
                                MappingDataManager $mappingDataManager,
                                ProductImporter $productImporter,
                                IngestionReportManager $ingestionReportManager,
                                Container $container,
                                S3FileManager $fileManager,
                                BrandManager $brandManager,
                                BarcodeManager $barcodeManager,
                                RetailerManager $retailerManager,
                                ManufacturerManager $manufacturerManager)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Product::class);
        $this->mappingDataManager = $mappingDataManager;
        $this->productImporter = $productImporter;
        $this->fileManager = $fileManager;
        $this->container = $container;
        $this->ingestionReportManager = $ingestionReportManager;
        $this->brandManager = $brandManager;
        $this->barcodeManager = $barcodeManager;
        $this->retailerManager = $retailerManager;
        $this->manufacturerManager = $manufacturerManager;
    }

    /**
     * @param Product $product
     */
    public function removeProduct(Product $product)
    {
        $this->em->remove($product);
        $this->em->flush();
    }

    /**
     * @return Product []
     */
    public function getAllProducts()
    {
        $result = $this->repository->findAll();

        return $result;
    }

    /**
     * @param Product $product
     * @param User $user
     * @return Product
     */
    public function createNewProduct(Product $product, User $user)
    {
        $product->setProductProvider($user->getCompany()->getProductProvider());
        $this->em->persist($product);
        $this->em->flush();

        return $product;
    }

    /**
     * @param $csvFile
     * @param User $user
     * @return ProductUploadResponse
     * @throws \Exception
     */
    public function uploadCSV($csvFile, User $user)
    {
        $response = new ProductUploadResponse();

        if (empty($csvFile)) {
            $response->setErrors(['File cannot be empty'])->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            return $response;
        }

        if (!($csv = base64_decode($csvFile)) || base64_encode($csv) != $csvFile) {
            $response->setErrors(['Please send data using base64 encoding.'])->setStatusCode(Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
            return $response;
        }

        if (!mb_check_encoding($csv, 'UTF-8')) {
            $response->setErrors(['Data should be UTF-8 encoded.'])->setStatusCode(Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
            return $response;
        }

        $fileArray = preg_split("/\\r\\n|\\r|\\n/", $csv);

        $filename = $user->getId() . '.csv';
        $this->fileManager->save($filename, $csv, $this->container->getParameter('csv_product_upload_dir'));

        $mappingDataOriginal = $this->productImporter->initialiseMappingData();
        $mappingData = $this->productImporter->initialiseMappingData(
            $this->mappingDataManager->getDataMappingByCompany($user->getCompany()));

        if (!$mappingData) {
            $response->setErrors(['Could not process mapping data'])->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            return $response;
        }

        $this->fileManager->get($user->getId() . '.csv', $this->container->getParameter('csv_product_upload_dir'));

        try {
            $file = new \SplFileObject('/tmp/' . $user->getId() . '.csv');
        } catch (\Exception $e) {
            $response->setErrors(['File cannot be found'])->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            return $response;
        }

        $reader = new CsvDataReader($file);

        $response->setFields(array_keys($mappingDataOriginal));
        $response->setColumnTitles($reader->getColumnHeaders());
        $response->setMappingData($mappingData);

        return $response;
    }

    /**
     * @param $mappingData
     * @param User $user
     * @return array|null
     */
    public function getSampleData($mappingData, User $user)
    {
        // todo @vaso this is never sent in the response.. what's it supposed to be doing?
        $response = new ProductUploadResponse();
        if (!$mappingData || empty($mappingData)) {
            $response->setErrors(['Please upload mapping data'])->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            return $response;
        }

        $this->fileManager->get($user->getId() . '.csv', $this->container->getParameter('csv_product_upload_dir'));

        try {
            $file = new \SplFileObject('/tmp/'. $user->getId() . '.csv');
        } catch (\Exception $e) {
            $response->setErrors(['File cannot be opened.'])->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            return $response;
        }

        $reader = new CsvDataReader($file);

        $sampleData = $this->productImporter->setReader($reader)->setDataMapping($mappingData)->getSampleData(3);

        if (isset($sampleData['errors'])) {
            $response->setErrors($sampleData['errors'])->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            return $response;
        }

        return $sampleData;
    }

    /**
     * @param $mappingData
     * @param User $user
     * @return ProductUploadConfirmMappingDataResponse
     */
    public function getConfirmMappingData($mappingData, User $user)
    {
        $response = new ProductUploadConfirmMappingDataResponse();

        if (!$mappingData || empty($mappingData)) {
            $response->setErrors(['Please upload mapping data'])->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            return $response;
        }

        // Data mapping should be validated before being updated
        $this->mappingDataManager->updateDataMappingByCompany($mappingData, $user->getCompany());

        $this->fileManager->get($user->getId() . '.csv', $this->container->getParameter('csv_product_upload_dir'));

        try {
            $file = new \SplFileObject('/tmp/' . $user->getId() . '.csv');
        } catch (\Exception $e) {
            $response->setErrors(['File cannot be opened.'])->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            return $response;
        }

        $reader = new CsvDataReader($file);

        $this->productImporter
            ->setReader($reader)
            ->setDataMapping($this->mappingDataManager->getDataMappingByCompany($user->getCompany()));

        // This actually imports the data and then returns a result for number of products added or updated.
        $ingestionReport = $this->ingestionReportManager->createOnDataImport($user, $user->getCompany()->getProductProvider());
        $importResult = $this->productImporter->importToDb($user->getCompany()->getProductProvider(), $ingestionReport, true,true);

        if (isset($importResult['errors'])) {
            $response->setErrors($importResult['errors'])->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            return $response;
        }

        $response->setProductsFound($importResult['products']['total']);
        $response->setNewProducts($importResult['products']['inserted']);
        $response->setProductsToBeUpdated($importResult['products']['updated']);

        return $response;
    }

    /**
     * @param bool $overwriteProducts
     * @param User $user
     * @return ProductUploadConfirmResponse
     */
    public function getUploadConfirm(User $user, $overwriteProducts = false)
    {
        $response = new ProductUploadConfirmResponse();

        $file = $this->fileManager->get($user->getId() . '.csv', $this->container->getParameter('csv_product_upload_dir'));

        try {
            $file = new \SplFileObject('/tmp/' . $user->getId() . '.csv');
        } catch (\Exception $e) {
            $response->setErrors(['File cannot be opened.'])->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            return $response;
        }
        $reader = new CsvDataReader($file);

        $this->productImporter
            ->setReader($reader)
            ->setDataMapping($this->mappingDataManager->getDataMappingByCompany($user->getCompany()));

        // This actually imports the data and then returns a result for number of products added or updated.
        $ingestionReport = $this->ingestionReportManager->createOnDataImport($user, $user->getCompany()->getProductProvider());
        // @todo Update tochange import script to be external run command instead of in browser.
        $importResult = $this->productImporter->importToDb($user->getCompany()->getProductProvider(), $ingestionReport, $overwriteProducts);

        if (isset($importResult['errors'])) {
            $response->setErrors($importResult['errors'])->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
            return $response;
        }

        $response->setProductsAdded($importResult['products']['inserted']);
        $response->setProductsUpdated($importResult['products']['updated']);

        $this->ingestionReportManager->updateOnDataImport($ingestionReport, $importResult);

        return $response;
    }


    /**
     * @param User $user
     * @return ProductDashboardResponse
     */
    public function getDashboard(User $user)
    {
        $productProvider = $this->em->getRepository(ProductProvider::class)->findOneByCompany($user->getCompany());

        $response = new ProductDashboardResponse();
        $response->setUsers(count($user->getCompany()->getUsers()));

        if ($lastIngestion = $this->em->getRepository(IngestionReport::class)->getLastReportByProvider(
            $user->getCompany()->getProductProvider())
        ) {
             $response->setLastImport($lastIngestion->getRunDate());
        }

        if ($productProvider) {
            $response->setActiveProducts($this->repository->getCountByProductProvider($productProvider, Product::STATUS_ACTIVE));
            $response->setTotalProducts($this->repository->getCountByProductProvider($productProvider));
        }

        return $response;
    }

    /**
     * @param $str
     * @return false|int|string
     */
    public function detectDelimiter($str)
    {
        $delimiters = array(
            ';' => 0,
            ',' => 0,
            "\t" => 0,
            "|" => 0
        );

        foreach ($delimiters as $delimiter => &$count) {
            $count = count(str_getcsv($str, $delimiter));
        }

        return array_search(max($delimiters), $delimiters);
    }

    public function rebuildQueryForFormAddProduct(Request $request)
    {

        if (is_array($request->get('brand'))) {
            $request->request->set('brand', $this->brandManager->getBrandOrCreate($request->get('brand')['name'])->getId());
        }

        if (is_array($request->get('manufacturer'))) {
            $request->request->set('manufacturer', $this->manufacturerManager->getManufacturerOrCreate($request->get('manufacturer')['name'])->getId());
        }

        if (is_array($request->get('retailer'))) {
            $request->request->set('retailer', $this->retailerManager->getRetailerOrCreate($request->get('retailer')['name'])->getId());
        }

        if (is_array($request->get('barcode'))) {
            $request->request->set('barcode', $this->barcodeManager->getBarcodeOrCreate($request->get('barcode')['barcode'])->getId());
        }

        return $request;
    }
}
