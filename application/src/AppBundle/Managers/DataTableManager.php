<?php

namespace AppBundle\Managers;

use AppBundle\Repository\Elastic\CommonRepository;
use AppBundle\Repository\InterfaceListPaging;
use Doctrine\ORM\EntityManagerInterface;
use FOS\ElasticaBundle\Manager\RepositoryManagerInterface;
use PHPUnit\Runner\Exception;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;

class DataTableManager
{
    private $em;
    private $elasticaManager;

    public function __construct(EntityManagerInterface $em, RepositoryManagerInterface $elasticaManager)
    {
        $this->em = $em;
        $this->elasticaManager = $elasticaManager;
    }

    /**
     * Params for lists <br> [ _page=1, _limit=25, _start=10, _end=20, q="search_word" ]
     * @param $entityName
     * @param Request $request
     * @param array $where
     * @param bool $elasticSearch
     * @return array
     */
    public function getListData($entityName, Request $request, $where = [], $elasticSearch = false): array
    {
        $elasticResults = [];

        if ($elasticSearch) {

            if ($request->get('q')) {
                try {
                    /** @var CommonRepository $elasticRepository */
                    $elasticRepository = $this->elasticaManager->getRepository($entityName);
                    if ($elasticRepository)
                        $elasticResults = $elasticRepository->getByName($request->get('q'), true);

                } catch (Exception $exception) {

                }
            }

        }


        /**
         * @var InterfaceListPaging $repository
         */
        $repository = $this->em->getRepository($entityName);
      //  if (!$repository instanceof InterfaceListPaging);

        if ($elasticResults) {
            $qb = $repository->getQueryBuilderForList('');
            $where['id'] = $elasticResults;
        } else {
            $qb = $repository->getQueryBuilderForList($request->get('q'));
        }

        $qb = $this->getSelect($request, $qb, $entityName);
        $qb = $this->getQueryBuilderNotEqual($request, $qb);
        $qb = $this->decorateQueryBuilderWhere($where, $qb);
        $qb = $this->decorateQueryBuilderOrder($request, $qb, $repository->getOrderMap());

        $totalCount = count($qb->getQuery()->getResult());

        $limits = $this->getLimits($request);

        $items = $qb
                ->setFirstResult($limits[0])
                ->setMaxResults(
                    $limits[1]
                )
                ->getQuery()
                ->getResult();

        return ['rows' => $items, 'totalCount' => $totalCount];
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getLimits(Request $request): array
    {
        if ($request->get('_page') !== null) {

            $limit = $request->get('_limit', 25);

            $page = $request->get('_page');

            $from = $page == 0 ? 0 : ($page - 1) * $limit;
            $to = $limit;

        } else {

            $from = $request->get('_start', 0);

            if($end = $request->get('_end')){
                if ($end >= $from) {
                    $to = $from == 0 ? $end : $end - $from;
                } else {
                    $to = 1;
                }
            } else {
                $to = $request->get('_limit', 25);
            }
        }

        return [$from, $to];
    }


    /**
     * @param Request $request
     * @param QueryBuilder $qb
     * @return QueryBuilder
     */
    protected function getQueryBuilderNotEqual(Request $request, QueryBuilder $qb):QueryBuilder
    {
        if ($request->get('id_ne')) {
            $qb->andWhere('dp.id<>:id_ne')
                ->setParameter('id_ne', $request->get('id_ne'))
            ;
        }

        return $qb;
    }


    /**
     * @param Request $request
     * @param QueryBuilder $qb
     * @param $entityName
     * @return QueryBuilder
     */
    protected function getSelect(Request $request, QueryBuilder $qb, $entityName):QueryBuilder
    {
        if ($request->get('_fields')) {

            $fields = explode(',' , $request->get('_fields'));
            $arr = [];
            foreach ($fields as $field) {
                $field = trim($field);
                if (property_exists($entityName, $field)) {
                    array_push($arr, 'dp.' . $field);
                }
            }

            $qb->select(implode(',', $arr));
        }

        return $qb;
    }


    /**
     * @param QueryBuilder $qb
     * @return QueryBuilder
     */
    protected function decorateQueryBuilderWhere($where, QueryBuilder $qb):QueryBuilder
    {
        if ($where) {
            foreach ($where as $key => $value)

            if (is_array($value)) {
                    $qb->andWhere('dp.' . $key . ' IN (:_' . $key .')')
                        ->setParameter('_' . $key, $value)
                    ;
                } else {
                    $qb->andWhere('dp.' . $key . ' =:_' . $key)
                        ->setParameter('_' . $key, $value)
                    ;
                }
        }

        return $qb;
    }

    /**
     * @param Request $request
     * @param QueryBuilder $qb
     * @param array $order_map
     * @return QueryBuilder
     */
    protected function decorateQueryBuilderOrder(Request $request, QueryBuilder $qb, $order_map):QueryBuilder
    {
        $sort = $request->get('_sort', 'id');
        $direction = strtolower($request->get('_order', 'asc'));

        if (isset($order_map[$sort]) && in_array($direction,  ['asc', 'desc'])) {
            $qb->orderBy($order_map[$sort], $direction);
        }

        return $qb;
    }
}
