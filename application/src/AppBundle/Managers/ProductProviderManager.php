<?php

namespace AppBundle\Managers;

use AppBundle\Entity\ProductProvider;

use Doctrine\ORM\EntityManagerInterface;

class ProductProviderManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(ProductProvider::class);
    }

    /**
     * @param ProductProvider $productProvider
     * @return ProductProvider
     */
    public function createNewProductProvider(ProductProvider $productProvider)
    {
        $this->em->persist($productProvider);
        $this->em->flush();

        return $productProvider;
    }

    /**
     * @param ProductProvider $productProvider
     */
    public function removeProductProvider(ProductProvider $productProvider)
    {
        $productProvider->setIsDeleted(true);
        $this->em->flush();
    }


    /**
     * @return ProductProvider
     */
    public function getDefaultProductProvider(): ProductProvider
    {
        return $this->repository->find(ProductProvider::PRODUCT_PROVIDER_DEFAULT_ID);
    }
}