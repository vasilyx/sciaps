<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Ingredient;
use AppBundle\Repository\Elastic\ProductRepository;
use AppBundle\Entity\IngredientUnfiltered;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductsIngredients;
use AppBundle\Repository\IngredientRepository;
use AppBundle\Response\IngredientsFromBulkResponse;
use AppBundle\Services\ProductImport\IngredientParser;
use FOS\ElasticaBundle\Manager\RepositoryManagerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

class IngredientManager
{
    private $em;
    /** @var IngredientRepository */
    private $repository;
    private $elasticaManager;
    private $ingredientParser;

    public function __construct(EntityManagerInterface $em,
                                RepositoryManagerInterface $elasticaManager,
                                IngredientParser $ingredientParser)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Ingredient::class);
        $this->elasticaManager = $elasticaManager;
        $this->ingredientParser = $ingredientParser;
    }

    /**
     * @param string $name
     * @return array|null
     */
    public function getIngredientByNameWithMatchType(string $name): ?array
    {
        try {
            /** @var Ingredient $ingredient */
            $ingredient = $this->repository->getIngredientByNameAndMatchTypes($name);

            if ($ingredient === null) return null;

            if ($ingredient->getInciName() == $name) {
                return [$ingredient, IngredientUnfiltered::MATCH_TYPE_INCI];
            }

            if ($ingredient->getCommonName() == $name) {
                return [$ingredient, IngredientUnfiltered::MATCH_TYPE_COMMON];
            }

            if ($ingredient->getInnName() == $name) {
                return [$ingredient, IngredientUnfiltered::MATCH_TYPE_INN];
            }
            return null;
        } catch (NonUniqueResultException $e) {
            return [null, IngredientUnfiltered::MATCH_TYPE_CONFLICT];
        }
    }

    /**
     * @param Ingredient $ingredient
     * @return Ingredient
     */
    public function updateIngredient(Ingredient $ingredient): Ingredient
    {
        $this->em->persist($ingredient);

        $this->em->flush();

        return $ingredient;
    }

    /**
     * @param $ingredient_id
     * @return array
     */
    public function getProductsByIngredient($ingredient_id)
    {
        $ingredientUnfiltered = $this->em->getRepository(IngredientUnfiltered::class)->findBy(['ingredientId' => $ingredient_id]);

        $productsIngredients = $this->em->getRepository(ProductsIngredients::class)->findBy(['ingredientUnfiltered' => $ingredientUnfiltered]);

        foreach ($productsIngredients as $key => $value) {
            $productsIngredientsId[] = $value->getProduct()->getId();
            $productsIngredientsMatch[] = $this->em->getRepository(IngredientUnfiltered::class)->find($value->getIngredientUnfiltered()->getId())->getMatchType();
        }

        /** @var ProductRepository $elasticRepository */
        $elasticRepository = $this->elasticaManager->getRepository(Product::class);

        $products = $elasticRepository->getById($productsIngredientsId);

        return !empty($products) ? $products : [];
    }


    /**
     * @param $bulk
     * @return IngredientsFromBulkResponse
     */
    public function getIngredientsFromBulk($bulk) : IngredientsFromBulkResponse
    {
        if (!$bulk) return new IngredientsFromBulkResponse();

        $ingredientList = $this->ingredientParser->getParsedIngredientsList($bulk);

        $res_ingredient = [];
        $res_legend = [];
        $uniq_symbol = [];

        if ($ingredientList) {
            foreach ($ingredientList as $ingredient) {
                if ($ingredient['name']) {

                    $symbol = '';

                    if ($ingredient['legend']) {
                        $symbol = $this->ingredientParser->getLegendFromIngredient($ingredient['legend']);

                        if (!in_array($symbol, $uniq_symbol)) {
                            array_push($res_legend, ['description' => str_replace($symbol, '', $ingredient['legend']), 'symbol' => $symbol]);
                            array_push($uniq_symbol, $symbol);
                        }
                    }

                    $unfiltered = $this->em->getRepository(IngredientUnfiltered::class)->findOneBy(['name' => $ingredient['name']]);
                    array_push($res_ingredient, ['name' => $ingredient['name'], 'symbol' => $symbol, 'id' => ($unfiltered ? $unfiltered->getId(): null) ]);
                }
            }
        }

        $response = new IngredientsFromBulkResponse();

        $response->setIngredients($res_ingredient)
                    ->setLegend($res_legend);


        return $response;
    }
}
