<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Organization;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;

class OrganizationManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Organization::class);
    }

    /**
     * @param Organization $organization
     */
    public function removeOrganization(Organization $organization)
    {
        $this->em->remove($organization);
        $this->em->flush();
    }

    /**
     * @param Organization $organization
     * @return Organization
     */
    public function createNewOrganization(Organization $organization)
    {
        $this->em->persist($organization);
        $this->em->flush();

        return $organization;
    }

    /**
     * @return StreamedResponse
     */
    public function createCSV()
    {
        $organizations = $this->repository->findAll();

        $response = new StreamedResponse();
        $response->setCallback(
            function () use ($organizations) {
                $handle = fopen('php://output', 'r+');
                foreach ($organizations as $row) {
                    $data = [
                        $row->getId(),
                        $row->getCompanyName(),
                        $row->getWebsiteURL(),
                        $row->getBarcode(),
                        $row->getNumberProducts(),
                        $row->getProductsProvidedVia(),
                        $row->getProductsSoldThrough(),
                        $row->getPersonName(),
                        $row->getPersonEmail(),
                        $row->getPersonComment(),
                    ];
                    fputcsv($handle, $data);
                }
                fclose($handle);
            }
        );

        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="Organizations.csv"');

        return $response;
    }
}
