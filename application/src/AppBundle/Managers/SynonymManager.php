<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Synonym;
use AppBundle\Repository\SynonymRepository;
use Doctrine\ORM\EntityManagerInterface;

class SynonymManager
{
    private $em;
    /** @var SynonymRepository */
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Synonym::class);
    }
}
