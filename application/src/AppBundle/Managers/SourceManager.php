<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Source;
use AppBundle\Entity\SourceDataProvider;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class SourceManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Source::class);
    }

    /**
     * @param Source $source
     * @param Request $request
     * @return Source
     */
    public function createNewSource(Source $source)
    {
        $this->em->persist($source);
        $this->em->flush();

        return $source;
    }

    /**
     * @param Source $source
     */
    public function removeSource(Source $source)
    {
        $this->em->remove($source);
        $this->em->flush();
    }
}
