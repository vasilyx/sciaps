<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Retailer;
use AppBundle\Repository\RetailerRepository;
use Doctrine\ORM\EntityManagerInterface;

class RetailerManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Retailer::class);
    }


    /**
     * @param string $Retailer
     * @return Retailer
     */
    public function getRetailer($Retailer)
    {
        return $this->repository->findOneByName($Retailer);
    }

    /**
     * @param string $Retailer
     * @param bool $flush
     * @return Retailer
     */
    public function getRetailerOrCreate($Retailer, $flush = true)
    {
        if ($obj = $this->getRetailer($Retailer)) {
            return $obj;
        } else {
            $obj = new Retailer();
            $obj->setName($Retailer);
            return $this->createNew($obj, $flush);
        }
    }

    /**
     * @param Retailer $Retailer
     * @param bool $flush
     * @return Retailer
     */
    public function createNew(Retailer $Retailer, $flush = true)
    {
        $this->em->persist($Retailer);

        if ($flush) $this->em->flush();

        return $Retailer;
    }

}
