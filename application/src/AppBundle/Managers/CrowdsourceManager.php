<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Barcode;
use AppBundle\Entity\CrowdsourcePending;
use AppBundle\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class CrowdsourceManager
{
    private $em;
    private $repository;
    private $productManager;
    private $barcodeManager;
    private $productProviderManager;

    public function __construct(EntityManagerInterface $em, ProductManager $productManager,
                                ProductProviderManager $productProviderManager,
                                BarcodeManager $barcodeManager)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(CrowdsourcePending::class);
        $this->productManager = $productManager;
        $this->productManager = $productManager;
        $this->productProviderManager = $productProviderManager;
        $this->barcodeManager = $barcodeManager;
    }

    /**
     * @param CrowdsourcePending $crowdsource
     */
    public function removeCrowdsource(CrowdsourcePending $crowdsource)
    {
        $this->em->remove($crowdsource);
        $this->em->flush();
    }

    /**
     * @return CrowdsourcePending []
     */
    public function getAllCrowdsources()
    {
        $result = $this->repository->findAll();

        return $result;
    }


    /**
     * @param Request $request
     * @return CrowdsourcePending
     */
    public function createNewCrowdsource(Request $request)
    {
        $crowdsource = new CrowdsourcePending();

      // $file = $form['attachment']->getData();
        //   $file->move($directory, $someNewFilename);

        $crowdsource->setBarcode($this->barcodeManager->getBarcodeOrCreate($request->get('barcode')));

        $this->em->persist($crowdsource);
        $this->em->flush();

        return $crowdsource;
    }

    /**
     * @param CrowdsourcePending $crowdsource
     * @return CrowdsourcePending
     */
    public function updateNewCrowdsource(CrowdsourcePending $crowdsource)
    {
        $this->em->persist($crowdsource);
        $this->em->flush();

        if ($crowdsource->getStatus() == CrowdsourcePending::STATUS_HEAD_APPROVED) {
            $this->addNewProduct($crowdsource);
        }

        return $crowdsource;
    }

    /**
     * @param CrowdsourcePending $crowdsource
     */
    protected function addNewProduct(CrowdsourcePending $crowdsource)
    {
        $product = new Product();
        $product->setName($crowdsource->getName());
        $product->setBarcode($crowdsource->getBarcode());
        $product->setBrand($crowdsource->getBrand());
        $product->setManufacturer($crowdsource->getManufacturer());
        $product->setVolume($crowdsource->getVolume());
        $product->setVolumeUnits($crowdsource->getVolumeUnits());
        $product->setManufacturer($crowdsource->getManufacturer());

        $product->setProductProvider($this->productProviderManager->getDefaultProductProvider());

        $this->productManager->createNewProduct($product);
    }
}
