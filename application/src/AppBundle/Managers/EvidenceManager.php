<?php

namespace AppBundle\Managers;

use AppBundle\Entity\BrandEvidences;
use AppBundle\Entity\Evidence;
use AppBundle\Entity\IngredientsEvidences;
use AppBundle\Entity\ManufacturerEvidences;
use AppBundle\Entity\ProductEvidences;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class EvidenceManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Evidence::class);
    }

    /**
     * @param Evidence $evidence
     * @return Evidence
     */
    public function createNewEvidence(Evidence $evidence): Evidence
    {
        if (!is_null($evidence->getParent())) {
            $parent_evidence = $this->repository->find($evidence->getParent()->getId());
            if (is_null($parent_evidence->getParent())) {
                $this->em->persist($evidence);
                $this->em->flush();
            }
        } else {
            $this->em->persist($evidence);
            $this->em->flush();
        }

        return $evidence;
    }

    /**
     * @param Evidence $evidence
     * @return bool
     */
    public function removeEvidence(Evidence $evidence)
    {
        if (!$this->checkAssociations($evidence)) {
            $this->em->remove($evidence);
            $this->em->flush();

            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @param Evidence $evidence
     * @return bool
     */
    public function checkAssociations(Evidence $evidence)
    {
        if (
            $this->em->getRepository(BrandEvidences::class)->findOneBy(['evidence' => $evidence]) ||
            $this->em->getRepository(IngredientsEvidences::class)->findOneBy(['evidence' => $evidence]) ||
            $this->em->getRepository(ProductEvidences::class)->findOneBy(['evidence' => $evidence]) ||
            $this->em->getRepository(ManufacturerEvidences::class)->findOneBy(['evidence' => $evidence])
        ) {
            return true;
        }

        return false;
    }
}
