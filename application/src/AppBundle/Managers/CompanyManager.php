<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Company;
use AppBundle\Entity\CompanyServices;
use AppBundle\Entity\DataEntryProvider;
use AppBundle\Entity\ProductProvider;
use AppBundle\Entity\SourceDataProvider;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use http\Env\Response;
use Symfony\Component\HttpFoundation\Request;

class CompanyManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Company::class);
    }

    /**
     * @param Company $company
     * @param Request $request
     * @return Company
     */
    public function createNewCompany(Company $company, Request $request = null)
    {
        $this->em->clear();

        $this->em->persist($company);

        $services = $request->request->get('services');

        if ($services) {
            $rmServices = $this->em->getRepository(CompanyServices::class)->findBy(array(
                'company' => $company,
            ));

            foreach ($rmServices as $service) {
                $this->removeCompanyService($service);
            }

            $array_service = $services;
            foreach ($array_service as $key => $val) {
                $array_service[$key] = trim($val);
            }

            foreach ($array_service as $key => $val) {

                $companyService = $this->em->getRepository(CompanyServices::class)->findOneBy(array(
                    'company' => $company,
                    'serviceType' => $val,
                ));

                if ($companyService) {
                    continue;
                }

                $companyServices = new CompanyServices();
                $companyServices->setCompany($company);
                $companyServices->setServiceType($val);

                $this->em->persist($companyServices);
            }
        }

        $this->em->flush();

        return $company;
    }

    /**
     * @param Company $company
     * @return Company
     */
    public function updateCompany(Company $company)
    {
        $this->em->persist($company);
        $this->em->flush();

        return $company;
    }

    /**
     * @param CompanyServices $companyServices
     * @internal param Company $company
     */
    public function removeCompanyService(CompanyServices $companyServices)
    {
        $this->em->remove($companyServices);
        $this->em->flush();
    }

    /**
     * @param Company $company
     */
    public function removeCompany(Company $company)
    {
        $this->em->remove($company);
        $this->em->flush();
    }

    /**
     * @param Company $company
     * @return CompanyServices[]|array
     */
    public function getServices(Company $company)
    {
        $listCompanyServices = $this->em->getRepository(CompanyServices::class)->findBy(array(
            'company' => $company
        ));

        return $listCompanyServices;
    }

    /**
     * @param Company
     * @return Company
     */
    public function convertServices(Company $company)
    {
        /**
         * @var CompanyServices $companyService
         */
        $companyService = $company->getCompanyServices();

        $code_array = array();

        foreach ($companyService as $value) {
            /**
             * @var CompanyServices $code
             */
            $code = $value;
            array_push($code_array, $code->getServiceType());
        }

        $company->setServices($code_array);

        return $company;
    }

    /**
     * @param Company $company
     * @param $service_code
     * @return Object
     */
    public function turnOnService(Company $company, $service_code)
    {
        switch ($service_code) {
            case Company::PRODUCT_TYPE:
                $provider = new ProductProvider();
                $provider->setProductProviderType(0);
                $provider->setConnectionType(0);
                break;
            case Company::SOURCE_TYPE:
                $provider = new SourceDataProvider();
                $provider->setSourceDataProviderType(0);
                break;
            case Company::DATAENTRY_TYPE:
                $provider = new DataEntryProvider();
                break;
            default:
                break;
        }
        $provider->setCompany($company);

        $companyService = $this->em->getRepository(CompanyServices::class)->findOneBy(array(
            'company' => $company,
            'serviceType' => $service_code
        ));
        if (!$companyService) {
            $companyService = new CompanyServices();
            $companyService->setCompany($company);
            $companyService->setServiceType($service_code);

            $this->em->persist($companyService);
            $this->em->flush();
        }

        $this->em->persist($provider);

        try {
            $this->em->flush();
        }
        catch (UniqueConstraintViolationException $e) {
        }

        // TODO REBUILD - could be null

        return $provider;
    }

    /**
     * @param Company $company
     * @param $service_code
     * @return Object
     */
    public function getService(Company $company, $service_code)
    {
        switch ($service_code) {
            case Company::PRODUCT_TYPE:
                $repositoryType = $this->em->getRepository(ProductProvider::class);
                break;
            case Company::SOURCE_TYPE:
                $repositoryType = $this->em->getRepository(SourceDataProvider::class);
                break;
            case Company::DATAENTRY_TYPE:
                $repositoryType = $this->em->getRepository(DataEntryProvider::class);
                break;
            default:
                break;
        }


        // TODO REBUILD - could be null
        $provider = $repositoryType->findOneBy(array(
                'company' => $company
            )
        );

        return $provider;
    }

    /**
     * @param Object $object
     * @param Company $company
     * @param $service_code
     * @return Object
     */
    public function createNewService(Object $object, Company $company, $service_code)
    {
        switch ($service_code) {
            case Company::PRODUCT_TYPE:
                $object->setCompany($company);
                break;
            case Company::SOURCE_TYPE:
                $object->setCompany($company);
                break;
            case Company::DATAENTRY_TYPE:
                $object->setCompany($company);
                break;
            default:
                break;
        }

        // TODO REBUILD - no breaks


        $this->em->persist($object);
        try {
            $this->em->flush();
        }
        catch (UniqueConstraintViolationException $e) {
        }

        return $object;
    }

    /**
     * @param Object $object
     * @param $service_code
     * @return DataEntryProvider|ProductProvider|SourceDataProvider|Object
     */
    public function updateService(Object $object, $service_code)
    {
        switch ($service_code) {
            case Company::PRODUCT_TYPE:
                /**
                 * @var ProductProvider $object
                 */
                $this->em->persist($object);
                break;
            case Company::SOURCE_TYPE:
                /**
                 * @var SourceDataProvider $object
                 */
                $this->em->persist($object);
                break;
            case Company::DATAENTRY_TYPE:
                /**
                 * @var DataEntryProvider $object
                 */
                $this->em->persist($object);
                break;
            default:
                break;
        }
        $this->em->flush();
        // TODO REBUILD - no breaks

        return $object;
    }

    /**
     * @param Company $company
     * @param $service_code
     * @return Object|null
     */
    public function getProduct(Company $company, $service_code)
    {
        switch ($service_code) {
            case Company::PRODUCT_TYPE:
                $provider = $this->em->getRepository(ProductProvider::class)->findOneBy(array(
                    'company' => $company,
                ));
                break;
            case Company::SOURCE_TYPE:
                $provider = $this->em->getRepository(SourceDataProvider::class)->findOneBy(array(
                    'company' => $company,
                ));
                break;
            case Company::DATAENTRY_TYPE:
                $provider = $this->em->getRepository(DataEntryProvider::class)->findOneBy(array(
                    'company' => $company,
                ));
                break;
            default:
                break;
        }

        return $provider ?? null;
    }


    /**
     * @return Company
     */
    public function getDefaultCompany(): Company
    {
        return $this->repository->find(Company::COMPANY_DEFAULT_ID);
    }
}
