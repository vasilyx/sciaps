<?php

namespace AppBundle\Managers;

use AppBundle\Entity\Attribute;
use AppBundle\Entity\Evidence;
use AppBundle\Entity\Ingredient;
use AppBundle\Entity\Source;
use AppBundle\Entity\IngredientsAttributes;
use AppBundle\Entity\IngredientsEvidences;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class IngredientAssociationManager
{
    private $em;
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(IngredientsEvidences::class);
    }

    /**
     * @param IngredientsEvidences $ingredientsEvidences
     * @param Request|null $request
     * @param Source $source
     * @param Evidence $evidence
     * @return null
     */
    public function createIngredientEvidence(IngredientsEvidences $ingredientsEvidences, Request $request = null, Source $source, Evidence $evidence)
    {
        $array_ingredients = $request->request->get('ingredients');

        foreach ($array_ingredients as $key => $val) {

            $ingredient = $this->em->getRepository(Ingredient::class)->find(trim($val));
            if (empty($ingredient)) continue;
            
            $ingredientEvidence = $this->repository->findBy(array(
                'source' => $source,
                'evidence' => $evidence,
                'ingredient' => $ingredient
            ));

            if (empty($ingredientEvidence)) {
                $ingredientEvidence = new IngredientsEvidences();
                $ingredientEvidence->setSource($source);
                $ingredientEvidence->setEvidence($evidence);
                $ingredientEvidence->setIngredient($ingredient);
                $ingredientEvidence->setStatusCode($ingredientsEvidences->getStatusCode());
                $ingredientEvidence->setDescription($ingredientsEvidences->getDescription());
                $this->em->persist($ingredientEvidence);
            }
        }

        $this->em->flush();

        return true;
    }

    /**
     * @param Source $source
     * @param Evidence $evidence
     * @param Ingredient $ingredient
     * @return IngredientsEvidences
     */
    public function findIngredientsEvidence(Source $source, Evidence $evidence, Ingredient $ingredient): IngredientsEvidences
    {
        $ingredientEvidence = $this->repository->findOneBy(array(
            'source' => $source,
            'evidence' => $evidence,
            'ingredient' => $ingredient
        ));

        return $ingredientEvidence;
    }

    /**
     * @param IngredientsEvidences $ingredientsEvidences
     * @return IngredientsEvidences
     */
    public function updateIngredientEvidence(IngredientsEvidences $ingredientsEvidences): IngredientsEvidences
    {
        $this->em->persist($ingredientsEvidences);
        $this->em->flush();

        return $ingredientsEvidences;
    }

    /**
     * @param Source $source
     * @param Evidence $evidence
     * @param Ingredient $ingredient
     * @return IngredientsEvidences|null
     */
    public function deleteIngredientEvidence(Source $source, Evidence $evidence, Ingredient $ingredient)
    {
        $ingredientEvidence = $this->repository->findOneBy(array(
            'source' => $source,
            'evidence' => $evidence,
            'ingredient' => $ingredient
        ));
        if (!empty($ingredientEvidence)) {
            $this->em->remove($ingredientEvidence);
            $this->em->flush();
        }

        return $ingredientEvidence;
    }

    /**
     * @param IngredientsAttributes $ingredientsAttributes
     * @param Request|null $request
     * @param Source $source
     * @param Attribute $attribute
     * @return null
     */
    public function createIngredientAttribute(IngredientsAttributes $ingredientsAttributes, Request $request = null, Source $source, Attribute $attribute)
    {
        $array_ingredients = $request->request->get('ingredients');

        foreach ($array_ingredients as $key => $val) {
            $ingredient = $this->em->getRepository(Ingredient::class)->find(trim($val));
            if (empty($ingredient)) continue;
            $ingredientAttribute = $this->em->getRepository(IngredientsAttributes::class)->findBy(array(
                'source' => $source,
                'attribute' => $attribute,
                'ingredient' => $ingredient
            ));
            if (empty($ingredientAttribute)) {
                $ingredientAttribute = new IngredientsAttributes();
                $ingredientAttribute->setSource($source);
                $ingredientAttribute->setAttribute($attribute);
                $ingredientAttribute->setIngredient($ingredient);
                $ingredientAttribute->setStatusCode($ingredientsAttributes->getStatusCode());
                $ingredientAttribute->setDescription($ingredientsAttributes->getDescription());
                $this->em->persist($ingredientAttribute);
            }
        }

        $this->em->flush();

        return true;
    }

    /**
     * @param Source $source
     * @param Attribute $attribute
     * @param Ingredient $ingredient
     * @return IngredientsAttributes
     */
    public function findIngredientsAttribute(Source $source, Attribute $attribute, Ingredient $ingredient): IngredientsAttributes
    {
        $ingredientAttribute = $this->em->getRepository(IngredientsAttributes::class)->findOneBy(array(
            'source' => $source,
            'attribute' => $attribute,
            'ingredient' => $ingredient
        ));

        return $ingredientAttribute;
    }

    /**
     * @param IngredientsAttributes $ingredientsAttributes
     * @return IngredientsAttributes
     */
    public function updateIngredientAttribute(IngredientsAttributes $ingredientsAttributes): IngredientsAttributes
    {
        $this->em->persist($ingredientsAttributes);
        $this->em->flush();

        return $ingredientsAttributes;
    }

    /**
     * @param Source $source
     * @param Attribute $attribute
     * @param Ingredient $ingredient
     * @return IngredientsAttributes|null
     */
    public function deleteIngredientAttribute(Source $source, Attribute $attribute, Ingredient $ingredient)
    {
        $ingredientAttribute = $this->em->getRepository(IngredientsAttributes::class)->findOneBy(array(
            'source' => $source,
            'attribute' => $attribute,
            'ingredient' => $ingredient
        ));
        if (!empty($ingredientAttribute)) {
            $this->em->remove($ingredientAttribute);
            $this->em->flush();
        }

        return $ingredientAttribute;
    }

    /**
     * @param Source $source
     * @return array
     */
    public function getEvidences(Source $source): array
    {
        $ingredientsEvidence = $this->em->getRepository(IngredientsEvidences::class)->findBy(array(
            'source' => $source
        ));

        return $ingredientsEvidence;
    }

    /**
     * @param Source $source
     * @return array
     */
    public function getAttributes(Source $source): array
    {
        $ingredientsAttribute = $this->em->getRepository(IngredientsAttributes::class)->findBy(array(
            'source' => $source
        ));

        return $ingredientsAttribute;
    }

    /**
     * @param Source $source
     * @param Evidence $evidence
     * @param Ingredient $ingredient
     * @return IngredientsEvidences|null
     */
    public function getIngredientsEvidences(Source $source, Evidence $evidence, Ingredient $ingredient)
    {
        $ingredientEvidence = $this->em->getRepository(IngredientsEvidences::class)->findOneBy(array(
            'source' => $source,
            'evidence' => $evidence,
            'ingredient' => $ingredient
        ));
        return $ingredientEvidence;
    }

    /**
     * @param Source $source
     * @param Attribute $attribute
     * @param Ingredient $ingredient
     * @return IngredientsAttributes|null
     */
    public function getIngredientsAttributes(Source $source, Attribute $attribute, Ingredient $ingredient)
    {
        $ingredientAttribute = $this->em->getRepository(IngredientsAttributes::class)->findOneBy(array(
            'source' => $source,
            'attribute' => $attribute,
            'ingredient' => $ingredient
        ));
        return $ingredientAttribute;
    }
}
