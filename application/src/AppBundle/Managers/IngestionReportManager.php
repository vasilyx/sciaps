<?php

namespace AppBundle\Managers;

use AppBundle\Entity\IngestionReport;
use AppBundle\Entity\ProductProvider;
use AppBundle\Entity\RegExp;
use AppBundle\Entity\User;
use AppBundle\Repository\IngestionReportRepository;
use AppBundle\Repository\RegExpRepository;
use Doctrine\ORM\EntityManagerInterface;

class IngestionReportManager
{
    private $em;

    /* @var IngestionReportRepository */
    private $repository;

    /* @var RegExpRepository */
    private $repositoryRegExp;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(IngestionReport::class);
        $this->repositoryRegExp = $this->em->getRepository(RegExp::class);
    }


    /**
     * @param User $user
     * @param ProductProvider $productProvider
     * @return IngestionReport
     */
    public function createOnDataImport(User $user, ProductProvider $productProvider)
    {
        $ingestionReport = new IngestionReport();
        $ingestionReport->setUser($user);
        $ingestionReport->setProductProvider($productProvider);
        $ingestionReport->setRunDate(new \DateTime());

        // Check to see if there is already an ingestion report, if so, duplicate the regex config.
        /* @var IngestionReport $previousIngestion */
        $previousIngestion = $this->repository->getLastReportByProvider($productProvider);

        /* @var RegExp $defaultLegendRegExp */
        $defaultLegendRegExp = $this->repositoryRegExp->getDefaultByType(RegExp::TYPE_LEGEND_PART);

        /* @var RegExp $defaultIngredientRegExp */
        $defaultIngredientRegExp = $this->repositoryRegExp->getDefaultByType(RegExp::TYPE_INGREDIENT_PART);

        if ($previousIngestion && $previousIngestion->getRegexp()) {
            foreach ($previousIngestion->getRegexp() as $regexp) {

                /* @var RegExp $regexp */
                if ($regexp->getRegexType() == RegExp::TYPE_LEGEND_PART) {

                    if ($defaultLegendRegExp->getVersion() > $regexp->getVersion()) {
                        $ingestionReport->addRegexp($defaultLegendRegExp);
                    } else {
                        $ingestionReport->addRegexp($regexp);
                    }
                }

                if ($regexp->getRegexType() == RegExp::TYPE_INGREDIENT_PART) {

                    if ($defaultIngredientRegExp->getVersion() > $regexp->getVersion()) {
                        $ingestionReport->addRegexp($defaultIngredientRegExp);
                    } else {
                        $ingestionReport->addRegexp($regexp);
                    }
                }
            }

        } else {
            $ingestionReport->addRegexp($defaultLegendRegExp);
            $ingestionReport->addRegexp($defaultIngredientRegExp);
        }

        $this->em->persist($ingestionReport);
        $this->em->flush();

        return $ingestionReport;
    }

    /**
     * @param IngestionReport $ingestionReport
     * @param array $result
     * @return IngestionReport
     */
    public function updateOnDataImport(IngestionReport $ingestionReport, $result = [])
    {
        $ingestionReport->setResponseJson(json_encode($result));
        $ingestionReport->setStatus(IngestionReport::STATUS_SUCCESS);

        $this->em->flush();

        return $ingestionReport;
    }

    public function getById(int $id)
    {
        return $this->repository->findOneOrNullById($id);
    }
}
