<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 06/10/2018
 * Time: 21:51
 */

namespace AppBundle\Services\Email;

use AppBundle\Services\Email\Types\Email;

interface EmailerInterface
{
    public function send(Email $email);
}
