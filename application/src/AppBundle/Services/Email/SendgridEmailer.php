<?php

namespace AppBundle\Services\Email;

use AppBundle\Services\Email\Types\Email;
use SendGrid;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * Class SendgridEmailer
 * @package AppBundle\Services\Email
 */
class SendgridEmailer extends AbstractEmailer
{
    /**
     * EmailManager constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * @param Email $email
     * @return bool|null|SendGrid\Response
     * @throws SendGrid\Mail\TypeException
     */
    public function send(Email $email)
    {
        $message = new SendGrid\Mail\Mail();
        $message->setFrom($this->container->getParameter('mail_send_from'), "SkinNinja Support");
        $message->setSubject($email->getSubject());
        $message->setGlobalSubject($email->getSubject());
        foreach ($email->getTo() as $address) {
            $message->addTo($address);
        }
        foreach ($email->getCc() as $address) {
            $message->addCc($address);
        }
        foreach ($email->getBcc() as $address) {
            $message->addBcc($address);
        }

        if ($email->getSendGridTemplateId()) {
            $message->setTemplateId($email->getSendGridTemplateId());

            foreach ($email->getDynamicContent() as $k => $v) {
                $data[$k] = strpos($k, 'link') === false ? $v : $this->container->getParameter('domain_url') . $v;
            }

            $message->addDynamicTemplateDatas($data);
        } else {
            $message->addContent("text/html", $email->getBody());
        }

        foreach ($email->getAttachments() as $attachment) {
            $file_encoded = base64_encode(file_get_contents($attachment['file']));
            $message->addAttachment(
                $file_encoded,
                $attachment['mime'],
                $attachment['filename'],
                "attachment"
            );
        }

        $sendGrid = new SendGrid($this->container->getParameter('sendgrid_api_key'));

        try {
            $response = $sendGrid->send($message);
            if ($response->statusCode() != "202") {
                file_put_contents('php://stderr', $response->body());
            }
        } catch (\Exception $e) {
            $response = null;
            file_put_contents('php://stderr', $e->getMessage());
        }

        return $response;
    }
}
