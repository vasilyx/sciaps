<?php

namespace AppBundle\Services\Email;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * Class EmailManager
 * @package AppBundle\Managers
 */
abstract class AbstractEmailer implements EmailerInterface
{
    protected $container;

    /**
     * EmailManager constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return Container
     */
    public function getContainer(): Container
    {
        return $this->container;
    }

    /**
     * @param Container $container
     * @return AbstractEmailer
     */
    public function setContainer(Container $container): self
    {
        $this->container = $container;

        return $this;
    }
}
