<?php

namespace AppBundle\Services\Email\Types;


class ResetPasswordEmail extends Email
{
    public function __construct(string $email = null, $params = [])
    {
        parent::__construct($email);

        $this->setDynamicContent([
            'subject' => 'Password Reset',
            'header' => 'Password Reset',
            'text' => 'Your password has just been reset. Please contact us if this is not the case',
        ]);
    }
}
