<?php
/**
 * Created by PhpStorm.
 * User: jasonlinthwaite
 * Date: 17/07/2018
 * Time: 09:46
 */

namespace AppBundle\Services\Email\Types;

/**
 * Class Email
 *
 * @package AppBundle\Services\Email\Types
 */
abstract class Email
{
    /* @var array $to Recipient of the email */
    public $to;

    /* @var array $from Email address sending email */
    public $from;

    /* @var array $attachments Attachments to include in the email */
    public $attachments;

    /* @var array $headers Defined headers */
    public $headers;

    /* @var string $subject Subject */
    public $subject;

    /* @var string $body HTML body of the email */
    public $body;

    /* @var array $cc CC */
    public $cc;

    /* @var array $bcc BCC */
    public $bcc;

    /* @var string $contentType HTML or standard */
    public $contentType;

    /**
     * @var string
     */
    protected $template = '';

    /**
     * @var array params for template
     */
    protected $renderParams = [];

    /**
     * @var string $sendGridTemplate Template ID in Sendgrid
     */
    protected $sendGridTemplateId;

    /**
     * @var array $dynamicContent Array of key value pairs for the template
     */
    protected $dynamicContent;

    /**
     * Email constructor.
     *
     * @param string $email
     *
     * @param array $params
     *
     * @throws \Exception
     */
    public function __construct(string $email = null)
    {
        if (empty($email)) {
            throw new \InvalidArgumentException('Invalid user provided');
        }

        // Default template is the transactional template.
        $this->setSendGridTemplateId('d-f4493b659bda4050aff95caffa06f956');
        $this->to = [$email];
        $this->attachments = [];
        $this->headers = [];
        $this->subject = '';
        $this->body = '';
        $this->cc = [];
        $this->bcc = [];
        $this->contentType = 'text/html';
    }

    /**
     * @return array
     */
    public function getTo(): array
    {
        return $this->to;
    }

    /**
     * @param array $to
     *
     * @return Email
     */
    public function setTo(array $to): Email
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @return array
     */
    public function getFrom(): array
    {
        return $this->from;
    }

    /**
     * @param array $from
     *
     * @return Email
     */
    public function setFrom(array $from): Email
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @return array
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

    /**
     * @param array $attachment
     *
     * @return Email
     */
    public function addAttachment(array $attachment): Email
    {
        $this->attachments[] = $attachment;

        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     *
     * @return Email
     */
    public function setHeaders(array $headers): Email
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     *
     * @return Email
     */
    public function setSubject(string $subject): Email
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param string $body
     *
     * @return Email
     */
    public function setBody(string $body): Email
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return array
     */
    public function getCc(): array
    {
        return $this->cc;
    }

    /**
     * @param array $cc
     *
     * @return Email
     */
    public function setCc(array $cc): Email
    {
        $this->cc = $cc;

        return $this;
    }

    /**
     * @return array
     */
    public function getBcc(): array
    {
        return $this->bcc;
    }

    /**
     * @param array $bcc
     *
     * @return Email
     */
    public function setBcc(array $bcc): Email
    {
        $this->bcc = $bcc;

        return $this;
    }

    /**
     * @return string
     */
    public function getContentType(): string
    {
        return $this->contentType;
    }

    /**
     * @param string $contentType
     *
     * @return Email
     */
    public function setContentType(string $contentType): Email
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @param string $template
     */
    public function setTemplate(string $template)
    {
        $this->template = $template;
    }

    /**
     * @return array
     */
    public function getRenderParams(): array
    {
        return $this->renderParams;
    }

    /**
     * @param array $renderParams
     */
    public function setRenderParams(array $renderParams)
    {
        $this->renderParams = $renderParams;
    }

    /**
     * @return string|null
     */
    public function getSendGridTemplateId(): ?string
    {
        return $this->sendGridTemplateId;
    }

    /**
     * @param string $sendGridTemplateId
     */
    public function setSendGridTemplateId(string $sendGridTemplateId): void
    {
        $this->sendGridTemplateId = $sendGridTemplateId;
    }

    /**
     * @return array
     */
    public function getDynamicContent(): array
    {
        return $this->dynamicContent;
    }

    /**
     * @param array $dynamicContent
     */
    public function setDynamicContent(array $dynamicContent): void
    {
        $this->dynamicContent = $dynamicContent;
    }
}
