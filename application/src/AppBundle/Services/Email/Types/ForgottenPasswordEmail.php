<?php

namespace AppBundle\Services\Email\Types;


class ForgottenPasswordEmail extends Email
{
    public function __construct(string $email = null, $params = [])
    {
        parent::__construct($email);


        $this->setDynamicContent([
            'subject' => 'Forgotten password',
            'header' => 'Password Reset',
            'text' => 'It looks like you requested to reset your password. Please click the link below to do so.',
            'c2a_link' => '/auth/reset-password/' . $params['forgottenPasswordToken'],
            'c2a_button' => 'Reset Password'
        ]);
    }
}
