<?php
namespace AppBundle\Services\Email\Types;


class WelcomeEmail extends Email
{
    /**
     * WelcomeEmail constructor.
     *
     * @param string $email Email address to send to
     * @param array $params
     *
     * @throws \Exception
     */
    public function __construct(string $email = null, $params = [])
    {
        parent::__construct($email);

        $this->setDynamicContent([
            'subject' => 'Welcome to SkinNinja',
            'header' => 'Activate Account',
            'text' => 'It looks like you requested an account. Please click the link below to do so.'
        ]);
    }

}
