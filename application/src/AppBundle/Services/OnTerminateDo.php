<?php

namespace AppBundle\Services;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;

/**
 * Class OnTerminateDo
 * @package AppBundle\Services
 */
class OnTerminateDo
{
    /** @var EntityRepository */
    private $jobs = [];

    /**
     * @param object     $object
     * @param string     $method - name of objects' method
     * @param mixed|null $vars
     *
     * @return $this
     */
    public function add($object, $method, $vars = null)
    {
        if (!is_object($object) || !is_string($method)) {
            return $this;
        }

        if (method_exists($object, $method)) {
            $this->jobs[] = [
                'object' => $object,
                'method' => $method,
                'vars' => $vars,
            ];
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return count($this->jobs);
    }

    /**
     * @return void
     */
    public function doJobs()
    {
        foreach ($this->jobs as $job) {
            call_user_func_array([$job['object'], $job['method']], $job['vars']);
        }
    }

}
