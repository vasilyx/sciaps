<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * Class ExternalCommands sets command for which need call from php
 * @package AppBundle\Services
 */
class ExternalCommands
{
    /** @var Container */
    private $container;

    /** @var string $rootDir project root directory */
    private $rootDir;

    /** @var string $logDir project log directory */
    private $logDir;

    /** @var string $env project environment */
    private $env;

    /** @var string $logFileName log file name */
    private $logFileName;

    /** @var string $isUnix */
    private $isUnix;
    private $asyncProcess;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->rootDir = realpath($container->get('kernel')->getRootDir());
        $this->logDir = realpath($container->get('kernel')->getLogDir());
        $this->env = $container->get('kernel')->getEnvironment();
        $this->logFileName = 'commands.' . $this->env . '.' . date('ym') . '.log';
        $this->isUnix = !(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN');
        $this->asyncProcess = $this->env == 'test' ? false : true;
    }

    /**
     * Call evrelab:importProducts
     * @param int $productProviderId
     */
    public function runImportProducts(int $productProviderId = 0)
    {
        $command = 'php -q '
            . $this->rootDir . '/bin/console --env='. $this->env .' evrelab:importProducts '
            . (int)$productProviderId
        ;

        // If not Windows
        if ($this->isUnix) {
            $command .= ' >> ' . $this->logDir . '/' . $this->logFileName
            . ' 2>>' . $this->logDir . '/' . $this->logFileName;
            if ($this->asyncProcess) {
                $command .= ' &';
            }
        }

        file_put_contents($this->logDir . '/' . $this->logFileName, '' . $command . "\n", FILE_APPEND);
        exec($command);
        file_put_contents($this->logDir . '/' . $this->logFileName, "\n", FILE_APPEND);
    }
}
