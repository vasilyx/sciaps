<?php

namespace AppBundle\Services;

use SessionHandlerInterface;

class RedisSession implements SessionHandlerInterface
{
    private $client;

    public function __construct(RedisClient $redisClient)
    {
        $this->client = $redisClient;
    }

    public function close(): bool
    {
        return true;
    }

    public function destroy($session_id): bool
    {
        return $this->client->unset($session_id);
    }

    public function gc($maxlifetime): bool
    {
        return true;
    }

    public function open($save_path, $session_name): bool
    {
        return true;
    }

    public function read($session_id): string
    {
        if ($data = $this->client->get($session_id)) {
            return $data;
        }

        return '';
    }

    public function write($session_id, $session_data, $second = 3600): bool
    {
        if ($this->client->set($session_id, $session_data, $second)) return true;

        return false;
    }
}
