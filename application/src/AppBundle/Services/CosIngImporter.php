<?php

namespace AppBundle\Services;

use AppBundle\Entity\Ingredient;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Services\FileUploads\LocalFileManager;


class CosIngImporter
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var \AppBundle\Repository\IngredientRepository */
    private $ingredientRepository;

    /** @var LocalFileManager */
    private $fileManager;

    /**
     * CosIngImporter constructor.
     *
     * @param EntityManagerInterface $em
     * @param LocalFileManager $fileManager
     */
    public function __construct(EntityManagerInterface $em, LocalFileManager $fileManager)
    {
        $this->em = $em;
        $this->ingredientRepository = $this->em->getRepository(Ingredient::class);
        $this->fileManager = $fileManager;
    }

    /**
     * @param string $url
     * @param int $batchSize
     * @return bool
     */
    public function import(string $url = "", int $batchSize = 1000): bool
    {
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);

        $data = $this->fileManager->getByUrl($url);

        $array = array_map('str_getcsv', $data);

        $count = 1;
        for ($i = 0; $i < count($array); $i++) {

            $temp_arr = [];
            for ($j = 0; $j < 11; $j++) {
                $temp_arr[] = isset($array[$i][$j]) && $array[$i][$j] != "" ? $array[$i][$j] : "";
            }

            if ($temp_arr[0] != 0) {
                $newIngredient = new Ingredient();
                $newIngredient->setCosingNumber(trim($temp_arr[0]));
                $newIngredient->setInciName(trim($temp_arr[1]));
                $newIngredient->setInnName(trim($temp_arr[2]));
                $newIngredient->setPhEurName(trim($temp_arr[3]));
                $newIngredient->setCasNumber(trim($temp_arr[4]));
                $newIngredient->setEcNumber(trim($temp_arr[5]));
                $newIngredient->setDescription(trim($temp_arr[6]));

                $hash = md5(serialize($newIngredient));
                $newIngredient->setCosingHash($hash);

                $ingredient = $this->ingredientRepository->findOneBy([
                    'cosingNumber' => $temp_arr[0]
                ]);

                if (!$ingredient) {
                    ++$count;
                    $this->em->persist($newIngredient);
                } else {
                    $hash = md5(serialize($ingredient));
                    $hashNewIngredient = md5(serialize($newIngredient));

                    if ($hash != $hashNewIngredient) {
                        $ingredient->setCosingHash($hash);
                        $ingredient->setCosingNumber(trim($temp_arr[0]));
                        $ingredient->setInciName(trim($temp_arr[1]));
                        $ingredient->setInnName(trim($temp_arr[2]));
                        $ingredient->setPhEurName(trim($temp_arr[3]));
                        $ingredient->setCasNumber(trim($temp_arr[4]));
                        $ingredient->setEcNumber(trim($temp_arr[5]));
                        $ingredient->setDescription(trim($temp_arr[6]));

                        ++$count;
                        $this->em->persist($ingredient);
                    }
                }

            }
            if ($count % $batchSize === 0) {
                $this->em->flush();
                $this->em->clear();
            }
            $ingredient = $temp_arr = null;
        }

        $this->em->flush();
        $this->em->clear();

        return true;
    }
}
