<?php
/**
 * Labstep.
 *
 * @author     Thomas Bullier <thomas@labstep.com>
 */

namespace AppBundle\Services;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Enable or disable listeners.
 */
class ListenerService
{
    /** @var EntityManagerInterface Doctrine entity manager */
    protected $em;

    /** @var array List of disabled listeners */
    protected $disabledListeners;

    /**
     * Constructor.
     *
     * @param EntityManagerInterface $em Doctrine entity manager
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->disabledListeners = [];
    }

    /**
     * Disables one doctrine listener.
     *
     * @param string $className Listener class name
     */
    public function disableListener($className)
    {
        $evm = $this->em->getEventManager();
        $this->disabledListeners = [];
        foreach ($evm->getListeners() as $event => $listeners) {
            foreach ($listeners as $listener) {
                if ($listener instanceof $className) {
                    $evm->removeEventListener([$event], $listener);
                    $this->disabledListeners[] = [
                        'event' => $event,
                        'listener' => $listener,
                    ];
                }
            }
        }
    }

    /**
     * Disables all doctrine listeners.
     */
    public function disableListeners()
    {
        $evm = $this->em->getEventManager();
        $this->disabledListeners = [];
        foreach ($evm->getListeners() as $event => $listeners) {
            foreach ($listeners as $listener) {
                $listenerClass = get_class($listener);
                if ('FOS\ElasticaBundle\Doctrine\Listener' !== $listenerClass
                    && 'Gedmo\Loggable\LoggableListener' !== $listenerClass
                    && false === strpos($listenerClass, 'AppBundle')) {
                    continue;
                }
                $evm->removeEventListener([$event], $listener);
                $this->disabledListeners[] = [
                    'event' => $event,
                    'listener' => $listener,
                ];
            }
        }
    }

    /**
     * Reactivate doctrine listeners.
     */
    public function enableListeners()
    {
        $evm = $this->em->getEventManager();
        foreach ($this->disabledListeners as $data) {
            $evm->addEventListener([$data['event']], $data['listener']);
        }
    }
}
