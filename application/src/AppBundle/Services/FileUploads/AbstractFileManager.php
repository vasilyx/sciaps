<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 22/09/2018
 * Time: 15:09
 */

namespace AppBundle\Services\FileUploads;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * Class AbstractFileManager
 * @package AppBundle\Services\FileUploads
 */
abstract class AbstractFileManager implements FileManagerInterface
{
    /**
     * @var mixed $stream
     */
    protected $stream;
    /**
     * @var Container
     */
    protected $container;

    /** @var string $csvProductUploadPath */
    protected $csvProductUploadPath;

    /**
     * AbstractFileManager constructor.
     * @param Container $container
     * @param string $csvProductUploadPath
     */
    public function __construct(Container $container, string $csvProductUploadPath)
    {
        $this->container = $container;
        $this->csvProductUploadPath = $csvProductUploadPath;
    }

    /**
     * @return mixed
     */
    public function getStream()
    {
        return $this->stream;
    }

    /**
     * @param mixed $stream
     */
    public function setStream($stream): void
    {
        $this->stream = $stream;
    }
}