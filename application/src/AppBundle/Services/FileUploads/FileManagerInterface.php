<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 22/09/2018
 * Time: 15:04
 */

namespace AppBundle\Services\FileUploads;

/**
 * Interface FileManagerInterface
 * @package AppBundle\Services\FileUploads
 */
interface FileManagerInterface
{
    /**
     * @param string $filename
     * @return mixed
     */
    public function get(string $filename);

    /**
     * @param string $filename
     * @param $data
     * @return bool
     */
    public function save(string $filename, $data): bool;

    /**
     * @param string $filename
     * @return bool
     */
    public function delete(string $filename): bool;

    /**
     * @param string $filename
     * @return mixed
     */
    public function openStream(string $filename);

    /**
     * @return bool
     */
    public function closeStream(): bool;
}