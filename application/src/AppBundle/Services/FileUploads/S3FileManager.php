<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 22/09/2018
 * Time: 15:09
 */

namespace AppBundle\Services\FileUploads;

use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class S3FileManager extends AbstractFileManager
{
    private $client;

    private $s3Bucket;

    public function __construct(Container $container)
    {
        parent::__construct($container, $container->getParameter('csv_product_upload_dir'));

        $this->container = $container;
        $params = [
            'version' => 'latest',
            'region' => 'eu-west-2'
        ];

        $this->s3Bucket = $container->getParameter('aws_s3_bucket');

        $this->setClient(new S3Client($params));
    }

    public function save(string $filename, $data, string $dir = null): bool
    {
        $path = (empty($dir) ? '' : $dir . (substr($dir, -1) == '/' ?: '/')) . $filename;

        try {
            $this->client->putObject([
                'Bucket' => $this->s3Bucket,
                'Key' => $path,
                'Body' => $data,
            ]);

            return true;
        } catch (S3Exception $e) {
            echo "There was an error uploading the file.\n";
            echo $e->getMessage();

            return false;
        }
    }

    public function get(string $filename, string $dir = null)
    {
        $path = (empty($dir) ? '' : $dir . (substr($dir, -1) == '/' ?: '/')) . $filename;

        $result = $this->client->getObject([
            'Bucket' => $this->s3Bucket,
            'Key' => $path,
            'SaveAs' => '/tmp/'.$filename
        ]);

        return file('/tmp/' . $filename);
    }

    public function delete(string $filename, string $dir = null): bool
    {
        $path = (empty($dir) ? '' : $dir . (substr($dir, -1) == '/' ?: '/')) . $filename;

        $result = $this->client->deleteObject([
            'Bucket' => $this->s3Bucket,
            'Key' => $path
        ]);
    }

    public function openStream(string $filename, string $dir = null)
    {
        $this->client->registerStreamWrapper();

        $url = $this->getS3Url($filename, $dir);
        if ($stream = fopen($url, 'r')) {
            $this->setStream($stream);
            return $stream;
        }
    }

    public function closeStream(): bool
    {
        return fclose($this->getStream());
    }

    public function getS3Url(string $filename, string $dir = null)
    {
        $path = (empty($dir) ? '' : $dir . (substr($dir, -1) == '/' ?: '/')) . $filename;
        $url = $this->client->getObjectUrl($this->s3Bucket, $path);

        return $url;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client): void
    {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }
}
