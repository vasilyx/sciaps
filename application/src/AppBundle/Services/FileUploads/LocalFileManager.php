<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 22/09/2018
 * Time: 15:09
 */

namespace AppBundle\Services\FileUploads;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class LocalFileManager extends AbstractFileManager
{
    public function __construct(Container $container)
    {
        parent::__construct($container, $container->getParameter('csv_path'));
    }

    public function save(string $filename, $data, string $dir = null): bool
    {
        $path = (empty($dir)? '' : (substr($dir, -1) == '/' ?: '/')) . $filename;

        if (stripos($data,'ftp://') !== false) {
            return file_put_contents($path, fopen($data, 'r')) !== false;
        }

        return file_put_contents($path, $data) !== false;
    }

    public function get(string $filename, string $dir = null)
    {
        $path = (empty($dir) ? '' : (substr($dir, -1) == '/' ?: '/')) . $filename;

        if (!file_exists($path) || !is_readable($path)) {
            return false;
        }

        return file($path);
    }

    function isURLExists($url)
    {
        $headers = get_headers($url);

        return stripos($headers[0],"200 OK") ? true : false;
    }

    public function getByUrl(string $filename, string $dir = null)
    {
        $path = (empty($dir) ? '' : (substr($dir, -1) == '/' ?: '/')) . $filename;

        if (!$this->isURLExists($path)) {
            return false;
        }

        return file($path);
    }

    /**
     * @param string $filename
     * @param string|null $dir
     * @return bool
     */
    public function delete(string $filename, string $dir = null): bool
    {
        $path = (empty($dir) ? '' : (substr($dir, -1) == '/' ?: '/')) . $filename;

        return unlink($path);
    }

    public function openStream(string $filename, string $dir = null)
    {
        $path = (empty($dir) ? '' : (substr($dir, -1) == '/' ?: '/')) . $filename;

        if (!file_exists($path) || !is_readable($path)) {
            return false;
        }

        if (($stream = fopen($path, "r")) !== FALSE) {
            $this->setStream($stream);
            return $stream;
        }

        return false;
    }

    public function closeStream(): bool
    {
        return fclose($this->getStream());
    }
}
