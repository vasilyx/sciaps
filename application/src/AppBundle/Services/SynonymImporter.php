<?php

namespace AppBundle\Services;

use AppBundle\Entity\Ingredient;
use AppBundle\Entity\IngredientSynonym;
use AppBundle\Entity\Synonym;
use AppBundle\Repository\Elastic\SynonymRepository;
use AppBundle\Repository\IngredientRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\ElasticaBundle\Manager\RepositoryManagerInterface;

class SynonymImporter
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var IngredientRepository */
    private $ingredientRepository;

    /* @var SynonymRepository */
    private $elasticSynonymRepository;

    /** @var RepositoryManagerInterface */
    private $elasticaManager;

    /**
     * ProductImporter constructor.
     * @param EntityManagerInterface $em
     * @param RepositoryManagerInterface $elasticaManager
     */
    public function __construct(EntityManagerInterface $em, RepositoryManagerInterface $elasticaManager)
    {
        $this->em = $em;
        $this->elasticaManager = $elasticaManager;

        $this->ingredientRepository = $this->em->getRepository(Ingredient::class);
        /* @var SynonymRepository */
        $this->elasticSynonymRepository = $this->elasticaManager->getRepository( Synonym::class);
    }

    /**
     * @return int
     */
    public function matchIngredient($output)
    {
        $ingredients =  $this->ingredientRepository->findAll();

        $addedCount = 0;

        /** @var Ingredient $ingredient */
        foreach ($ingredients as $ingredient)
        {
            $added = false;

            if ($ingredient->getCommonName() && $cids = $this->elasticSynonymRepository>exists($ingredient->getCommonName(), 'name')) {
                $added = $this->addIngredientSynonym($ingredient, $cids);
            }

            if (!$added && $ingredient->getInciName() && $cids = $this->elasticSynonymRepository>exists($ingredient->getInciName(), 'name')) {
                $added = $this->addIngredientSynonym($ingredient, $cids);
            }

            if (!$added && $ingredient->getInnName() && $cids = $this->elasticSynonymRepository>exists($ingredient->getInnName(), 'name')) {
                $added = $this->addIngredientSynonym($ingredient, $cids);
            }

            if ($added) {
                $addedCount++;

                if ($addedCount % 1000 ) {
                    $this->em->flush();
                    $output->writeln('Added ' . $addedCount);
                }
            }
        }

        $this->em->flush();

        return $addedCount;
    }


    /**
     * @param Ingredient $ingredient
     * @param $synonyms
     * @param bool $flush
     * @return bool
     */
    protected function addIngredientSynonym(Ingredient $ingredient, $synonyms, $flush = false)
    {
        if (!$synonyms && $synonyms[0]) return false;

        $ingredientSynonym = new IngredientSynonym();
        $ingredientSynonym->setCid($synonyms[0]->getCid());
        $ingredientSynonym->setIngredient($ingredient);

        $this->em->persist($ingredientSynonym);

        if ($flush) {
            $this->em->flush();
        }

        return true;
    }
}
