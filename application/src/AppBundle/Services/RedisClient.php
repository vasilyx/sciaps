<?php

namespace AppBundle\Services;

use Doctrine\Common\Cache\PredisCache;
use Predis\Client as PredisClient;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;


class RedisClient
{
    /**
     * Predis Client intitialised
     *
     * @var PredisClient
     */
    private $client;

    /** @var bool $isCluster Is this a cluster? */
    private $isCluster = true;

    public function __construct(Container $container)
    {
        $this->isCluster = $container->getParameter('redis_cluster_enabled');

        $args = [];
        if ($this->isCluster) {
            $args['cluster'] = 'redis';
            $parameters = [
                [
                    'scheme' => $container->getParameter('redis_scheme'),
                    'host' => $container->getParameter('redis_host'),
                    'port' => $container->getParameter('redis_port')
                ]
            ];
        } else {
            $parameters = [
                'scheme' => $container->getParameter('redis_scheme'),
                'host' => $container->getParameter('redis_host'),
                'port' => $container->getParameter('redis_port'),
            ];

        }

        $prefix = $container->getParameter('redis_prefix');

        if (!empty($prefix)) $args['prefix'] = $prefix;

        $this->client = new PredisClient($parameters, $args);
    }

    public function getRedisClient()
    {
        return $this->client;
    }

    public function set($key, $value, $second = 3600)
    {
        return $this->client->setex($key, $second, $value);
    }

    public function get($key)
    {
        if ($this->client->exists($key)) {
            return $this->client->get($key);
        }

        return false;
    }

    public function unset($key)
    {
        if ($this->client->del($key)) return true;

        return false;
    }
}