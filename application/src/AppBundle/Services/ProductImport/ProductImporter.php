<?php

namespace AppBundle\Services\ProductImport;

use AppBundle\Entity\Barcode;
use AppBundle\Entity\Brand;
use AppBundle\Entity\IngestionReport;
use AppBundle\Entity\IngredientLegend;
use AppBundle\Entity\IngredientUnfiltered;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductProvider;
use AppBundle\Entity\ProductsIngredients;
use AppBundle\Entity\RegExp;
use AppBundle\Managers\BarcodeManager;
use AppBundle\Managers\BrandManager;
use AppBundle\Managers\IngredientLegendManager;
use AppBundle\Managers\IngredientUnfilteredManager;
use AppBundle\Managers\ProductProviderManager;
use AppBundle\Services\ListenerService;
use Doctrine\ORM\EntityManagerInterface;

class ProductImporter
{
    /* @var \AppBundle\Services\ProductImport\AbstractDataReader $reader Method to get data into the importer */
    protected $reader;

    /* @var IngredientParser $ingredientParser Parsing object to use */
    protected $ingredientParser;

    /* @var EntityManagerInterface */
    private $em;

    /* @var BarcodeManager */
    private $barcodeManager;

    /* @var BrandManager */
    private $brandManager;

    /* @var ProductProviderManager */
    private $productProviderManager;

    /**
     * @var array $dataMapping Array of mapping between external data and fields for database. Should be in the form
     * $dataMapping = [
     *      'dbField' => 'csvColumn',
     *      ...
     * ]
     */
    protected $dataMapping;

    /* @var array $requiredDataFields Required fields to be stored against the database. */
    protected $requiredDataFields = [
        'barcode'
    ];

    /* @var array $allowedDataFields Permitted fields to be stored against the database. */
    protected $allowedDataFields = [
        'barcode',
        'remote_id',
        'name',
        'manufacturer',
        'brand',
        'description',
        'ingredients'
    ];

    /**
     * @var IngredientUnfilteredManager
     */
    private $ingredientUnfilteredManager;

    /**
     * @var IngredientLegendManager
     */
    private $ingredientLegendManager;
    /**
     * @var ListenerService
     */
    private $listenerService;

    /**
     * ProductImporter constructor.
     * @param EntityManagerInterface $em
     * @param DataReaderInterface $reader
     * @param IngredientParser $ingredientParser
     * @param IngredientUnfilteredManager $ingredientUnfilteredManager
     * @param IngredientLegendManager $ingredientLegendManager
     * @param ProductProviderManager $productProviderManager
     * @param BarcodeManager $barcodeManager
     * @param BrandManager $brandManager
     * @param ListenerService $listenerService
     */
    public function __construct(EntityManagerInterface $em,
                                DataReaderInterface $reader,
                                IngredientParser $ingredientParser,
                                IngredientUnfilteredManager $ingredientUnfilteredManager,
                                IngredientLegendManager $ingredientLegendManager,
                                ProductProviderManager $productProviderManager,
                                BarcodeManager $barcodeManager,
                                BrandManager $brandManager,
                                ListenerService $listenerService)
    {
        $this->ingredientParser = $ingredientParser;
        $this->em = $em;
        $this->barcodeManager = $barcodeManager;
        $this->brandManager = $brandManager;
        $this->reader = $reader;
        $this->ingredientUnfilteredManager = $ingredientUnfilteredManager;
        $this->ingredientLegendManager = $ingredientLegendManager;
        $this->productProviderManager = $productProviderManager;
        $this->listenerService = $listenerService;
    }

    public function setIngredientRegex(array $config): ?self
    {
        if (empty($config)) return null;

        $this->ingredientParser->setRegex($config);

        return $this;
    }

    /**
     * Check the mapping data is compatible with CSV column headings.
     * A valid data map requires the following:
     *     1. There can be required fields which must be mapped to a CSV column
     *     2. Only registered fields can be used to map CSV columns
     *     3. A database field, if included in the data map, must be mapped to a CSV column
     *
     * @param array $columnHeadings
     * @return array
     */
    public function validateMappingData(array $columnHeadings): array
    {
        if (empty($columnHeadings)) {
            $errors[] = 'CSV column headings must be provided';
            return ['errors' => $errors];
        } elseif (count(array_filter(array_keys($columnHeadings), 'is_string')) > 0) {
            $errors[] = 'CSV column headings should not be an associative array';
            return ['errors' => $errors];
        }

        // Get the dataMap
        $dataMap = $this->getDataMapping();
        $errors = [];

        // If there is no mapping data then return error
        if (empty($dataMap)) {
            $errors[] = 'The data map cannot be empty';
            return ['errors' => $errors];
        }

        /**
         * Check that the mapping is allowed by the CSV file as well as the database fields
         * ['name' => 'Product Name', 'brand' => 'CSV Column for Brand']
         */
        // Hashed array is much faster with isset than normal array and in_array
        $allowedFieldsHash = array_fill_keys($this->allowedDataFields, true);
        $csvColumnsHash = array_fill_keys($columnHeadings, true);

        // Count does not have to be the same
        //if (count($csvColumnsHash) !== count($dataMap)) return false;

        // Required fields must exist
        foreach ($this->requiredDataFields as $required)
            if (!isset($dataMap[$required])) {
                $errors[] = sprintf('The "%s" field is required.', $required);
            } elseif (empty($dataMap[$required])) {
                $errors[] = sprintf('The "%s" field must be mapped to a CSV column', $required);
            }

        foreach ($dataMap as $dbKey => $dataKey) {
            // Database field must be allowed
            if (!isset($allowedFieldsHash[$dbKey])) {
                $errors[] = sprintf('The "%s" field is not an allowed database field.', $dbKey);
            }
            if (!empty($dataKey)) {
                // CSV column key must exist in CSV data
                if (!isset($csvColumnsHash[$dataKey])) {
                    $errors[] = sprintf('The "%s" field does not exist in the provided CSV file', $dataKey);
                }
            } else {
                // DB field must be mapped to the csv
                $errors[] = sprintf('The "%s" must be mapped to a database field', $dataKey);
            }
        }

        if (count($errors) >= 1) return ['errors' => $errors];

        return $dataMap;
    }

    /**
     * This function maps all allowable fields to an array for use as a data map.
     *
     * If it is provided with original mapping data, it will ensure conformity to the standard.
     * This is probably not perfect, is it will coerce an invalid data map to a valid map (if possible) instead of
     * throwing an error
     *
     * @param array|null $originalMappingData
     * @return array|null
     * @throws \Exception
     */
    public function initialiseMappingData(array $originalMappingData = null): ?array
    {
        $mappedHeaders = [];

        if (!empty($this->allowedDataFields)) {
            foreach ($this->allowedDataFields as $allowedDbField) {
                if (!empty($originalMappingData)) {
                    foreach ($originalMappingData as $dbField => $dataKey) {
                        if ($allowedDbField == $dbField) {
                            $mappedHeaders[$allowedDbField] = $dataKey;
                        }
                    }
                } else {
                    $mappedHeaders[$allowedDbField] = $allowedDbField;
                }
            }
        } else {
            throw new \Exception('Allowed data fields have not been set');
        }

        return empty($mappedHeaders) ? null : $mappedHeaders;
    }

    /**
     * Reset data mapping to ensure new maps can be used
     */
    public function resetDataMapping(): self
    {
        $this->dataMapping = null;

        return $this;
    }

    public function getDataMapping(): ?array
    {
        return $this->dataMapping;
    }

    /**
     * @param array|null $dataMapping
     * @return ProductImporter
     */
    public function setDataMapping(?array $dataMapping): self
    {
        if (empty($dataMapping)) {
            $this->dataMapping = null;
        } else {
            foreach ($dataMapping as $dbKey => $dataKey) {
                // Remove mapping that doesn't have an associated csv column heading
                if (empty($dataKey)) unset($dataMapping[$dbKey]);
            }

            $this->dataMapping = $dataMapping;
        }

        return $this;
    }

    public function getSampleData(int $count = 3): array
    {
        // Validate mapping data
        $headerColumns = $this->reader->getColumnHeaders();
        $dataMap = $this->validateMappingData($headerColumns);

        // Return if there are errors
        if (isset($dataMap['errors'])) {
            return ['errors' => $dataMap['errors']];
        }

        $this->reader->rewind();

        // We want this to be totally dynamic. Therefore we need the data provided as well as the data mapping of which
        // columns link to which database fields.
        // Loop through each row in the data
        $sample = [];
        $i = 0;
        while (false != ($data = $this->reader->getNextRow())) {
            if (count($data) !== count($headerColumns)) continue;
            $dataRow = array_combine($headerColumns, $data);
            foreach ($dataMap as $dbKey => $dataKey) {
                // The database field has a corresponding column in the csv and the data has the column.
                if (isset($dataRow[$dataKey])) $sample[$i][$dataKey] = trim($dataRow[$dataKey]);
            }
            $i++;
            if ($i >= $count) break;
        }

        return $sample;
    }

    /*    public function processIngredients()
        {
            $importedData = $this->data->getImportedData();

            if (!empty($importedData)) {
                foreach ($importedData as $row => $data) {
                    if (empty($data['raw_ingredients'])) continue;
                    $importedData[$row]['parsedIngredients'] = $this->ingredientParser->getParsedIngredientsList($data['raw_ingredients']);
                }

                $this->importedData = $importedData;
            }
        }*/

    /**
     * @param IngestionReport $ingestionReport
     * @return null
     */
    public function setIngredientRegexByIngestion(IngestionReport $ingestionReport)
    {
        $config = [];

        foreach ($ingestionReport->getRegexp() as $regexp) {

            /* @var RegExp $regexp */
            if ($regexp->getRegexType() == RegExp::TYPE_LEGEND_PART) {
                $config['legendPartMatch'] = $regexp->getRegex();
            }

            if ($regexp->getRegexType() == RegExp::TYPE_INGREDIENT_PART) {
                $config['itemMatch'] = $regexp->getRegex();
            }
        }

        $this->ingredientParser->setRegex($config);

        return null;
    }

    /**
     * Import data to database takes provided data and imports into database. It then provides a report.
     *
     * @param ProductProvider $productProvider
     * @param $ingestionReport
     * @param bool $overwriteProducts
     * @param bool $analyseOnly
     * @return array
     */
    public function importToDb(ProductProvider $productProvider, IngestionReport $ingestionReport, $overwriteProducts = false, bool $analyseOnly = false): array
    {
        ini_set('max_execution_time', 0);

        $startMem = memory_get_peak_usage();
        $startTime = microtime(true);
        $complexity = 0;

        // Update to include switching off Elastica listener
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->listenerService->disableListener('FOS\ElasticaBundle\Doctrine\Listener');

        // Validate mapping data
        $headerColumns = $this->reader->getColumnHeaders();
        $dataMap = $this->validateMappingData($headerColumns);
        $complexityCount = 0;
        $complexity = 0;

        // Return if there are errors
        if (isset($dataMap['errors'])) {
            return ['errors' => $dataMap['errors']];
        }

        $this->setIngredientRegexByIngestion($ingestionReport);

        $rowEmpty = $colEmpty = $dataEmpty = 0;

        $report = [
            'rows' => [
                // Total rows in file
                'total' => 0,
                // Rows processed and processed (or ignored)
                'processed' => 0,
                // Rows skipped as csv error
                'ignored' => 0,
                'errors' => [
                    'incorrectFieldCount' => 0,
                    'missingBarcode' => 0
                ]
            ],
            'products' => [
                // Total products (with a barcode)
                'total' => 0,
                // Product data, already exists and not overwriting - so ignore.
                'ignored' => 0,
                'unique' => 0,
                // Product data, already exists and overwriting - so update
                'updated' => 0,
                // Product data, doesn't exist, insert
                'inserted' => 0,
                'errors' => []
            ],
            'fields' => [
                'barcode' => []
            ],
            'debug' => [
                'memory' => [
                    'start' => $startMem,
                    'end' => 0,
                    'consumption' => 0
                ],
                'time' => [
                    'start' => $startTime,
                    'end' => 0,
                    'taken' => 0
                ]
            ]
        ];

        $fields = [
            'missing' => 0,
            'unique' => 0,
            'duplicate' => 0,
            'valid' => 0,
            'invalid' => 0
        ];
        $report['fields']['barcode'] = $fields;

        // Make sure we are at the beginning of the file
        $this->reader->rewind();

        // We want this to be totally dynamic. Therefore we need the data provided as well as the data mapping of which
        // columns link to which database fields.
        // Loop through each row in the data

        $hashmap = [];
        $count = 1;
        // Get all barcodes in the system as [barcode => id, ...]
        $barcodeMap = $this->barcodeManager->getBarcodesHashmap();
        $brandMap = $this->brandManager->getBrandsHashmap();
        $ingredientUnfilteredMap = $this->ingredientUnfilteredManager->getIngredientUnfilteredHashmap();
        $ingredientLegendMap = $this->ingredientLegendManager->getIngredientLegendHashmap();

        while (false != ($data = $this->reader->getNextRow())) {
            $report['rows']['total']++;
            // Check the length of the row is correct
            if (count($data) !== count($headerColumns)) {
                $report['rows']['ignored']++;
                $report['rows']['errors']['incorrectFieldCount']++;
                continue;
            }

            $report['rows']['processed']++;
            // Get each row as associative array with the column headings as keys
            $dataRow = array_combine($headerColumns, $data);

            // Get the product by barcode
            $barcodeNumber = trim($dataRow[$dataMap['barcode']]);
            $barcodeNumberHash = md5($barcodeNumber);
            if (!empty($barcodeNumber)) {
                $report['products']['total']++;
                // Barcode hasn't been imported and it doesn't exist in the DB
                if (!isset($barcodeMap[$barcodeNumberHash])) {
                    $barcode = new Barcode($barcodeNumber);
                    // Won't have an ID until flushed
                    $barcodeMap[$barcodeNumberHash] = $barcode;
//                    $this->em->persist($barcode);
                } else {
                    // Barcode map is set and barcode exists in the map
                    $barcode = $barcodeMap[$barcodeNumberHash];
                    if (!($barcode instanceof Barcode)) {
                        // If barcode isnt a Barcode then it's just an ID
                        $barcode = $this->barcodeManager->getBarcode($barcodeNumber);
                        $barcodeMap[$barcodeNumberHash] = $barcode;
                    } else {
                        $report['products']['ignored']++;
                        $report['fields']['barcode']['duplicate']++;
                        continue;
                    }
                }

                if ($barcode && $barcode->isValid()) {
                    $report['fields']['barcode']['valid']++;
                } else {
                    $report['products']['ignored']++;
                    $report['fields']['barcode']['invalid']++;
                    continue;
                }

                $product = $this->em->getRepository(Product::class)->findOneBy([
                    'barcode' => $barcode,
                    'productProvider' => $productProvider
                ]);
            } else {
                // Barcode doesn't exist (or is empty) so skip row
                $report['rows']['ignored']++;
                $report['rows']['errors']['missingBarcode']++;
                continue;
            }

            if ($product) {
                if ($overwriteProducts) {
                    $report['products']['updated']++;
                } else {
                    // We aren't overwriting the product so let's just go to next
                    $report['products']['ignored']++;
                    continue;
                }
            } else {
                $product = new Product();
                $product->setBarcode($barcode);
                $product->setProductProvider($productProvider);
                $report['products']['inserted']++;
            }

            $colEmpty = 0;
            foreach ($dataMap as $dbKey => $dataKey) {
                if (!isset($report['fields'][$dbKey]))
                    $report['fields'][$dbKey] = $fields;

                if (!isset($hashmap[$dbKey]))
                    $hashmap[$dbKey] = [];

                // The database field has a corresponding column in the csv and the data has the column.
                if (isset($dataRow[$dataKey])) {
                    $value = trim($dataRow[$dataKey]);
                    // Watch out! MD5 is case sensitive. 'CASE' will give a different MD5 to 'case'
                    $hashValue = md5(strtolower($value));

                    if (empty($value)) {
                        $report['fields'][$dbKey]['missing']++;
                        $colEmpty++;
                        $dataEmpty++;
                    }

                    if (!isset($hashmap[$dbKey][md5(strtolower($value))])) {
                        $hashmap[$dbKey][$hashValue] = true;
                        $report['fields'][$dbKey]['unique']++;
                    } else {
                        $report['fields'][$dbKey]['duplicate']++;
                    }

                    // These are manually configured until there is mapping between key and entity field
                    switch ($dbKey) :
                        case 'name':
                            $product->setName($value);
                            break;
                        case 'description':
                            $product->setDescription($value);
                            break;
                        case 'remote_id':
                            $product->setRemoteId($value);
                            break;
                        case 'brand':
                            // Brand hasn't been imported and it doesn't exist in the DB
                            if (!isset($brandMap[$hashValue])) {
                                $brand = new Brand();
                                $brand->setName($value);
                                // Won't have an ID until flushed
                                $brandMap[$hashValue] = $brand;
                                //$this->em->persist($brand);
                            } else {
                                // Brand map is set and brand exists in the map
                                $brand = $brandMap[$hashValue];
                                if (!($brand instanceof Brand)) {
                                    // If brand isn't a Brand then it's just an ID
                                    $brand = $this->brandManager->getBrandById($brand);
                                    $brandMap[$hashValue] = $brand;
                                }
                            }

                            if ($brand) $product->setBrand($brand);
                            break;
                        case 'ingredients':
                            if (!empty($value) && !$analyseOnly) {
                                $product->setIngredients($value);
                                $this->importIngredients($product, $ingredientUnfilteredMap, $ingredientLegendMap);
                                $complexityCount++;
                                $complexity += $this->ingredientParser->getIngredientComplexity($ingredientUnfilteredMap);
                            }

                            break;
                    endswitch;
                } else {
                    $report['fields'][$dbKey]['missing']++;
                }
            }
            if ($colEmpty === count($dataMap)) {
                $report['rows']['ignored']++;
                $rowEmpty++;
                continue;
            }

            if (!$analyseOnly) {
                $this->em->persist($product);
                if (!$product->getId() && $product->getProductProvider()->getId() != ProductProvider::PRODUCT_PROVIDER_DEFAULT_ID) {
                    $additionalProduct = clone $product;
                    $additionalProduct->setProductProvider($this->productProviderManager->getDefaultProductProvider());
                    $additionalProduct->setRemoteId(null);
                    $this->em->persist($additionalProduct);
                }

                if ($count % 500 === 0) {
                    $this->em->flush();
                    $this->em->clear(Barcode::class);
                    $this->em->clear(Brand::class);
                    $this->em->clear(Product::class);
                    $this->em->clear(ProductsIngredients::class);
                    $this->em->clear(IngredientUnfiltered::class);
                    $this->em->clear(IngredientLegend::class);
                    $barcodeMap = $this->barcodeManager->getBarcodesHashmap();
                    $brandMap = $this->brandManager->getBrandsHashmap();
                    $ingredientUnfilteredMap = $this->ingredientUnfilteredManager->getIngredientUnfilteredHashmap();
//                    print_r($ingredientUnfilteredMap);
                    $ingredientLegendMap = $this->ingredientLegendManager->getIngredientLegendHashmap();
                }
            }
//            $barcode = null;
//            $brand = null;
//            $product = null;
//            $additionalProduct = null;
            $count++;
        }

        if ($complexityCount) {
            $ingestionReport->setComplexity($complexity/$complexityCount);
        }

        $this->em->flush();
        $this->em->clear(Barcode::class);
        $this->em->clear(Brand::class);
        $this->em->clear(Product::class);
        $this->em->clear(ProductsIngredients::class);
        $this->em->clear(IngredientUnfiltered::class);
        $this->em->clear(IngredientLegend::class);

        $barcodeMap = null;
        $brandMap = null;
        $ingredientUnfilteredMap = null;
        $ingredientLegendMap = null;

        $this->ingredientUnfilteredManager->assignToIngredients();

        $this->listenerService->enableListeners();

        $report['debug']['memory']['end'] = memory_get_peak_usage();
        $report['debug']['memory']['consumption'] = round(($report['debug']['memory']['end'] - $startMem) / 1048576, 3);

        $report['debug']['time']['end'] = microtime(true);
        $report['debug']['time']['taken'] = round($report['debug']['time']['end'] - $report['debug']['time']['start'], 3);

        return $report;
    }

    /**
     * @param DataReaderInterface $reader
     * @return ProductImporter
     */
    public function setReader(DataReaderInterface $reader): self
    {
        $this->reader = $reader;
        return $this;
    }

    /**
     * Import a string of ingredients into the database
     *
     * @param Product $product
     * @param array $ingredientUnfilteredMap
     * @param array $ingredientLegendMap
     * @return null
     */
    public function importIngredients(Product $product, array &$ingredientUnfilteredMap = [], array &$ingredientLegendMap = [])
    {
        $ingredientString = $product->getIngredients();

        if (($ingredients = $this->ingredientParser->getParsedIngredientsList($ingredientString)) !== null) {

            foreach ($ingredients as $k => $ingredient) {
                // Limit on database field length. There is no situation that an ingredient name should be bigger than
                // this but it could be if the parser gets it wrong, or the ingredients are provided incorrectly.
                if (strlen($ingredient['name']) > 255) continue;

                $nameHash = md5(strtolower($ingredient['name']));
                // Ingredient hasn't been imported and it doesn't exist in the DB
                if (!isset($ingredientUnfilteredMap[$nameHash])) {
                    $ingredientUnfiltered = new IngredientUnfiltered();
                    $ingredientUnfiltered->setName($ingredient['name']);
                    // Won't have an ID until flushed
                    $ingredientUnfilteredMap[$nameHash] = $ingredientUnfiltered;
//                    $this->em->persist($ingredientUnfiltered);
                } else {
                    // Ingredient map is set and ingredient exists in the map
                    $ingredientUnfiltered = $ingredientUnfilteredMap[$nameHash];
                    if (!($ingredientUnfiltered instanceof IngredientUnfiltered)) {
                        // If ingredient isn't a Ingredient then it's just an ID
                        $ingredientUnfiltered = $this->ingredientUnfilteredManager->getIngredientUnfilteredByName($ingredient['name']);
                        $ingredientUnfilteredMap[$nameHash] = $ingredientUnfiltered;
                    }
                }

                if (!$ingredientUnfiltered) continue;

                // Limit on database field length. There is no situation that an ingredient legend should be bigger than
                // this but it could be if the parser gets it wrong, or the legends are provided incorrectly.
                if (!empty($ingredient['legend']) && strlen($ingredient['legend']) <= 255) {
                    $legendHash = md5(strtolower($ingredient['legend']));
                    // Legend hasn't been imported and it doesn't exist in the DB
                    if (!isset($ingredientLegendMap[$legendHash])) {
                        $ingredientLegend = new IngredientLegend();
                        $ingredientLegend->setName($ingredient['legend']);

                        // Won't have an ID until flushed
                        $ingredientLegendMap[$legendHash] = $ingredientLegend;
                        $this->em->persist($ingredientLegend);
                    } else {
                        // Legend map is set and legend exists in the map
                        $ingredientLegend = $ingredientLegendMap[$legendHash];
                        if (!($ingredientLegend instanceof IngredientLegend)) {
                            // If legend isn't a Legend then it's just an ID
                            $ingredientLegend = $this->ingredientLegendManager->getIngredientLegendByName($ingredient['legend']);
                            $ingredientLegendMap[$legendHash] = $ingredientLegend;
                        }
                    }
                    if (!$ingredientLegend) $ingredientLegend = null;
                } else {
                    $ingredientLegend = null;
                }

                $productIngredient = new ProductsIngredients();
                $productIngredient
                    ->setIngredientOrder($k)
                    ->setProduct($product)
                    ->setIngredientUnfiltered($ingredientUnfiltered)
                    ->setIngredientLegend($ingredientLegend);

                $this->em->persist($productIngredient);
                $ingredientUnfiltered = null;
                $ingredientLegend = null;
                $productIngredient = null;
            }
        }

        $ingredients = null;
        return null;
    }
}
