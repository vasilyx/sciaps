<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 01/10/2018
 * Time: 10:37
 */

namespace AppBundle\Services\ProductImport;

class CsvDataReader extends AbstractDataReader
{
    /**
     * @var string
     */
    private $delimiter;
    /**
     * @var string
     */
    private $enclosure;
    /**
     * @var string
     */
    private $escape;

    /**
     * CsvData constructor.
     * @param \SplFileObject $file
     * @param string $delimiter
     * @param string $enclosure
     * @param string $escape
     */
    public function __construct(\SplFileObject $file = null, $delimiter = ',', $enclosure = '"', $escape = '\\')
    {
        ini_set('auto_detect_line_endings', true);

        if ($file) {
            $this->source = $file;
            $this->source->setFlags(
                \SplFileObject::READ_CSV |
                \SplFileObject::READ_AHEAD |
                \SplFileObject::DROP_NEW_LINE |
                \SplFileObject::SKIP_EMPTY);

            $this->source->setCsvControl($delimiter, $enclosure, $escape);

            $this->setColumnHeaders($this->getHeaderRow());
        }

        $this->delimiter = $delimiter;
        $this->enclosure = $enclosure;
        $this->escape = $escape;
    }

    /**
     * @param \SplFileObject $source
     * @return CsvDataReader
     */
    public function setSource($source): self
    {
        $this->source = $source;

        $this->source->setFlags(
            \SplFileObject::READ_CSV |
            \SplFileObject::READ_AHEAD |
            \SplFileObject::DROP_NEW_LINE |
            \SplFileObject::SKIP_EMPTY);

        $this->source->setCsvControl($this->delimiter, $this->enclosure, $this->escape);

        $this->setColumnHeaders($this->getHeaderRow());

        return $this;
    }

    public function getNextRow(): ?array
    {
        return $this->source->fgetcsv($this->delimiter, $this->enclosure, $this->escape);
    }

    public function getHeaderRow()
    {
        $this->source->rewind();

        $headerRow = $this->source->current();
        return $headerRow;
    }

    public function setColumnHeaders(array $columnHeaders): self
    {
        $this->columnHeaders = array_map('trim', $columnHeaders);
        return $this;
    }

    public function getColumnHeaders(): ?array
    {
        return $this->columnHeaders;
    }

    public function rowCount(): int
    {
        if ($this->rowCount === null) {
            $start = $this->source->key();
            $this->source->seek(PHP_INT_MAX);
            $this->rowCount = $this->source->key();

            $this->source->seek($start);

        }
        return $this->rowCount;
    }

    /**
     * @inheritdoc
     */
    public function valid()
    {
        return $this->source->valid();
    }

    /**
     * @inheritdoc
     */
    public function seek($row)
    {
        $this->source->seek($row);
    }

    /**
     * @inheritdoc
     */
    public function next()
    {
        $this->source->next();
    }

    /**
     * @inheritdoc
     */
    public function rewind()
    {
        $this->source->rewind();
    }

    /**
     * @inheritdoc
     */
    public function current()
    {
        return $this->source->current();
    }

    /**
     * @inheritdoc
     */
    public function key()
    {
        return $this->source->key();
    }


    /*    protected $data;

        public function importFromHardrive(string $fileName, $parseHeader = true, $maxLines = 0)
        {
            $this->logger->debug("CSV import started");

            $stream = $this->fileManager->openStream($fileName);

            $data = $this->getCsvDataAsArray($stream, $parseHeader, $maxLines);

            $this->fileManager->closeStream();

            $this->logger->debug("CSV data loaded into memory");

            return $data;
        }

        public function load(string $fileName, $type = 'array')
        {
            $this->data = new \SplFileObject($fileName);

            if ($type == 'array') {
                $this->importedData = $this->getCsvDataAsArray();
            }
        }

        public function importFromStream($stream, $parseHeader = true, $maxLines = 0)
        {
    //        $this->logger->debug("CSV import started");
            $data = $this->getCsvDataAsArray($stream, $parseHeader, $maxLines);
    //        $this->logger->debug("CSV data loaded into memory");

            return $data;
        }

        /**
         * Returns array of data as provided in the CSV file
         *
         * @param $stream
         * @param bool $parseHeader
         * @param int $maxLines
         *
         * @return array
         */
 /*   public function getCsvDataAsArray($stream, bool $parseHeader, int $maxLines)
    {
        // If $maxLines is set to 0, then get all the data
        // So loop limit is ignored
        $lineCount = $maxLines > 0 ? 0 : -1;

        $header = null;

        $startMem = memory_get_peak_usage();
        $colCount = $rowEmpty = $dataEmpty = 0;
        $data = [];
        while ($lineCount < $maxLines && ($row = fgetcsv($stream, $this->maxLineLength, $this->delimiter)) !== FALSE) {
            if ($parseHeader) {
                if (!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            } else {
                $data[] = $row;
            }

            $colEmpty = 0;
            foreach ($row as $k => $v) {
                if (empty($v)) {
                    $colEmpty++;
                    $dataEmpty++;
                }
            }

            $colCount = count($row);
            if ($colEmpty === $colCount) $rowEmpty++;
            if ($maxLines > 0) $lineCount++;
        }

        $endMem = memory_get_peak_usage();
        $this->reports['rawData']['info']['stats']['summary']['totalRows'] = count($data);
        $this->reports['rawData']['info']['stats']['summary']['totalColumns'] = $colCount;
        $this->reports['rawData']['info']['stats']['summary']['emptyRows'] = $rowEmpty;
        $this->reports['rawData']['info']['stats']['summary']['emptyData'] = ($dataEmpty / (count($data) * $colCount)) * 100;
        $this->reports['rawData']['info']['stats']['summary']['memoryUsage'] = (round((($endMem - $startMem) / 1024), 2)) . " kb";

        return $data;
    }

    public function exportToCsv(string $fileLocation = 'default.csv')
    {
        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
        if (!($bytes = file_put_contents($fileLocation, $serializer->encode($this->getImportedData(), 'csv')))) {
            return false;
        }

        return true;
    }

    public function exportRowToCsv(string $fileLocation = 'default.csv')
    {
        $file = fopen($fileLocation, "w");
        $data = $this->getImportedData();

        fputcsv($file, ['ingredient', 'raw_ingredient', 'legend']);
        foreach ($data as $line) {
            if (empty($line['parsedIngredients'])) continue;
            foreach ($line['parsedIngredients'] as $k => $value) {
                $data = [
                    $value['name'],
                    $value['rawName'],
                    $value['legend']
                ];
                fputcsv($file, $data);
            }
        }

        fclose($file);
    }*/
}
