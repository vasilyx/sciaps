<?php

namespace AppBundle\Services\ProductImport;

/**
 * Created by PhpStorm.
 * User: jasonlinthwaite
 * Date: 05/07/2018
 * Time: 14:58
 */
class IngredientParser
{
    protected $rawIngredientText;
    protected $parsedIngredientList;

    /* @var array $standardPhrases List of standard phrases to be removed from the ingredient string, e.g. "INCI:" */
    protected $standardPhrases;

    /* @var array $allowedLegendSymbols Array of symbols parsed as a legend symbol in ingredient, e.g. Aqua*† */
    protected $allowedLegendSymbols;

    /* @var array $allowedLegendSymbolCount Array with minimum / maximum count of allowed symbol number e.g. [1, 4] */
    protected $allowedLegendSymbolCount;

    /* @var string $ingredientLegendMatch RegEx that will match the prefix part of the legend string */
    protected $ingredientLegendPrefixMatch;

    /* @var string $ingredientLegendMatch RegEx that will match the legend part of the ingredient string */
    protected $ingredientLegendMatch;

    /* @var string $legendPartMatch RegEx that will match each of the legends in the ingredient string */
    protected $legendPartMatch;

    /* @var string $itemMatch RegEx that will match each of the ingredients in the ingredient string */
    protected $itemMatch;

    /**
     * ParsingUtilities constructor.
     */
    public function __construct()
    {
        $this->resetRegex();
    }

    public function resetRegex()
    {
        $this->standardPhrases = [
            "INCI\:",
            "\(INCI\)\:",
            "Ingredients \(INCI\)\:",
            "Active Ingredients:",
            "Inactive",
            "For allergens\, See ingredients in bold",
            "Does not contain nuts",
            "All blended in a unique process",
            "Source:",
            "No parabens\.*"
        ];
        $this->allowedLegendSymbols = [
            "+",
            "*",
            "†"
        ];
        $this->allowedLegendSymbolCount = [1, 4];
        $this->ingredientLegendPrefixMatch = "([*|^|†]{1,4})";
        $this->ingredientLegendMatch = "([*|^†]{1,4})";
        $this->legendPartMatch = "[^,][\s]{0,3}[\(]{0,1}([\*|\^|†]{1,4}(?!\,|\(|\))[a-z|A-Z|0-9|\s|\,|\%|\-|\)|\:]{3,}?[\.|$])|(\(\d\)[^,|^\.|^\)][a-z|A-Z|0-9|\s|\,|\%|\-|\)|\:]{3,})";
        $this->itemMatch = "([a-z|A-Z|0-9|\s|,|\(|\)|\%|\.|\*|\/|\'|\[|\]|†|\^|\+|\-|\º|\&|\é|\á|\™]{3,}?)(?:(?:,(?![\d]|[^\(]*\))|$)|(?:\.(?!\d)))";

        return $this;
    }

    public function setRegex(array $config = [])
    {
        if (isset($config['standardPhrases'])) $this->standardPhrases = $config['standardPhrases'];
        if (isset($config['allowedLegendSymbols'])) $this->allowedLegendSymbols = $config['allowedLegendSymbols'];
        if (isset($config['allowedLegendSymbolCount'])) $this->allowedLegendSymbolCount = $config['allowedLegendSymbolCount'];
        if (isset($config['ingredientLegendPrefixMatch'])) $this->ingredientLegendPrefixMatch = $config['ingredientLegendPrefixMatch'];
        if (isset($config['ingredientLegendMatch'])) $this->ingredientLegendMatch = $config['ingredientLegendMatch'];
        if (isset($config['legendPartMatch'])) $this->legendPartMatch = $config['legendPartMatch'];
        if (isset($config['itemMatch'])) $this->itemMatch = $config['itemMatch'];
    }

    public function trimSpaceDotsAndCommas(string $string): string
    {
        if (empty($string)) return false;

        $string = ltrim($string, ',. ');
        $string = rtrim($string, ',. ');

        return $string;
    }

    /**
     * Ingredient strings can contain phrases that should be removed before parsing
     *
     * @param string $string Raw ingredient string
     *
     * @return string|bool
     */
    public function removePhrases(string $string): string
    {
        if (empty($string)) return $string;

        if (!empty($this->standardPhrases)) {
            foreach ($this->standardPhrases as $phrase) {
                $string = preg_replace("/" . $phrase . "/i", '', $string);
            }
        }

        return $string;
    }

    /**
     * Ingredient strings can contain newlines that should be removed before parsing
     *
     * @param string $string Raw ingredient string
     * @param string $replace What to replace new line characters with - default ","
     *
     * @return string
     */
    public function removeNewLines(string $string, $replace = ','): string
    {
        if (empty($string)) return false;

        return str_replace(["\r\n", "\n\r", "\r", "\n"], $replace, $string);
    }

    /**
     * Catchall function to prepare an ingredient string for processing
     *
     * @param string $string
     *
     * @return string|bool
     */
    public function cleanString(string $string): string
    {
        if (!($string = $this->removePhrases($string))) return false;
        if (!($string = $this->removeNewLines($string))) return false;

        return trim($string);
    }

    /**
     * Gets the legend part of an ingredient, e.g. Aqua*. *From natural sources. Returns '*'
     *
     * @param string $ingredient
     *
     * @return null|string
     */
    public function getLegendFromIngredient(string $ingredient): ?string
    {
        if (empty($ingredient)) return null;

        $legendMatches = [];

        preg_match("/" . $this->ingredientLegendMatch . "/i", $ingredient, $legendMatches);

        return empty($legendMatches) ? null : $legendMatches[0];
    }

    /**
     * Given a string of ingredients from a supplier, separate the ingredient part from the legend part.
     *
     * For example:
     * aqua, linalool, *citronum, **alcohol. *natural, **organic
     *
     * aqua, linalool, *citronum, **alcohol             ==> Ingredients
     * [ 0 => * From natural sources, ** Organic by-product]    ==> Legend
     *
     * @param $string
     *
     * @return array|null
     */
    public function splitIngredientsAndLegend(string $string): ?array
    {
        if (empty($string)) return null;

        preg_match_all("/" . $this->legendPartMatch . "/i", $string, $legendMatches);

        if (empty($legendMatches)) {
            // No legend found in this string
            $response = [$string, null];
        } else {
            // $legendMatches[0] is an array of full pattern matches
            foreach ($legendMatches[0] as &$match) {
                if (empty($match)) continue;
                // Try and remove the legend from the full string by removing each of the legend matches
                $string = preg_replace("/" . preg_quote($match) . "/i", '', $string);
                // Cleanup the legend text (e.g. * From natural sources
                $match = $this->trimSpaceDotsAndCommas($match);
            }

            $response = [
                $string,
                $legendMatches[0]
            ];
        }

        /**
         * [ingredientText, legendParts]
         */
        return $response;
    }

    public function getDelimiter($string)
    {
        $delimiter = ",";

        if (substr_count($string, ".") > substr_count($string, $delimiter)) {
            $delimiter = ".";
        }

        return $delimiter;
    }

    /**
     * Convert a string of ingredients into an array of ingredients
     *
     * @param string $ingredientString A string of ingredients e.g. "Water, Gold*, Sodium"
     *
     * @return array|null
     */
    public function explodeIngredients(string $ingredientString): ?array
    {
        if (empty($ingredientString)) return null;

        $ingredientMatches = [];

        // This is not as simple as splitting on commas due to ingredients being able to include commas and sometimes
        // the ingredient text doesnt even include commas!
        preg_match_all("/" . $this->itemMatch . "/i", $ingredientString, $ingredientMatches);

        if (empty($ingredientMatches)) return null;

        foreach ($ingredientMatches[0] as &$match) {
            // Clean the match
            $match = $this->trimSpaceDotsAndCommas($match);
        }

        return empty($ingredientMatches[0]) ? null : $ingredientMatches[0];
    }

    /**
     * Parse a string of ingredients provided from a third party and return the ingredients list (string) and the
     * legend if it exists as an array of matches.
     *
     * Aqua, gold*, platinum, leaf extract**, CI 20932*. * from natural sources, ** organic.
     *
     * Aqua, gold*, platinum, leaf extract**, CI 20932*.    => Ingredients text => returns string
     * * from natural sources, ** organic.                  => Ingredients legend => returns array
     *
     * @param string $rawIngredientText String of ingredients, e.g. "water, gold*, sodium. * From natural sources"
     * @return array|null
     */
    public function getParsedIngredientsList(string $rawIngredientText): ?array
    {
        if (empty($rawIngredientText)) return null;

        // Clean text to remove extraneous information (e.g. INCI:, "The ingredients are: "). Different providers
        // contain different info here)
        $cleanIngredientsText = $this->cleanString($rawIngredientText);

        // Separate legend from ingredients
        list($ingredientText, $legendParts) = $this->splitIngredientsAndLegend($cleanIngredientsText);

        // Explode ingredients text into an array
        $ingredientsList = $this->explodeIngredients($ingredientText);

        // Parse and clean the array
        $ingredientArray = $ingredientsList ? $this->parseIngredientList($ingredientsList, $legendParts) : null;

        return empty($ingredientArray) ? null : $ingredientArray;
    }

    /**
     * @param $string
     * @return int
     */
    public function getIngredientComplexity($string): int
    {
        $result = 0;
        $delimiter = $this->getDelimiter($string);

        // Contains Punctuation	Check to see if ingredients string contains punctuation. [ ] ( ) ! < > #	+1
        if ($this->stringContains(['[', ']' , '(', ')' , '!', '#' ,'<', '>'], $string)) {
            $result += 1;
        }

        //  Incorrect Delimiter	Checks to find out if the delimeter is a comma	+2
        if ($delimiter === ',') {
            $result += 2;
        }

        // No Delimiters	Checks to find out if there is no delimiter in the string	+3
        if ($this->stringContains($delimiter, $string)) {
            $result += 3;
        }

        // Active Ingredients	Checks to see if ingredients string contains "Active Ingredients"	+3
        if ($this->stringContains('Active Ingredients', $string)) {
            $result += 3;
        }

        // TODO
        // Incorrect Punctuation	Check to see if ingredients string contains incorrect punctuation. This will be if brackets are started but not completed.	+6
        // Duplicate Names	Checks to see if an ingredient string contains INCI names as well as common names. For example: "Aqua (Water)".	+2


        return $result;
    }

    /**
     * @param $needle
     * @param $haystack
     * @return bool
     */

    protected function stringContains($needle, $haystack)
    {
        if (is_array($needle)) {
            foreach ($needle as $item) {
                if (strpos($haystack, $item) !== false) return true;
            }

            return false;

        } else {
            return strpos($haystack, $needle) !== false;
        }
    }

    /**
     * Generate an array of ingredients in the form [ name => string, rawName => string, legend => string ]
     *
     * @param array $ingredientsList Array of unparsed ingredients.
     * @param array $legendParts Array of legend parts, e.g. "* From natural sources"
     *
     * @return array|null
     */
    public function parseIngredientList(array $ingredientsList, array $legendParts): ?array
    {
        if (empty($ingredientsList)) return null;

        $parsedIngredientList = [];
        $legendError = 0;

        foreach ($ingredientsList as $k => $ingredient) {
            // Remove whitespace, commas and full stops
            $ingredient = strtolower(str_replace(' ,.', '', trim($ingredient)));
            $ingredientItem = [
                'name' => $ingredient,
                'rawName' => $ingredient,
                'legend' => ''
            ];

            if ($legendParts) {
                if (($legendText = $this->getLegendFromIngredient($ingredient)) !== null) {
                    // e.g. *, or **
                    // This infers the index. e.g. if it is * then index must be 0 (i.e. 1st element)
                    // Very bad way of doing it. e.g. if there is ** as well as †† then this would be incorrect
                    $legendLength = strlen($legendText);

                    $ingredientItem['name'] = trim(str_replace($legendText, '', $ingredient));
                    if (isset($legendParts[$legendLength - 1])) {
                        $ingredientItem['legend'] = $legendParts[$legendLength - 1];
                    } else {
                        // Something has gone wrong in the parsing as legend length is not set, i.e. legend hasn't
                        // captured correct number of legends
                        $legendError++;
                        continue;
                    }
                };
            }
            $parsedIngredientList[] = $ingredientItem;
        }

        return empty($parsedIngredientList) ? null : $parsedIngredientList;
    }
}
