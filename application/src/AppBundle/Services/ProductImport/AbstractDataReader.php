<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 01/10/2018
 * Time: 10:37
 */

namespace AppBundle\Services\ProductImport;

abstract class AbstractDataReader implements DataReaderInterface, \SeekableIterator
{
    /* @var array $columnHeaders Non-associative array of headers for the provided data */
    protected $columnHeaders;

    /* @var integer $rowCount Total number of rows in the data provided */
    protected $rowCount;

    /* @var mixed $source Source of the data */
    protected $source;

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }
}
