<?php
/**
 * Created by PhpStorm.
 * User: jasonlinthwaite
 * Date: 09/07/2018
 * Time: 22:10
 */

namespace AppBundle\Services\ProductImport\Amazon;

use ApaiIO\ResponseTransformer\XmlToArray;
use AppBundle\Utils\ParsingUtilities;

use \ApaiIO\Configuration\GenericConfiguration;
use \ApaiIO\Operations\Search;
use ApaiIO\Operations\Lookup;
use \ApaiIO\ApaiIO;
//use AppBundle\Services\ProductImport\APIProductImporter;

class AmazonImporterUtilities// extends APIProductImporter
{
    private $conf;
    private $client;
    private $request;
    private $connection;

    /**
     * APIProductImporter constructor.
     */
    public function __construct()
    {
        $this->conf = new GenericConfiguration();
        $this->client = new \GuzzleHttp\Client();
        $this->request = new \ApaiIO\Request\GuzzleRequest($this->client);

/*        if (!isset($configuration['countryCode']) ||
            !isset($configuration['secretAssocAccessKey']) ||
            !isset($configuration['secretAssocSecretKey']) ||
            !isset($configuration['secretAssocTag'])) {
            return false;
        }

        $this->conf
            ->setCountry($configuration['countryCode'])
            ->setAccessKey($configuration['secretAssocAccessKey'])
            ->setSecretKey($configuration['secretAssocSecretKey'])
            ->setAssociateTag($configuration['secretAssocTag'])
            ->setRequest($this->request)
            ->setResponseTransformer(new XmlToArray());*/

        $this->connection = new ApaiIO($this->conf);
    }

    public function searchByProductName($product, $count = 10)
    {
        $search = new Search();

        $utils = new ParsingUtilities();
        $query = $utils->buildProductName($product);

        $search->setKeywords($query);
        $search->setSort("-price");
        $search->setCategory('Beauty');
        $search->setResponseGroup(['ItemAttributes']);

        $result = $this->connection->runOperation($search);

        if (is_array($result) && isset($result['Items']) && isset($result['Items']['Item'])) {
            $result['Items']['Item'] = array_slice($result['Items']['Item'], 0, $count);

            return $result['Items'];
        } else {
            return [];
        }
    }

    public function searchByBarcode($barcode, $type, $count = 10)
    {
        $lookup = new Lookup();

        switch ($type) {
            case 'EAN':
                $type = 'EAN';
                break;
            case 'UPC':
                $type = 'UPC';
                break;
            default:
                return false;
        }

        $lookup->setIdType($type);
        $lookup->setSearchIndex('Beauty');
        $lookup->setItemId($barcode);
        $lookup->setResponseGroup(['ItemAttributes']);

        $result = $this->connection->runOperation($lookup);

        if (is_array($result) && isset($result['Items']) && isset($result['Items']['Item'])) {
            $result['Items']['Item'] = array_slice($result['Items']['Item'], 0, $count);

            return $result['Items'];
        } else {
            return false;
        }
    }
}