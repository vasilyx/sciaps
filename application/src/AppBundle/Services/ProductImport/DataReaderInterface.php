<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 01/10/2018
 * Time: 10:37
 */

namespace AppBundle\Services\ProductImport;

interface DataReaderInterface
{
    public function setColumnHeaders(array $columnHeaders);
    public function getColumnHeaders(): ?array;

    public function rowCount(): int;
    public function getNextRow(): ?array;

    public function setSource($source);
    public function getSource();
}
