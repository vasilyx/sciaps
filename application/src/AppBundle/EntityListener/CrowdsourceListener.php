<?php
namespace AppBundle\EntityListener;

use AppBundle\Entity\LogEvent;
use AppBundle\Entity\CrowdsourcePending;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Mapping\PostPersist;
use Symfony\Component\DependencyInjection\Container;


class CrowdsourceListener
{
    private $container;
    private $em;
    private $logs = [];

    /**
     * CrowdsourceListener constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
    }


    public function preUpdate(CrowdsourcePending $entity, $event)
    {
        if ($event->hasChangedField('status')) {
            array_push($this->logs, $this->getLogOnStatusChange($entity));
        }
    }

    public function postUpdate($args)
    {
        foreach ($this->logs as $log){
            $this->em->persist($log);
        }

        $this->em->flush();
        $this->logs = [];
    }

    protected function getLogOnStatusChange(CrowdsourcePending $entity): ?LogEvent
    {
        $logEvent = new LogEvent();

        $logEvent->setActionId(LogEvent::ACTION_CHANGED_STATUS);
        $logEvent->setEntityId($entity->getId());
        $logEvent->setEntityTypeId(LogEvent::ENTITY_TYPE_CROWDSOURCE);
        $logEvent->setUser($this->getCurrentUser());
        $logEvent->setValue($entity->getStatus());
        $logEvent->setField('status');
        return $logEvent;
    }


    protected function getCurrentUser()
    {
        return $this->container->get('security.token_storage')->getToken()->getUser();
    }
}