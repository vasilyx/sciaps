<?php

namespace AppBundle\Command;

use AppBundle\Services\CosIngImporter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class CosIngImporterCommand extends ContainerAwareCommand
{
    const URL_FILE = "http://ec.europa.eu/growth/tools-databases/cosing/pdf/COSING_Ingredients-Fragrance%20Inventory_v2.csv";

    /**
     * @var CosIngImporter
     */
    private $cosIngImporter;

    private $em;

    public function __construct(CosIngImporter $cosIngImporter, EntityManagerInterface $em)
    {
        parent::__construct();

        $this->cosIngImporter = $cosIngImporter;
        $this->em = $em;
    }


    protected function configure()
    {
        $this
            ->setName('evrelab:importCosIng')
            ->setDescription('Command to import ingredient data from CosIng database')
            ->setHelp('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('CosIng data import process');
        $timeStart = microtime(true);
        $memStart = memory_get_peak_usage();
        $output->writeln('Start time: '. (new \DateTime())->format('H:i:s'));
        if ($this->cosIngImporter->import(self::URL_FILE)) {
            $output->writeln('File was parsed');
        }
        $timeEnd = microtime(true);
        $memEnd = memory_get_peak_usage();
        $output->writeln('End Time: ' . (new \DateTime())->format('H:i:s'));
        $output->writeln('Debug:');
        $output->writeln('Time Taken: ' . round(($timeEnd - $timeStart),2) . "s");
        $output->writeln('Mem Usage: ' . round(($memEnd - $memStart) / (1024*1024),3) . "Mb");
    }
}
