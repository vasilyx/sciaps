<?php

namespace AppBundle\Command;

use AppBundle\Services\SynonymImporter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


class SynonymMatchCommand extends ContainerAwareCommand
{
    /**
     * @var SynonymImporter
     */
    private $synonymImporter;

    private $em;

    public function __construct(SynonymImporter $synonymImporter, EntityManagerInterface $em)
    {
        parent::__construct();

        $this->synonymImporter = $synonymImporter;
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('evrelab:pubchem:matchWithIngredients')
            ->setDescription('Match all filtered ingredients with synonyms that don\'t currently have a matched CID')
            ->setHelp('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $addedCount = $this->synonymImporter->matchIngredient($output);

        $output->writeln('Added ' . $addedCount);

        $output->writeln('Fished synonym match.');
    }
}
