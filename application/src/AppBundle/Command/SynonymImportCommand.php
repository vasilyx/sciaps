<?php

namespace AppBundle\Command;

use AppBundle\Services\FileUploads\LocalFileManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


class SynonymImportCommand extends ContainerAwareCommand
{
    private $em;
    /**
     * @var LocalFileManager
     */
    private $localFileManager;

    /**
     * SynonymImportCommand constructor.
     * @param EntityManagerInterface $em
     * @param LocalFileManager $localFileManager
     */
    public function __construct(EntityManagerInterface $em, LocalFileManager $localFileManager)
    {
        parent::__construct();

        $this->em = $em;
        $this->localFileManager = $localFileManager;
    }

    protected function configure()
    {
        $this
            ->setName('evrelab:pubchem:import')
            ->setDescription('Bulk synonym import from PubChem. This should always be run as a separate process.')
            ->addArgument('f', InputArgument::OPTIONAL, 'File path', null)
            ->setHelp('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filePath = $input->getArgument('f');

        if (!$filePath) {
            $output->writeln('Downloading file from Pubchem... ftp://ftp.ncbi.nlm.nih.gov/pubchem/Compound/Extras/CID-Synonym-filtered.gz');
            $url = 'ftp://ftp.ncbi.nlm.nih.gov/pubchem/Compound/Extras/CID-Synonym-filtered.gz';
            // Download file from PubChem
            $downloadedFilePath = '/tmp/pubchem-synonym-filtered.gz';
            $file = $this->localFileManager->save($downloadedFilePath, $url);

            if (!$file) {
                $output->writeln('File download failed');
                return 1;
            }

            $output->writeln("File successfully downloaded to... $downloadedFilePath");

            // Need to unzip file
            $bufferSize = 1048576; // Read 1mb at a time
            $file = gzopen($downloadedFilePath, 'rb');
            $filePath = str_replace('.gz', '.csv', $downloadedFilePath);
            $output->writeln('Opening handle to downloaded file');
            $outputFile = fopen($filePath, 'wb');

            $output->writeln('Starting unzip');
            while (!gzeof($file)) {
                // Read buffer-size bytes
                // Both fwrite and gzread and binary-safe
                fwrite($outputFile, gzread($file, $bufferSize));
            }
            $output->writeln("Finished unzip to... $filePath");
            // Files are done, close files
            fclose($outputFile);
            gzclose($file);

            // Remove file after use
            $output->writeln("Deleting... $downloadedFilePath");
            $this->localFileManager->delete($downloadedFilePath);
            $output->writeln("$downloadedFilePath deleted");
        }

        $output->writeln('Synonym import start');

        $connection = $this->em->getConnection();
        $connection->getConfiguration()->setSQLLogger(null);

        $sql = 'INSERT INTO synonym (cid, `name`) VALUES ';
        $arrayToPush = [];

        $partSize = 100000;
        $sqlCount = 0;
        $inserted = 0;
        $time_start = microtime(true);

        $file = new \SplFileObject($filePath , 'r');

        while (!$file->eof()) {

            $array = explode("\t", $file->fgets());

            if (count($array) > 1) {
                if (strlen($array[1]) > 512) {
                    $output->writeln('Length: ' . strlen($array[1]) . ' String: ' . $array[1]);
                } else {
                    array_push($arrayToPush, '(' . $array[0] . ', ' . $connection->quote(trim($array[1])) . ')');
                }
                ++$sqlCount;
            }

            if ($sqlCount === $partSize) {
                $inserted += $connection->exec($sql . implode(',', $arrayToPush));
                $sqlCount = 0;

                unset($arrayToPush);
                $arrayToPush = [];

                $output->writeln('Inserted: ' . $inserted . '; Time: ' . round((microtime(true) - $time_start) . 's', 2));
            }
        }

        if (count($arrayToPush) > 0) {
            $inserted += $connection->exec($sql . implode(',', $arrayToPush));
        }

        $output->writeln("Synonyms imported into MySQL database");

        // Remove file after use
        $output->writeln("Deleting... $filePath");
        $this->localFileManager->delete($filePath);
        $output->writeln("$filePath deleted");

        $output->writeln('Finished! Inserted: ' . $inserted . '; Time: ' . round((microtime(true) - $time_start).'s', 2));
    }
}
