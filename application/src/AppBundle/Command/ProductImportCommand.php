<?php

namespace AppBundle\Command;

use AppBundle\Entity\IngestionReport;
use AppBundle\Managers\IngestionReportManager;
use AppBundle\Services\ProductImport\ProductImporter;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ProductImportCommand extends ContainerAwareCommand
{
    private $em;
    /**
     * @var ProductImporter
     */
    private $productImporter;
    /**
     * @var IngestionReportManager
     */
    private $ingestionReportManager;

    /**
     * ProductImportCommand constructor.
     * @param EntityManagerInterface $em
     * @param ProductImporter $productImporter
     * @param IngestionReportManager $ingestionReportManager
     */
    public function __construct(EntityManagerInterface $em, ProductImporter $productImporter, IngestionReportManager $ingestionReportManager)
    {
        parent::__construct();

        $this->em = $em;
        $this->productImporter = $productImporter;
        $this->ingestionReportManager = $ingestionReportManager;
    }

    protected function configure()
    {
        $this
            ->setName('evrelab:importProducts')
            ->addArgument(
                'report',
                InputArgument::REQUIRED,
                'Ingestion Report ID for who the report data belongs to'
            )
            ->addOption(
                'batch',
                'b',
                InputOption::VALUE_OPTIONAL,
                'Batch size to use for import process',
                500
                )
            ->setDescription('Command to import products')
            ->setHelp('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Starting product import process');

        /** @var IngestionReport $report */
        $report = $this->ingestionReportManager->getById($this->getArgument('report'));

        if (!$report) {
            $output->writeln('Report does not exist');
            return false;
        }

        $productProvider = $report->getProductProvider();
        $user = $report->getUser();

        if (!$productProvider) {
            $output->writeln('Provider provider not found');
            return false;
        } elseif (!$user) {
            $output->writeln('User not found');
            return false;
        }

        $this->productImporter->importToDb($productProvider, $report, $report->getSettings()['overwrite']);

    }
}
