<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


class RemoveProductCSVCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:products:csv:purge')
            ->setDescription('Check and remove old csv files.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $folderName = $this->getContainer()->getParameter('csv_path');

        $fileAgeLimit = 24*3600*3; // 3 days
        $count = 0;

        if (file_exists($folderName)) {
            foreach (new \DirectoryIterator($folderName) as $fileInfo) {
                if ($fileInfo->isDot()) {
                    continue;
                }
                if ($fileInfo->isFile() && time() - $fileInfo->getCTime() >= $fileAgeLimit) {
                    unlink($fileInfo->getRealPath());
                    $count++;
                }
            }
        }

        $output->writeln('Files purged - ' . $count);
    }
}