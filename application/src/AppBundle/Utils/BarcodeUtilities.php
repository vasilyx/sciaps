<?php

namespace AppBundle\Utils;
use AppBundle\Entity\Barcode;

/**
 * Created by PhpStorm.
 * User: jasonlinthwaite
 * Date: 05/07/2018
 * Time: 14:58
 */
class BarcodeUtilities
{
    /**
     * @param $barcode
     *
     * @return string
     */
    static function padBarcode(string $barcode): int
    {
        $l = strlen($barcode);

        if ($l >= 12 && $l <= 14) {
            return str_pad($barcode, 14, "0", STR_PAD_LEFT);
        }

        return $barcode;
    }

    /**
     * Make sure that the format is a barcode is correct - must be a string of numbers, 0-9 ONLY
     *
     * @param $barcode
     *
     * @return bool
     */
    static public function isFormatValid(string $barcode): bool
    {
        if (empty($barcode) || !!(preg_match('/[^0-9]/', $barcode))) {
            return false;
        }

        return true;

    }

    /**
     * Function to calculate the check digit. The barcode should not include the check digit.
     * Valid for GTIN-8, 12, 13, 14, GSIN, SSCC
     * https://www.gs1.org/services/how-calculate-check-digit-manually
     *
     * @param string $barcode
     *
     * @return int
     */
    static public function calculateCheckDigit(string $barcode): int
    {
        $barcode = strrev(strval($barcode));
        $i = $digit = $sum = 0;
        $length = strlen($barcode);

        while ($i < $length) {
            $sum += $i % 2 == 0 ? $barcode[$i] * 3 : $barcode[$i];
            $i++;
        }

        $mod10 = $sum % 10;
        $digit = $mod10 > 0 ? 10 - $mod10 : 0;

        return $digit;
    }

    /**
     * Full barcode including check digit
     *
     * @param string $barcode
     *
     * @return bool
     */
    static public function isValidGTIN(string $barcode): bool
    {
        if (!self::isFormatValid($barcode)) {
            return false;
        }

        return self::calculateCheckDigit(substr($barcode, 0, -1)) == substr($barcode, -1);
    }

    /**
     * Return the barcode type as a class constant when provided a full barcode including check digit
     *
     * @param string $barcode
     *
     * @return int|null
     */
    static public function getBarcodeType(string $barcode): ?int
    {
        $l = strlen($barcode);

        if (!self::isFormatValid($barcode)) return 0;

        // To check UPCE vs EAN-8 have to expand UPCE to UPCA first and then check.
        // Need to also check EAN-8 (first 7 digits) checksum as it could also be valid. Warn if both valid, but assume
        // UPCE

        $type = null;
        if ($l <= 8) {
            $UPCABarcode = self::convertUPCEtoUPCA($barcode);

            if ($UPCABarcode) {
                if (self::isValidGTIN($UPCABarcode)) {
                    $type = Barcode::TYPE_UPCE;
                } else {
                    $type = Barcode::TYPE_EAN8;
                }
            } else {
                if (self::isValidGTIN($barcode)) {
                    $type = Barcode::TYPE_EAN8;
                }
            }
        } else {
            if (self::isValidGTIN($barcode)) {
                if ($l == 12 || ($l == 13 && $barcode[0] == "0")) {
                    $type = Barcode::TYPE_UPCA;
                } elseif ($l == 13) {
                    $type = Barcode::TYPE_EAN13;
                } elseif ($l == 14 && substr($barcode, 0, 2) == "00") {
                    $type = Barcode::TYPE_UPCA;
                } elseif ($l == 14 && substr($barcode, 0, 1) == "0") {
                    $type = Barcode::TYPE_EAN13;
                } elseif ($l == 14) {
                    $type = Barcode::TYPE_GTIN14;
                }
            }
        }

        return $type;
    }

    /**
     * http://www.keepautomation.com/upce/upce-to-upca-conversion.html
     * @param      $barcodeOriginal
     * @param bool $fullExport
     *
     * @return array|string|boolean
     */
    static public function convertUPCEtoUPCA($barcodeOriginal, $fullExport = false)
    {
        // Let's just make sure we're getting UPCE value (this could conflict with EAN-8
        $barcode = self::extractUPCEValue($barcodeOriginal);

        if (!$barcode) return false;

        if (in_array($barcode{5}, ["0", "1", "2"])) {
            $mfrnum = $barcode{0} . $barcode{1} . $barcode{5} . "00";
            $itemnum = "00" . $barcode{2} . $barcode{3} . $barcode{4};
        } elseif ($barcode{5} == "3") {
            $mfrnum = $barcode{0} . $barcode{1} . $barcode{2} . "00";
            $itemnum = "000" . $barcode{3} . $barcode{4};
        } elseif ($barcode{5} == "4") {
            $mfrnum = $barcode{0} . $barcode{1} . $barcode{2} . $barcode{3} . "0";
            $itemnum = "0000" . $barcode{4};
        } elseif ($barcode{5} == "5") {
            $mfrnum = $barcode{0} . $barcode{1} . $barcode{2} . $barcode{3} . $barcode{4};
            $itemnum = "0000" . $barcode{5};
        } else {
            $mfrnum = $barcode{0} . $barcode{1} . $barcode{2} . $barcode{3} . $barcode{4};
            $itemnum = "0000" . $barcode{5};
        }

        // Let's append a 0 onto the conversion
        // Did the conversion work? The check digit should be the same.
        $newBarcode = "0" . $mfrnum . $itemnum;
        $checkDigit = self::calculateCheckDigit($newBarcode);

        if (substr($barcodeOriginal, -1) == $checkDigit) {
            return $fullExport ? ['manufacturerNumber' => $mfrnum, 'itemNumber' => $itemnum] : $newBarcode . $checkDigit;
        } else {
            return false;
        }
    }

    /**
     * Full barcode in either 6, 7 or 8 digit format
     *
     * @param string $barcode
     * @param bool $fullExport
     *
     * @return array|bool|string
     */
    static public function extractUPCEValue(string $barcode, $fullExport = false)
    {
        $l = strlen($barcode);

        $checkDigit = $numberSystemDigit = '';

        if ($l == 6) {
            // Assume we're getting just middle 6 digits
            $upce = $barcode;
        } elseif ($l == 7) {
            /*
             * This could be a EAN-8 barcode without the check digit but
             * assume it is just check digit of UPCE and truncate last digit
             */
            $upce = substr($barcode, 0, -1);
            $checkDigit = substr($barcode, -1);
        } elseif ($l == 8) {
            /*
             * This could be a EAN-8 barcode with the check digit but assume it is just check digit of UPCE
             * and truncate first and last digit; assume first digit is number system digit and last
             * digit is check digit
             */
            $upce = substr($barcode, 1, -1);
            $checkDigit = substr($barcode, -1);
            $numberSystemDigit = substr($barcode, 0, 1);
        } else {
            return false;
        }

        return $fullExport ? [
            'upce' => $upce,
            'checkDigit' => $checkDigit,
            'numberSystemDigit' => $numberSystemDigit
        ] : $upce;
    }
}
