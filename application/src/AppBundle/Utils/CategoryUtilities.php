<?php

namespace AppBundle\Utils;


/**
 * Created by PhpStorm.
 * User: jasonlinthwaite
 * Date: 05/07/2018
 * Time: 14:58
 */
class CategoryUtilities
{
    private static $bodyParts = [
        'face',
        'facial',
        'body',
        'eyes',
        'eye',
        'hand',
        'hands',
        'feet',
        'foot',
        'scalp',
        'lips',
        'lip',
        'cuticle',
        'cuticles',
        'nail',
        'nails',
        'decolletage',
        'decollete',
        'neck',
        'nipples',
        'back',
        'leg',
        'under arms',
        'under arm',
        'hair',
        'beard',
        'moustache',
        'eyebrow',
        'brow',
        'skin',
        'teeth'
    ];

    public static $categoryMap = [
        "Gift set" =>
            "gift sets?, travel sets?",
        "Cleanser" =>
            "{} cleansers?, {} wash(es)?, {} foams?, {} wipes?,
            pore strips?, toners?, makeup removers?, remover pads?,
            removal pads?, make-up removers?, eye makeup removers?,
            eye make-up removers?, Hand Sanitizers?, Cleansing Gels?,
            washing gels?, bodywash(es)?, hand gels?, facewash(es)?,
            cleansing milk, cleansing water, hand washing liquids?,
            hand foamers?, washcloth(es)?, soaps?, foaming gels?,
            handwash(es)?, pore tapes?, cleansing mousse, make up removers?,
            make-up removing, cleansing muds?, cleansing bars?, micellar water,
            cleansing strips?, micellar solutions?, cleansing solutions?,
            sanitizer gels?, sanitiser gels?, sanitizing acohol gels?,
            sanitizing gels?, sanitising gels?, sanitizers?, sanitisers?,
            hand sanitisers?",
        "Exfoliator" =>
            "exfoliators?, {} exfoliators?, {} scrubs?,{} polish(es)?,
            {} peels?, Exfoliating gels?, {} peeling",
        "Spot treatment" =>
            "Spot treamtments?, Blemish treatments?, spot gels?,
            spot correctors?, acne treatments?, breakout clearing gels?,
            blemish gels?, blemish care, blemish sticks?, anti-blemish,
            Anti-Acne, Acne Clear, spot erasers?, anti-dark spots?,
            dark spots?",
        "Sun Care" =>
            "sun care, {} suncare, {} sun creams?, {} sun lotions?,
            {} sun sprays?, {} aftersun, {} sunscreens?,
            {} protection, Aloe vera gels?,
            After sun, sun screens?, sunblocks?, sun blocks?, SPF",
        "Tools" =>
            "Tools?, sponges?, nail clippers?, brush(es)?, nail files?,
            nail scissors, manicure sets?, nail brush(es)?, hairbrush(es)?,
            cotton balls?, Nail Clippers?, Tweezers?, mirrors?, (nappy|nappies),
            diapers?, hair bands?",
        "Nails" =>
            "nails?, (Nail polish|Nail polishes), nail polish removers?,
            nail colours?,
            nail colors?, cuticle oils?, nail oils?, base coats?,
            nail hardeners?, nail strengtheners?, quick dry, top coats?,
            nail arts?, false nails?, cuticle removers?,  Nail Lacquers?,
            Nail Varnish(es)?, lacquers?, Enamel removers?, nailpolish(es)?,
            nail enamels?, nail gloss(es)?, nail stickers?, nail paints?,
            nail sets?, nail glues?, nail finish(es)?, nail gels?, nail wraps?",
        "Shampoo" =>
            "Shampoos?",
        "Conditioner" =>
            "Conditioners?, Hair Revitalizers?, Hair Detanglers?,
            Detangling Sprays?, hair polishers?, hair softeners?,
            (hair therapy|hair therapies)",
        "Hair colour" =>
            "Hair colours?, Hair colors?, hair colour removers?,
            hair color removers?, hair dyes?, hair highlighters?,
            semi-permanents?, permanents?, hair tints?, hair bleach(es)?,
            hair gloss(es)?",
        "Styling Hair" =>
            "styling hair, hair gels?, hair mousse, hair sprays?, hair wax(es)?,
            (hair putty|hair putties), hair styling, Hairsprays?,
            styling gels?, Styling Souffles?, style gels?, Hair Colourants?,
            styling mousse, styling sprays?, mud wax(es)?, hairstyling,
            hair colouring, fixing mousse, flat iron sprays?,
            Straightening Protectors?, hold mousse, fixing mousse, bodifying,
            activating mousse, activate mousse, Volumising, volumizing,
            curls?, volume mousse, curling, densifying",
        "Shower" =>
            "shower, Shower gels?, bubble bath, {} soaps?, bath foams?,bath oils?,
            bath salts?, bath creams?, bath soaks?, bath, bathing, foambath,
            bathsoaks?, shower milk, bathfoams?",
        "Medicine" =>
            "Medicine,
            Beauty supplement, {} Vitamins?, {} beauty vitamins?, vitamins?,
            multivitamins?, {} supplements?, supplements?, Nasal sprays?,
            (pain relief|pain relieves), pain relievers?, pain gels?,
            pain patch(es)?, pain relieving,  Menstrual Pain, Decongestions?,
            Congestions?, Fever, hayfever, hay fever,
            (allergy relief|allergy relieves), allergy sprays?,
            anti-allergy, antihistamines?, allergy eye drops,
            (itch relief|itch relieves), (Diarrhoea Relief|diarrhoea relieves),
            Anti-Diarrhoea, antacids?, (cold relief|cold relieves), tablets?,
            cough, lozenges?, caplets?, capsules?, syrups?, flu, colic,
            relieve pain, oral solutions?, oral suspensions?, oral sprays?,
            oral rehydrations?, floral water, nicotine, sleeping aid, pills,
            Effervescents?, throat sprays?, granules?, eye drops?,
            heat patch(es)?",
        "Oral care" =>
            "oral care, Toothpastes?, mouthwash(es)?, teeth whitening,dental care,
            denture care, dental floss(es)?, denture cleaners?, mouthrinses?,
            Toothbrush(es)?, Mouth sprays?, Tooth Whitening, dental gels?,
            breath sprays?, Oral rinses?, mouth fresheners?, breath freshening,
            Tongue Cleaners?, Tongue Scrapers?, mouth rinses?, denture cleaning,
            oral gels?, flossers?, dentures?, interdental brush(es)?,
            interdental floss(es)?, interdental sticks?, interdental tapes?",
        "Parfum" =>
            "Parfums?, Perfumes?, Fragrances?, eau de toilette, eau de parfum,
            Colognes?, EDP, EDT, Body Mists?, Body splash (es)?, perfumed",
        "Hair Removal" =>
            "hair removals?, {} Hair removal creams?, {} wax strips?,
            {} hair remover wax(es)?,{} shave gels?, {} shaving gels?,
            (Depilatory|depilatories), depilation, {} razors?, razor blades?,
            epilators?, nose wax(es)?, bikini wax(es)?, moustache wax(es)?,
            {} shaving creams?, {} shave creams?, {} shaving foams?,
            {} shave foams?, {} shave brush(es)?, shaving brush(es)?,
            {} shaving serums?, {} shave serums?, {} shaving oils?,
            {} shave oils?, {} shaving balms?, {} shave balms?,
            {} pre-shaves?, Razors?, Facial waxing, hair removers?,
            blades?, shaving mousse",
        "Grooming" =>
            "grooming,moustache wax(es)?,aftershaves?,beard oils?,{} aftershaves?,
            beard oils?, {} post-shaves?, after shaves?, after-shaves?",
        "Aromatherapy" =>
            "(Aromatherapy|aromatherapies), Essential oils?, pure oils?,
            massage oils?",
        "Feminine Care" =>
            "Feminine Care
            Feminine hygienes?, feminine wash(es)?, feminine wipes?,
            intimate wash(es)?, intimate moisturisers?, Intimate Washing,
            Pantyliners?, sanitary pads?, sanitary towels?, sanitary napkins?,
            feminine pads?, tampons?, pads?, intimate hygienes?,
            intimate detergents?, intimate wipes?, intimate towels?,
            feminine intimate gels?",
        "Deodorant" =>
            "deodorants?, anti-perspirants?, underarms?, Body sprays?,
            Deos?, Deodorizing Sprays?, antiperspirants?",
        "Sexual Health" =>
            "sexual health, lubricants?, Massage gels?,
            (lubricating jelly|lubricating jellies),
            lubes?, arousal balms?, pleasure gels?, condoms?",
        "Tanning" =>
            "Tanning & Bronzing, Face tans?, selft tans?,selft tanning,body tans?,
            tan moisturisers?, tan mousse, tan lotions?,
            (tanning jelly|tanning jellies), tanning oils?, bronzing mists?,
            airbrush(es)?, tans?, bronzing lotions?, tinted, fake tans?,
            self tanners?, self-tanning, Bronzing pearls?",
        "Eye Makeup" =>
            "eye makeup, brows?, liners?, eyeliners?, eye pencils?, eye primers?,
            lash(es)?, eyeshadows?, mascaras?, eye palettes?, eyelash(es)?,
            eye shadows?, eye enhancers?, false eyelash(es)?, eyelash glues?,
            eye colours?, eye colors?, shadows?, Smoky Eyes?, eye crayons?",
        "Lip Makeup" =>
            "lip makeup, Lip colours?, lip colors?, lip & cheek colours?,
            lip & cheek colors?, lip & cheek stains?, lip oils?,
            lip crayons?, lip sticks?, lip gloss(es)?, lip pencils?,
            lip primers?, lip enhancers?, lip plumpers?, lip stains?,
            lip gels?, lipshines?, lipgloss(es)?, lipsticks?, lip tints?,
            lip shines?, lipcolours?, lipcolors?, lipliners?, Lip Palettes?",
        "Face makeup" =>
            "Face makeup, BB creams?, CC creams?, blushers?, bronzers?,
            concealers?,
            primers?, foundations?, face palettes?, contours?, cover-ups?,
            correctors?, highlighters?, glow pencils?, tinted moisturisers?,
            setting sprays?, fixing sprays?, setting mists?, fixing mists?,
            tinted moisturizers?, cheek stains?, powders?, blush(es)?",
        "Talcum powder" =>
            "Talcum powder, talcs?",
        "Anti-aging" =>
            "Anti-aging, Anti-wrinkle, Wrinkle Fillers?,
            Wrinkle Reducers?, anti-ageing",
        "First Aid" =>
            "First Aid, Plasters?, bandages?, antiseptic sprays?,
                Antiseptic Disinfectants?, Antiseptic alcohols?,
                Antiseptic liquids?, Antiseptic solutions?, dressings?,
                sterile cottons?, Sterile Saline Solutions?, blister patch(es)?,
                anti-blister gels?, anti-blister sticks?, blister care,
                hydrogen peroxide, band aids?, dressings?, gauzes?",
        "Repellent" =>
            "Repellents?, Anti-bite sprays?, bite sprays?",
        "moisturiser" =>
            "moisturisers?, creme, creams?, moisturizers?, lotions?,
                balms?, ointments?, salves?, emollients?, {} moisturisers?,
                {} moisturizers?, {} creams?, {} lotions?, {} creme,
                {} balms?, {} ointments?, {} salves?, {} emollients?,
                hydrators?, hydration, {} butters?, masques?, masks?,
                clays?, {} masks?, {} clays?, {} masques?, oils?, {} oils?,
                serums?, {} serums?, treatments?, {} treatments?, Anti-Cellulite,
                (Petroleum Jelly|petroleum jellies), {} Emulsions?, lipbalms?,
                {} souffles?, {} milk, (hand therapy|hand therapies),
                (foot therapy|foot therapies), eye gels?, dark circles?,
                anti-dark circles?, facial mists?"
    ];

    public static function mapCategories($categories)
    {
        if (empty($categories) || !is_array($categories)) return false;

        $bodyParts = self::$bodyParts;
        $categoryMap = self::$categoryMap;

        $idMap = $catFlat = [];

        foreach ($categoryMap as $category => $synonymOptions) {
            if (empty($synonymOptions)) continue;

            $synonyms = array_map('trim', explode(',', $synonymOptions));

            // Get the ID of the category from the database (or provided categories)
            foreach ($categories as $data) {
                if ($data['name'] == $category) {
                    $idMap[$category] = $data['id'];
                }
            }

            foreach ($synonyms as $synonym) {
                if (empty($synonym)) continue;
                if (strpos($synonym, '{}') === false) {
                    $catFlat[$synonym] = [$category, "\b$synonym\b"];
                } else {
                    $baseSynonym = trim(str_replace("{}", '', $synonym));
                    $catFlat[$baseSynonym] = [$category, "\b$baseSynonym\b"];

                    foreach ($bodyParts as $part) {
                        $synonymBodyPart = str_replace('{}', $part, $synonym);
                        $catFlat[$synonymBodyPart] = [$category, "\b$synonymBodyPart\b"];
                    }
                }
            }
        }

        return [$idMap, $catFlat];
    }

    /**
     * @param      $product
     * @param null $categories
     *
     * @return bool|mixed
     */
    public static function categoriseProduct($product, $categories = null)
    {
        if (empty($categories)) return false;

        // Check name
        $name = $product['name'];
        $description = $product['description'];

        // Name
        if ($categoryId = self::getProductCategory($name, $categories)) {
            return $categoryId;
        } elseif ($categoryId = self::getProductCategory($description, $categories)) {
            return $categoryId;
        }

        return false;
    }

    /**
     * @param      $searchString
     * @param null $categories
     *
     * @return mixed
     */
    public static function getProductCategory($searchString, $categories = null)
    {
        list($ids, $catFlat) = self::mapCategories($categories);

        $finalCategory = '';

        foreach ($catFlat as $data) {
            $pattern = $data[1];
            if (preg_match("/$pattern/i", $searchString)) {
                $finalCategory = $data[0];
                break;
            }
        }

        if (empty($finalCategory)) {
            return false;
        } else {
            return $ids[$finalCategory];
        }
    }
}