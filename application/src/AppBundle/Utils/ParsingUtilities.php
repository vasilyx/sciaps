<?php

namespace AppBundle\Utils;

/**
 * Created by PhpStorm.
 * User: jasonlinthwaite
 * Date: 05/07/2018
 * Time: 14:58
 */
class ParsingUtilities
{
    protected $regExVolume = "(?:(\d*[\.,]*\d+(?![\.,]))\s*((?:(?:g|ml|us|fl|oz|lb|lbm|\.)\s*)+(?!\w)))";

    /**
     * ParsingUtilities constructor.
     */
    public function __construct($config = [])
    {
        if (isset($config['regExVolume'])) $this->regExVolume = $config['regExVolume'];
    }

    public function buildProductName($product)
    {
        // Refactor when force class type
        $name = $product['name'];
        $brand = isset($product['brand']) ? $product['brand'] : '';
        $manufacturer = isset($product['manufacturer']) ? $product['manufacturer'] : '';
        $volume = isset($product['volume']) ? $product['volume'] : '';
        $units = isset($product['volume_units']) ? $product['volume_units'] : '';

        // If product begins with brand do not include
        $nameParts = explode(' ', $name);
        if ($nameParts[0] == $brand || $nameParts[0] == $manufacturer) {
            $finalProductName = $name;
        } else {
            $finalProductName = $brand . " " . $name;
        }

        // If product contains volume, do not include
        if (!(preg_match("/" . $this->regExVolume . "/i", $finalProductName)) &&
            !empty($volume) &&
            !empty($units)) {
            $finalProductName .= " " . $volume . $units;
        }

        return $finalProductName;
    }

    /**
     * @param string $name
     * @param array $array
     * @return array|null
     */
    public static function similarName(string $name, array $array): ?array
    {
        if (empty($array) || empty($name)) return null;

        $similarity = 0;
        $similarities = [];

        foreach ($array as $k => $productName) {
            // Use similar_text() rather than levenshtein() as result is better
            // https://www.w3schools.com/php/func_string_similar_text.asp
            $sim = similar_text($name, $productName);
            if ($sim > $similarity) {
                $similarity = $sim;
                $bestProduct = $k;
            }
            $similarities[$k] = $sim;
        }

        return isset($bestProduct) ? ['best' => $bestProduct, 'all' => $similarities] : null;
    }
}
