<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180830113938 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_evidences (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, source_evidence_id INT DEFAULT NULL, INDEX status_idx (source_evidence_id), INDEX product_idx (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_evidences ADD CONSTRAINT FK_681508B54584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_evidences ADD CONSTRAINT FK_681508B58957ACB1 FOREIGN KEY (source_evidence_id) REFERENCES source_evidence (id)');
        $this->addSql('DROP TABLE product_concerns');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_concerns (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, source_evidence_id INT DEFAULT NULL, INDEX product_idx (product_id), INDEX status_idx (source_evidence_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_concerns ADD CONSTRAINT FK_89B90BD4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_concerns ADD CONSTRAINT FK_89B90BD8957ACB1 FOREIGN KEY (source_evidence_id) REFERENCES source_evidence (id)');
        $this->addSql('DROP TABLE product_evidences');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }
}
