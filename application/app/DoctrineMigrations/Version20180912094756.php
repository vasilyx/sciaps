<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180912094756 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();
        try {
            $this->addSql('ALTER TABLE user CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
            $this->addSql('ALTER TABLE attribute DROP FOREIGN KEY FK_FA7AEFFB727ACA70');
            $this->addSql('ALTER TABLE attribute ADD CONSTRAINT FK_FA7AEFFB727ACA70 FOREIGN KEY (parent_id) REFERENCES attribute (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE evidence DROP FOREIGN KEY FK_C615710727ACA70');
            $this->addSql('ALTER TABLE evidence ADD CONSTRAINT FK_C615710727ACA70 FOREIGN KEY (parent_id) REFERENCES evidence (id) ON DELETE CASCADE');

            $conn->commit();
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
