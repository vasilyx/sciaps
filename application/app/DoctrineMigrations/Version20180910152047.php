<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180910152047 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();
        try{
            $this->addSql('CREATE TABLE log_event (id INT AUTO_INCREMENT NOT NULL, added_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, action_id INT NOT NULL, subject_id INT NOT NULL, subject_type_id INT NOT NULL, status INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
            $this->addSql('ALTER TABLE crowdsource_pending CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
            $this->addSql('ALTER TABLE user CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
            $conn->commit();
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
