<?php declare(strict_types=1);

namespace Application\Migrations;

use AppBundle\Entity\RegExp;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

 
final class Version20180930221328 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();

        try {
            $this->addSql("INSERT INTO `regexp` (`regex`, regex_type, added_at) VALUES (\"[^,][\s]{0,3}[\(]{0,1}([\*|\^|†]{1,4}(?!\,|\(|\))[a-z|A-Z|0-9|\s|\,|\%|\-|\)|\:]{3,}?[\.|$])|(\(\d\)[^,|^\.|^\)][a-z|A-Z|0-9|\s|\,|\%|\-|\)|\:]{3,})\", " . RegExp::TYPE_LEGEND_PART . ", NOW() )");
            $this->addSql("INSERT INTO `regexp` (`regex`, regex_type, added_at) VALUES (\"([a - z | A - Z | 0 - 9 | \s |,|\( | \) | \% | \. | \* | \/ | \'|\[|\]|†|\^|\+|\-|\º|\&|\é|\á|\™]{3,}?)(?:(?:,(?![\d]|[^\(]*\))|$)|(?:\.(?!\d)))\", " . RegExp::TYPE_INGREDIENT_PART . ", NOW() )");
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
