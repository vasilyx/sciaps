<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180723124327 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE holding_company (id INT AUTO_INCREMENT NOT NULL, added_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, name VARCHAR(45) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE providers_products');
        $this->addSql('ALTER TABLE product ADD barcode_id INT DEFAULT NULL, ADD holding_company_id INT DEFAULT NULL, ADD safety_warnings LONGTEXT DEFAULT NULL, ADD storage_advisory LONGTEXT DEFAULT NULL, ADD product_claims LONGTEXT DEFAULT NULL, ADD free_from_advisory LONGTEXT DEFAULT NULL, ADD country_of_origin VARCHAR(25) DEFAULT NULL, ADD health_conditions_advisory LONGTEXT DEFAULT NULL, ADD skin_types_advisory LONGTEXT DEFAULT NULL, ADD hair_types_advisory LONGTEXT DEFAULT NULL, ADD application_area_advisory LONGTEXT DEFAULT NULL, ADD package_type INT DEFAULT NULL, ADD recycling_information LONGTEXT DEFAULT NULL, DROP holding_company, CHANGE name name VARCHAR(45) NOT NULL, CHANGE volume_units volume_units INT DEFAULT NULL, CHANGE version data_provider_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADF593F7E0 FOREIGN KEY (data_provider_id) REFERENCES data_provider (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD29439E58 FOREIGN KEY (barcode_id) REFERENCES barcode (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD61C4CB47 FOREIGN KEY (holding_company_id) REFERENCES holding_company (id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADF593F7E0 ON product (data_provider_id)');
        $this->addSql('CREATE INDEX holding_company_idx ON product (holding_company_id)');
        $this->addSql('CREATE INDEX barcode_idx ON product (barcode_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD61C4CB47');
        $this->addSql('CREATE TABLE providers_products (id INT AUTO_INCREMENT NOT NULL, master_product_id INT DEFAULT NULL, product_id INT DEFAULT NULL, data_provider_id INT DEFAULT NULL, added_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, sku VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, stock VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, price VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, INDEX master_product_idx (master_product_id), INDEX product_idx (product_id), INDEX providers_products_provider_fk (data_provider_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE providers_products ADD CONSTRAINT FK_1185E4D64584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE providers_products ADD CONSTRAINT FK_1185E4D64D9AC4D4 FOREIGN KEY (master_product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE providers_products ADD CONSTRAINT FK_1185E4D6F593F7E0 FOREIGN KEY (data_provider_id) REFERENCES data_provider (id)');
        $this->addSql('DROP TABLE holding_company');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADF593F7E0');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD29439E58');
        $this->addSql('DROP INDEX IDX_D34A04ADF593F7E0 ON product');
        $this->addSql('DROP INDEX holding_company_idx ON product');
        $this->addSql('DROP INDEX barcode_idx ON product');
        $this->addSql('ALTER TABLE product ADD version INT DEFAULT NULL, ADD holding_company VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, DROP data_provider_id, DROP barcode_id, DROP holding_company_id, DROP safety_warnings, DROP storage_advisory, DROP product_claims, DROP free_from_advisory, DROP country_of_origin, DROP health_conditions_advisory, DROP skin_types_advisory, DROP hair_types_advisory, DROP application_area_advisory, DROP package_type, DROP recycling_information, CHANGE name name VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE volume_units volume_units VARCHAR(4) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
