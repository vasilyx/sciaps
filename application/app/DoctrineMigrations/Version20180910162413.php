<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180910162413 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE source DROP FOREIGN KEY FK_5F8A7F7362E73146');
        $this->addSql('DROP INDEX source_data_provider_idx ON source');
        $this->addSql('ALTER TABLE source ADD title VARCHAR(45) DEFAULT NULL, ADD name_author VARCHAR(45) DEFAULT NULL, ADD article_url VARCHAR(45) DEFAULT NULL, ADD article_type VARCHAR(45) DEFAULT NULL, ADD evidence_upload_date DATETIME DEFAULT NULL, DROP name, DROP url, CHANGE source_data_provider_id company_id INT NOT NULL, CHANGE source_data_date import_data_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE source ADD CONSTRAINT FK_5F8A7F73979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_5F8A7F73979B1AD6 ON source (company_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE source DROP FOREIGN KEY FK_5F8A7F73979B1AD6');
        $this->addSql('DROP INDEX IDX_5F8A7F73979B1AD6 ON source');
        $this->addSql('ALTER TABLE source ADD name VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD url VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, ADD source_data_date DATETIME DEFAULT NULL, DROP title, DROP name_author, DROP article_url, DROP article_type, DROP import_data_date, DROP evidence_upload_date, CHANGE company_id source_data_provider_id INT NOT NULL');
        $this->addSql('ALTER TABLE source ADD CONSTRAINT FK_5F8A7F7362E73146 FOREIGN KEY (source_data_provider_id) REFERENCES source_data_provider (id)');
        $this->addSql('CREATE INDEX source_data_provider_idx ON source (source_data_provider_id)');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }
}
