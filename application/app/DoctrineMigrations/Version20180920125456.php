<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180920125456 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();
        try{
            $this->addSql('CREATE TABLE mapping_data (id INT AUTO_INCREMENT NOT NULL, updated_at DATETIME DEFAULT NULL, data VARCHAR(512) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
            $this->addSql('ALTER TABLE product_provider ADD mapping_data_id INT DEFAULT NULL');
            $this->addSql('ALTER TABLE product_provider ADD CONSTRAINT FK_5974190B1CA5481C FOREIGN KEY (mapping_data_id) REFERENCES mapping_data (id) ON DELETE CASCADE');
            $this->addSql('CREATE UNIQUE INDEX UNIQ_5974190B1CA5481C ON product_provider (mapping_data_id)');
            $conn->commit();
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
