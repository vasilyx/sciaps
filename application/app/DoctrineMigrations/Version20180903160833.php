<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180903160833 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product CHANGE description description LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE company ADD source_provider_id INT DEFAULT NULL, ADD dataentry_provider_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094F362CF80A FOREIGN KEY (source_provider_id) REFERENCES source_data_provider (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FE87B12F9 FOREIGN KEY (dataentry_provider_id) REFERENCES dataentry_provider (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094F362CF80A ON company (source_provider_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094FE87B12F9 ON company (dataentry_provider_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094F362CF80A');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FE87B12F9');
        $this->addSql('DROP INDEX UNIQ_4FBF094F362CF80A ON company');
        $this->addSql('DROP INDEX UNIQ_4FBF094FE87B12F9 ON company');
        $this->addSql('ALTER TABLE company DROP source_provider_id, DROP dataentry_provider_id');
        $this->addSql('ALTER TABLE product CHANGE description description VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }
}
