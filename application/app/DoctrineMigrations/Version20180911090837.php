<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180911090837 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE ingredients_evidences DROP FOREIGN KEY FK_9C494026259AE7B');
        $this->addSql('DROP INDEX ingredient_idx ON ingredients_evidences');
        $this->addSql('ALTER TABLE ingredients_evidences CHANGE ingredient_synonym_group_id ingredient_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ingredients_evidences ADD CONSTRAINT FK_9C49402933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id)');
        $this->addSql('CREATE INDEX ingredient_idx ON ingredients_evidences (ingredient_id)');
        $this->addSql('ALTER TABLE ingredients_attributes DROP FOREIGN KEY FK_F79836E86259AE7B');
        $this->addSql('DROP INDEX ingredient_idx ON ingredients_attributes');
        $this->addSql('ALTER TABLE ingredients_attributes CHANGE ingredient_synonym_group_id ingredient_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ingredients_attributes ADD CONSTRAINT FK_F79836E8933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id)');
        $this->addSql('CREATE INDEX ingredient_idx ON ingredients_attributes (ingredient_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ingredients_attributes DROP FOREIGN KEY FK_F79836E8933FE08C');
        $this->addSql('DROP INDEX ingredient_idx ON ingredients_attributes');
        $this->addSql('ALTER TABLE ingredients_attributes CHANGE ingredient_id ingredient_synonym_group_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ingredients_attributes ADD CONSTRAINT FK_F79836E86259AE7B FOREIGN KEY (ingredient_synonym_group_id) REFERENCES ingredient_synonym_group (id)');
        $this->addSql('CREATE INDEX ingredient_idx ON ingredients_attributes (ingredient_synonym_group_id)');
        $this->addSql('ALTER TABLE ingredients_evidences DROP FOREIGN KEY FK_9C49402933FE08C');
        $this->addSql('DROP INDEX ingredient_idx ON ingredients_evidences');
        $this->addSql('ALTER TABLE ingredients_evidences CHANGE ingredient_id ingredient_synonym_group_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ingredients_evidences ADD CONSTRAINT FK_9C494026259AE7B FOREIGN KEY (ingredient_synonym_group_id) REFERENCES ingredient_synonym_group (id)');
        $this->addSql('CREATE INDEX ingredient_idx ON ingredients_evidences (ingredient_synonym_group_id)');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }
}
