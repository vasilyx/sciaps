<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180903164829 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE dataentry_provider ADD company_id INT DEFAULT NULL, DROP company');
        $this->addSql('ALTER TABLE dataentry_provider ADD CONSTRAINT FK_A291C7D0979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A291C7D0979B1AD6 ON dataentry_provider (company_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dataentry_provider DROP FOREIGN KEY FK_A291C7D0979B1AD6');
        $this->addSql('DROP INDEX UNIQ_A291C7D0979B1AD6 ON dataentry_provider');
        $this->addSql('ALTER TABLE dataentry_provider ADD company VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, DROP company_id');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }
}
