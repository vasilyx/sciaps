<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180917062844 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();
        try{
            $this->addSql('CREATE TABLE crowdsource_pending_ingredients (id INT AUTO_INCREMENT NOT NULL, ingredient_id INT DEFAULT NULL, crowdsource_pending_id INT DEFAULT NULL, added_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, ingredient_order INT DEFAULT NULL, INDEX crowdsource_pending_idx (crowdsource_pending_id), INDEX ingredient_idx (ingredient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
            $this->addSql('ALTER TABLE crowdsource_pending_ingredients ADD CONSTRAINT FK_ECE2BF34933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id)');
            $this->addSql('ALTER TABLE crowdsource_pending_ingredients ADD CONSTRAINT FK_ECE2BF34A87D54CE FOREIGN KEY (crowdsource_pending_id) REFERENCES crowdsource_pending (id)');

            $conn->commit();
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
