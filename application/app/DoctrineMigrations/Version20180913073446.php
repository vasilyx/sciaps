<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180913073446 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();
        try{
            $this->addSql('DROP TABLE organization_countries');
            $this->addSql('CREATE TABLE organization_country (organization_id INT NOT NULL, country_id INT NOT NULL, INDEX IDX_9EA1412532C8A3DE (organization_id), INDEX IDX_9EA14125F92F3E70 (country_id), PRIMARY KEY(organization_id, country_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
            $this->addSql('ALTER TABLE organization_country ADD CONSTRAINT FK_9EA1412532C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE organization_country ADD CONSTRAINT FK_9EA14125F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE CASCADE');

            $conn->commit();
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
