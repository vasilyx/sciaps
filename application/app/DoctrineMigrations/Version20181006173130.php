<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181006173130 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();

        $legendPartMatch = "[^,][\s]{0,3}[\(]{0,1}([\*|\^|†]{1,4}(?!\,|\(|\))[a-z|A-Z|0-9|\s|\,|\%|\-|\)|\:]{3,}?[\.|$])|(\(\d\)[^,|^\.|^\)][a-z|A-Z|0-9|\s|\,|\%|\-|\)|\:]{3,})";
        $itemMatch = "([a-z|A-Z|0-9|\s|,|\(|\)|\%|\.|\*|\/|\'|\[|\]|†|\^|\+|\-|\º|\&|\é|\á|\™]{3,}?)(?:(?:,(?![\d]|[^\(]*\))|$)|(?:\.(?!\d)))";

        try {
            $this->addSql('UPDATE `regexp` SET `regex`= ' . $conn->quote($legendPartMatch) . ' WHERE id = 1');
            $this->addSql('UPDATE `regexp` SET `regex`= ' . $conn->quote($itemMatch) . ' WHERE id = 2');
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema): void
    {
    }
}
