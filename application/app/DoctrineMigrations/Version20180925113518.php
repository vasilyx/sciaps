<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180925113518 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();
        try {
            $this->addSql('DELETE FROM ingredient_synonym_group');
            $this->addSql('ALTER TABLE brands_ingredients DROP FOREIGN KEY FK_F1AF060C8C5289C9');
            $this->addSql('ALTER TABLE ingredient DROP FOREIGN KEY FK_6BAF78708C5289C9');
            $this->addSql('ALTER TABLE ingredient_synonym_group DROP FOREIGN KEY FK_D45D9DAB8C5289C9');
            $this->addSql('DROP TABLE ingredient_group');
            $this->addSql('ALTER TABLE user CHANGE status status INT DEFAULT 0 NOT NULL');
            $this->addSql('ALTER TABLE products_ingredients ADD name VARCHAR(255) DEFAULT NULL, ADD match_type INT DEFAULT NULL');
            $this->addSql('ALTER TABLE product_attributes CHANGE status_code status_code INT NOT NULL');
            $this->addSql('DROP INDEX group_idx ON ingredient');
            $this->addSql('ALTER TABLE ingredient ADD common_name VARCHAR(255) DEFAULT NULL, ADD inci_name VARCHAR(255) DEFAULT NULL, ADD inn_name VARCHAR(255) DEFAULT NULL, ADD ph_eur_name VARCHAR(255) DEFAULT NULL, ADD chemical_iupac_name VARCHAR(255) DEFAULT NULL, ADD description LONGTEXT DEFAULT NULL, ADD cas_number VARCHAR(255) DEFAULT NULL, ADD ec_number VARCHAR(255) DEFAULT NULL, ADD cosing_number VARCHAR(255) DEFAULT NULL, ADD cosing_hash LONGTEXT DEFAULT NULL, ADD pub_chem_id INT DEFAULT NULL, DROP ingredient_group_id, DROP name, DROP primary_ingredient, DROP synonym_group_id');
            $this->addSql('DROP INDEX ingredient_idx ON brands_ingredients');
            $this->addSql('ALTER TABLE brands_ingredients CHANGE ingredient_group_id ingredient_id INT DEFAULT NULL');
            $this->addSql('ALTER TABLE brands_ingredients ADD CONSTRAINT FK_F1AF060C933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id)');
            $this->addSql('CREATE INDEX ingredient_idx ON brands_ingredients (ingredient_id)');
            $this->addSql('DROP INDEX ingredient_group_idx ON ingredient_synonym_group');
            $this->addSql('ALTER TABLE ingredient_synonym_group CHANGE ingredient_group_id ingredient_id INT DEFAULT NULL');
            $this->addSql('ALTER TABLE ingredient_synonym_group ADD CONSTRAINT FK_D45D9DAB933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id)');
            $this->addSql('CREATE INDEX ingredient_idx ON ingredient_synonym_group (ingredient_id)');
            $this->addSql('ALTER TABLE product_evidences CHANGE status_code status_code INT NOT NULL');
            $this->addSql('ALTER TABLE attribute CHANGE default_name default_name VARCHAR(45) NOT NULL, CHANGE symbol symbol VARCHAR(4) NOT NULL');
            $this->addSql('ALTER TABLE evidence CHANGE default_name default_name VARCHAR(45) NOT NULL, CHANGE symbol symbol VARCHAR(4) NOT NULL');
            $this->addSql('ALTER TABLE ingredients_evidences CHANGE status_code status_code INT NOT NULL');
            $this->addSql('ALTER TABLE manufacturer_attributes CHANGE status_code status_code INT NOT NULL');
            $this->addSql('ALTER TABLE crowdsource_pending CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
            $this->addSql('ALTER TABLE manufacturer_evidences CHANGE status_code status_code INT NOT NULL');
            $this->addSql('ALTER TABLE ingredients_attributes CHANGE status_code status_code INT NOT NULL');

            $conn->commit();
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
