<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180718143441 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE concern DROP FOREIGN KEY FK_281EFBA8727ACA70');
        $this->addSql('ALTER TABLE evidence_concern DROP FOREIGN KEY FK_C0C7CD39BC6DCCD5');
        $this->addSql('ALTER TABLE brand_attributes DROP FOREIGN KEY FK_B44B8AE9B3FF07C');
        $this->addSql('ALTER TABLE ingredients_attributes DROP FOREIGN KEY FK_F79836E89B3FF07C');
        $this->addSql('ALTER TABLE manufacturer_attributes DROP FOREIGN KEY FK_E68FA57036AFE5FF');
        $this->addSql('ALTER TABLE product_attributes DROP FOREIGN KEY FK_A2FCC15B36AFE5FF');
        $this->addSql('ALTER TABLE brand_concerns DROP FOREIGN KEY FK_41B693B36AFE5FF');
        $this->addSql('ALTER TABLE ingredients_concerns DROP FOREIGN KEY FK_B2F8F46336AFE5FF');
        $this->addSql('ALTER TABLE manufacturer_concerns DROP FOREIGN KEY FK_46F36ABC36AFE5FF');
        $this->addSql('ALTER TABLE product_concerns DROP FOREIGN KEY FK_89B90BD36AFE5FF');
        $this->addSql('ALTER TABLE evidence_attribute DROP FOREIGN KEY FK_5D9A29E0DF2C97AD');
        $this->addSql('ALTER TABLE evidence_concern DROP FOREIGN KEY FK_C0C7CD39DF2C97AD');
        $this->addSql('CREATE TABLE source_attribute (id INT AUTO_INCREMENT NOT NULL, attribute_id INT DEFAULT NULL, source_id INT DEFAULT NULL, status_code INT DEFAULT NULL, name VARCHAR(45) DEFAULT NULL, description TEXT DEFAULT NULL, INDEX source_idx (source_id), INDEX attribute_idx (attribute_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evidence (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, added DATETIME NOT NULL, updated DATETIME DEFAULT NULL, default_name VARCHAR(45) DEFAULT NULL, default_description TEXT DEFAULT NULL, symbol VARCHAR(4) DEFAULT NULL, INDEX parent_idx (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ingredients_evidences (id INT AUTO_INCREMENT NOT NULL, ingredient_synonym_group_id INT DEFAULT NULL, source_evidence_id INT DEFAULT NULL, INDEX status_idx (source_evidence_id), INDEX ingredient_idx (ingredient_synonym_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE source_evidence (id INT AUTO_INCREMENT NOT NULL, evidence_id INT DEFAULT NULL, source_id INT DEFAULT NULL, status_code INT DEFAULT NULL, name VARCHAR(45) DEFAULT NULL, description TEXT DEFAULT NULL, INDEX source_idx (source_id), INDEX evidence_idx (evidence_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE source (id INT AUTO_INCREMENT NOT NULL, data_provider_id INT DEFAULT NULL, added DATETIME NOT NULL, updated DATETIME DEFAULT NULL, name VARCHAR(45) DEFAULT NULL, url VARCHAR(45) DEFAULT NULL, source_data_date DATETIME DEFAULT NULL, last_checked_date DATETIME DEFAULT NULL, INDEX data_provider_idx (data_provider_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE manufacturer_evidences (id INT AUTO_INCREMENT NOT NULL, manufacturer_id INT DEFAULT NULL, source_evidence_id INT DEFAULT NULL, INDEX status_idx (source_evidence_id), INDEX manufacturer_idx (manufacturer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE brand_evidences (id INT AUTO_INCREMENT NOT NULL, brand_id INT DEFAULT NULL, source_evidence_id INT DEFAULT NULL, INDEX status_idx (source_evidence_id), INDEX brand_idx (brand_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE source_attribute ADD CONSTRAINT FK_92E47C35B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id)');
        $this->addSql('ALTER TABLE source_attribute ADD CONSTRAINT FK_92E47C35953C1C61 FOREIGN KEY (source_id) REFERENCES source (id)');
        $this->addSql('ALTER TABLE evidence ADD CONSTRAINT FK_C615710727ACA70 FOREIGN KEY (parent_id) REFERENCES evidence (id)');
        $this->addSql('ALTER TABLE ingredients_evidences ADD CONSTRAINT FK_9C494026259AE7B FOREIGN KEY (ingredient_synonym_group_id) REFERENCES ingredient_synonym_group (id)');
        $this->addSql('ALTER TABLE ingredients_evidences ADD CONSTRAINT FK_9C494028957ACB1 FOREIGN KEY (source_evidence_id) REFERENCES source_evidence (id)');
        $this->addSql('ALTER TABLE source_evidence ADD CONSTRAINT FK_4F4161C2B528FC11 FOREIGN KEY (evidence_id) REFERENCES evidence (id)');
        $this->addSql('ALTER TABLE source_evidence ADD CONSTRAINT FK_4F4161C2953C1C61 FOREIGN KEY (source_id) REFERENCES source (id)');
        $this->addSql('ALTER TABLE source ADD CONSTRAINT FK_5F8A7F73F593F7E0 FOREIGN KEY (data_provider_id) REFERENCES data_provider (id)');
        $this->addSql('ALTER TABLE manufacturer_evidences ADD CONSTRAINT FK_1F5C50D9A23B42D FOREIGN KEY (manufacturer_id) REFERENCES manufacturer (id)');
        $this->addSql('ALTER TABLE manufacturer_evidences ADD CONSTRAINT FK_1F5C50D98957ACB1 FOREIGN KEY (source_evidence_id) REFERENCES source_evidence (id)');
        $this->addSql('ALTER TABLE brand_evidences ADD CONSTRAINT FK_6CC2AE5944F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id)');
        $this->addSql('ALTER TABLE brand_evidences ADD CONSTRAINT FK_6CC2AE598957ACB1 FOREIGN KEY (source_evidence_id) REFERENCES source_evidence (id)');
        $this->addSql('DROP TABLE brand_concerns');
        $this->addSql('DROP TABLE concern');
        $this->addSql('DROP TABLE evidence_attribute');
        $this->addSql('DROP TABLE evidence_concern');
        $this->addSql('DROP TABLE evidence_data');
        $this->addSql('DROP TABLE ingredients_concerns');
        $this->addSql('DROP TABLE manufacturer_concerns');
        $this->addSql('ALTER TABLE products_categories CHANGE added added DATETIME NOT NULL, CHANGE updated updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE attribute CHANGE added added DATETIME NOT NULL');
        $this->addSql('ALTER TABLE products_retailers CHANGE added added DATETIME NOT NULL, CHANGE updated updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user_session DROP datetime_added_at, DROP datetime_updated_at');
        $this->addSql('ALTER TABLE ingredient CHANGE added added DATETIME NOT NULL, CHANGE updated updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE manufacturer CHANGE added added DATETIME NOT NULL, CHANGE updated updated DATETIME DEFAULT NULL');
        $this->addSql('DROP INDEX status01_idx ON manufacturer_attributes');
        $this->addSql('ALTER TABLE manufacturer_attributes CHANGE evidence_concern_id source_attribute_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE manufacturer_attributes ADD CONSTRAINT FK_E68FA570600CF242 FOREIGN KEY (source_attribute_id) REFERENCES source_attribute (id)');
        $this->addSql('CREATE INDEX status_idx ON manufacturer_attributes (source_attribute_id)');
        $this->addSql('DROP INDEX status_idx ON product_concerns');
        $this->addSql('ALTER TABLE product_concerns CHANGE evidence_concern_id source_evidence_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_concerns ADD CONSTRAINT FK_89B90BD8957ACB1 FOREIGN KEY (source_evidence_id) REFERENCES source_evidence (id)');
        $this->addSql('CREATE INDEX status_idx ON product_concerns (source_evidence_id)');
        $this->addSql('DROP INDEX status001_idx ON brand_attributes');
        $this->addSql('ALTER TABLE brand_attributes CHANGE evidence_attribute_id source_attribute_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE brand_attributes ADD CONSTRAINT FK_B44B8AE600CF242 FOREIGN KEY (source_attribute_id) REFERENCES source_attribute (id)');
        $this->addSql('CREATE INDEX source_attribute_idx ON brand_attributes (source_attribute_id)');
        $this->addSql('ALTER TABLE product CHANGE added added DATETIME NOT NULL, CHANGE updated updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE barcode CHANGE added added DATETIME DEFAULT NULL, CHANGE updated updated DATETIME DEFAULT NULL');
        $this->addSql('DROP INDEX status0000_idx ON product_attributes');
        $this->addSql('ALTER TABLE product_attributes CHANGE evidence_concern_id source_attribute_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_attributes ADD CONSTRAINT FK_A2FCC15B600CF242 FOREIGN KEY (source_attribute_id) REFERENCES source_attribute (id)');
        $this->addSql('CREATE INDEX source_attribute_idx ON product_attributes (source_attribute_id)');
        $this->addSql('ALTER TABLE product_image CHANGE added added DATETIME NOT NULL, CHANGE updated updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE data_provider ADD added DATETIME NOT NULL, DROP datetime_updated_at, CHANGE datetime_added_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE providers_products CHANGE added added DATETIME NOT NULL');
        $this->addSql('ALTER TABLE user ADD added DATETIME NOT NULL, DROP datetime_updated_at, CHANGE datetime_added_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE retailer CHANGE added added DATETIME NOT NULL, CHANGE updated updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE brands_ingredients CHANGE added added DATETIME NOT NULL, CHANGE updated updated DATETIME DEFAULT NULL');
        $this->addSql('DROP INDEX status1_idx ON ingredients_attributes');
        $this->addSql('ALTER TABLE ingredients_attributes CHANGE evidence_attribute_id source_attribute_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ingredients_attributes ADD CONSTRAINT FK_F79836E8600CF242 FOREIGN KEY (source_attribute_id) REFERENCES source_attribute (id)');
        $this->addSql('CREATE INDEX source_attribute_idx ON ingredients_attributes (source_attribute_id)');
        $this->addSql('ALTER TABLE brand CHANGE added added DATETIME DEFAULT NULL, CHANGE updated updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE ingestion_analytics ADD added DATETIME NOT NULL, ADD updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE products_ingredients CHANGE added added DATETIME NOT NULL, CHANGE updated updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE product_category CHANGE added added DATETIME NOT NULL, CHANGE updated updated DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE manufacturer_attributes DROP FOREIGN KEY FK_E68FA570600CF242');
        $this->addSql('ALTER TABLE brand_attributes DROP FOREIGN KEY FK_B44B8AE600CF242');
        $this->addSql('ALTER TABLE product_attributes DROP FOREIGN KEY FK_A2FCC15B600CF242');
        $this->addSql('ALTER TABLE ingredients_attributes DROP FOREIGN KEY FK_F79836E8600CF242');
        $this->addSql('ALTER TABLE evidence DROP FOREIGN KEY FK_C615710727ACA70');
        $this->addSql('ALTER TABLE source_evidence DROP FOREIGN KEY FK_4F4161C2B528FC11');
        $this->addSql('ALTER TABLE product_concerns DROP FOREIGN KEY FK_89B90BD8957ACB1');
        $this->addSql('ALTER TABLE ingredients_evidences DROP FOREIGN KEY FK_9C494028957ACB1');
        $this->addSql('ALTER TABLE manufacturer_evidences DROP FOREIGN KEY FK_1F5C50D98957ACB1');
        $this->addSql('ALTER TABLE brand_evidences DROP FOREIGN KEY FK_6CC2AE598957ACB1');
        $this->addSql('ALTER TABLE source_attribute DROP FOREIGN KEY FK_92E47C35953C1C61');
        $this->addSql('ALTER TABLE source_evidence DROP FOREIGN KEY FK_4F4161C2953C1C61');
        $this->addSql('CREATE TABLE brand_concerns (id INT AUTO_INCREMENT NOT NULL, brand_id INT DEFAULT NULL, evidence_concern_id INT DEFAULT NULL, INDEX status_idx (evidence_concern_id), INDEX brand_idx (brand_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE concern (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, added DATETIME DEFAULT NULL, updated DATETIME DEFAULT NULL, default_name VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, default_description TEXT DEFAULT NULL COLLATE utf8_unicode_ci, symbol VARCHAR(4) DEFAULT NULL COLLATE utf8_unicode_ci, INDEX parent_idx (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evidence_attribute (id INT AUTO_INCREMENT NOT NULL, attribute_id INT DEFAULT NULL, evidence_data_id INT DEFAULT NULL, status_code INT DEFAULT NULL, name VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, description TEXT DEFAULT NULL COLLATE utf8_unicode_ci, INDEX evidence_data_idx (evidence_data_id), INDEX concern0_idx (attribute_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evidence_concern (id INT AUTO_INCREMENT NOT NULL, concern_id INT DEFAULT NULL, evidence_data_id INT DEFAULT NULL, status_code INT DEFAULT NULL, name VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, description TEXT DEFAULT NULL COLLATE utf8_unicode_ci, INDEX concern_idx (concern_id), INDEX evidence_data_idx (evidence_data_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evidence_data (id INT AUTO_INCREMENT NOT NULL, data_provider_id INT DEFAULT NULL, added DATETIME DEFAULT NULL, updated DATETIME DEFAULT NULL, name VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, url VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, evidence_data_date DATETIME DEFAULT NULL, last_checked_date DATETIME DEFAULT NULL, INDEX data_provider_idx (data_provider_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ingredients_concerns (id INT AUTO_INCREMENT NOT NULL, ingredient_synonym_group_id INT DEFAULT NULL, evidence_concern_id INT DEFAULT NULL, INDEX status_idx (evidence_concern_id), INDEX ingredient_idx (ingredient_synonym_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE manufacturer_concerns (id INT AUTO_INCREMENT NOT NULL, manufacturer_id INT DEFAULT NULL, evidence_concern_id INT DEFAULT NULL, INDEX status_idx (evidence_concern_id), INDEX manufacturer_idx (manufacturer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE brand_concerns ADD CONSTRAINT FK_41B693B36AFE5FF FOREIGN KEY (evidence_concern_id) REFERENCES evidence_concern (id)');
        $this->addSql('ALTER TABLE brand_concerns ADD CONSTRAINT FK_41B693B44F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id)');
        $this->addSql('ALTER TABLE concern ADD CONSTRAINT FK_281EFBA8727ACA70 FOREIGN KEY (parent_id) REFERENCES concern (id)');
        $this->addSql('ALTER TABLE evidence_attribute ADD CONSTRAINT FK_5D9A29E0B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id)');
        $this->addSql('ALTER TABLE evidence_attribute ADD CONSTRAINT FK_5D9A29E0DF2C97AD FOREIGN KEY (evidence_data_id) REFERENCES evidence_data (id)');
        $this->addSql('ALTER TABLE evidence_concern ADD CONSTRAINT FK_C0C7CD39BC6DCCD5 FOREIGN KEY (concern_id) REFERENCES concern (id)');
        $this->addSql('ALTER TABLE evidence_concern ADD CONSTRAINT FK_C0C7CD39DF2C97AD FOREIGN KEY (evidence_data_id) REFERENCES evidence_data (id)');
        $this->addSql('ALTER TABLE evidence_data ADD CONSTRAINT FK_234CCDFAF593F7E0 FOREIGN KEY (data_provider_id) REFERENCES data_provider (id)');
        $this->addSql('ALTER TABLE ingredients_concerns ADD CONSTRAINT FK_B2F8F46336AFE5FF FOREIGN KEY (evidence_concern_id) REFERENCES evidence_concern (id)');
        $this->addSql('ALTER TABLE ingredients_concerns ADD CONSTRAINT FK_B2F8F4636259AE7B FOREIGN KEY (ingredient_synonym_group_id) REFERENCES ingredient_synonym_group (id)');
        $this->addSql('ALTER TABLE manufacturer_concerns ADD CONSTRAINT FK_46F36ABC36AFE5FF FOREIGN KEY (evidence_concern_id) REFERENCES evidence_concern (id)');
        $this->addSql('ALTER TABLE manufacturer_concerns ADD CONSTRAINT FK_46F36ABCA23B42D FOREIGN KEY (manufacturer_id) REFERENCES manufacturer (id)');
        $this->addSql('DROP TABLE source_attribute');
        $this->addSql('DROP TABLE evidence');
        $this->addSql('DROP TABLE ingredients_evidences');
        $this->addSql('DROP TABLE source_evidence');
        $this->addSql('DROP TABLE source');
        $this->addSql('DROP TABLE manufacturer_evidences');
        $this->addSql('DROP TABLE brand_evidences');
        $this->addSql('ALTER TABLE attribute CHANGE added added DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE barcode CHANGE added added VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE updated updated VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE brand CHANGE added added VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE updated updated VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('DROP INDEX source_attribute_idx ON brand_attributes');
        $this->addSql('ALTER TABLE brand_attributes CHANGE source_attribute_id evidence_attribute_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE brand_attributes ADD CONSTRAINT FK_B44B8AE9B3FF07C FOREIGN KEY (evidence_attribute_id) REFERENCES evidence_attribute (id)');
        $this->addSql('CREATE INDEX status001_idx ON brand_attributes (evidence_attribute_id)');
        $this->addSql('ALTER TABLE brands_ingredients CHANGE added added VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE updated updated VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE data_provider ADD datetime_updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, DROP added, CHANGE updated datetime_added_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE ingestion_analytics DROP added, DROP updated');
        $this->addSql('ALTER TABLE ingredient CHANGE added added VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE updated updated VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('DROP INDEX source_attribute_idx ON ingredients_attributes');
        $this->addSql('ALTER TABLE ingredients_attributes CHANGE source_attribute_id evidence_attribute_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ingredients_attributes ADD CONSTRAINT FK_F79836E89B3FF07C FOREIGN KEY (evidence_attribute_id) REFERENCES evidence_attribute (id)');
        $this->addSql('CREATE INDEX status1_idx ON ingredients_attributes (evidence_attribute_id)');
        $this->addSql('ALTER TABLE manufacturer CHANGE added added VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE updated updated VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('DROP INDEX status_idx ON manufacturer_attributes');
        $this->addSql('ALTER TABLE manufacturer_attributes CHANGE source_attribute_id evidence_concern_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE manufacturer_attributes ADD CONSTRAINT FK_E68FA57036AFE5FF FOREIGN KEY (evidence_concern_id) REFERENCES evidence_attribute (id)');
        $this->addSql('CREATE INDEX status01_idx ON manufacturer_attributes (evidence_concern_id)');
        $this->addSql('ALTER TABLE product CHANGE added added VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE updated updated VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('DROP INDEX source_attribute_idx ON product_attributes');
        $this->addSql('ALTER TABLE product_attributes CHANGE source_attribute_id evidence_concern_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_attributes ADD CONSTRAINT FK_A2FCC15B36AFE5FF FOREIGN KEY (evidence_concern_id) REFERENCES evidence_attribute (id)');
        $this->addSql('CREATE INDEX status0000_idx ON product_attributes (evidence_concern_id)');
        $this->addSql('ALTER TABLE product_category CHANGE added added VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE updated updated VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('DROP INDEX status_idx ON product_concerns');
        $this->addSql('ALTER TABLE product_concerns CHANGE source_evidence_id evidence_concern_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_concerns ADD CONSTRAINT FK_89B90BD36AFE5FF FOREIGN KEY (evidence_concern_id) REFERENCES evidence_concern (id)');
        $this->addSql('CREATE INDEX status_idx ON product_concerns (evidence_concern_id)');
        $this->addSql('ALTER TABLE product_image CHANGE added added VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE updated updated VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE products_categories CHANGE added added VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE updated updated VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE products_ingredients CHANGE added added VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE updated updated VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE products_retailers CHANGE added added VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE updated updated VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE providers_products CHANGE added added DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE retailer CHANGE added added VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE updated updated VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE user ADD datetime_updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, DROP added, CHANGE updated datetime_added_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user_session ADD datetime_added_at DATETIME DEFAULT NULL, ADD datetime_updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }
}
