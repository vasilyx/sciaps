<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180814141824 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company (id INT AUTO_INCREMENT NOT NULL, added_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, company_name VARCHAR(45) DEFAULT NULL, contact_name VARCHAR(45) DEFAULT NULL, website_url VARCHAR(45) DEFAULT NULL, email_domain VARCHAR(45) DEFAULT NULL, phone VARCHAR(45) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_companies (company_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_E439D0DB979B1AD6 (company_id), INDEX IDX_E439D0DBA76ED395 (user_id), PRIMARY KEY(company_id, user_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE users_companies ADD CONSTRAINT FK_E439D0DB979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE users_companies ADD CONSTRAINT FK_E439D0DBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('DROP TABLE users_data_providers');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users_companies DROP FOREIGN KEY FK_E439D0DB979B1AD6');
        $this->addSql('CREATE TABLE users_data_providers (data_provider_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_13320D94F593F7E0 (data_provider_id), INDEX IDX_13320D94A76ED395 (user_id), PRIMARY KEY(data_provider_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE users_data_providers ADD CONSTRAINT FK_13320D94A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE users_data_providers ADD CONSTRAINT FK_13320D94F593F7E0 FOREIGN KEY (data_provider_id) REFERENCES data_provider (id)');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE users_companies');
    }
}
