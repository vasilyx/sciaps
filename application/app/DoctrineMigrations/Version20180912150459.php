<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180912150459 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();
        try {
            $this->addSql('ALTER TABLE user CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
            $this->addSql('ALTER TABLE ingredients_evidences DROP FOREIGN KEY FK_9C49402953C1C61');
            $this->addSql('ALTER TABLE ingredients_evidences DROP FOREIGN KEY FK_9C49402B528FC11');
            $this->addSql('ALTER TABLE ingredients_evidences ADD CONSTRAINT FK_9C49402953C1C61 FOREIGN KEY (source_id) REFERENCES source (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE ingredients_evidences ADD CONSTRAINT FK_9C49402B528FC11 FOREIGN KEY (evidence_id) REFERENCES evidence (id)');

            $conn->commit();
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
