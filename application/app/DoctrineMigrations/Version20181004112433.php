<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181004112433 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();

        try {
            $this->addSql('CREATE TABLE ingredient_unfiltered (id INT AUTO_INCREMENT NOT NULL, added_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, match_type INT DEFAULT NULL, ingredient_id INT DEFAULT NULL, UNIQUE INDEX ingredient_unfiltered_name (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
            $this->addSql('ALTER TABLE products_ingredients DROP FOREIGN KEY FK_B1BD73D4933FE08C');
            $this->addSql('DROP INDEX ingredient_idx ON products_ingredients');
            $this->addSql('ALTER TABLE products_ingredients ADD ingredient_unfiltered_id INT DEFAULT NULL, DROP ingredient_id, DROP name, DROP match_type');
            $this->addSql('ALTER TABLE products_ingredients ADD CONSTRAINT FK_B1BD73D451BF854C FOREIGN KEY (ingredient_unfiltered_id) REFERENCES ingredient_unfiltered (id)');
            $this->addSql('CREATE INDEX ingredient_idx ON products_ingredients (ingredient_unfiltered_id)');
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
