<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180911153718 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE brand_attributes DROP FOREIGN KEY FK_B44B8AE600CF242');
        $this->addSql('ALTER TABLE ingredients_attributes DROP FOREIGN KEY FK_F79836E8600CF242');
        $this->addSql('ALTER TABLE manufacturer_attributes DROP FOREIGN KEY FK_E68FA570600CF242');
        $this->addSql('ALTER TABLE product_attributes DROP FOREIGN KEY FK_A2FCC15B600CF242');
        $this->addSql('ALTER TABLE brand_evidences DROP FOREIGN KEY FK_6CC2AE598957ACB1');
        $this->addSql('ALTER TABLE ingredients_evidences DROP FOREIGN KEY FK_9C494028957ACB1');
        $this->addSql('ALTER TABLE manufacturer_evidences DROP FOREIGN KEY FK_1F5C50D98957ACB1');
        $this->addSql('ALTER TABLE product_evidences DROP FOREIGN KEY FK_681508B58957ACB1');
        $this->addSql('DROP TABLE source_attribute');
        $this->addSql('DROP TABLE source_evidence');
        $this->addSql('DROP INDEX source_attribute_idx ON brand_attributes');
        $this->addSql('ALTER TABLE brand_attributes ADD attribute_id INT DEFAULT NULL, ADD status_code INT DEFAULT NULL, ADD description TEXT DEFAULT NULL, CHANGE source_attribute_id source_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE brand_attributes ADD CONSTRAINT FK_B44B8AE953C1C61 FOREIGN KEY (source_id) REFERENCES source (id)');
        $this->addSql('ALTER TABLE brand_attributes ADD CONSTRAINT FK_B44B8AEB6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id)');
        $this->addSql('CREATE INDEX source_idx ON brand_attributes (source_id)');
        $this->addSql('CREATE INDEX attribute_idx ON brand_attributes (attribute_id)');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('DROP INDEX source_attribute_idx ON product_attributes');
        $this->addSql('ALTER TABLE product_attributes ADD attribute_id INT DEFAULT NULL, ADD status_code INT DEFAULT NULL, ADD description TEXT DEFAULT NULL, CHANGE source_attribute_id source_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_attributes ADD CONSTRAINT FK_A2FCC15B953C1C61 FOREIGN KEY (source_id) REFERENCES source (id)');
        $this->addSql('ALTER TABLE product_attributes ADD CONSTRAINT FK_A2FCC15BB6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id)');
        $this->addSql('CREATE INDEX source_idx ON product_attributes (source_id)');
        $this->addSql('CREATE INDEX attribute_idx ON product_attributes (attribute_id)');
        $this->addSql('DROP INDEX status_idx ON product_evidences');
        $this->addSql('ALTER TABLE product_evidences ADD evidence_id INT DEFAULT NULL, ADD status_code INT DEFAULT NULL, ADD description TEXT DEFAULT NULL, CHANGE source_evidence_id source_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_evidences ADD CONSTRAINT FK_681508B5953C1C61 FOREIGN KEY (source_id) REFERENCES source (id)');
        $this->addSql('ALTER TABLE product_evidences ADD CONSTRAINT FK_681508B5B528FC11 FOREIGN KEY (evidence_id) REFERENCES evidence (id)');
        $this->addSql('CREATE INDEX source_idx ON product_evidences (source_id)');
        $this->addSql('CREATE INDEX evidence_idx ON product_evidences (evidence_id)');
        $this->addSql('DROP INDEX status_idx ON ingredients_evidences');
        $this->addSql('ALTER TABLE ingredients_evidences ADD evidence_id INT DEFAULT NULL, ADD status_code INT DEFAULT NULL, ADD description TEXT DEFAULT NULL, CHANGE source_evidence_id source_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ingredients_evidences ADD CONSTRAINT FK_9C49402953C1C61 FOREIGN KEY (source_id) REFERENCES source (id)');
        $this->addSql('ALTER TABLE ingredients_evidences ADD CONSTRAINT FK_9C49402B528FC11 FOREIGN KEY (evidence_id) REFERENCES evidence (id)');
        $this->addSql('CREATE INDEX source_idx ON ingredients_evidences (source_id)');
        $this->addSql('CREATE INDEX evidence_idx ON ingredients_evidences (evidence_id)');
        $this->addSql('DROP INDEX status_idx ON brand_evidences');
        $this->addSql('ALTER TABLE brand_evidences ADD evidence_id INT DEFAULT NULL, ADD status_code INT DEFAULT NULL, ADD description TEXT DEFAULT NULL, CHANGE source_evidence_id source_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE brand_evidences ADD CONSTRAINT FK_6CC2AE59953C1C61 FOREIGN KEY (source_id) REFERENCES source (id)');
        $this->addSql('ALTER TABLE brand_evidences ADD CONSTRAINT FK_6CC2AE59B528FC11 FOREIGN KEY (evidence_id) REFERENCES evidence (id)');
        $this->addSql('CREATE INDEX source_idx ON brand_evidences (source_id)');
        $this->addSql('CREATE INDEX evidence_idx ON brand_evidences (evidence_id)');
        $this->addSql('DROP INDEX status_idx ON manufacturer_attributes');
        $this->addSql('ALTER TABLE manufacturer_attributes ADD attribute_id INT DEFAULT NULL, ADD status_code INT DEFAULT NULL, ADD description TEXT DEFAULT NULL, CHANGE source_attribute_id source_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE manufacturer_attributes ADD CONSTRAINT FK_E68FA570953C1C61 FOREIGN KEY (source_id) REFERENCES source (id)');
        $this->addSql('ALTER TABLE manufacturer_attributes ADD CONSTRAINT FK_E68FA570B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id)');
        $this->addSql('CREATE INDEX source_idx ON manufacturer_attributes (source_id)');
        $this->addSql('CREATE INDEX attribute_idx ON manufacturer_attributes (attribute_id)');
        $this->addSql('DROP INDEX status_idx ON manufacturer_evidences');
        $this->addSql('ALTER TABLE manufacturer_evidences ADD evidence_id INT DEFAULT NULL, ADD status_code INT DEFAULT NULL, ADD description TEXT DEFAULT NULL, CHANGE source_evidence_id source_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE manufacturer_evidences ADD CONSTRAINT FK_1F5C50D9953C1C61 FOREIGN KEY (source_id) REFERENCES source (id)');
        $this->addSql('ALTER TABLE manufacturer_evidences ADD CONSTRAINT FK_1F5C50D9B528FC11 FOREIGN KEY (evidence_id) REFERENCES evidence (id)');
        $this->addSql('CREATE INDEX source_idx ON manufacturer_evidences (source_id)');
        $this->addSql('CREATE INDEX evidence_idx ON manufacturer_evidences (evidence_id)');
        $this->addSql('DROP INDEX source_attribute_idx ON ingredients_attributes');
        $this->addSql('ALTER TABLE ingredients_attributes ADD attribute_id INT DEFAULT NULL, ADD status_code INT DEFAULT NULL, ADD description TEXT DEFAULT NULL, CHANGE source_attribute_id source_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ingredients_attributes ADD CONSTRAINT FK_F79836E8953C1C61 FOREIGN KEY (source_id) REFERENCES source (id)');
        $this->addSql('ALTER TABLE ingredients_attributes ADD CONSTRAINT FK_F79836E8B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id)');
        $this->addSql('CREATE INDEX source_idx ON ingredients_attributes (source_id)');
        $this->addSql('CREATE INDEX attribute_idx ON ingredients_attributes (attribute_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE source_attribute (id INT AUTO_INCREMENT NOT NULL, attribute_id INT DEFAULT NULL, source_id INT DEFAULT NULL, status_code INT DEFAULT NULL, name VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, description TEXT DEFAULT NULL COLLATE utf8_unicode_ci, INDEX source_idx (source_id), INDEX attribute_idx (attribute_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE source_evidence (id INT AUTO_INCREMENT NOT NULL, evidence_id INT DEFAULT NULL, source_id INT DEFAULT NULL, status_code INT DEFAULT NULL, name VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, description TEXT DEFAULT NULL COLLATE utf8_unicode_ci, INDEX source_idx (source_id), INDEX evidence_idx (evidence_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE source_attribute ADD CONSTRAINT FK_92E47C35953C1C61 FOREIGN KEY (source_id) REFERENCES source (id)');
        $this->addSql('ALTER TABLE source_attribute ADD CONSTRAINT FK_92E47C35B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id)');
        $this->addSql('ALTER TABLE source_evidence ADD CONSTRAINT FK_4F4161C2953C1C61 FOREIGN KEY (source_id) REFERENCES source (id)');
        $this->addSql('ALTER TABLE source_evidence ADD CONSTRAINT FK_4F4161C2B528FC11 FOREIGN KEY (evidence_id) REFERENCES evidence (id)');
        $this->addSql('ALTER TABLE brand_attributes DROP FOREIGN KEY FK_B44B8AE953C1C61');
        $this->addSql('ALTER TABLE brand_attributes DROP FOREIGN KEY FK_B44B8AEB6E62EFA');
        $this->addSql('DROP INDEX source_idx ON brand_attributes');
        $this->addSql('DROP INDEX attribute_idx ON brand_attributes');
        $this->addSql('ALTER TABLE brand_attributes ADD source_attribute_id INT DEFAULT NULL, DROP source_id, DROP attribute_id, DROP status_code, DROP description');
        $this->addSql('ALTER TABLE brand_attributes ADD CONSTRAINT FK_B44B8AE600CF242 FOREIGN KEY (source_attribute_id) REFERENCES source_attribute (id)');
        $this->addSql('CREATE INDEX source_attribute_idx ON brand_attributes (source_attribute_id)');
        $this->addSql('ALTER TABLE brand_evidences DROP FOREIGN KEY FK_6CC2AE59953C1C61');
        $this->addSql('ALTER TABLE brand_evidences DROP FOREIGN KEY FK_6CC2AE59B528FC11');
        $this->addSql('DROP INDEX source_idx ON brand_evidences');
        $this->addSql('DROP INDEX evidence_idx ON brand_evidences');
        $this->addSql('ALTER TABLE brand_evidences ADD source_evidence_id INT DEFAULT NULL, DROP source_id, DROP evidence_id, DROP status_code, DROP description');
        $this->addSql('ALTER TABLE brand_evidences ADD CONSTRAINT FK_6CC2AE598957ACB1 FOREIGN KEY (source_evidence_id) REFERENCES source_evidence (id)');
        $this->addSql('CREATE INDEX status_idx ON brand_evidences (source_evidence_id)');
        $this->addSql('ALTER TABLE ingredients_attributes DROP FOREIGN KEY FK_F79836E8953C1C61');
        $this->addSql('ALTER TABLE ingredients_attributes DROP FOREIGN KEY FK_F79836E8B6E62EFA');
        $this->addSql('DROP INDEX source_idx ON ingredients_attributes');
        $this->addSql('DROP INDEX attribute_idx ON ingredients_attributes');
        $this->addSql('ALTER TABLE ingredients_attributes ADD source_attribute_id INT DEFAULT NULL, DROP source_id, DROP attribute_id, DROP status_code, DROP description');
        $this->addSql('ALTER TABLE ingredients_attributes ADD CONSTRAINT FK_F79836E8600CF242 FOREIGN KEY (source_attribute_id) REFERENCES source_attribute (id)');
        $this->addSql('CREATE INDEX source_attribute_idx ON ingredients_attributes (source_attribute_id)');
        $this->addSql('ALTER TABLE ingredients_evidences DROP FOREIGN KEY FK_9C49402953C1C61');
        $this->addSql('ALTER TABLE ingredients_evidences DROP FOREIGN KEY FK_9C49402B528FC11');
        $this->addSql('DROP INDEX source_idx ON ingredients_evidences');
        $this->addSql('DROP INDEX evidence_idx ON ingredients_evidences');
        $this->addSql('ALTER TABLE ingredients_evidences ADD source_evidence_id INT DEFAULT NULL, DROP source_id, DROP evidence_id, DROP status_code, DROP description');
        $this->addSql('ALTER TABLE ingredients_evidences ADD CONSTRAINT FK_9C494028957ACB1 FOREIGN KEY (source_evidence_id) REFERENCES source_evidence (id)');
        $this->addSql('CREATE INDEX status_idx ON ingredients_evidences (source_evidence_id)');
        $this->addSql('ALTER TABLE manufacturer_attributes DROP FOREIGN KEY FK_E68FA570953C1C61');
        $this->addSql('ALTER TABLE manufacturer_attributes DROP FOREIGN KEY FK_E68FA570B6E62EFA');
        $this->addSql('DROP INDEX source_idx ON manufacturer_attributes');
        $this->addSql('DROP INDEX attribute_idx ON manufacturer_attributes');
        $this->addSql('ALTER TABLE manufacturer_attributes ADD source_attribute_id INT DEFAULT NULL, DROP source_id, DROP attribute_id, DROP status_code, DROP description');
        $this->addSql('ALTER TABLE manufacturer_attributes ADD CONSTRAINT FK_E68FA570600CF242 FOREIGN KEY (source_attribute_id) REFERENCES source_attribute (id)');
        $this->addSql('CREATE INDEX status_idx ON manufacturer_attributes (source_attribute_id)');
        $this->addSql('ALTER TABLE manufacturer_evidences DROP FOREIGN KEY FK_1F5C50D9953C1C61');
        $this->addSql('ALTER TABLE manufacturer_evidences DROP FOREIGN KEY FK_1F5C50D9B528FC11');
        $this->addSql('DROP INDEX source_idx ON manufacturer_evidences');
        $this->addSql('DROP INDEX evidence_idx ON manufacturer_evidences');
        $this->addSql('ALTER TABLE manufacturer_evidences ADD source_evidence_id INT DEFAULT NULL, DROP source_id, DROP evidence_id, DROP status_code, DROP description');
        $this->addSql('ALTER TABLE manufacturer_evidences ADD CONSTRAINT FK_1F5C50D98957ACB1 FOREIGN KEY (source_evidence_id) REFERENCES source_evidence (id)');
        $this->addSql('CREATE INDEX status_idx ON manufacturer_evidences (source_evidence_id)');
        $this->addSql('ALTER TABLE product_attributes DROP FOREIGN KEY FK_A2FCC15B953C1C61');
        $this->addSql('ALTER TABLE product_attributes DROP FOREIGN KEY FK_A2FCC15BB6E62EFA');
        $this->addSql('DROP INDEX source_idx ON product_attributes');
        $this->addSql('DROP INDEX attribute_idx ON product_attributes');
        $this->addSql('ALTER TABLE product_attributes ADD source_attribute_id INT DEFAULT NULL, DROP source_id, DROP attribute_id, DROP status_code, DROP description');
        $this->addSql('ALTER TABLE product_attributes ADD CONSTRAINT FK_A2FCC15B600CF242 FOREIGN KEY (source_attribute_id) REFERENCES source_attribute (id)');
        $this->addSql('CREATE INDEX source_attribute_idx ON product_attributes (source_attribute_id)');
        $this->addSql('ALTER TABLE product_evidences DROP FOREIGN KEY FK_681508B5953C1C61');
        $this->addSql('ALTER TABLE product_evidences DROP FOREIGN KEY FK_681508B5B528FC11');
        $this->addSql('DROP INDEX source_idx ON product_evidences');
        $this->addSql('DROP INDEX evidence_idx ON product_evidences');
        $this->addSql('ALTER TABLE product_evidences ADD source_evidence_id INT DEFAULT NULL, DROP source_id, DROP evidence_id, DROP status_code, DROP description');
        $this->addSql('ALTER TABLE product_evidences ADD CONSTRAINT FK_681508B58957ACB1 FOREIGN KEY (source_evidence_id) REFERENCES source_evidence (id)');
        $this->addSql('CREATE INDEX status_idx ON product_evidences (source_evidence_id)');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }
}
