<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181015102617 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();

        try {
            $this->addSql('CREATE TABLE ingredient_synonym (id INT AUTO_INCREMENT NOT NULL, ingredient_id INT DEFAULT NULL, cid INT NOT NULL, INDEX ingredient_idx (ingredient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
            $this->addSql('ALTER TABLE ingredient_synonym ADD CONSTRAINT FK_A706550E933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id)');
            $this->addSql('DROP TABLE ingredient_synonym_group');

            $conn->commit();
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
