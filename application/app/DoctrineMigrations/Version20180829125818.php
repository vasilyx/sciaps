<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180829125818 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FF593F7E0');
        $this->addSql('ALTER TABLE ingestion_analytics DROP FOREIGN KEY FK_F07A521AF593F7E0');
        $this->addSql('ALTER TABLE ingestion_report DROP FOREIGN KEY FK_8B163774F593F7E0');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADF593F7E0');
        $this->addSql('ALTER TABLE product_category DROP FOREIGN KEY FK_CDFC7356F593F7E0');
        $this->addSql('CREATE TABLE product_provider (id INT AUTO_INCREMENT NOT NULL, added_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, product_provider_type INT NOT NULL, status INT NOT NULL, connection_type INT NOT NULL, company_name VARCHAR(45) DEFAULT NULL, display_name VARCHAR(45) DEFAULT NULL, website_url VARCHAR(45) DEFAULT NULL, email_domain VARCHAR(45) DEFAULT NULL, ingestion_reports TINYINT(1) DEFAULT NULL, ingestion_analytics TINYINT(1) DEFAULT NULL, is_deleted TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE data_provider');
        $this->addSql('DROP INDEX data_provider_idx ON ingestion_report');
        $this->addSql('ALTER TABLE ingestion_report CHANGE data_provider_id product_provider_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ingestion_report ADD CONSTRAINT FK_8B163774A67C36B5 FOREIGN KEY (product_provider_id) REFERENCES product_provider (id)');
        $this->addSql('CREATE INDEX product_provider_idx ON ingestion_report (product_provider_id)');
        $this->addSql('DROP INDEX data_provider_idx ON ingestion_analytics');
        $this->addSql('ALTER TABLE ingestion_analytics CHANGE data_provider_id product_provider_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ingestion_analytics ADD CONSTRAINT FK_F07A521AA67C36B5 FOREIGN KEY (product_provider_id) REFERENCES product_provider (id)');
        $this->addSql('CREATE INDEX product_provider_idx ON ingestion_analytics (product_provider_id)');
        $this->addSql('DROP INDEX IDX_D34A04ADF593F7E0 ON product');
        $this->addSql('ALTER TABLE product CHANGE data_provider_id product_provider_id INT NOT NULL');
        $this->addSql('DELETE FROM product');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADA67C36B5 FOREIGN KEY (product_provider_id) REFERENCES product_provider (id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADA67C36B5 ON product (product_provider_id)');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE retailer CHANGE data_provider_id product_provider_id INT DEFAULT NULL');
        $this->addSql('DROP INDEX data_provider_idx ON product_category');
        $this->addSql('ALTER TABLE product_category CHANGE data_provider_id product_provider_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC7356A67C36B5 FOREIGN KEY (product_provider_id) REFERENCES product_provider (id)');
        $this->addSql('CREATE INDEX product_provider_idx ON product_category (product_provider_id)');
        $this->addSql('DROP INDEX UNIQ_4FBF094FF593F7E0 ON company');
        $this->addSql('ALTER TABLE company CHANGE data_provider_id product_provider_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FA67C36B5 FOREIGN KEY (product_provider_id) REFERENCES product_provider (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094FA67C36B5 ON company (product_provider_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ingestion_report DROP FOREIGN KEY FK_8B163774A67C36B5');
        $this->addSql('ALTER TABLE ingestion_analytics DROP FOREIGN KEY FK_F07A521AA67C36B5');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADA67C36B5');
        $this->addSql('ALTER TABLE product_category DROP FOREIGN KEY FK_CDFC7356A67C36B5');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FA67C36B5');
        $this->addSql('CREATE TABLE data_provider (id INT AUTO_INCREMENT NOT NULL, status INT NOT NULL, ingestion_reports TINYINT(1) DEFAULT NULL, ingestion_analytics TINYINT(1) DEFAULT NULL, data_provider_type INT NOT NULL, company_name VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, display_name VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, website_url VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, email_domain VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, updated_at DATETIME DEFAULT NULL, added_at DATETIME NOT NULL, connection_type INT NOT NULL, is_deleted TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE product_provider');
        $this->addSql('DROP INDEX UNIQ_4FBF094FA67C36B5 ON company');
        $this->addSql('ALTER TABLE company CHANGE product_provider_id data_provider_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FF593F7E0 FOREIGN KEY (data_provider_id) REFERENCES data_provider (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094FF593F7E0 ON company (data_provider_id)');
        $this->addSql('DROP INDEX product_provider_idx ON ingestion_analytics');
        $this->addSql('ALTER TABLE ingestion_analytics CHANGE product_provider_id data_provider_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ingestion_analytics ADD CONSTRAINT FK_F07A521AF593F7E0 FOREIGN KEY (data_provider_id) REFERENCES data_provider (id)');
        $this->addSql('CREATE INDEX data_provider_idx ON ingestion_analytics (data_provider_id)');
        $this->addSql('DROP INDEX product_provider_idx ON ingestion_report');
        $this->addSql('ALTER TABLE ingestion_report CHANGE product_provider_id data_provider_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ingestion_report ADD CONSTRAINT FK_8B163774F593F7E0 FOREIGN KEY (data_provider_id) REFERENCES data_provider (id)');
        $this->addSql('CREATE INDEX data_provider_idx ON ingestion_report (data_provider_id)');
        $this->addSql('DROP INDEX IDX_D34A04ADA67C36B5 ON product');
        $this->addSql('ALTER TABLE product CHANGE product_provider_id data_provider_id INT NOT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADF593F7E0 FOREIGN KEY (data_provider_id) REFERENCES data_provider (id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADF593F7E0 ON product (data_provider_id)');
        $this->addSql('DROP INDEX product_provider_idx ON product_category');
        $this->addSql('ALTER TABLE product_category CHANGE product_provider_id data_provider_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC7356F593F7E0 FOREIGN KEY (data_provider_id) REFERENCES data_provider (id)');
        $this->addSql('CREATE INDEX data_provider_idx ON product_category (data_provider_id)');
        $this->addSql('ALTER TABLE retailer CHANGE product_provider_id data_provider_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }
}
