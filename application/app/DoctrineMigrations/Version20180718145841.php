<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180718145841 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE products_categories CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE attribute CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE products_retailers CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE evidence CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE ingredient CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE manufacturer CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE product CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE barcode ADD added_at DATETIME NOT NULL, ADD updated_at DATETIME DEFAULT NULL, DROP added, DROP updated');
        $this->addSql('ALTER TABLE product_image CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE data_provider CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE providers_products CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE retailer CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE source CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE brands_ingredients CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE brand ADD added_at DATETIME NOT NULL, ADD updated_at DATETIME DEFAULT NULL, DROP added, DROP updated');
        $this->addSql('ALTER TABLE ingestion_analytics CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE products_ingredients CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE product_category CHANGE added added_at DATETIME NOT NULL, CHANGE updated updated_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE attribute CHANGE added_at added DATETIME NOT NULL, CHANGE updated_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE barcode ADD updated DATETIME DEFAULT NULL, DROP added_at, CHANGE updated_at added DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE brand ADD updated DATETIME DEFAULT NULL, DROP added_at, CHANGE updated_at added DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE brands_ingredients CHANGE added_at added DATETIME NOT NULL, CHANGE updated_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE data_provider CHANGE updated_at updated DATETIME DEFAULT NULL, CHANGE added_at added DATETIME NOT NULL');
        $this->addSql('ALTER TABLE evidence CHANGE added_at added DATETIME NOT NULL, CHANGE updated_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE ingestion_analytics CHANGE added_at added DATETIME NOT NULL, CHANGE updated_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE ingredient CHANGE added_at added DATETIME NOT NULL, CHANGE updated_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE manufacturer CHANGE added_at added DATETIME NOT NULL, CHANGE updated_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE product CHANGE added_at added DATETIME NOT NULL, CHANGE updated_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE product_category CHANGE added_at added DATETIME NOT NULL, CHANGE updated_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE product_image CHANGE added_at added DATETIME NOT NULL, CHANGE updated_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE products_categories CHANGE added_at added DATETIME NOT NULL, CHANGE updated_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE products_ingredients CHANGE added_at added DATETIME NOT NULL, CHANGE updated_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE products_retailers CHANGE added_at added DATETIME NOT NULL, CHANGE updated_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE providers_products CHANGE added_at added DATETIME NOT NULL, CHANGE updated_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE retailer CHANGE added_at added DATETIME NOT NULL, CHANGE updated_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE source CHANGE added_at added DATETIME NOT NULL, CHANGE updated_at updated DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated DATETIME DEFAULT NULL, CHANGE added_at added DATETIME NOT NULL');
    }
}
