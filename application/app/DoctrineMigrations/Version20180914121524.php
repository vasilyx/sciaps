<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180914121524 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();
        try {
            $this->addSql('ALTER TABLE brand_attributes DROP FOREIGN KEY FK_B44B8AE44F5D008');
            $this->addSql('ALTER TABLE brand_attributes DROP FOREIGN KEY FK_B44B8AEB6E62EFA');
            $this->addSql('ALTER TABLE brand_attributes ADD CONSTRAINT FK_B44B8AE44F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE brand_attributes ADD CONSTRAINT FK_B44B8AEB6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE');
            $this->addSql('CREATE UNIQUE INDEX sourceAttrBrand_UNIQUE ON brand_attributes (source_id, attribute_id, brand_id)');
            $this->addSql('ALTER TABLE user CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
            $this->addSql('ALTER TABLE product_attributes DROP FOREIGN KEY FK_A2FCC15B4584665A');
            $this->addSql('ALTER TABLE product_attributes DROP FOREIGN KEY FK_A2FCC15BB6E62EFA');
            $this->addSql('ALTER TABLE product_attributes ADD CONSTRAINT FK_A2FCC15B4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE product_attributes ADD CONSTRAINT FK_A2FCC15BB6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE');
            $this->addSql('CREATE UNIQUE INDEX sourceAttrProd_UNIQUE ON product_attributes (source_id, attribute_id, product_id)');
            $this->addSql('ALTER TABLE log_event CHANGE added_at added_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
            $this->addSql('ALTER TABLE product_evidences DROP FOREIGN KEY FK_681508B54584665A');
            $this->addSql('ALTER TABLE product_evidences DROP FOREIGN KEY FK_681508B5B528FC11');
            $this->addSql('ALTER TABLE product_evidences ADD CONSTRAINT FK_681508B54584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE product_evidences ADD CONSTRAINT FK_681508B5B528FC11 FOREIGN KEY (evidence_id) REFERENCES evidence (id) ON DELETE CASCADE');
            $this->addSql('CREATE UNIQUE INDEX sourceEvidProd_UNIQUE ON product_evidences (source_id, evidence_id, product_id)');
            $this->addSql('ALTER TABLE ingredients_evidences DROP FOREIGN KEY FK_9C49402933FE08C');
            $this->addSql('ALTER TABLE ingredients_evidences DROP FOREIGN KEY FK_9C49402B528FC11');
            $this->addSql('ALTER TABLE ingredients_evidences ADD CONSTRAINT FK_9C49402933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE ingredients_evidences ADD CONSTRAINT FK_9C49402B528FC11 FOREIGN KEY (evidence_id) REFERENCES evidence (id) ON DELETE CASCADE');
            $this->addSql('CREATE UNIQUE INDEX sourceEvidIngred_UNIQUE ON ingredients_evidences (source_id, evidence_id, ingredient_id)');
            $this->addSql('ALTER TABLE brand_evidences DROP FOREIGN KEY FK_6CC2AE5944F5D008');
            $this->addSql('ALTER TABLE brand_evidences DROP FOREIGN KEY FK_6CC2AE59B528FC11');
            $this->addSql('ALTER TABLE brand_evidences ADD CONSTRAINT FK_6CC2AE5944F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE brand_evidences ADD CONSTRAINT FK_6CC2AE59B528FC11 FOREIGN KEY (evidence_id) REFERENCES evidence (id) ON DELETE CASCADE');
            $this->addSql('CREATE UNIQUE INDEX sourceEvidBrand_UNIQUE ON brand_evidences (source_id, evidence_id, brand_id)');
            $this->addSql('ALTER TABLE manufacturer_attributes DROP FOREIGN KEY FK_E68FA570A23B42D');
            $this->addSql('ALTER TABLE manufacturer_attributes DROP FOREIGN KEY FK_E68FA570B6E62EFA');
            $this->addSql('ALTER TABLE manufacturer_attributes ADD CONSTRAINT FK_E68FA570A23B42D FOREIGN KEY (manufacturer_id) REFERENCES manufacturer (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE manufacturer_attributes ADD CONSTRAINT FK_E68FA570B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE');
            $this->addSql('CREATE UNIQUE INDEX sourceAttrManuf_UNIQUE ON manufacturer_attributes (source_id, attribute_id, manufacturer_id)');
            $this->addSql('ALTER TABLE crowdsource_pending CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
            $this->addSql('ALTER TABLE manufacturer_evidences DROP FOREIGN KEY FK_1F5C50D9A23B42D');
            $this->addSql('ALTER TABLE manufacturer_evidences DROP FOREIGN KEY FK_1F5C50D9B528FC11');
            $this->addSql('ALTER TABLE manufacturer_evidences ADD CONSTRAINT FK_1F5C50D9A23B42D FOREIGN KEY (manufacturer_id) REFERENCES manufacturer (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE manufacturer_evidences ADD CONSTRAINT FK_1F5C50D9B528FC11 FOREIGN KEY (evidence_id) REFERENCES evidence (id) ON DELETE CASCADE');
            $this->addSql('CREATE UNIQUE INDEX sourceEvidManuf_UNIQUE ON manufacturer_evidences (source_id, evidence_id, manufacturer_id)');
            $this->addSql('ALTER TABLE ingredients_attributes DROP FOREIGN KEY FK_F79836E8933FE08C');
            $this->addSql('ALTER TABLE ingredients_attributes DROP FOREIGN KEY FK_F79836E8953C1C61');
            $this->addSql('ALTER TABLE ingredients_attributes DROP FOREIGN KEY FK_F79836E8B6E62EFA');
            $this->addSql('ALTER TABLE ingredients_attributes ADD CONSTRAINT FK_F79836E8933FE08C FOREIGN KEY (ingredient_id) REFERENCES ingredient (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE ingredients_attributes ADD CONSTRAINT FK_F79836E8953C1C61 FOREIGN KEY (source_id) REFERENCES source (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE ingredients_attributes ADD CONSTRAINT FK_F79836E8B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE');
            $this->addSql('CREATE UNIQUE INDEX sourceAttrIngred_UNIQUE ON ingredients_attributes (source_id, attribute_id, ingredient_id)');

            $conn->commit();
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
