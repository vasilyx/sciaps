<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181001132428 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();

        try {
            $this->addSql('CREATE TABLE ingestion_report_reg_exp (ingestion_report_id INT NOT NULL, reg_exp_id INT NOT NULL, INDEX IDX_DD0E3AFB516A1052 (ingestion_report_id), INDEX IDX_DD0E3AFB6BD5409D (reg_exp_id), PRIMARY KEY(ingestion_report_id, reg_exp_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
            $this->addSql('ALTER TABLE ingestion_report_reg_exp ADD CONSTRAINT FK_DD0E3AFB516A1052 FOREIGN KEY (ingestion_report_id) REFERENCES ingestion_report (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE ingestion_report_reg_exp ADD CONSTRAINT FK_DD0E3AFB6BD5409D FOREIGN KEY (reg_exp_id) REFERENCES `regexp` (id) ON DELETE CASCADE');
            $this->addSql('DROP TABLE product_provider_reg_exp');
            $this->addSql('ALTER TABLE crowdsource_pending CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');

        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
