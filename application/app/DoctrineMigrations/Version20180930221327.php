<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20180930221327 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();
        try {
            $this->addSql('CREATE TABLE product_provider_reg_exp (product_provider_id INT NOT NULL, reg_exp_id INT NOT NULL, INDEX IDX_81F5796EA67C36B5 (product_provider_id), INDEX IDX_81F5796E6BD5409D (reg_exp_id), PRIMARY KEY(product_provider_id, reg_exp_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
            $this->addSql('CREATE TABLE `regexp` (id INT AUTO_INCREMENT NOT NULL, added_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, regex LONGTEXT NOT NULL, regex_type INT NOT NULL, version INT DEFAULT 0 NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
            $this->addSql('ALTER TABLE product_provider_reg_exp ADD CONSTRAINT FK_81F5796EA67C36B5 FOREIGN KEY (product_provider_id) REFERENCES product_provider (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE product_provider_reg_exp ADD CONSTRAINT FK_81F5796E6BD5409D FOREIGN KEY (reg_exp_id) REFERENCES `regexp` (id) ON DELETE CASCADE');    $conn->commit();
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
