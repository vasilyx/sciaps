<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180903164622 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE dataentry_provider ADD company VARCHAR(255) NOT NULL');
        $this->addSql('DROP INDEX service_idx ON company_services');
        $this->addSql('ALTER TABLE company_services DROP service_id');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094F362CF80A');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FA67C36B5');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FE87B12F9');
        $this->addSql('DROP INDEX UNIQ_4FBF094FA67C36B5 ON company');
        $this->addSql('DROP INDEX UNIQ_4FBF094F362CF80A ON company');
        $this->addSql('DROP INDEX UNIQ_4FBF094FE87B12F9 ON company');
        $this->addSql('ALTER TABLE company DROP product_provider_id, DROP source_provider_id, DROP dataentry_provider_id');
        $this->addSql('ALTER TABLE product_provider ADD company_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_provider ADD CONSTRAINT FK_5974190B979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5974190B979B1AD6 ON product_provider (company_id)');
        $this->addSql('ALTER TABLE source_data_provider ADD company_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE source_data_provider ADD CONSTRAINT FK_9D766B22979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9D766B22979B1AD6 ON source_data_provider (company_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company ADD product_provider_id INT DEFAULT NULL, ADD source_provider_id INT DEFAULT NULL, ADD dataentry_provider_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094F362CF80A FOREIGN KEY (source_provider_id) REFERENCES source_data_provider (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FA67C36B5 FOREIGN KEY (product_provider_id) REFERENCES product_provider (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FE87B12F9 FOREIGN KEY (dataentry_provider_id) REFERENCES dataentry_provider (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094FA67C36B5 ON company (product_provider_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094F362CF80A ON company (source_provider_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094FE87B12F9 ON company (dataentry_provider_id)');
        $this->addSql('ALTER TABLE company_services ADD service_id INT NOT NULL');
        $this->addSql('CREATE INDEX service_idx ON company_services (service_id)');
        $this->addSql('ALTER TABLE dataentry_provider DROP company');
        $this->addSql('ALTER TABLE product_provider DROP FOREIGN KEY FK_5974190B979B1AD6');
        $this->addSql('DROP INDEX UNIQ_5974190B979B1AD6 ON product_provider');
        $this->addSql('ALTER TABLE product_provider DROP company_id');
        $this->addSql('ALTER TABLE source_data_provider DROP FOREIGN KEY FK_9D766B22979B1AD6');
        $this->addSql('DROP INDEX UNIQ_9D766B22979B1AD6 ON source_data_provider');
        $this->addSql('ALTER TABLE source_data_provider DROP company_id');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }
}
