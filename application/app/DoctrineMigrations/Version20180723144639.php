<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180723144639 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE barcode DROP FOREIGN KEY FK_97AE02664584665A');
        $this->addSql('ALTER TABLE barcode DROP FOREIGN KEY FK_97AE0266702F5E65');
        $this->addSql('DROP INDEX product_idx ON barcode');
        $this->addSql('DROP INDEX barcode_product_fk ON barcode');
        $this->addSql('DROP INDEX type_idx ON barcode');
        $this->addSql('ALTER TABLE barcode ADD barcode_original_type INT NOT NULL, DROP product_id, DROP barcode_type_id, CHANGE barcode_original barcode_original VARCHAR(45) NOT NULL');
        $this->addSql('CREATE INDEX type_idx ON barcode (barcode_original_type)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX type_idx ON barcode');
        $this->addSql('ALTER TABLE barcode ADD product_id INT DEFAULT NULL, ADD barcode_type_id INT DEFAULT NULL, DROP barcode_original_type, CHANGE barcode_original barcode_original VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE barcode ADD CONSTRAINT FK_97AE02664584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE barcode ADD CONSTRAINT FK_97AE0266702F5E65 FOREIGN KEY (barcode_type_id) REFERENCES barcode_type (id)');
        $this->addSql('CREATE INDEX product_idx ON barcode (id, product_id)');
        $this->addSql('CREATE INDEX barcode_product_fk ON barcode (product_id)');
        $this->addSql('CREATE INDEX type_idx ON barcode (barcode_type_id)');
    }
}
