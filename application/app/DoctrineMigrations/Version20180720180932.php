<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180720180932 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users_data_providers MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE users_data_providers DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE users_data_providers DROP FOREIGN KEY FK_13320D94A76ED395');
        $this->addSql('ALTER TABLE users_data_providers DROP FOREIGN KEY FK_13320D94F593F7E0');
        $this->addSql('ALTER TABLE users_data_providers DROP id, CHANGE data_provider_id data_provider_id INT NOT NULL, CHANGE user_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE users_data_providers ADD PRIMARY KEY (data_provider_id, user_id)');
        $this->addSql('DROP INDEX data_provider_idx ON users_data_providers');
        $this->addSql('CREATE INDEX IDX_13320D94F593F7E0 ON users_data_providers (data_provider_id)');
        $this->addSql('DROP INDEX users_data_providers_user ON users_data_providers');
        $this->addSql('CREATE INDEX IDX_13320D94A76ED395 ON users_data_providers (user_id)');
        $this->addSql('ALTER TABLE users_data_providers ADD CONSTRAINT FK_13320D94A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE users_data_providers ADD CONSTRAINT FK_13320D94F593F7E0 FOREIGN KEY (data_provider_id) REFERENCES data_provider (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users_data_providers DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE users_data_providers DROP FOREIGN KEY FK_13320D94F593F7E0');
        $this->addSql('ALTER TABLE users_data_providers DROP FOREIGN KEY FK_13320D94A76ED395');
        $this->addSql('ALTER TABLE users_data_providers ADD id INT AUTO_INCREMENT NOT NULL, CHANGE data_provider_id data_provider_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users_data_providers ADD PRIMARY KEY (id)');
        $this->addSql('DROP INDEX idx_13320d94f593f7e0 ON users_data_providers');
        $this->addSql('CREATE INDEX data_provider_idx ON users_data_providers (data_provider_id)');
        $this->addSql('DROP INDEX idx_13320d94a76ed395 ON users_data_providers');
        $this->addSql('CREATE INDEX users_data_providers_user ON users_data_providers (user_id)');
        $this->addSql('ALTER TABLE users_data_providers ADD CONSTRAINT FK_13320D94F593F7E0 FOREIGN KEY (data_provider_id) REFERENCES data_provider (id)');
        $this->addSql('ALTER TABLE users_data_providers ADD CONSTRAINT FK_13320D94A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }
}
