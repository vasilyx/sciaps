<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181008071220 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();

        try {

            $this->addSql('CREATE TABLE synonym (id INT AUTO_INCREMENT NOT NULL, cid INT NOT NULL, name VARCHAR(512) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }

    }

    public function down(Schema $schema) : void
    {
    }
}
