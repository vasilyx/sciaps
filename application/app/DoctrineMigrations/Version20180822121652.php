<?php declare(strict_types=1);

namespace Application\Migrations;

use AppBundle\DataFixtures\Countries;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180822121652 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(45) NOT NULL, shortcode VARCHAR(2) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE retailer DROP FOREIGN KEY FK_C75A3ED3EE96A67A');
        $this->addSql('DROP TABLE country_code');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('DROP INDEX code_idx ON retailer');
        $this->addSql('ALTER TABLE retailer CHANGE country_code_id country_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE retailer ADD CONSTRAINT FK_C75A3ED3F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('CREATE INDEX code_idx ON retailer (country_id)');

        //Load Countries
        $countries = new Countries();
        $list_country = $countries->getCountries();
        foreach ($list_country as $key => $value) {
            $this->addSql("INSERT INTO country (name, shortcode) VALUES ('$value', '$key')");
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE retailer DROP FOREIGN KEY FK_C75A3ED3F92F3E70');
        $this->addSql('CREATE TABLE country_code (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(3) DEFAULT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP INDEX code_idx ON retailer');
        $this->addSql('ALTER TABLE retailer CHANGE country_id country_code_id INT DEFAULT NULL');
        $this->addSql('CREATE INDEX code_idx ON retailer (country_code_id)');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }
}
