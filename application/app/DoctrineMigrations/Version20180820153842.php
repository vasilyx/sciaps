<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180820153842 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE permission_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, disabled TINYINT(1) DEFAULT \'0\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE permission_group_permission (permission_group_id INT NOT NULL, permission_id INT NOT NULL, INDEX IDX_B61B5A37B6C0CF1 (permission_group_id), INDEX IDX_B61B5A37FED90CCA (permission_id), PRIMARY KEY(permission_group_id, permission_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE permission_group_permission ADD CONSTRAINT FK_B61B5A37B6C0CF1 FOREIGN KEY (permission_group_id) REFERENCES permission_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE permission_group_permission ADD CONSTRAINT FK_B61B5A37FED90CCA FOREIGN KEY (permission_id) REFERENCES permission (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE user_session');
        $this->addSql('DROP TABLE users_companies');
        $this->addSql('ALTER TABLE data_provider ADD is_deleted TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD company_id INT NOT NULL, ADD forgotten_password_token VARCHAR(64) DEFAULT NULL, ADD datetime_forgotten_password DATETIME DEFAULT NULL, ADD confirm_email_token VARCHAR(64) DEFAULT NULL, ADD role INT DEFAULT NULL, ADD status INT DEFAULT 0, CHANGE password password VARCHAR(128) NOT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_8D93D649979B1AD6 ON user (company_id)');
        $this->addSql('ALTER TABLE company ADD data_provider_id INT DEFAULT NULL, ADD evidence_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FF593F7E0 FOREIGN KEY (data_provider_id) REFERENCES data_provider (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FB528FC11 FOREIGN KEY (evidence_id) REFERENCES evidence (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094FF593F7E0 ON company (data_provider_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094FB528FC11 ON company (evidence_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE permission_group_permission DROP FOREIGN KEY FK_B61B5A37B6C0CF1');
        $this->addSql('CREATE TABLE user_session (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX UNIQ_8849CBDE5F37A13B (token), INDEX IDX_8849CBDEA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_companies (company_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_E439D0DB979B1AD6 (company_id), INDEX IDX_E439D0DBA76ED395 (user_id), PRIMARY KEY(company_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_session ADD CONSTRAINT FK_8849CBDEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_companies ADD CONSTRAINT FK_E439D0DB979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE users_companies ADD CONSTRAINT FK_E439D0DBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('DROP TABLE permission_group');
        $this->addSql('DROP TABLE permission_group_permission');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FF593F7E0');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FB528FC11');
        $this->addSql('DROP INDEX UNIQ_4FBF094FF593F7E0 ON company');
        $this->addSql('DROP INDEX UNIQ_4FBF094FB528FC11 ON company');
        $this->addSql('ALTER TABLE company DROP data_provider_id, DROP evidence_id');
        $this->addSql('ALTER TABLE data_provider DROP is_deleted');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649979B1AD6');
        $this->addSql('DROP INDEX IDX_8D93D649979B1AD6 ON user');
        $this->addSql('ALTER TABLE user DROP company_id, DROP forgotten_password_token, DROP datetime_forgotten_password, DROP confirm_email_token, DROP role, DROP status, CHANGE password password VARCHAR(40) NOT NULL COLLATE utf8_unicode_ci');
    }
}
