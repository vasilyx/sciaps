<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180924103128 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();
        try {
            $this->addSql('CREATE TABLE customer__user (id INT AUTO_INCREMENT NOT NULL, added_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, email VARCHAR(255) NOT NULL, username VARCHAR(128) NOT NULL, first_name VARCHAR(128) DEFAULT NULL, password VARCHAR(128) NOT NULL, last_login DATETIME DEFAULT NULL, current_login DATETIME DEFAULT NULL, verification_code VARCHAR(45) DEFAULT NULL, forgotten_password_token VARCHAR(64) DEFAULT NULL, datetime_forgotten_password DATETIME DEFAULT NULL, confirm_email_token VARCHAR(64) DEFAULT NULL, status INT DEFAULT 0 NOT NULL, UNIQUE INDEX email_UNIQUE (email), UNIQUE INDEX username_UNIQUE (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
            $conn->commit();
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();
        try {
            $this->addSql('DROP TABLE customer__user');
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }
}
