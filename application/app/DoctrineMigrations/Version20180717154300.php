<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180717154300 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE data_provider DROP FOREIGN KEY FK_581ABA40B6B21A1A');
        $this->addSql('DROP TABLE data_provider_type');
        $this->addSql('ALTER TABLE user_session CHANGE datetime_updated_at datetime_updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('DROP INDEX type_idx ON data_provider');
        $this->addSql('ALTER TABLE data_provider ADD data_provider_type INT NOT NULL, ADD company_name VARCHAR(45) DEFAULT NULL, ADD display_name VARCHAR(45) DEFAULT NULL, ADD website_url VARCHAR(45) DEFAULT NULL, ADD email_domain VARCHAR(45) DEFAULT NULL, ADD datetime_added_at DATETIME DEFAULT NULL, ADD datetime_updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, DROP data_provider_type_id, DROP added, DROP updated, CHANGE status status INT NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE datetime_updated_at datetime_updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE data_provider_type (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE data_provider ADD data_provider_type_id INT DEFAULT NULL, ADD updated DATETIME DEFAULT NULL, DROP data_provider_type, DROP company_name, DROP display_name, DROP website_url, DROP email_domain, DROP datetime_updated_at, CHANGE status status TINYINT(1) DEFAULT NULL, CHANGE datetime_added_at added DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE data_provider ADD CONSTRAINT FK_581ABA40B6B21A1A FOREIGN KEY (data_provider_type_id) REFERENCES data_provider_type (id)');
        $this->addSql('CREATE INDEX type_idx ON data_provider (data_provider_type_id)');
        $this->addSql('ALTER TABLE user CHANGE datetime_updated_at datetime_updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE user_session CHANGE datetime_updated_at datetime_updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }
}
