<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180921161208 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE attribute CHANGE default_name default_name VARCHAR(45) NOT NULL, CHANGE symbol symbol VARCHAR(4) NOT NULL');
        $this->addSql('ALTER TABLE evidence CHANGE default_name default_name VARCHAR(45) NOT NULL, CHANGE symbol symbol VARCHAR(4) NOT NULL');
        $this->addSql('ALTER TABLE manufacturer_attributes CHANGE status_code status_code INT NOT NULL');
        $this->addSql('ALTER TABLE product_evidences CHANGE status_code status_code INT NOT NULL');
        $this->addSql('ALTER TABLE crowdsource_pending CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE product_attributes CHANGE status_code status_code INT NOT NULL');
        $this->addSql('ALTER TABLE ingredients_evidences CHANGE status_code status_code INT NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE status status INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE ingredients_attributes CHANGE status_code status_code INT NOT NULL');
        $this->addSql('ALTER TABLE manufacturer_evidences CHANGE status_code status_code INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE attribute CHANGE default_name default_name VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE symbol symbol VARCHAR(4) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE crowdsource_pending CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE evidence CHANGE default_name default_name VARCHAR(45) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE symbol symbol VARCHAR(4) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE ingredients_attributes CHANGE status_code status_code INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ingredients_evidences CHANGE status_code status_code INT DEFAULT NULL');
        $this->addSql('ALTER TABLE manufacturer_attributes CHANGE status_code status_code INT DEFAULT NULL');
        $this->addSql('ALTER TABLE manufacturer_evidences CHANGE status_code status_code INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_attributes CHANGE status_code status_code INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_evidences CHANGE status_code status_code INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE status status INT DEFAULT 0');
    }
}
