<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180912144903 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();
        try {
            $this->addSql('ALTER TABLE brand_attributes DROP FOREIGN KEY FK_B44B8AE953C1C61');
            $this->addSql('ALTER TABLE brand_attributes ADD CONSTRAINT FK_B44B8AE953C1C61 FOREIGN KEY (source_id) REFERENCES source (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE user CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
            $this->addSql('ALTER TABLE product_attributes DROP FOREIGN KEY FK_A2FCC15B953C1C61');
            $this->addSql('ALTER TABLE product_attributes ADD CONSTRAINT FK_A2FCC15B953C1C61 FOREIGN KEY (source_id) REFERENCES source (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE product_evidences DROP FOREIGN KEY FK_681508B5953C1C61');
            $this->addSql('ALTER TABLE product_evidences ADD CONSTRAINT FK_681508B5953C1C61 FOREIGN KEY (source_id) REFERENCES source (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE brand_evidences DROP FOREIGN KEY FK_6CC2AE59953C1C61');
            $this->addSql('ALTER TABLE brand_evidences ADD CONSTRAINT FK_6CC2AE59953C1C61 FOREIGN KEY (source_id) REFERENCES source (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE manufacturer_attributes DROP FOREIGN KEY FK_E68FA570953C1C61');
            $this->addSql('ALTER TABLE manufacturer_attributes ADD CONSTRAINT FK_E68FA570953C1C61 FOREIGN KEY (source_id) REFERENCES source (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE manufacturer_evidences DROP FOREIGN KEY FK_1F5C50D9953C1C61');
            $this->addSql('ALTER TABLE manufacturer_evidences ADD CONSTRAINT FK_1F5C50D9953C1C61 FOREIGN KEY (source_id) REFERENCES source (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE source CHANGE article_type article_type INT DEFAULT NULL');

            $conn->commit();
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
