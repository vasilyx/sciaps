<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181004140215 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();

        try {
            $this->addSql('CREATE TABLE ingredient_legend (id INT AUTO_INCREMENT NOT NULL, added_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, UNIQUE INDEX ingredient_legend_name (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
            $this->addSql('ALTER TABLE products_ingredients ADD ingredient_legend_id INT DEFAULT NULL');
            $this->addSql('ALTER TABLE products_ingredients ADD CONSTRAINT FK_B1BD73D4D349396E FOREIGN KEY (ingredient_legend_id) REFERENCES ingredient_legend (id)');
            $this->addSql('CREATE INDEX IDX_B1BD73D4D349396E ON products_ingredients (ingredient_legend_id)');
            $this->addSql('ALTER TABLE crowdsource_pending CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
