<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180720153357 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE source_data_provider (id INT AUTO_INCREMENT NOT NULL, added_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, source_data_provider_type INT NOT NULL, company_name VARCHAR(45) DEFAULT NULL, display_name VARCHAR(45) DEFAULT NULL, website_url VARCHAR(45) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE source DROP FOREIGN KEY FK_5F8A7F73F593F7E0');
        $this->addSql('DROP INDEX data_provider_idx ON source');
        $this->addSql('ALTER TABLE source CHANGE data_provider_id source_data_provider_id INT NOT NULL');
        $this->addSql('ALTER TABLE source ADD CONSTRAINT FK_5F8A7F7362E73146 FOREIGN KEY (source_data_provider_id) REFERENCES data_provider (id)');
        $this->addSql('CREATE INDEX source_data_provider_idx ON source (source_data_provider_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE source_data_provider');
        $this->addSql('ALTER TABLE source DROP FOREIGN KEY FK_5F8A7F7362E73146');
        $this->addSql('DROP INDEX source_data_provider_idx ON source');
        $this->addSql('ALTER TABLE source CHANGE source_data_provider_id data_provider_id INT NOT NULL');
        $this->addSql('ALTER TABLE source ADD CONSTRAINT FK_5F8A7F73F593F7E0 FOREIGN KEY (data_provider_id) REFERENCES data_provider (id)');
        $this->addSql('CREATE INDEX data_provider_idx ON source (data_provider_id)');
    }
}
