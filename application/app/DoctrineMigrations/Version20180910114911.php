<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180910114911 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();
        try{
            $this->addSql('ALTER TABLE crowdsource_pending ADD brand_id INT DEFAULT NULL, ADD manufacturer_id INT DEFAULT NULL, ADD added_at DATETIME NOT NULL, ADD updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ADD name VARCHAR(45) DEFAULT NULL, ADD description LONGTEXT DEFAULT NULL, ADD volume NUMERIC(10, 2) DEFAULT NULL, ADD volume_units INT DEFAULT NULL');
            $this->addSql('ALTER TABLE crowdsource_pending ADD CONSTRAINT FK_CD659E1044F5D008 FOREIGN KEY (brand_id) REFERENCES brand (id)');
            $this->addSql('ALTER TABLE crowdsource_pending ADD CONSTRAINT FK_CD659E10A23B42D FOREIGN KEY (manufacturer_id) REFERENCES manufacturer (id)');
            $this->addSql('CREATE INDEX brand_idx ON crowdsource_pending (brand_id)');
            $this->addSql('CREATE INDEX manufacturer_idx ON crowdsource_pending (manufacturer_id)');
            $conn->commit();
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}