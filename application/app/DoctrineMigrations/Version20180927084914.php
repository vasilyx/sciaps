<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180927084914 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();
        try {
            $this->addSql('ALTER TABLE ingredient CHANGE common_name common_name LONGTEXT DEFAULT NULL, CHANGE inci_name inci_name LONGTEXT DEFAULT NULL, CHANGE inn_name inn_name LONGTEXT DEFAULT NULL, CHANGE ph_eur_name ph_eur_name LONGTEXT DEFAULT NULL, CHANGE chemical_iupac_name chemical_iupac_name LONGTEXT DEFAULT NULL, CHANGE cas_number cas_number LONGTEXT DEFAULT NULL, CHANGE ec_number ec_number LONGTEXT DEFAULT NULL, CHANGE cosing_number cosing_number LONGTEXT DEFAULT NULL');
            $this->addSql('ALTER TABLE crowdsource_pending CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');

            $conn->commit();
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
