<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180904115920 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE organization_countries (id INT AUTO_INCREMENT NOT NULL, organization_id INT DEFAULT NULL, country_id INT DEFAULT NULL, INDEX organization_idx (organization_id), INDEX country_idx (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE organization (id INT AUTO_INCREMENT NOT NULL, added_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, company_name VARCHAR(45) DEFAULT NULL, website_url VARCHAR(45) DEFAULT NULL, barcode TINYINT(1) DEFAULT NULL, number_products INT DEFAULT NULL, person_name VARCHAR(45) DEFAULT NULL, person_email VARCHAR(45) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE organization_countries ADD CONSTRAINT FK_655131AB32C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id)');
        $this->addSql('ALTER TABLE organization_countries ADD CONSTRAINT FK_655131ABF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE organization_countries DROP FOREIGN KEY FK_655131AB32C8A3DE');
        $this->addSql('DROP TABLE organization_countries');
        $this->addSql('DROP TABLE organization');
        $this->addSql('ALTER TABLE user CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }
}
