<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180921132517 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $conn = $this->connection;
        $conn->beginTransaction();
        try {
            $this->addSql('SET FOREIGN_KEY_CHECKS=0;');
            $this->addSql('DELETE FROM product_provider');
            $this->addSql('DELETE FROM dataentry_provider');
            $this->addSql('DELETE FROM source_data_provider');
            $this->addSql('ALTER TABLE brand_attributes CHANGE updated_at updated_at DATETIME DEFAULT NULL');
            $this->addSql('ALTER TABLE user CHANGE updated_at updated_at DATETIME DEFAULT NULL');
            $this->addSql('ALTER TABLE dataentry_provider DROP added_at, DROP updated_at, DROP is_deleted, CHANGE company_id company_id INT NOT NULL');
            $this->addSql('ALTER TABLE product_attributes CHANGE updated_at updated_at DATETIME DEFAULT NULL');
            $this->addSql('ALTER TABLE log_event CHANGE added_at added_at DATETIME NOT NULL');
            $this->addSql('ALTER TABLE product_provider DROP company_name, DROP display_name, DROP website_url, DROP email_domain, DROP is_deleted, CHANGE company_id company_id INT NOT NULL, CHANGE status status INT NOT NULL, CHANGE ingestion_reports ingestion_reports INT NOT NULL, CHANGE ingestion_analytics ingestion_analytics INT NOT NULL');
            $this->addSql('ALTER TABLE product_evidences CHANGE updated_at updated_at DATETIME DEFAULT NULL');
            $this->addSql('ALTER TABLE attribute CHANGE updated_at updated_at DATETIME DEFAULT NULL');
            $this->addSql('ALTER TABLE evidence CHANGE updated_at updated_at DATETIME DEFAULT NULL');
            $this->addSql('ALTER TABLE ingredients_evidences CHANGE updated_at updated_at DATETIME DEFAULT NULL');
            $this->addSql('ALTER TABLE brand_evidences CHANGE updated_at updated_at DATETIME DEFAULT NULL');
            $this->addSql('ALTER TABLE source_data_provider DROP added_at, DROP updated_at, DROP source_data_provider_type, DROP company_name, DROP display_name, DROP website_url, DROP is_deleted, CHANGE company_id company_id INT NOT NULL');
            $this->addSql('ALTER TABLE manufacturer_attributes CHANGE updated_at updated_at DATETIME DEFAULT NULL');
            $this->addSql('ALTER TABLE crowdsource_pending CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
            $this->addSql('ALTER TABLE manufacturer_evidences CHANGE updated_at updated_at DATETIME DEFAULT NULL');
            $this->addSql('ALTER TABLE ingredients_attributes CHANGE updated_at updated_at DATETIME DEFAULT NULL');

            $this->addSql('SET FOREIGN_KEY_CHECKS=1;');
            $conn->commit();
        } catch (\Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
