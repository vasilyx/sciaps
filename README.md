# Docker Symfony (PHP7-FPM - NGINX - MySQL - ELK)
 
## Installation

1. Build/run containers with (with and without detached mode)

    ```bash
    $ docker-compose -f docker-compose.local.yml build
    ```

2. Update your system host file (add evrelab.local)

    ```bash
    # MacOs:
    $ sudo sh -c 'echo "127.0.0.1   evrelab.local" >> /etc/hosts'
    ----
    # UNIX: Get container IP address and update host (replace IP according to your configuration) (on Windows, edit C:\Windows\System32\drivers\etc\hosts)
    $ sudo sh -c 'echo $(docker network inspect bridge | grep Gateway | grep -o -E '[0-9\.]+') "evrelab.local" >> /etc/hosts'
    ```

    **Note:** For **OS X**, please take a look [here](https://docs.docker.com/docker-for-mac/networking/) and for **Windows** read [this](https://docs.docker.com/docker-for-windows/#/step-4-explore-the-application-and-run-examples) (4th step).

3. Prepare app before up
    1. Create environment file for your device and enter appropriate parameters
    ```bash
       $ mv .env.dist .env
    ```
    
3. Prepare Symfony app
    1. Update app/config/parameters.yml (it was already created in Dockerfile)

        ```yml
        parameters:
            database_host: db
        ```

    2. Composer install & create database

        ```bash
        $ docker exec -it app_corecog bash
        # Composer
        $ composer install
        # ---
        # Symfony3
        $ sf3 doctrine:database:create
        $ sf3 doctrine:migrations:migrate --no-interaction
        $ sf3 doctrine:fixtures:load --no-interaction
        ```

## Usage

Just run `docker-compose -f docker-compose.local.yml up -d --build`, then:

* [CoreCog App](http://evrelab.local/) 
* [CoreCog Dev App](http://evrelab.local/app_dev.php)  
* [API DOC](http://evrelab.local/app_dev.php/api/doc)
 